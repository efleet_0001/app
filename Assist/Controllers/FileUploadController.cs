﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Assist.Controllers
{
    [EnableCors("CorsPolicy")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class FileUploadController : ControllerBase
    {
        private IHostingEnvironment _env;
        public FileUploadController(IHostingEnvironment env)
        {
            _env = env;
        }
        [HttpPost]
        [ActionName("Upload")]
        public IActionResult Upload(IFormFile upload)
        {
            var file = Request.Form.Files[0];

            string strpath = System.IO.Path.GetExtension(file.FileName);
            var NewFileName = file.FileName.Split(".")[0];
            var filename = NewFileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + strpath;

            var webRoot = _env.ContentRootPath;

            var filePath = webRoot + "/uploads/images" + $@"/{ filename}";

            using (FileStream fs = System.IO.File.Create(filePath))
            {
                file.CopyTo(fs);
                fs.Flush();
            }
            return Ok(filename);
        }
    }
}