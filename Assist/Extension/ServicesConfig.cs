﻿
using Assist.Middleware;
using AutoMapper;
using AssistBLL.Mapping;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Microsoft.OpenApi.Models;
using System;
using AssistDAL;
using AssistBLL.Services.Trans;
using AssistBLL.Services.Master;
using AssistBLL.Implementation.Trans;
using AssistBLL.Implementation.Master;
using AssistBLL.Business;
using AssistDAL.RepositoriesInterface.Common;
using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Trans;
using AssistDAL.RepositoriesImplementaion.Trans;
using AssistDAL.RepositoriesInterface.Master;
using AssistDAL.RepositoriesImplementaion.Master;
using AssistDAL.Repository;
using AssistBLL.Common.Logging;
using AssistDAL.RepositoriesInterface.UserControl;
using AssistDAL.RepositoriesImplementaion.UserControl;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistDAL.RepositoriesImplementaion.Kisan;
using AssistDAL.RepositoriesInterface.General;
using AssistDAL.RepositoriesImplementaion.General;
using AssistBLL.Services.UserControl;
using AssistBLL.Services.Kisan;
using AssistBLL.Services.General;
using AssistBLL.Implementation.UserControl;
using AssistBLL.Implementation.Kisan;
using AssistBLL.Implementation.General;

namespace Assist.Extension
{
    public static class ServicesConfig
    {
        public static void AddDefaultRepositories(this IServiceCollection services)
        {

            services.AddAutoMapper(typeof(AutoMapperConfig));
            var mapper = new Mapper(new MapperConfiguration(cfg => {
                cfg.AddProfile<AutoMapperConfig>();
                // Rest of your configuration
            }));
            //services.AddAutoMapper(new Type[] { typeof(AutoMapperConfig) });
            //// Mapper.Initialize(cfg => cfg.AddProfile<AutoMapperConfig>());
            //// Auto Mapper Configurations
            //var mappingConfig = new MapperConfiguration(mc =>
            //{
            //    mc.AddProfile(new AutoMapperConfig());
            //});

            //IMapper mapper = mappingConfig. CreateMapper();
            //services.AddSingleton(mapper);

            services.AddSingleton<ILoggerManager, LoggerManager>();
            // Register dependency of app repository here
            services.AddScoped<ApplicationContext>();
            services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));

            // DI for repository
            services.AddScoped<IUserMasterRepository, UserMasterRepository>();
            services.AddScoped<IAgriDasboardRepository, AgriDashboardRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();//not used
            services.AddScoped<IAnswerRepository, AnswerRepository>();
            services.AddScoped<ISubCategoryRepository, SubCategoryRepository>();
            services.AddScoped<ISocialProfileRepository, SocialProfileRepository>();
            services.AddScoped<IRoleMasterRepository, RoleMasterRepository>();
            services.AddScoped<IProfileRepository, ProfileRepository>();
            services.AddScoped<ILocaleInformationRepository, LocaleInformationRepository>();
            services.AddScoped<IHolidayMasterRepository, HolidayMasterRepository>();
            services.AddScoped<IFiscalMasterRepository, FiscalMasterRepository>();
            services.AddScoped<IContactMasterRepository, ContactMasterRepository>();
            services.AddScoped<ICompanyMasterRepository, CompanyMasterRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IProfileDetailsRepository, ProfileDetailsRepository>();
            services.AddScoped<INewsRepository, NewsRepository>();
            services.AddScoped<INewsCategoryRepository, NewsCategoryRepository>();
            services.AddScoped<IStaticRepository, StaticRepository>();
            services.AddScoped<ISolutionRepository, SolutionRepository>();
            services.AddScoped<IloginRepository, loginRepository>();
            services.AddScoped<INotificationRepository, NotificationRepository>();
            services.AddScoped<IImageuploadRepository, ImageuploadRepository>();
            services.AddScoped<IMemberMappingRepository, MemberMappingRepository>();
            // Dependency injection
            services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));
            services.AddScoped(typeof(IUserBusiness), typeof(UserBusiness));
            services.AddScoped(typeof(IUserMasterService), typeof(UserMasterService));
            services.AddScoped(typeof(IAgriDashboardBusinesss), typeof(AgriDasboardBusiness));//not in use
            services.AddScoped(typeof(IAnswerService), typeof(AnswerService));
            services.AddScoped(typeof(ISubCategoryService), typeof(SubCategoryService));
            services.AddScoped(typeof(ISocialProfileService), typeof(SocialProfileService));
            services.AddScoped(typeof(IRoleMasterService), typeof(RoleMasterService));
            services.AddScoped(typeof(IProfileService), typeof(ProfileService));
            services.AddScoped(typeof(ILocaleInformationService), typeof(LocaleInformationService));
            services.AddScoped(typeof(IHolidayMasterService), typeof(HolidayMasterService));
            services.AddScoped(typeof(IFiscalMasterService), typeof(FiscalMasterService));
            services.AddScoped(typeof(ICompanyMasterService), typeof(CompanyMasterService));
            services.AddScoped(typeof(ICategoryService), typeof(CategoryService));
            services.AddScoped(typeof(IProfileDetailService), typeof(ProfileDetailService));
            services.AddScoped(typeof(INewsCategoryService), typeof(NewsCategoryService));
            services.AddScoped(typeof(INewsService), typeof(NewsService));
            services.AddScoped(typeof(IStaticService), typeof(StaticService));
            services.AddScoped(typeof(ISolutionService), typeof(SolutionService));
            services.AddScoped(typeof(IloginServices), typeof(loginServices));
            services.AddScoped(typeof(INotificationService), typeof(NotificationService));
            services.AddScoped(typeof(IImageuploadService), typeof(ImageuploadService));
            services.AddScoped(typeof(IMemberMappingService), typeof(MemberMappingService));
        }

        public static void AddExtensionServices(this IServiceCollection services)
        {
            // Register dependency of extension here

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "ToDo API",
                    Description = "A simple example ASP.NET Core Web API",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "Shayne Boyer",
                        Email = string.Empty,
                        Url = new Uri("https://twitter.com/spboyer"),
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use under LICX",
                        Url = new Uri("https://example.com/license"),
                    }
                });
                // Set the comments path for the Swagger JSON and UI.
                //var xmlFile = $"{System.Reflection.Assembly.GetExecutingAssembly().GetName().Name}.xml";
                //var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                //c.IncludeXmlComments(xmlPath);
            });
           
          //  Mapper.Initialize(cfg => cfg.AddProfile<AutoMapperConfig>());



        }

        public static void AppConfigure(this IApplicationBuilder app, IHostingEnvironment env)
        {

            app.UseMiddleware<HandleGlobalExeptionMiddleware>();
            app.UseCors("CorsPolicy");
            app.Use(async (context, next) =>
            {
                await next();

                if (context.Response.StatusCode == 404
                    && !Path.HasExtension(context.Request.Path.Value))
                {
                    context.Request.Path = "/error.html";
                    await next();
                }
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseRouting();
           

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
            }


        }
    }
}
