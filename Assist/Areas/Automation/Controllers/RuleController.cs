﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AssistBLL.Services.Master;
using AssistBLL.ViewModel.Automation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Assist.Areas.Automation.Controllers
{
    [Area("Automation")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RuleController : ControllerBase
    {
        private readonly IWorkFlowRuleService _iWorkFlowRuleService;
        public RuleController(IWorkFlowRuleService iWorkFlowRuleService)
        {
            _iWorkFlowRuleService = _iWorkFlowRuleService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(WorkFlowRuleModel um)
        {
            var res = _iWorkFlowRuleService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("RuleId")]
        [ActionName("Edit")]
        public IActionResult Edit(int RuleId, WorkFlowRuleModel um)
        {
            if (RuleId == um.RuleId)
            {
                var res = _iWorkFlowRuleService.Edit(RuleId, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{RuleId}")]
        [ActionName("Delete")]
        public IActionResult Delete(int RuleId)
        {
            var res = _iWorkFlowRuleService.Delete(RuleId);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{RuleName}/{SubscriberID}/{RuleId}/{type}")]
        [ActionName("CheckDuplicateWorkFlowRule")]
        public IActionResult CheckDuplicateWorkFlowRule(string RuleName, int? SubscriberID, int RuleId, string type)
        {
            var res = _iWorkFlowRuleService.CheckDuplicateWorkFlowRule(RuleName, SubscriberID, RuleId, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{RuleId}")]
        [ActionName("GetWorkFlowRuleById")]
        public IActionResult GetWorkFlowRuleById(int RuleId)
        {
            var data = _iWorkFlowRuleService.GetWorkFlowRuleById(RuleId);
            if (data == null)
            {
                return NotFound();
            }
            return Ok(data);
        }

        [HttpGet("{RuleId}")]
        [ActionName("GetWorkFlowRuleDetails")]
        public IActionResult GetWorkFlowRuleDetails(int RuleId)
        {
            var data = _iWorkFlowRuleService.GetWorkFlowRuleById(RuleId);
            if (data == null)
            {
                return NotFound();
            }
            return Ok(data);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllWorkFlowRule")]
        public IEnumerable<WorkFlowRuleModel> GetAllWorkFlowRule(int SubscriberID)
        {
            var data = _iWorkFlowRuleService.GetAllWorkFlowRule(SubscriberID);
            return data;
        }
    }
}