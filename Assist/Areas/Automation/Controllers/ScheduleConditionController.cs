﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AssistBLL.Services.Master;
using AssistBLL.ViewModel.Automation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Assist.Areas.Automation.Controllers
{
    [Area("Automation")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ScheduleConditionController : ControllerBase
    {
        private readonly IWorkFlowScheduleConditionService _iWorkFlowScheduleConditionService;
        public ScheduleConditionController(IWorkFlowScheduleConditionService iWorkFlowScheduleConditionService)
        {
            _iWorkFlowScheduleConditionService = iWorkFlowScheduleConditionService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(WorkFlowScheduleConditionModel um)
        {
            var res = _iWorkFlowScheduleConditionService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("RuleId")]
        [ActionName("Edit")]
        public IActionResult Edit(int RuleId, WorkFlowScheduleConditionModel um)
        {
            if (RuleId == um.RuleId)
            {
                var res = _iWorkFlowScheduleConditionService.Edit(RuleId, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{RuleId}")]
        [ActionName("Delete")]
        public IActionResult Delete(int RuleId)
        {
            var res = _iWorkFlowScheduleConditionService.Delete(RuleId);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{ScheduleId}/{SubscriberID}/{RuleId}/{type}")]
        [ActionName("CheckDuplicateWFScheduleCondition")]
        public IActionResult CheckDuplicateWFScheduleCondition(int ScheduleId, int? SubscriberID, int RuleId, string type)
        {
            var res = _iWorkFlowScheduleConditionService.CheckDuplicateWFScheduleCondition(ScheduleId, SubscriberID, RuleId, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{RuleId}")]
        [ActionName("GetWFScheduleConditionById")]
        public IActionResult GetWFScheduleConditionById(int RuleId)
        {
            var data = _iWorkFlowScheduleConditionService.GetWFScheduleConditionById(RuleId);
            if (data == null)
            {
                return NotFound();
            }
            return Ok(data);
        }

       
        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllWFScheduleCondition")]
        public IEnumerable<WorkFlowScheduleConditionModel> GetAllWFScheduleCondition(int SubscriberID)
        {
            var Company = _iWorkFlowScheduleConditionService.GetAllWFScheduleCondition(SubscriberID);
            return Company;
        }
    }
}