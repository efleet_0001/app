﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AssistBLL.Services.Master;
using AssistBLL.ViewModel.Automation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Assist.Areas.Automation.Controllers
{
    [Area("Automation")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RuleScheduleController : ControllerBase
    {
        private readonly IWorkFlowRuleScheduleService _iWorkFlowRuleScheduleService;
        public RuleScheduleController(IWorkFlowRuleScheduleService iWorkFlowRuleScheduleService)
        {
            _iWorkFlowRuleScheduleService = iWorkFlowRuleScheduleService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(WorkFlowRuleScheduleModel um)
        {
            var res = _iWorkFlowRuleScheduleService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("ScheduleId")]
        [ActionName("Edit")]
        public IActionResult Edit(int ScheduleId, WorkFlowRuleScheduleModel um)
        {
            if (ScheduleId == um.ScheduleId)
            {
                var res = _iWorkFlowRuleScheduleService.Edit(ScheduleId, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{ScheduleId}")]
        [ActionName("Delete")]
        public IActionResult Delete(int ScheduleId)
        {
            var res = _iWorkFlowRuleScheduleService.Delete(ScheduleId);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{ScheduleName}/{SubscriberID}/{ScheduleId}/{type}")]
        [ActionName("CheckDuplicateWorkFlowRuleSchedule")]
        public IActionResult CheckDuplicateWorkFlowRuleSchedule(string ScheduleName, int? SubscriberID, int ScheduleId, string type)
        {
            var res = _iWorkFlowRuleScheduleService.CheckDuplicateWorkFlowRuleSchedule(ScheduleName, SubscriberID, ScheduleId, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{ScheduleId}")]
        [ActionName("GetWorkFlowRuleScheduleById")]
        public IActionResult GetWorkFlowRuleScheduleById(int ScheduleId)
        {
            var Category = _iWorkFlowRuleScheduleService.GetWorkFlowRuleScheduleById(ScheduleId);
            if (Category == null)
            {
                return NotFound();
            }
            return Ok(Category);
        }

        [HttpGet("{ScheduleId}")]
        [ActionName("GetWorkFlowRuleScheduleDetails")]
        public IActionResult GetWorkFlowRuleScheduleDetails(int ScheduleId)
        {
            var Category = _iWorkFlowRuleScheduleService.GetWorkFlowRuleScheduleById(ScheduleId);
            if (Category == null)
            {
                return NotFound();
            }
            return Ok(Category);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllWorkFlowRuleSchedule")]
        public IEnumerable<WorkFlowRuleScheduleModel> GetAllWorkFlowRuleSchedule(int SubscriberID)
        {
            var data = _iWorkFlowRuleScheduleService.GetAllWorkFlowRuleSchedule(SubscriberID);
            return data;
        }
    }
}