﻿using AutoMapper;

//using Assist.Model;
using AssistBLL.Business;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Assist.Areas.Kisan.Controllers
{
    [Area("Kisan")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AgriDashboardController : Controller
    {
        private IConfiguration _config;
        private IAgriDashboardBusinesss _agriDashboaradBusiness;
        private readonly IMapper _mapper;

        public AgriDashboardController(IConfiguration config, IMapper mapper, IAgriDashboardBusinesss agriDashboaradBusiness)
        {
            _config = config;
            _mapper = mapper;
            _agriDashboaradBusiness = agriDashboaradBusiness;
        }
        [AllowAnonymous]
        [HttpGet("{SubscriberID}")]
        [ActionName("GetEntityDetails")]
        public IActionResult GetEntityDetails(int SubscriberID)
        {
            var EntityDetail = _agriDashboaradBusiness.ExecuteStoreQuery();
            return Ok(EntityDetail);
        }
    }
}