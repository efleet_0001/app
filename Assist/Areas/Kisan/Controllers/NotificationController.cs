﻿using AssistBLL.KisanModel.Trans;
using AssistBLL.Services.Kisan;
using Microsoft.AspNetCore.Mvc;

namespace Assist.Areas.Kisan.Controllers
{
    [Area("Kisan")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly INotificationService _iNotificationService;

        public NotificationController(INotificationService iNotificationService)
        {
            _iNotificationService = iNotificationService;
        }
        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(NotificationModel um)
        {
            var res = _iNotificationService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }
        [HttpPut("NewsID")]
        [ActionName("Edit")]
        public IActionResult Edit(int NotificationId, NotificationModel um)
        {
            if (NotificationId == um.NotificationID)
            {
                var res = _iNotificationService.Edit(NotificationId, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{NewsID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int NotificationID)
        {
            var res = _iNotificationService.Delete(NotificationID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }
        [HttpGet("{MemberID}/{subscriberID}")]
        [ActionName("GetAppNotification")]
        public IActionResult GetAppNotification(int MemberID, int subscriberID)
        {
            var data = _iNotificationService.GetAppNotification(MemberID,subscriberID);
            if (data == null)
            {
                return NotFound();
            }
            return Ok(data);
        }
        [HttpGet("{MemberID}/{subscriberID}")]
        [ActionName("GetAllAppNotification")]
        public IActionResult GetAllAppNotification(int MemberID, int subscriberID)
        {
            var data = _iNotificationService.GetAllAppNotification(MemberID, subscriberID);
            if (data == null)
            {
                return NotFound();
            }
            return Ok(data);
        }
        [HttpGet("{MemberID}/{subscriberID}")]
        [ActionName("GetSMSNotification")]
        public IActionResult GetSMSNotification(int MemberID, int subscriberID)
        {
            var data = _iNotificationService.GetSMSNotification(MemberID, subscriberID);
            if (data == null)
            {
                return NotFound();
            }
            return Ok(data);
        }
        [HttpGet("{MemberID}/{subscriberID}")]
        [ActionName("GetAllSMSNotification")]
        public IActionResult GetAllSMSNotification(int MemberID, int subscriberID)
        {
            var data = _iNotificationService.GetAllSMSNotification(MemberID, subscriberID);
            if (data == null)
            {
                return NotFound();
            }
            return Ok(data);
        }
        [HttpGet("{MemberID}/{subscriberID}")]
        [ActionName("GetWhatsAppNotification")]
        public IActionResult GetWhatsAppNotification(int MemberID, int subscriberID)
        {
            var data = _iNotificationService.GetWhatsAppNotification(MemberID, subscriberID);
            if (data == null)
            {
                return NotFound();
            }
            return Ok(data);
        }
        [HttpGet("{MemberID}/{subscriberID}")]
        [ActionName("GetAllWhatsAppNotification")]
        public IActionResult GetAllWhatsAppNotification(int MemberID, int subscriberID)
        {
            var data = _iNotificationService.GetAllWhatsAppNotification(MemberID, subscriberID);
            if (data == null)
            {
                return NotFound();
            }
            return Ok(data);
        }
       
    }
}