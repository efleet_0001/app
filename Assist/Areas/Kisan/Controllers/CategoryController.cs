﻿using AssistBLL.KisanModel.Master;
using AssistBLL.Services.Kisan;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Assist.Areas.Kisan.Controllers
{
    [Area("Kisan")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _iCategoryService;

        public CategoryController(ICategoryService iCategoryService)
        {
            _iCategoryService = iCategoryService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(CategoryModel um)
        {
            var res = _iCategoryService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("CategoryID")]
        [ActionName("Edit")]
        public IActionResult Edit(int CategoryID, CategoryModel um)
        {
            if (CategoryID == um.CatID)
            {
                var res = _iCategoryService.Edit(CategoryID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{CategoryID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int CategoryID)
        {
            var res = _iCategoryService.Delete(CategoryID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{CategoryName}/{SubscriberID}/{CategoryID}/{type}")]
        [ActionName("CheckDuplicateCategory")]
        public IActionResult CheckDuplicateCategory(string CategoryName, int? SubscriberID, int CategoryID, string type)
        {
            var res = _iCategoryService.CheckDuplicateCategory(CategoryName, SubscriberID, CategoryID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{CategoryID}")]
        [ActionName("GetCategoryById")]
        public IActionResult GetCategoryById(int CategoryID)
        {
            var Category = _iCategoryService.GetCategoryById(CategoryID);
            if (Category == null)
            {
                return NotFound();
            }
            return Ok(Category);
        }

        [HttpGet("{CategoryID}")]
        [ActionName("GetCategoryDetails")]
        public IActionResult GetCategoryDetails(int CategoryID)
        {
            var Category = _iCategoryService.GetCategoryById(CategoryID);
            if (Category == null)
            {
                return NotFound();
            }
            return Ok(Category);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllCategory")]
        public IEnumerable<CategoryModel> GetAllCategory(int SubscriberID)
        {
            var Category = _iCategoryService.GetAllCategory(SubscriberID);
            return Category;
        }
    }
}