﻿using System.Collections.Generic;
using AssistBLL.KisanModel.Trans;
using AssistBLL.Services.Kisan;
using Microsoft.AspNetCore.Mvc;

namespace Assist.Areas.Kisan.Controllers
{
    [Area("Kisan")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AnswerController : ControllerBase
    {
        private readonly IAnswerService _iAnswerService;

        public AnswerController(IAnswerService iAnswerService)
        {
            _iAnswerService = iAnswerService;
        }
        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(AnswerModel um)
        {
            var res = _iAnswerService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("AnswerID")]
        [ActionName("Edit")]
        public IActionResult Edit(int AnswerID, AnswerModel um)
        {
            if (AnswerID == um.AnsID)
            {
                var res = _iAnswerService.Edit(AnswerID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{AnswerID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int AnswerID)
        {
            var res = _iAnswerService.Delete(AnswerID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{AnswerName}/{SubscriberID}/{AnswerID}/{type}")]
        [ActionName("CheckDuplicateAnswer")]
        public IActionResult CheckDuplicateAnswer(string AnswerName, int? SubscriberID, int AnswerID, string type)
        {
            var res = _iAnswerService.CheckDuplicateAnswer(AnswerName, SubscriberID, AnswerID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }
        [HttpGet("{AnswerID}")]
        [ActionName("GetAnswerById")]
        public IActionResult GetAnswerById(int AnswerID)
        {
            var answer = _iAnswerService.GetAnswerById(AnswerID);
            if (answer == null)
            {
                return NotFound();
            }
            return Ok(answer);
        }
        [HttpGet("{AnswerID}")]
        [ActionName("GetAnswerDetails")]
        public IActionResult GetAnswerDetails(int AnswerID)
        {
            var answer = _iAnswerService.GetAnswerById(AnswerID);
            if (answer == null)
            {
                return NotFound();
            }
            return Ok(answer);
        }
        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllAnswer")]
        public IEnumerable<AnswerModel> GetAllAnswer(int SubscriberID)
        {
            var answer = _iAnswerService.GetAllAnswer(SubscriberID);
            return answer;
        }
    }
}