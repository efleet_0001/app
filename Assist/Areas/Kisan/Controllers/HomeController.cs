﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AssistBLL.Business;
using AssistBLL.Common.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Assist.Areas.Kisan.Controllers
{
    [Area("Kisan")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HomeController : Controller
    {
        private IUserBusiness _userBusiness;
        // Logger
        private ILoggerManager _logger;

        public HomeController(IUserBusiness userBusiness, ILoggerManager logger)
        {
            _userBusiness = userBusiness;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("Index")]
        public IActionResult Index()
        {
            _logger.LogInfo("New request to DemoController.Index ");

            return Ok(new string[] { "This is AllowAnonymous api." });
        }

       
        [HttpGet, Authorize(Roles = "GUESS")]
        [Route("Guess")]
        public IActionResult Guess()
        {
            var currentUser = HttpContext.User;

            if (!currentUser.HasClaim(c => c.Type == ClaimTypes.NameIdentifier))
                return Unauthorized();

            var name = currentUser.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.ToString();
          
            var user = _userBusiness.GetUser(id: 0, username: name);

            return Ok(new { content = " Guess can view this content. " });
        }

        [HttpGet, Authorize(Roles = "ADMIN")]
        [Route("Admin")]
        public IActionResult Admin()
        {
            var currentUser = HttpContext.User;

            if (!currentUser.HasClaim(c => c.Type == ClaimTypes.NameIdentifier))
                return Unauthorized();

            var name = currentUser.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value.ToString();

            var user = _userBusiness.GetUser(id: 0, username: name);

            return Ok(new { content = "Admin can view this content." });
        }
    }
}
