﻿using AssistBLL.KisanModel.Trans;
using AssistBLL.KisanModel.Trans.Data;
using AssistBLL.Services.Kisan;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Assist.Areas.Kisan.Controllers
{
    [Area("Kisan")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class NewsCategoryController : ControllerBase
    {
        private readonly INewsCategoryService _iNewsCategoryService;

        public NewsCategoryController(INewsCategoryService iNewsCategoryService)
        {
            _iNewsCategoryService = iNewsCategoryService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(NewsCategoryModel um)
        {
            var res = _iNewsCategoryService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("NewsCategoryID")]
        [ActionName("Edit")]
        public IActionResult Edit(int NewsCategoryID, NewsCategoryModel um)
        {
            if (NewsCategoryID == um.NewsCatID)
            {
                var res = _iNewsCategoryService.Edit(NewsCategoryID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{NewsCategoryID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int NewsCategoryID)
        {
            var res = _iNewsCategoryService.Delete(NewsCategoryID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{NewsCatName}/{SubscriberID}/{NewsCategoryID}/{type}")]
        [ActionName("CheckDuplicateRole")]
        public IActionResult CheckDuplicateNewsCat(string NewsCatName, int? SubscriberID, int NewsCategoryID, string type)
        {
            var res = _iNewsCategoryService.CheckDuplicateNewsCat(NewsCatName, SubscriberID, NewsCategoryID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{NewsCategoryID}")]
        [ActionName("GetNewsCatById")]
        public IActionResult GetNewsCatById(int NewsCategoryID)
        {
            var NewsCatMaster = _iNewsCategoryService.GetNewsCatById(NewsCategoryID);
            if (NewsCatMaster == null)
            {
                return NotFound();
            }
            return Ok(NewsCatMaster);
        }

        [HttpGet("{NewsCategoryID}")]
        [ActionName("GetNewsCatDetails")]
        public IActionResult GetNewsCatDetails(int NewsCategoryID)
        {
            var NewsCatMaster = _iNewsCategoryService.GetNewsCatById(NewsCategoryID);
            if (NewsCatMaster == null)
            {
                return NotFound();
            }
            return Ok(NewsCatMaster);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllNewsCat")]
        public IEnumerable<NewsCategoryModelData> GetAllNewsCat(int SubscriberID)
        {
            var NewsCatMaster = _iNewsCategoryService.GetAllNewsCat(SubscriberID);
            return NewsCatMaster;
        }
    }
}