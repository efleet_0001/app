﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using AutoMapper;

using Assist.Model;
using AssistBLL.Business;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Assist.Areas.Kisan.Controllers
{
    [Area("Kisan")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TokenController : Controller
    {
        private IConfiguration _config;
        private IUserBusiness _userBusiness;
        private readonly IMapper _mapper;

        public TokenController(IConfiguration config, IMapper mapper, IUserBusiness userBusiness)
        {
            _config = config;
            _mapper = mapper;
            _userBusiness = userBusiness;
        }
        // [AllowAnonymous]
        [HttpPost]
        [ActionName("authenticate")]
        public IActionResult authenticate([FromBody] LogInModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Values.ToArray());
            }
            IActionResult respon = Unauthorized();
            var isValid = _userBusiness.Authenticate(viewModel.Username, viewModel.Password);
            if (isValid)
            {
                var thisUser = _userBusiness.GetUser(id: 0, username: viewModel.Username);
                var viewModelFromEntity = _mapper.Map<UserModel>(thisUser);
                var defaultRoles = "GUESS"; // SET your user role here!
                var tokenStr = BuildToken(viewModelFromEntity, defaultRoles);
                // respon = Ok(new { token = tokenStr, message = "another message" });
                // respon = Ok(tokenStr);
                respon = Ok(tokenStr);
            }
            return respon;
        }
        [AllowAnonymous]
        [Route("CreateToken")]
        [HttpPost]
        public IActionResult CreateToken([FromBody] LogInModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Values.ToArray());
            }
            IActionResult respon = Unauthorized();
            var isValid = _userBusiness.Authenticate(viewModel.Username, viewModel.Password);
            if (isValid)
            {
                var thisUser = _userBusiness.GetUser(id: 0, username: viewModel.Username);
                var viewModelFromEntity = _mapper.Map<UserModel>(thisUser);
                var defaultRoles = "GUESS"; // SET your user role here!
                var tokenStr = BuildToken(viewModelFromEntity, defaultRoles);
                // respon = Ok(new { token = tokenStr, message = "another message" });
                respon = Ok(tokenStr);
            }
            return respon;
        }

        private string BuildToken(UserModel myUser, string Roles)
        {

            IdentityOptions _options = new IdentityOptions();
            var claims = new[] {
               // new Claim(JwtRegisteredClaimNames.GivenName, myUser.Alias ),
                new Claim(JwtRegisteredClaimNames.Email, myUser.Email),
                new Claim(JwtRegisteredClaimNames.Acr, myUser.Mobile),
                new Claim(JwtRegisteredClaimNames.Sub, myUser.SubscriberID.ToString()),
               // new Claim(JwtRegisteredClaimNames.Birthdate, myUser.DOB.ToString()),
               // new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(_options.ClaimsIdentity.UserNameClaimType, myUser.Alias),
                new Claim(ClaimTypes.Role, Roles)
              };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);


            var token = new JwtSecurityToken(
              _config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims, expires: DateTime.Now.AddMinutes(20),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}