﻿using System.Collections.Generic;
using AssistBLL.KisanModel.Trans;
using AssistBLL.KisanModel.Trans.Data;
using AssistBLL.Services.Kisan;
using AssistBLL.Services.Trans;
using Microsoft.AspNetCore.Mvc;

namespace Assist.Areas.Kisan.Controllers
{
    [Area("Kisan")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SolutionController : ControllerBase
    {
        private readonly ISolutionService _iSolutionService;

        public SolutionController(ISolutionService iSolutionService)
        {
            _iSolutionService = iSolutionService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(SolutionModel um)
        {
            var res = _iSolutionService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("SolnID")]
        [ActionName("Edit")]
        public IActionResult Edit(int SolnID, SolutionModel um)
        {
            if (SolnID == um.SolnID)
            {
                var res = _iSolutionService.Edit(SolnID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{SolnID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int SolnID)
        {
            var res = _iSolutionService.Delete(SolnID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{SolutionTag}/{SubscriberID}/{SolnID}/{type}")]
        [ActionName("CheckDuplicateSolution")]
        public IActionResult CheckDuplicateSolution(string SolutionTag, int? SubscriberID, int SolnID, string type)
        {
            var res = _iSolutionService.CheckDuplicateSolution(SolutionTag, SubscriberID, SolnID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{SolnID}")]
        [ActionName("GetSolutionById")]
        public IActionResult GetSolutionById(int SolnID)
        {
            var details = _iSolutionService.GetSolutionById(SolnID);
            if (details == null)
            {
                return NotFound();
            }
            return Ok(details);
        }

        [HttpGet("{SolnID}")]
        [ActionName("GetSolutionDetails")]
        public IActionResult GetSolutionDetails(int SolnID)
        {
            var details = _iSolutionService.GetSolutionById(SolnID);
            if (details == null)
            {
                return NotFound();
            }
            return Ok(details);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllSolution")]
        public IEnumerable<SolutionModelData> GetAllSolution(int SubscriberID)
        {
            var details = _iSolutionService.GetAllSolution(SubscriberID);
            return details;
        }
    }
}