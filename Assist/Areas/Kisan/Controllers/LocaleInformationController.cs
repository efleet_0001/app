﻿using AssistBLL.Services.Master;
using AssistBLL.ViewModel.Master;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Assist.Areas.Kisan.Controllers
{
    [Area("Kisan")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LocaleInformationController : ControllerBase
    {
        private readonly ILocaleInformationService _iLocaleInformationService;

        public LocaleInformationController(ILocaleInformationService iLocaleInformationService)
        {
            _iLocaleInformationService = iLocaleInformationService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(LocaleInformationModel um)
        {
            var res = _iLocaleInformationService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("LocaleInfoID")]
        [ActionName("Edit")]
        public IActionResult Edit(int LocaleInfoID, LocaleInformationModel um)
        {
            if (LocaleInfoID == um.LocID)
            {
                var res = _iLocaleInformationService.Edit(LocaleInfoID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{LocaleInfoID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int LocaleInfoID)
        {
            var res = _iLocaleInformationService.Delete(LocaleInfoID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{LocaleInfoName}/{SubscriberID}/{LocaleInfoID}/{type}")]
        [ActionName("CheckDuplicateLocaleInfo")]
        public IActionResult CheckDuplicateLocaleInfo(string LocaleInfoName, int? SubscriberID, int LocaleInfoID, string type)
        {
            var res = _iLocaleInformationService.CheckDuplicateLocaleInfo(LocaleInfoName, SubscriberID, LocaleInfoID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{LocaleInfoID}")]
        [ActionName("GetLocaleInfoById")]
        public IActionResult GetLocaleInfoById(int LocaleInfoID)
        {
            var locinfoMaster = _iLocaleInformationService.GetLocaleInfoById(LocaleInfoID);
            if (locinfoMaster == null)
            {
                return NotFound();
            }
            return Ok(locinfoMaster);
        }

        [HttpGet("{LocaleInfoID}")]
        [ActionName("GetLocInfoDetails")]
        public IActionResult GetLocInfoDetails(int LocaleInfoID)
        {
            var locinfoMaster = _iLocaleInformationService.GetLocaleInfoById(LocaleInfoID);
            if (locinfoMaster == null)
            {
                return NotFound();
            }
            return Ok(locinfoMaster);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetLocInfo")]
        public IEnumerable<LocaleInformationModel> GetLocInfo(int SubscriberID)
        {
            var locinfoMaster = _iLocaleInformationService.GetAll(SubscriberID);
            return locinfoMaster;
        }
    }
}