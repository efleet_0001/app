﻿using AssistBLL.KisanModel.Trans;
using AssistBLL.KisanModel.Trans.Data;
using AssistBLL.Services.Kisan;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Assist.Areas.Kisan.Controllers
{
    [Area("Kisan")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private readonly INewsService _iNewsService;

        public NewsController(INewsService iNewsService)
        {
            _iNewsService = iNewsService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(NewsModel um)
        {
            var res = _iNewsService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("NewsID")]
        [ActionName("Edit")]
        public IActionResult Edit(int NewsID, NewsModel um)
        {
            if (NewsID == um.NewsID)
            {
                var res = _iNewsService.Edit(NewsID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{NewsID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int NewsID)
        {
            var res = _iNewsService.Delete(NewsID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{NewsName}/{SubscriberID}/{NewsID}/{type}")]
        [ActionName("CheckDuplicateNews")]
        public IActionResult CheckDuplicateNews(string NewsName, int? SubscriberID, int NewsID, string type)
        {
            var res = _iNewsService.CheckDuplicateNews(NewsName, SubscriberID, NewsID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{NewsID}")]
        [ActionName("GetNewsById")]
        public IActionResult GetNewsById(int NewsID)
        {
            var NewsMaster = _iNewsService.GetNewsById(NewsID);
            if (NewsMaster == null)
            {
                return NotFound();
            }
            return Ok(NewsMaster);
        }

        [HttpGet("{NewsID}")]
        [ActionName("GetNewsDetails")]
        public IActionResult GetNewsDetails(int NewsID)
        {
            var NewsMaster = _iNewsService.GetNewsById(NewsID);
            if (NewsMaster == null)
            {
                return NotFound();
            }
            return Ok(NewsMaster);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllNews")]
        public IEnumerable<NewsModelData> GetAllNews(int SubscriberID)
        {
            var NewsMaster = _iNewsService.GetAllNews(SubscriberID);
            return NewsMaster;
        }
        [HttpGet("{SubscriberID}/{LanguageID}/{UserID}/{EntityID}")]
        [ActionName("GetEntityNews")]
        public IEnumerable<EntityCateModel> GetEntityNews(int SubscriberID, int LanguageID, long UserID,int EntityID)
        {
            List<EntityCateModel> list = new List<EntityCateModel>();
            var catdata = _iNewsService.GetCateGory(SubscriberID, LanguageID, UserID, EntityID);
            var data = _iNewsService.GetEntityNew(1, SubscriberID, LanguageID, UserID, EntityID);
            foreach (var d in catdata)
            {
                List<EntityNewDataModel> listsub = new List<EntityNewDataModel>();
                long CateID = d.CatID;
                var data1 = _iNewsService.GetEntityNew(CateID, SubscriberID, LanguageID, UserID, EntityID);
                foreach (var i in data1)
                {
                    EntityNewDataModel objsubmodel = new EntityNewDataModel();
                    objsubmodel.NewsID = i.NewsID;
                    objsubmodel.SorceTypeID = i.SorceTypeID;
                    objsubmodel.SorceTypeName = i.SorceTypeName;
                    objsubmodel.MediaSourceTypeID = i.MediaSourceTypeID;
                    objsubmodel.MediaSourceTypeName = i.MediaSourceTypeName;
                    objsubmodel.Date = i.Date;
                    objsubmodel.SourceInfoName = i.SourceInfoName;
                    objsubmodel.Headline = i.Headline;
                    objsubmodel.NewsContent = i.NewsContent;
                    objsubmodel.NewsByName = i.NewsByName;
                    objsubmodel.Time = i.Time;
                    listsub.Add(objsubmodel);
                }
                EntityCateModel obj = new EntityCateModel();
                obj.CatID = d.CatID;
                obj.CateName = d.CateName;
                obj.EntityNewDataModels = listsub;

                list.Add(obj);
            }
            return list;
        }
        [HttpGet("{SubscriberID}/{LanguageID}/{UserID}/{EntityID}")]
        [ActionName("GetEntityNewsCat")]
        public IEnumerable<NewCategoryDataModel> GetEntityNewsCat(int SubscriberID, int LanguageID, long UserID, int EntityID)
        {
           
            var catdata = _iNewsService.GetCateGory(SubscriberID, LanguageID, UserID, EntityID);
            var data = _iNewsService.GetEntityNew(1, SubscriberID, LanguageID, UserID, EntityID);
            return catdata;
        }
        [HttpGet("{CatId}/{SubscriberID}/{LanguageID}/{UserID}/{EntityID}")]
        [ActionName("GetEntityNewsby")]
        public IEnumerable<EntityNewDataModel> GetEntityNewsby(int CatId,int SubscriberID, int LanguageID, long UserID, int EntityID)
        {
            var data = _iNewsService.GetEntityNew(CatId, SubscriberID, LanguageID, UserID, EntityID);
            return data;
        }
    }
}