﻿using AssistBLL.KisanModel.Master;
using AssistBLL.KisanModel.Master.Data;
using AssistBLL.Services.Kisan;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Assist.Areas.Kisan.Controllers
{
    [Area("Kisan")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SubcategoryController : ControllerBase
    {
        private readonly ISubCategoryService _iSubCategoryService;

        public SubcategoryController(ISubCategoryService iSubCategoryService)
        {
            _iSubCategoryService = iSubCategoryService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(SubCategoryModel um)
        {
            var res = _iSubCategoryService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("SubCatID")]
        [ActionName("Edit")]
        public IActionResult Edit(int SubCatID, SubCategoryModel um)
        {
            if (SubCatID == um.Subcat_ID)
            {
                var res = _iSubCategoryService.Edit(SubCatID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{SubCatID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int SubCatID)
        {
            var res = _iSubCategoryService.Delete(SubCatID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{SubCatName}/{SubscriberID}/{SubCatID}/{type}")]
        [ActionName("CheckDuplicateSubCat")]
        public IActionResult CheckDuplicateSubCat(string SubCatName, int? SubscriberID, int SubCatID, string type)
        {
            var res = _iSubCategoryService.CheckDuplicateSubCat(SubCatName, SubscriberID, SubCatID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{SubCatID}")]
        [ActionName("GetSubCatById")]
        public IActionResult GetSubCatById(int SubCatID)
        {
            var Category = _iSubCategoryService.GetSubCatById(SubCatID);
            if (Category == null)
            {
                return NotFound();
            }
            return Ok(Category);
        }

        [HttpGet("{SubCatID}")]
        [ActionName("GetSubCatDetails")]
        public IActionResult GetSubCatDetails(int SubCatID)
        {
            var subcategory = _iSubCategoryService.GetSubCatById(SubCatID);
            if (subcategory == null)
            {
                return NotFound();
            }
            return Ok(subcategory);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllSubCat")]
        public IEnumerable<SubCategoryModelData> GetAllSubCat(int SubscriberID)
        {
            var subcategory = _iSubCategoryService.GetAllSubCat(SubscriberID);
            return subcategory;
        }
    }
}