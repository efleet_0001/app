﻿using AssistBLL.KisanModel.Trans;
using AssistBLL.Services.Kisan;
using Microsoft.AspNetCore.Mvc;

namespace Assist.Areas.Kisan.Controllers
{
    [Area("Kisan")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MemberMappingController : ControllerBase
    {
        private readonly IMemberMappingService _iUserGroupService;

        public MemberMappingController(IMemberMappingService iUserGroupService)
        {
            _iUserGroupService = iUserGroupService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(MemberMappingModel um)
        {
            var res = _iUserGroupService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("MappingID")]
        [ActionName("Edit")]
        public IActionResult Edit(int MappingID, MemberMappingModel um)
        {
            if (MappingID == um.MappingID)
            {
                var res = _iUserGroupService.Edit(MappingID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{MappingID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int MappingID)
        {
            var res = _iUserGroupService.Delete(MappingID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{MemberID}/{SubscriberID}/{MappingID}/{type}")]
        [ActionName("CheckDuplicateMemberMapping")]
        public IActionResult CheckDuplicateMemberMapping(long MemberID, int? SubscriberID, long MappingID, string type)
        {
            var res = _iUserGroupService.CheckDuplicateMemberMapping(MemberID, SubscriberID, MappingID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{MappingID}")]
        [ActionName("GetMemberMappingById")]
        public IActionResult GetMemberMappingById(int MappingID)
        {
            var NewsCatMaster = _iUserGroupService.GetMemberMappingById(MappingID);
            if (NewsCatMaster == null)
            {
                return NotFound();
            }
            return Ok(NewsCatMaster);
        }

        [HttpGet("{MappingID}")]
        [ActionName("GetAllMemberMapping")]
        public IActionResult GetAllMemberMapping(int MappingID)
        {
            var NewsCatMaster = _iUserGroupService.GetAllMemberMapping(MappingID);
            if (NewsCatMaster == null)
            {
                return NotFound();
            }
            return Ok(NewsCatMaster);
        }

      
    }
}