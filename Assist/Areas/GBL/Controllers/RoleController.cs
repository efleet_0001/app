﻿using AssistBLL.Services.UserControl;
using AssistBLL.ViewModel.UserControl;
using AssistBLL.ViewModel.UserControl.Data;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleMasterService _iRoleMasterService;

        public RoleController(IRoleMasterService iRoleMasterService)
        {
            _iRoleMasterService = iRoleMasterService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(RoleModel um)
        {
            var res = _iRoleMasterService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("RoleID")]
        [ActionName("Edit")]
        public IActionResult Edit(int RoleID, RoleModel um)
        {
            if (RoleID == um.RoleID)
            {
                var res = _iRoleMasterService.Edit(RoleID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{RoleID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int RoleID)
        {
            var res = _iRoleMasterService.Delete(RoleID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{RoleName}/{SubscriberID}/{RoleID}/{type}")]
        [ActionName("CheckDuplicateRole")]
        public IActionResult CheckDuplicateRole(string RoleName, int? SubscriberID, int RoleID, string type)
        {
            var res = _iRoleMasterService.CheckDuplicateRole(RoleName, SubscriberID, RoleID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{RoleID}")]
        [ActionName("GetRoleById")]
        public IActionResult GetRoleById(int RoleID)
        {
            var roleMaster = _iRoleMasterService.GetRoleById(RoleID);
            if (roleMaster == null)
            {
                return NotFound();
            }
            return Ok(roleMaster);
        }

        [HttpGet("{RoleID}")]
        [ActionName("GetRoleDetails")]
        public IActionResult GetRoleDetails(int RoleID)
        {
            var roleMaster = _iRoleMasterService.GetRoleById(RoleID);
            if (roleMaster == null)
            {
                return NotFound();
            }
            return Ok(roleMaster);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllRole")]
        public IEnumerable<RoleModelData> GetAllRole(int SubscriberID)
        {
            var roleMaster = _iRoleMasterService.GetAllRole(SubscriberID);
            return roleMaster;
        }
    }
}