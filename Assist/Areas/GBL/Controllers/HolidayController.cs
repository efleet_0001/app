﻿using AssistBLL.Services.General;
using AssistBLL.ViewModel.General;
using AssistBLL.ViewModel.Master;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HolidayController : ControllerBase
    {
        private readonly IHolidayMasterService _iHolidayMasterService;

        public HolidayController(IHolidayMasterService iHolidayMasterService)
        {
            _iHolidayMasterService = iHolidayMasterService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(HolidayModel um)
        {
            var res = _iHolidayMasterService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("HolidayID")]
        [ActionName("Edit")]
        public IActionResult Edit(int HolidayID, HolidayModel um)
        {
            if (HolidayID == um.HoliDayID)
            {
                var res = _iHolidayMasterService.Edit(HolidayID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{HolidayID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int HolidayID)
        {
            var res = _iHolidayMasterService.Delete(HolidayID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{HolidayName}/{SubscriberID}/{HolidayID}/{type}")]
        [ActionName("CheckDuplicateHoliday")]
        public IActionResult CheckDuplicateHoliday(string HolidayName, int? SubscriberID, int HolidayID, string type)
        {
            var res = _iHolidayMasterService.CheckDuplicateHoliday(HolidayName, SubscriberID, HolidayID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{HolidayID}")]
        [ActionName("GetHolidayById")]
        public IActionResult GetHolidayById(int HolidayID)
        {
            var holidayMaster = _iHolidayMasterService.GetHolidayById(HolidayID);
            if (holidayMaster == null)
            {
                return NotFound();
            }
            return Ok(holidayMaster);
        }

        [HttpGet("{HolidayID}")]
        [ActionName("GetHolidayDetails")]
        public IActionResult GetHolidayDetails(int HolidayID)
        {
            var holidayMaster = _iHolidayMasterService.GetHolidayById(HolidayID);
            if (holidayMaster == null)
            {
                return NotFound();
            }
            return Ok(holidayMaster);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllHoliday")]
        public IEnumerable<HolidayModel> GetAllHoliday(int SubscriberID)
        {
            var holidayMaster = _iHolidayMasterService.GetAllHoliday(SubscriberID);
            return holidayMaster;
        }
    }
}