﻿using AssistBLL.Services.UserControl;
using AssistBLL.ViewModel.Master;
using AssistBLL.ViewModel.UserControl;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly IProfileService _iProfileService;

        public ProfileController(IProfileService iProfileService)
        {
            _iProfileService = iProfileService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(ProfileModel um)
        {
            var res = _iProfileService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("ProfileID")]
        [ActionName("Edit")]
        public IActionResult Edit(int ProfileID, ProfileModel um)
        {
            if (ProfileID == um.ProfileID)
            {
                var res = _iProfileService.Edit(ProfileID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{ProfileID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int ProfileID)
        {
            var res = _iProfileService.Delete(ProfileID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{ProfileName}/{SubscriberID}/{ProfileID}/{type}")]
        [ActionName("CheckDuplicateProfile")]
        public IActionResult CheckDuplicateProfile(string ProfileName, int? SubscriberID, int ProfileID, string type)
        {
            var res = _iProfileService.CheckDuplicateProfile(ProfileName, SubscriberID, ProfileID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{ProfileID}")]
        [ActionName("GetProfileById")]
        public IActionResult GetProfileById(int ProfileID)
        {
            var profileMaster = _iProfileService.GetProfileById(ProfileID);
            if (profileMaster == null)
            {
                return NotFound();
            }
            return Ok(profileMaster);
        }

        [HttpGet("{ProfileID}")]
        [ActionName("GetProfileDetails")]
        public IActionResult GetProfileDetails(int ProfileID)
        {
            var profileMaster = _iProfileService.GetProfileById(ProfileID);
            if (profileMaster == null)
            {
                return NotFound();
            }
            return Ok(profileMaster);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllProfile")]
        public IEnumerable<ProfileModel> GetAllProfile(int SubscriberID)
        {
            var profileMaster = _iProfileService.GetAllProfile(SubscriberID);
            return profileMaster;
        }
    }
}