﻿using AssistBLL.Services.Master;
using Microsoft.AspNetCore.Mvc;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class StaticController : ControllerBase
    {
        private readonly IStaticService _iStaticService;

        public StaticController(IStaticService iStaticService)
        {
            _iStaticService = iStaticService;
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllApplication")]
        public IActionResult GetAllApplication(int SubscriberID)
        {
            var ApplicationDetails = _iStaticService.GetAllApplication(SubscriberID);
            if (ApplicationDetails == null)
            {
                return NotFound();
            }
            return Ok(ApplicationDetails);
        }
        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllClient")]
        public IActionResult GetAllClient(int SubscriberID)
        {
            var ClientDetails = _iStaticService.GetAllClient(SubscriberID);
            if (ClientDetails == null)
            {
                return NotFound();
            }
            return Ok(ClientDetails);
        }
        [HttpGet("{SubscriberID}/{ConstantGroupID}")]
        [ActionName("GetAllConstant")]
        public IActionResult GetAllConstant(int SubscriberID, int ConstantGroupID)
        {
            var ConstantDetails = _iStaticService.GetAllConstant(SubscriberID, ConstantGroupID);
            if (ConstantDetails == null)
            {
                return NotFound();
            }
            return Ok(ConstantDetails);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllCountry")]
        public IActionResult GetAllCountry(int SubscriberID)
        {
            var CountryDetails = _iStaticService.GetAllCountry(SubscriberID);
            if (CountryDetails == null)
            {
                return NotFound();
            }
            return Ok(CountryDetails);
        }

        [HttpGet("{SubscriberID}/{CountryID}")]
        [ActionName("GetAllState")]
        public IActionResult GetAllState(int SubscriberID, int CountryID)
        {
            var StateDetails = _iStaticService.GetAllState(SubscriberID, CountryID);
            if (StateDetails == null)
            {
                return NotFound();
            }
            return Ok(StateDetails);
        }

        [HttpGet("{SubscriberID}/{StateID}")]
        [ActionName("GetAllDistrict")]
        public IActionResult GetAllDistrict(int SubscriberID, int StateID)
        {
            var DistrictDetails = _iStaticService.GetAllDistrict(SubscriberID, StateID);
            if (DistrictDetails == null)
            {
                return NotFound();
            }
            return Ok(DistrictDetails);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllLanguage")]
        public IActionResult GetAllLanguage(int SubscriberID)
        {
            var LanguageDetails = _iStaticService.GetAllLanguage(SubscriberID);
            if (LanguageDetails == null)
            {
                return NotFound();
            }
            return Ok(LanguageDetails);
        }

        [HttpGet("{DistrictID}")]
        [ActionName("GetAllHolidays")]
        public IActionResult GetAllHolidays(int DistrictID)
        {
            var HolidayDetails = _iStaticService.GetAllHolidays(DistrictID);
            if (HolidayDetails == null)
            {
                return NotFound();
            }
            return Ok(HolidayDetails);
        }
    }
}