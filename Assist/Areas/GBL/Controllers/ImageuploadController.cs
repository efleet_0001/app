﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AssistBLL.Services.Trans;
using AssistBLL.ViewModel.Trans;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ImageuploadController : ControllerBase
    {
        private readonly IImageuploadService _iImageuploadService;

        public ImageuploadController(IImageuploadService iImageuploadService)
        {
            _iImageuploadService = iImageuploadService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(ImageUploadModel um)
        {
            var res = _iImageuploadService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("{TransactionID}/{TransactionTypeID}")]
        [ActionName("Edit")]
        public IActionResult Edit(long TransactionID, int TransactionTypeID, ImageUploadModel um)
        {
            if (TransactionID == um.TransctionID && TransactionTypeID==um.ImageTypeID)
            {
                var res = _iImageuploadService.Edit(TransactionID, TransactionTypeID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{TransactionID}/{TransactionTypeID}")]
        [ActionName("Delete")]
        public IActionResult Delete(long TransactionID, int TransactionTypeID)
        {
            var res = _iImageuploadService.Delete(TransactionID, TransactionTypeID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{Name}/{SubscriberID}/{TransactionID}/{TransactypeID}/{type}")]
        [ActionName("CheckDuplicateImage")]
        public IActionResult CheckDuplicateImage(string Name, int? SubscriberID, long TransactionID, int TransactypeID, string type)
        {
            var res = _iImageuploadService.CheckDuplicateImage(Name, SubscriberID, TransactionID, TransactypeID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{TransactionID}/{TransactionTypeID}")]
        [ActionName("GetImageById")]
        public IActionResult GetImageById(long TransactionID, int TransactionTypeID)
        {
            var FYMaster = _iImageuploadService.GetImageById(TransactionID, TransactionTypeID);
            if (FYMaster == null)
            {
                return NotFound();
            }
            return Ok(FYMaster);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllImage")]
        public IActionResult GetAllImage(int SubscriberID)
        {
            var FYMaster = _iImageuploadService.GetAllImage(SubscriberID);
            if (FYMaster == null)
            {
                return NotFound();
            }
            return Ok(FYMaster);
        }

      
    }
}