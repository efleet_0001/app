﻿using System.Collections.Generic;
using AssistBLL.Services.Master;
using AssistBLL.ViewModel.General.Data;
using AssistBLL.ViewModel.Master;
using Microsoft.AspNetCore.Mvc;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ContactController : ControllerBase
    {

        private readonly IContactMasterService _iContactMasterService;
        public ContactController(IContactMasterService iContactMasterService)
        {
            _iContactMasterService = iContactMasterService;
        }
        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(ContactModel um)
        {
            var res = _iContactMasterService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("ContactID")]
        [ActionName("Edit")]
        public IActionResult Edit(int ContactID, ContactModel um)
        {
            if (ContactID == um.ConID)
            {
                var res = _iContactMasterService.Edit(ContactID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{ContactID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int ContactID)
        {
            var res = _iContactMasterService.Delete(ContactID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{ContactName}/{SubscriberID}/{ContactID}/{type}")]
        [ActionName("CheckDuplicateContact")]
        public IActionResult CheckDuplicateContact(string ContactName, int? SubscriberID, int ContactID, string type)
        {
            var res = _iContactMasterService.CheckDuplicateContact(ContactName, SubscriberID, ContactID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }
        [HttpGet("{ContactID}")]
        [ActionName("GetContactById")]
        public IActionResult GetContactById(int ContactID)
        {
            var contact = _iContactMasterService.GetContactById(ContactID);
            if (contact == null)
            {
                return NotFound();
            }
            return Ok(contact);
        }
        [HttpGet("{ContactID}")]
        [ActionName("GetContactDetails")]
        public IActionResult GetContactDetails(int ContactID)
        {
            var contact = _iContactMasterService.GetContactDetails(ContactID);
            if (contact == null)
            {
                return NotFound();
            }
            return Ok(contact);
        }
        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllContact")]
        public IEnumerable<ContactModelData> GetAllContact(int SubscriberID)
        {
            var contact = _iContactMasterService.GetAllContact(SubscriberID);
            return contact;
        }
    }
}