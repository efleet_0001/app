﻿using AssistBLL.Services.UserControl;
using AssistBLL.ViewModel.UserControl;
using AssistBLL.ViewModel.UserControl.Data;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProfileDetailsController : ControllerBase
    {
        private readonly IProfileDetailService _iProfileDetailService;

        public ProfileDetailsController(IProfileDetailService iProfileDetailService)
        {
            _iProfileDetailService = iProfileDetailService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(ProfileDetailsModel um)
        {
            var res = _iProfileDetailService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("ProfileID")]
        [ActionName("Edit")]
        public IActionResult Edit(int ProfileID, ProfileDetailsModel um)
        {
            if (ProfileID == um.ProfileID)
            {
                var res = _iProfileDetailService.Edit(ProfileID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{ProfileID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int ProfileID)
        {
            var res = _iProfileDetailService.Delete(ProfileID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{ProfileName}/{SubscriberID}/{ProfileID}/{type}")]
        [ActionName("CheckDuplicateProfile")]
        public IActionResult CheckDuplicateProfile(string ProfileName, int? SubscriberID, int ProfileID, string type)
        {
            var res = _iProfileDetailService.CheckDuplicateProfile(ProfileName, SubscriberID, ProfileID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{ProfileID}")]
        [ActionName("GetProfileById")]
        public IActionResult GetProfileById(int ProfileID)
        {
            var profileMaster = _iProfileDetailService.GetProfileById(ProfileID);
            if (profileMaster == null)
            {
                return NotFound();
            }
            return Ok(profileMaster);
        }

        [HttpGet("{ProfileID}")]
        [ActionName("GetProfileDetails")]
        public IActionResult GetProfileDetails(int ProfileID)
        {
            var profileMaster = _iProfileDetailService.GetProfileById(ProfileID);
            if (profileMaster == null)
            {
                return NotFound();
            }
            return Ok(profileMaster);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllProfile")]
        public IEnumerable<ProfileDetailsModelData> GetAllProfile(int SubscriberID)
        {
            var profileMaster = _iProfileDetailService.GetAllProfile(SubscriberID);
            return profileMaster;
        }
        //[HttpGet]
        //[ActionName("GetTimeZone")]
        //public IEnumerable<ReadOnlyCollection> GetTimeZone()
        //{
        //    ReadOnlyCollection<TimeZoneInfo> tz;
        //    tz = TimeZoneInfo.GetSystemTimeZones();
        //    return tz;
        //}
    }
}