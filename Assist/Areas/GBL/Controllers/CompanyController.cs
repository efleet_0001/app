﻿using AssistBLL.Services.General;
using AssistBLL.ViewModel.General;
using AssistBLL.ViewModel.General.Data;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly ICompanyMasterService _iCompanyMasterService;
        public CompanyController(ICompanyMasterService iCompanyMasterService)
        {
            _iCompanyMasterService = iCompanyMasterService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(CompanyDetailsModel um)
        {
            var res = _iCompanyMasterService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("CompanyID")]
        [ActionName("Edit")]
        public IActionResult Edit(int CompanyID, CompanyDetailsModel um)
        {
            if (CompanyID == um.CompID)
            {
                var res = _iCompanyMasterService.Edit(CompanyID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{CompanyID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int CompanyID)
        {
            var res = _iCompanyMasterService.Delete(CompanyID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{CompanyName}/{SubscriberID}/{CompanyID}/{type}")]
        [ActionName("CheckDuplicateCompany")]
        public IActionResult CheckDuplicateCompany(string CompanyName, int? SubscriberID, int CompanyID, string type)
        {
            var res = _iCompanyMasterService.CheckDuplicateCompany(CompanyName, SubscriberID, CompanyID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{CompanyID}")]
        [ActionName("GetCompanyById")]
        public IActionResult GetCompanyById(int CompanyID)
        {
            var Category = _iCompanyMasterService.GetCompanyById(CompanyID);
            if (Category == null)
            {
                return NotFound();
            }
            return Ok(Category);
        }

        [HttpGet("{CompanyID}")]
        [ActionName("GetCompanyDetails")]
        public IActionResult GetCompanyDetails(int CompanyID)
        {
            var Category = _iCompanyMasterService.GetCompanyById(CompanyID);
            if (Category == null)
            {
                return NotFound();
            }
            return Ok(Category);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllCompany")]
        public IEnumerable<CompanyDetailsModelData> GetAllCompany(int SubscriberID)
        {
            var Company = _iCompanyMasterService.GetAllCompany(SubscriberID);
            return Company;
        }
    }
}