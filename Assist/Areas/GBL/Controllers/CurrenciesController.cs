﻿using AssistBLL.Services.General;
using AssistBLL.ViewModel.General;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CurrenciesController : ControllerBase
    {
        private readonly ICurrenciesService _iCurrenciesService;

        public CurrenciesController(ICurrenciesService iCurrenciesService)
        {
            _iCurrenciesService = iCurrenciesService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(CurrenciesModel um)
        {
            var res = _iCurrenciesService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("ID")]
        [ActionName("Edit")]
        public IActionResult Edit(int ID, CurrenciesModel um)
        {
            if (ID == um.ID)
            {
                var res = _iCurrenciesService.Edit(ID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{ID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int ID)
        {
            var res = _iCurrenciesService.Delete(ID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{TypeID}/{SubscriberID}/{ID}/{type}")]
        [ActionName("CheckDuplicateCurrencies")]
        public IActionResult CheckDuplicateCurrencies(int TypeID, int? SubscriberID, int ID, string type)
        {
            var res = _iCurrenciesService.CheckDuplicateCurrencies(TypeID, SubscriberID, ID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{ID}")]
        [ActionName("GetCurrenciesById")]
        public IActionResult GetCurrenciesById(int ID)
        {
            var FYMaster = _iCurrenciesService.GetCurrenciesById(ID);
            if (FYMaster == null)
            {
                return NotFound();
            }
            return Ok(FYMaster);
        }

        [HttpGet("{ID}")]
        [ActionName("GetFYCurrencies")]
        public IActionResult GetFYCurrencies(int ID)
        {
            var FYMaster = _iCurrenciesService.GetCurrenciesById(ID);
            if (FYMaster == null)
            {
                return NotFound();
            }
            return Ok(FYMaster);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllCurrencies")]
        public IEnumerable<CurrenciesModel> GetAllCurrencies(int SubscriberID)
        {
            var FYMaster = _iCurrenciesService.GetAllCurrencies(SubscriberID);
            return FYMaster;
        }
    }
}