﻿using AssistBLL.Services.General;
using AssistBLL.ViewModel.General;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FiscalController : ControllerBase
    {
        private readonly IFiscalMasterService _iFiscalMasterService;

        public FiscalController(IFiscalMasterService iFiscalMasterService)
        {
            _iFiscalMasterService = iFiscalMasterService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(FiscalYearModel um)
        {
            var res = _iFiscalMasterService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("FYID")]
        [ActionName("Edit")]
        public IActionResult Edit(int FYID, FiscalYearModel um)
        {
            if (FYID == um.FYID)
            {
                var res = _iFiscalMasterService.Edit(FYID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{FYID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int FYID)
        {
            var res = _iFiscalMasterService.Delete(FYID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{FiscalName}/{SubscriberID}/{FYID}/{type}")]
        [ActionName("CheckDuplicateFiscal")]
        public IActionResult CheckDuplicateFiscal(string FiscalName, int? SubscriberID, int FYID, string type)
        {
            var res = _iFiscalMasterService.CheckDuplicateFiscal(FiscalName, SubscriberID, FYID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{FYID}")]
        [ActionName("GetFiscalById")]
        public IActionResult GetFiscalById(int FYID)
        {
            var FYMaster = _iFiscalMasterService.GetFiscalById(FYID);
            if (FYMaster == null)
            {
                return NotFound();
            }
            return Ok(FYMaster);
        }

        [HttpGet("{FYID}")]
        [ActionName("GetFYDetails")]
        public IActionResult GetFYDetails(int FYID)
        {
            var FYMaster = _iFiscalMasterService.GetFiscalById(FYID);
            if (FYMaster == null)
            {
                return NotFound();
            }
            return Ok(FYMaster);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllFiscal")]
        public IEnumerable<FiscalYearModel> GetAllFiscal(int SubscriberID)
        {
            var FYMaster = _iFiscalMasterService.GetAllFiscal(SubscriberID);
            return FYMaster;
        }
    }
}