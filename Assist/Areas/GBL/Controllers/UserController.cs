﻿using AutoMapper;

using Assist.Model;
using AssistBLL.Business;
using Assist;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using AssistBLL.ViewModel.Master;
using AssistDB.UserControl;
using AssistBLL.ViewModel.UserControl;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : Controller
    {

        private IConfiguration _config;
        private IUserBusiness _userBusiness;
        private readonly IMapper _mapper;

        public UserController(IConfiguration config, IMapper mapper, IUserBusiness userBusiness)
        {
            _config = config;
            _mapper = mapper;
            _userBusiness = userBusiness;
        }
        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll()
        {
            var user = _userBusiness.GetUsers();
            return Ok(user);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public IActionResult GetUser(int id)
        {
            var user = _userBusiness.GetUser(id);
            return Ok(user);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("authenticate")]
        public IActionResult authenticate(string username, string password)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState.Values.ToArray());
            //}
            IActionResult respon = Unauthorized();
            var isValid = _userBusiness.Authenticate(username, password);
            if (isValid)
            {
                var thisUser = _userBusiness.GetUser(id: 0, username: password);
                var viewModelFromEntity = _mapper.Map<UserModel>(thisUser);

                respon = Ok(viewModelFromEntity);
            }
            return respon;
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("post")]
        public IActionResult post([FromBody] Usermaster um)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            IActionResult respon = Unauthorized();
            var res = _userBusiness.Add(um);
            if (res.IsSuccess)
            {
                var viewModelFromEntity = _mapper.Map<UserMasteModel>(res.Data);
                respon = Ok(viewModelFromEntity);
                return Ok(res.Data);
            }
            //string msg = "You have already Added";
            return Content(respon.ToString());
          
        }
        [HttpPut("UserID")]
        [ActionName("Edit")]
        public IActionResult Edit(int UserID, Usermaster um)
        {
            if (UserID == um.UserID)
            {
                var res = _userBusiness.Edit(UserID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{UserID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int UserID)
        {
            var res = _userBusiness.Delete(UserID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }
    }
}