﻿using AssistBLL.Services.General;
using AssistBLL.ViewModel.General;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BusinessHourController : ControllerBase
    {
        private readonly IBusinessHourService _iBusinessHourService;

        public BusinessHourController(IBusinessHourService iBusinessHourService)
        {
            _iBusinessHourService = iBusinessHourService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(BusinessHourModel um)
        {
            var res = _iBusinessHourService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("ID")]
        [ActionName("Edit")]
        public IActionResult Edit(int ID, BusinessHourModel um)
        {
            if (ID == um.ID)
            {
                var res = _iBusinessHourService.Edit(ID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{ID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int ID)
        {
            var res = _iBusinessHourService.Delete(ID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{TypeID}/{SubscriberID}/{ID}/{type}")]
        [ActionName("CheckDuplicateBusinesshour")]
        public IActionResult CheckDuplicateBusinesshour(int TypeID, int? SubscriberID, int ID, string type)
        {
            var res = _iBusinessHourService.CheckDuplicateBusinesshour(TypeID, SubscriberID, ID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{ID}")]
        [ActionName("GetBusinesshourById")]
        public IActionResult GetBusinesshourById(int ID)
        {
            var FYMaster = _iBusinessHourService.GetBusinesshourById(ID);
            if (FYMaster == null)
            {
                return NotFound();
            }
            return Ok(FYMaster);
        }

       

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllBusinesshour")]
        public IEnumerable<BusinessHourModel> GetAllBusinesshour(int SubscriberID)
        {
            var FYMaster = _iBusinessHourService.GetAllBusinesshour(SubscriberID);
            return FYMaster;
        }
    }
}