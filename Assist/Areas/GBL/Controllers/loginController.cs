﻿using System.Linq;
using Assist.Model;
using AssistBLL.Services.Trans;
using Microsoft.AspNetCore.Mvc;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class loginController : ControllerBase
    {
        private readonly IloginServices _iloginServices;

        public loginController(IloginServices iloginServices)
        {
            this._iloginServices = iloginServices;
        }
        [HttpPost]
        [ActionName("Getlogin")]
        public IActionResult Getlogin([FromBody] LogInModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Values.ToArray());
            }
            IActionResult respon = Unauthorized();
            var getlogin = _iloginServices.Getlogin(viewModel.Username, viewModel.Password);
            return Ok(getlogin);
        }
        [HttpGet("{SubscriberID}/{UserID}")]
        [ActionName("GetFYDetails")]
        public IActionResult GetFYDetails(int SubscriberID, int UserID)
        {
            var getFYdetails = _iloginServices.GetFYDetails(SubscriberID, SubscriberID);
            return Ok(getFYdetails);
        }
        [HttpGet("{SubscriberID}/{UserID}")]
        [ActionName("GetPasswordHash")]
        public IActionResult GetPasswordHash(int SubscriberID, int UserID)
        {
            var userPasswordHash = _iloginServices.GetPasswordHash(SubscriberID, SubscriberID);
            return Ok(userPasswordHash);
        }
        [HttpPost]
        [ActionName("ChangePassword")]
        public IActionResult ChangePassword([FromBody] ChangePassword viewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Values.ToArray());
            }
            IActionResult respon = Unauthorized();
            var userPasswordHash = _iloginServices.ChangePassword(viewModel.SubscriberID, viewModel.UserId, viewModel.username, viewModel.UserPasswordHash);
            return Ok(userPasswordHash);
        }
        [HttpGet("{UserID}")]
        [ActionName("GetSubscriber")]
        public IActionResult GetSubscriber(long UserID)
        {
            var deatail = _iloginServices.GetSubscriber(UserID);
            return Ok(deatail);
        }
        [HttpGet("{ProfileID}")]
        [ActionName("GetMProfile")]
        public IActionResult GetMProfile(long ProfileID)
        {
            var deatail = _iloginServices.GetMProfile(ProfileID);
            return Ok(deatail);
        }
        [HttpGet("{ProfileID}")]
        [ActionName("ProfileDetail")]
        public IActionResult ProfileDetail(long ProfileID)
        {
            var deatail = _iloginServices.ProfileDetail(ProfileID);
            return Ok(deatail);
        }
        [HttpGet("{LocaleID}")]
        [ActionName("GetLocaleInfo")]
        public IActionResult GetLocaleInfo(long LocaleID)
        {
            var deatail = _iloginServices.GetLocaleInfo(LocaleID);
            return Ok(deatail);
        }
    }
}