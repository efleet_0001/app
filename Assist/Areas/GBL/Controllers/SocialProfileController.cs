﻿using AssistBLL.Services.Master;
using AssistBLL.Services.UserControl;
using AssistBLL.ViewModel.Master;
using AssistBLL.ViewModel.UserControl;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SocialProfileController : ControllerBase
    {
        private readonly ISocialProfileService _iSocialProfileService;

        public SocialProfileController(ISocialProfileService iSocialProfileService)
        {
            _iSocialProfileService = iSocialProfileService;
        }

        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(SocialProfileModel um)
        {
            var res = _iSocialProfileService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("S_ProfileID")]
        [ActionName("Edit")]
        public IActionResult Edit(int S_ProfileID, SocialProfileModel um)
        {
            if (S_ProfileID == um.SocID)
            {
                var res = _iSocialProfileService.Edit(S_ProfileID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{S_ProfileID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int S_ProfileID)
        {
            var res = _iSocialProfileService.Delete(S_ProfileID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{S_ProfileName}/{SubscriberID}/{S_ProfileID}/{type}")]
        [ActionName("CheckDuplicateUser")]
        public IActionResult CheckDuplicateSProfile(string S_ProfileName, int? SubscriberID, int S_ProfileID, string type)
        {
            var res = _iSocialProfileService.CheckDuplicateSProfile(S_ProfileName, SubscriberID, S_ProfileID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet("{S_ProfileID}")]
        [ActionName("GetSProfileById")]
        public IActionResult GetSProfileById(int S_ProfileID)
        {
            var userMaster = _iSocialProfileService.GetSProfileById(S_ProfileID);
            if (userMaster == null)
            {
                return NotFound();
            }
            return Ok(userMaster);
        }

        [HttpGet("{S_ProfileID}")]
        [ActionName("GetSProfileDetails")]
        public IActionResult GetSProfileDetails(int S_ProfileID)
        {
            var SProfileMaster = _iSocialProfileService.GetSProfileById(S_ProfileID);
            if (SProfileMaster == null)
            {
                return NotFound();
            }
            return Ok(SProfileMaster);
        }

        [HttpGet("{SubscriberID}")]
        [ActionName("GetAllS_Profile")]
        public IEnumerable<SocialProfileModel> GetAllS_Profile(int SubscriberID)
        {
            var SProfileMaster = _iSocialProfileService.GetAllS_Profile(SubscriberID);
            return SProfileMaster;
        }
    }
}