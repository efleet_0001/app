﻿using System.Collections.Generic;
using AssistBLL.Services.UserControl;
using AssistBLL.ViewModel.UserControl;
using AssistBLL.ViewModel.UserControl.Data;
using Microsoft.AspNetCore.Mvc;

namespace Assist.Areas.GBL.Controllers
{
    [Area("GBL")]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserMasterController : ControllerBase
    {
        private readonly IUserMasterService _iUserMasterService;

        public UserMasterController(IUserMasterService iUserMasterService)
        {
            _iUserMasterService = iUserMasterService;
        }
        [HttpPost]
        [ActionName("Add")]
        public IActionResult Post(UserMasteModel um)
        {
            var res = _iUserMasterService.Add(um);
            if (res.IsSuccess)
            {
                return Ok(res.Data);
            }
            string msg = "You have already Added";
            return Content(msg);
        }

        [HttpPut("UserID")]
        [ActionName("Edit")]
        public IActionResult Edit(int UserID, UserMasteModel um)
        {
            if (UserID == um.UserID)
            {
                var res = _iUserMasterService.Edit(UserID, um);
                if (res.IsSuccess)
                {
                    return Ok(res);
                }
                return NotFound();
            }
            return NotFound();
        }

        [HttpDelete("{UserID}")]
        [ActionName("Delete")]
        public IActionResult Delete(int UserID)
        {
            var res = _iUserMasterService.Delete(UserID);
            if (res.IsSuccess)
            {
                return Ok(res);
            }
            return NotFound(res.Errors[0]);
        }

        [HttpGet("{UserName}/{SubscriberID}/{UserID}/{type}")]
        [ActionName("CheckDuplicateUser")]
        public IActionResult CheckDuplicateUser(string UserName, int? SubscriberID, int UserID, string type)
        {
            var res = _iUserMasterService.CheckDuplicateUser(UserName, SubscriberID, UserID, type);
            if (res == null)
            {
                return Ok(true);
            }
            return Ok(false);
        }
        [HttpGet("{UserID}")]
        [ActionName("GetUserById")]
        public IActionResult GetUserById(int UserID)
        {
            var userMaster = _iUserMasterService.GetUserById(UserID);
            if (userMaster == null)
            {
                return NotFound();
            }
            return Ok(userMaster);
        }
        [HttpGet("{UserID}")]
        [ActionName("GetUserDetails")]
        public IActionResult GetUserDetails(int UserID)
        {
            var officeMaster = _iUserMasterService.GetUserDetails(UserID);
            if (officeMaster == null)
            {
                return NotFound();
            }
            return Ok(officeMaster);
        }
        [HttpGet("{SubscriberID}")]
        [ActionName("GetListOfUser")]
        public IEnumerable<UserMasteModelData> GetListOfUser(int SubscriberID)
        {
            var userMaster = _iUserMasterService.GetListOfUser(SubscriberID);
            return userMaster;
        }

    }
}