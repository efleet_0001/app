﻿using System.ComponentModel.DataAnnotations;

namespace Assist.Model
{
    public class LogInModel
    {
        [Required(ErrorMessage = "Username is required")]
        [MinLength(4, ErrorMessage = "Length of username must be >= 4")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
    }
    public class ChangePassword 
    {
        public  long SubscriberID { get; set; }
        public long UserId { get; set; }
        public string username { get; set; }
        public string UserPasswordHash { get; set; }
    }
}