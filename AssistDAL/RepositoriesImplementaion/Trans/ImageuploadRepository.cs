﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Trans;
using AssistDB.GBLMaster.Trans;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.Trans
{
    public class ImageuploadRepository : GenericRepository<ImageUpload>, IImageuploadRepository
    {
        public ImageuploadRepository(ApplicationContext context) : base(context)
        {
        }
        public ImageUpload GetImageById(long TransactionID, int TransactionTypeID)
        {
            var Details = new ImageUpload();
            Details = (from data in Context.imageUploads
                       where data.StatusID != 3 && data.TransctionID == TransactionID && data.ImageTypeID == TransactionTypeID
                       orderby data.CreatedDate descending

                       select new ImageUpload
                       {
                           ImageID = data.ImageID,
                           TransctionID = data.TransctionID,
                           Name = data.Name,
                           ImageTypeID = data.ImageTypeID,
                           Path = data.Path,
                           StatusID = data.StatusID,
                           Discription = data.Discription,
                           CSID = data.CSID,
                           MSID = data.MSID,
                           SubscriberID = data.SubscriberID,
                           CreatedBy = data.CreatedBy,
                           ModifiedBy = data.ModifiedBy,
                           Rowguid = data.Rowguid,
                           CreatedDate = data.CreatedDate,
                           ModifiedDate = data.ModifiedDate

                       }).FirstOrDefault();

            return Details;
        }

        public IEnumerable<ImageUpload> GetAllImage(int SubscriberID)
        {
            var Details = new List<ImageUpload>();
            Details = (from data in Context.imageUploads
                       where data.StatusID != 3 && data.SubscriberID == SubscriberID
                       orderby data.CreatedDate descending

                       select new ImageUpload
                       {
                           ImageID = data.ImageID,
                           TransctionID = data.TransctionID,
                           Name = data.Name,
                           ImageTypeID = data.ImageTypeID,
                           Path = data.Path,
                           StatusID = data.StatusID,
                           Discription = data.Discription,
                           CSID = data.CSID,
                           MSID = data.MSID,
                           SubscriberID = data.SubscriberID,
                           CreatedBy = data.CreatedBy,
                           ModifiedBy = data.ModifiedBy,
                           Rowguid = data.Rowguid,
                           CreatedDate = data.CreatedDate,
                           ModifiedDate = data.ModifiedDate

                       }).ToList();

            return Details;
        }

        public ImageUpload CheckDuplicateImage(string Name, int? SubscriberID, long TransactionID, int TransactypeID, string type)
        {
            var imageUpload = new ImageUpload();
            if (type == "EDIT")
            {
                imageUpload = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.TransctionID == TransactionID && x.ImageTypeID != TransactypeID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                imageUpload = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.TransctionID == TransactionID && x.ImageTypeID == TransactypeID); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return imageUpload;
        }
    }
}
