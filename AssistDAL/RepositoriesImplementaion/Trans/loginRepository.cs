﻿using AssistDAL.RepositoriesInterface.Trans;
using AssistDB.login.DB.login;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.Trans
{
    public class loginRepository : IloginRepository
    {
        protected ApplicationContext _Context;

        public loginRepository(ApplicationContext context)
        {
            _Context = context;
        }

        public IEnumerable<FYModel> GetFYDetails(int SubscriberID, int UserID)
        {
            List<FYModel> lst = _Context.FYModels
            .FromSqlRaw("PROC_BUS_GBL_E_GetFYDetail @SubscriberID,@UserID",
            new SqlParameter("@SubscriberID", SubscriberID),
            new SqlParameter("@UserID", UserID)

            ).ToList();
            return lst;
        }
        public SessionModel CreateSession(SessionModel sessionModel)
        {
            List<SessionModel> lst = _Context.SessionModels
          .FromSqlRaw("PROC_BUS_GBL_I_CreateSession @SessionID,@SubscriberID,@UserID,@StartTimeDate,@EndTimeDate,@IPAddress",
          new SqlParameter("@SessionID", sessionModel.SessionID),
          new SqlParameter("@SubscriberID", sessionModel.SubscriberID),
          new SqlParameter("@UserID", sessionModel.UserID),
          new SqlParameter("@StartTimeDate", sessionModel.StartTimeDate),
          new SqlParameter("@EndTimeDate", sessionModel.EndTimeDate),
           new SqlParameter("@IPAddress", sessionModel.IPAddress)
          ).ToList();
            return lst.FirstOrDefault();
        }
        public string GetPasswordHash(int SubscriberID, int UserID)
        {
            string passwordhash = "";
            var userMaster = _Context.Usermasters.Where(x => x.SubscriberID == SubscriberID && x.UserID == UserID).FirstOrDefault();
            if (userMaster.PasswordHash != null)
            {
                return passwordhash = userMaster.PasswordHash.ToString();
            }
            return passwordhash;
        }
        public string ChangePassword(long SubscriberID, long UserID, string username, string passwordhash)
        {
            string HashPassword = "";
            SessionModel lst = _Context.SessionModels
           .FromSqlRaw("PROC_BUS_GBL_I_CreateSession @SubscriberID,@UserID,@passwordhash",
           new SqlParameter("@SubscriberID", SubscriberID),
           new SqlParameter("@UserID", UserID),
           new SqlParameter("@passwordhash", passwordhash)
                          ).FirstOrDefault();
            return HashPassword = lst.UserID.ToString();
        }
        public loginModel Getlogin(string username, string password)
        {

            //var param = new SqlParameter[] {
            //            new SqlParameter() {
            //                ParameterName = "@LoginID",
            //                SqlDbType =  System.Data.SqlDbType.VarChar,
            //                Size = 100,
            //                Direction = System.Data.ParameterDirection.Input,
            //                Value = username
            //            },
            //            new SqlParameter() {
            //                ParameterName = "@Password",
            //                SqlDbType =  System.Data.SqlDbType.Int,
            //                Direction = System.Data.ParameterDirection.Input,
            //                Value = password
            //            }};
            //var lst = _Context.loginModels.FromSqlRaw("PROC_BUS_GBL_E_ForLogOn @LoginID,@Password", param).FirstOrDefault();
            //return lst;
            var spParams = new object[] { username, password };
            var data = _Context.loginModels.FromSqlRaw("PROC_BUS_GBL_E_ForLogOn {0},{1}", username, password).ToList();
            //var lst = _Context.loginModels
            // .FromSqlRaw("PROC_BUS_GBL_E_ForLogOn @LoginID,@Password",
            // new SqlParameter("@LoginID", username),
            // new SqlParameter("@Password", password)
            // ).Single();
            //return lst;
            return data.FirstOrDefault();

        }
        public Subscriber GetSubscriber(long UserID)
        {
            var lst = _Context.Subscribers
            .FromSqlRaw("PROC_BUS_GBL_E_GetSubscriber {0}", UserID).ToList();
            return lst.FirstOrDefault();
        }
        public MProfile GetMProfile(long ProfileID)
        {
            var lst = _Context.MProfiles
            .FromSqlRaw("PROC_BUS_GBL_E_GetMProfile {0}", ProfileID).ToList();
            return lst.FirstOrDefault();
        }
        public LProfileDetail ProfileDetail(long ProfileID)
        {
            var lst = _Context.LProfileDetails
            .FromSqlRaw("PROC_BUS_GBL_E_GetMProfileDetail {0}", ProfileID).ToList();
            return lst.FirstOrDefault();
        }
        public LocaleInfo GetLocaleInfo(long LocaleID)
        {
            var lst = _Context.LocaleInfos
            .FromSqlRaw("PROC_BUS_GBL_E_GetLocaleInfo {0}", LocaleID).ToList();
            return lst.FirstOrDefault();
        }
    }
}
