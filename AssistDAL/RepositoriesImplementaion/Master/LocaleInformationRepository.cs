﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB.GBLMaster.Master;
using AssistDB.GBLMaster.Master.ViewData;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.Master
{
    public class LocaleInformationRepository : GenericRepository<LocaleInformation>, ILocaleInformationRepository
    {
        public LocaleInformationRepository(ApplicationContext context) : base(context)
        {
        }
        public LocaleInformationData GetLocaleInfoById(int LocaleInfoID)
        {
            var LocaleInfoDetails = new LocaleInformationData();
            LocaleInfoDetails = (from Data in Context.LocaleInformations
                                 let Language = Data.Language
                                 let Country = Data.Country
                                 let State = Data.State
                                 let District = Data.District
                                 where Data.LocID == LocaleInfoID
                                 select new LocaleInformationData
                                 {
                                     LocID = Data.LocID,
                                     LanguageID = Data.LanguageID,
                                     CountryID = Data.CountryID,
                                     StateID = Data.StateID,
                                     DistrictID = Data.DistrictID,
                                     PinCode = Data.PinCode,
                                     VillMName = Data.VillMName,
                                     Latlong = Data.Latlong,
                                     TimeFormat = Data.TimeFormat,
                                     Time_Zone = Data.Time_Zone,
                                     Currency = Data.Currency,
                                     Signature = Data.Signature,
                                     LanguageName = Data.Language != null ? Language.Name : string.Empty,
                                     CountryName = Data.Country != null ? Country.CountryName : string.Empty,
                                     StateName = Data.State != null ? State.StateName : string.Empty,
                                     DistrictName = Data.District != null ? District.DistrictName : string.Empty

                                 }).FirstOrDefault();

            return LocaleInfoDetails;
        }
        public LocaleInformation CheckDuplicateLocaleInfo(string LocaleInfoName, int? SubscriberID, int LocaleInfoID, string type)
        {
            var LocaleInfoDetails = new LocaleInformation();
            if (type == "EDIT")
            {
                LocaleInfoDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.LocID != LocaleInfoID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                LocaleInfoDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return LocaleInfoDetails;
        }
        public IEnumerable<LocaleInformationData> GetAll(int SubscriberID)
        {
            var LocaleInfoDetails = new List<LocaleInformationData>();
            LocaleInfoDetails = (from Data in Context.LocaleInformations
                                 let Language = Data.Language
                                 let Country = Data.Country
                                 let State = Data.State
                                 let District = Data.District
                                 where Data.SubscriberID == SubscriberID
                                 select new LocaleInformationData
                                 {
                                     LocID = Data.LocID,
                                     LanguageID = Data.LanguageID,
                                     CountryID = Data.CountryID,
                                     StateID = Data.StateID,
                                     DistrictID = Data.DistrictID,
                                     PinCode = Data.PinCode,
                                     VillMName = Data.VillMName,
                                     Latlong = Data.Latlong,
                                     TimeFormat = Data.TimeFormat,
                                     Time_Zone = Data.Time_Zone,
                                     Currency = Data.Currency,
                                     Signature = Data.Signature,
                                     LanguageName = Data.Language != null ? Language.Name : string.Empty,
                                     CountryName = Data.Country != null ? Country.CountryName : string.Empty,
                                     StateName = Data.State != null ? State.StateName : string.Empty,
                                     DistrictName = Data.District != null ? District.DistrictName : string.Empty

                                 }).ToList();

            return LocaleInfoDetails;
        }

    }
}
