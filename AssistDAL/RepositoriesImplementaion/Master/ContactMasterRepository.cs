﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB.GBLMaster.Master;
using AssistDB.GBLMaster.Master.ViewData;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.Master
{
    public class ContactMasterRepository : GenericRepository<Contact>, IContactMasterRepository
    {
        public ContactMasterRepository(ApplicationContext context) : base(context)
        {
        }
        public ContactData GetContactById(int ContactID)
        {
            var ContactDetails = new ContactData();
            ContactDetails = (from Data in Context.Contacts
                              let District = Data.District
                              let State = Data.State
                              let Country = Data.Country
                              where Data.ConID == ContactID
                              select new ContactData
                              {
                                  ConID = Data.ConID,
                                  Street = Data.Street,
                                  City = Data.City,
                                  DistrictID = Data.DistrictID,
                                  StateID = Data.StateID,
                                  CountryID = Data.CountryID,
                                  Zip_Code = Data.Zip_Code,
                                  Description = Data.Description,
                                  DistrictName = District != null ? District.DistrictName : string.Empty,
                                  StateName = State != null ? State.StateName : string.Empty,
                                  CountryName = Country != null ? Country.CountryName : string.Empty
                              }).FirstOrDefault();

            return ContactDetails;
        }
        public Contact CheckDuplicateContact(string ContactName, int? SubscriberID, int ContactID, string type)
        {
            var ContactDetails = new Contact();
            if (type == "EDIT")
            {
                ContactDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.City == ContactName && x.ConID != ContactID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                ContactDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.City == ContactName && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return ContactDetails;
        }
        public IEnumerable<ContactData> GetAllContact(int SubscriberID)
        {
            var ContactDetails = new List<ContactData>();
            ContactDetails = (from Data in Context.Contacts
                              let District = Data.District
                              let State = Data.State
                              let Country = Data.Country
                              where Data.SubscriberID == SubscriberID
                              select new ContactData
                              {
                                  ConID = Data.ConID,
                                  Street = Data.Street,
                                  City = Data.City,
                                  DistrictID = Data.DistrictID,
                                  StateID = Data.StateID,
                                  CountryID = Data.CountryID,
                                  Zip_Code = Data.Zip_Code,
                                  Description = Data.Description,
                                  DistrictName = District != null ? District.DistrictName : string.Empty,
                                  StateName = State != null ? State.StateName : string.Empty,
                                  CountryName = Country != null ? Country.CountryName : string.Empty
                              }).ToList();

            return ContactDetails;
        }

    }
}
