﻿using AssistDAL.RepositoriesInterface.Master;
using AssistDB.BaseMaster;
using AssistDB.GBLMaster.Master;
using AssistDB.General;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.Master
{
    public class StaticRepository : IStaticRepository
    {
        private ApplicationContext _context;
        public StaticRepository(ApplicationContext context)
        {
            _context = context;
        }
        public IEnumerable<Application> GetAllApplication(int SubscriberID)
        {
            var ConstantDetails = new List<Application>();
            ConstantDetails = (from Data in _context.Applications
                               where Data.SubscriberID == SubscriberID && Data.StatusID == 1
                               select new Application
                               {
                                   ApplicationId = Data.ApplicationId,
                                   AppTypeId = Data.AppTypeId,
                                   AppNatureId = Data.AppNatureId,
                                   AppName = Data.AppName,
                                   Alias = Data.Alias,
                                   Description = Data.Description

                               }).ToList();

            return ConstantDetails;
        }
        public IEnumerable<Client> GetAllClient(int SubscriberID)
        {
            var ConstantDetails = new List<Client>();
            ConstantDetails = (from Data in _context.Clients
                               where Data.SubscriberID == SubscriberID && Data.StatusID == 1
                               select new Client
                               {
                                   ClientID = Data.ClientID,
                                   Name = Data.Name,
                                   Code = Data.Code,
                                   Description = Data.Description

                               }).ToList();

            return ConstantDetails;
        }
        public IEnumerable<Constant> GetAllConstant(int SubscriberID, int ConstantGroupID)
        {
            var ConstantDetails = new List<Constant>();
            ConstantDetails = (from Data in _context.Constants
                               where Data.ConsGroupID == ConstantGroupID
                               select new Constant
                               {
                                   ConstantID = Data.ConstantID,
                                   ConsGroupID = Data.ConsGroupID,
                                   Name = Data.Name,
                                   Alias = Data.Alias

                               }).ToList();

            return ConstantDetails;
        }
        public IEnumerable<Country> GetAllCountry(int SubscriberID)
        {
            var CountryDetails = new List<Country>();
            CountryDetails = (from Data in _context.Countrys
                              where Data.StatusID == true
                              select new Country
                              {
                                  ContryID = Data.ContryID,
                                  CountryName = Data.CountryName,
                                  Description = Data.Description

                              }).ToList();

            return CountryDetails;
        }
        public IEnumerable<State> GetAllState(int SubscriberID, int CountryID)
        {
            var StateDetails = new List<State>();
            StateDetails = (from Data in _context.States
                            where Data.Status == true && Data.CoutryID == CountryID
                            select new State
                            {
                                StateID = Data.StateID,
                                StateName = Data.StateName,
                                Statehood = Data.Statehood,
                                Code = Data.Code,
                                Capital = Data.Capital,
                                LargestCity = Data.LargestCity,
                                Population = Data.Population,
                                Area = Data.Area,
                                Languge = Data.Languge,
                                Discription = Data.Discription
                            }).ToList();

            return StateDetails;
        }
        public IEnumerable<District> GetAllDistrict(int SubscriberID, int StateID)
        {
            var DistrictDetails = new List<District>();
            DistrictDetails = (from Data in _context.Districts
                               where Data.Status == 1 && Data.StateID == StateID
                               select new District
                               {
                                   DistrictID = Data.DistrictID,
                                   DistrictName = Data.DistrictName,
                                   Districthood = Data.Districthood,
                                   Code = Data.Code,
                                   Capital = Data.Capital,
                                   LargestCity = Data.LargestCity,
                                   Population = Data.Population,
                                   Area = Data.Area,
                                   Languge = Data.Languge,
                                   Discription = Data.Discription
                               }).ToList();

            return DistrictDetails;
        }
        public IEnumerable<Language> GetAllLanguage(int SubscriberID)
        {
            var LanguageDetails = new List<Language>();
            LanguageDetails = (from Data in _context.Languages
                               where Data.StatusID == true
                               select new Language
                               {
                                   LanID = Data.LanID,
                                   Culture = Data.Culture,
                                   Name = Data.Name

                               }).ToList();

            return LanguageDetails;
        }
        public IEnumerable<Holiday> GetAllHolidays(int DistrictID)
        {
            var HolidayDetails = new List<Holiday>();
            HolidayDetails = (from Data in _context.Holidays
                              where Data.StatusID == 1
                              select new Holiday
                              {
                                  HoliDayID = Data.HoliDayID,
                                  HolidayName = Data.HolidayName,
                                  HolidayDate = Data.HolidayDate

                              }).ToList();

            return HolidayDetails;
        }
    }
}
