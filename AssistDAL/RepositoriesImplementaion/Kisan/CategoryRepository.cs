﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB.Kisan.Master;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.Kisan
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationContext context) : base(context)
        {
        }
        public Category GetCategoryById(int CategoryID)
        {
            var CategoryDetails = new Category();
            CategoryDetails = (from Data in Context.Categorys
                               where Data.CatID == CategoryID
                               select new Category
                               {
                                   CatID = Data.CatID,
                                   Name = Data.Name,
                                   Alias = Data.Alias,
                                   Description = Data.Description,
                                   SubCategories = Data.SubCategories.Where(x => x.StatusID == 1).ToList(),
                               }).FirstOrDefault();

            return CategoryDetails;
        }
        public Category CheckDuplicateCategory(string CategoryName, int? SubscriberID, int CategoryID, string type)
        {
            var CategoryDetails = new Category();
            if (type == "EDIT")
            {
                CategoryDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Name == CategoryName && x.CatID != CategoryID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                CategoryDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Name == CategoryName && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return CategoryDetails;
        }
        public IEnumerable<Category> GetAllCategory(int SubscriberID)
        {
            var CategoryDetails = new List<Category>();
            CategoryDetails = (from Data in Context.Categorys
                               where Data.SubscriberID == SubscriberID
                               select new Category
                               {
                                   CatID = Data.CatID,
                                   Name = Data.Name,
                                   Alias = Data.Alias,
                                   Description = Data.Description,
                                   SubCategories = Data.SubCategories.Where(x => x.StatusID == 1).ToList(),
                               }).ToList();

            return CategoryDetails;
        }

    }
}
