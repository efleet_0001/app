﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistDAL.RepositoriesInterface.Trans;
using AssistDB.Kisan.Trans;
using AssistDB.Kisan.Trans.Data;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.Kisan
{
    public class SolutionRepository : GenericRepository<Solution>, ISolutionRepository
    {
        public SolutionRepository(ApplicationContext context) : base(context)
        {
        }
        public SolutionData GetSolutionById(int SolnID)
        {
            var Details = new SolutionData();
            Details = (from data in Context.Solutions
                       let problemMaster = data.problemMaster
                       where data.StatusID != 3 && data.SolnID == SolnID
                       orderby data.CreatedDate descending

                       select new SolutionData
                       {
                           SolnID = data.SolnID,
                           ProblemID = data.ProblemID,
                           SolutionTag = data.SolutionTag,
                           Description = data.Description,
                           ProblemName = problemMaster != null ? problemMaster.ProblemName : string.Empty,
                           StatusID = data.StatusID,
                           CSID = data.CSID,
                           MSID = data.MSID,
                           SubscriberID = data.SubscriberID,
                           CreatedBy = data.CreatedBy,
                           ModifiedBy = data.ModifiedBy,
                           Rowguid = data.Rowguid,
                           CreatedDate = data.CreatedDate,
                           ModifiedDate = data.ModifiedDate

                       }).FirstOrDefault();

            return Details;
        }

        public IEnumerable<SolutionData> GetAllSolution(int SubscriberID)
        {
            var Details = new List<SolutionData>();
            Details = (from data in Context.Solutions
                       let problemMaster = data.problemMaster
                       where data.StatusID != 3 && data.SubscriberID == SubscriberID
                       orderby data.CreatedDate descending

                       select new SolutionData
                       {
                           SolnID = data.SolnID,
                           ProblemID = data.ProblemID,
                           SolutionTag = data.SolutionTag,
                           Description = data.Description,
                           ProblemName = problemMaster != null ? problemMaster.ProblemName : string.Empty,
                           StatusID = data.StatusID,
                           CSID = data.CSID,
                           MSID = data.MSID,
                           SubscriberID = data.SubscriberID,
                           CreatedBy = data.CreatedBy,
                           ModifiedBy = data.ModifiedBy,
                           Rowguid = data.Rowguid,
                           CreatedDate = data.CreatedDate,
                           ModifiedDate = data.ModifiedDate

                       }).ToList();

            return Details;
        }

        public Solution CheckDuplicateSolution(string SolutionTag, int? SubscriberID, int SolnID, string type)
        {
            var usermaster = new Solution();
            if (type == "EDIT")
            {
                usermaster = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.SolutionTag == SolutionTag && x.SolnID != SolnID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                usermaster = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.SolutionTag == SolutionTag); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return usermaster;
        }
    }
}
