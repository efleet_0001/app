﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistDAL.RepositoriesInterface.Trans;
using AssistDB.Kisan.Trans;
using AssistDB.Kisan.Trans.Data;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.Kisan
{
    public class NotificationRepository : GenericRepository<Notification>, INotificationRepository
    {
        protected ApplicationContext _Context;

        public NotificationRepository(ApplicationContext context) : base(context)
        {
        }
        public IEnumerable<NotificationData> GetAppNotification(int MemberID, int subscriberID)
        {
            var data = new List<NotificationData>();
            data = (from Data in Context.Notifications
                    let NotificationType = Data.NotificationType
                    let NotificationSourceTypes = Data.NotificationSourceTypes
                    where Data.MemberID == MemberID && Data.SubscriberID == subscriberID && Data.StatusID == 1 && Data.Type == 19 && Data.IsRead == false
                    orderby Data.NotificationID descending
                    select new NotificationData
                    {
                        NotificationID = Data.NotificationID,
                        Headline = Data.Headline,
                        Type = Data.Type,
                        Notificationtext = Data.Notificationtext,
                        Imagepath = Data.Imagepath,
                        IsRead = Data.IsRead,
                        NotificationSourceType = Data.NotificationSourceType,
                        MemberID = Data.MemberID,
                        Typename = NotificationType.Name != null ? NotificationType.Name : string.Empty,
                        NotificationSourceTypename = Data.NotificationSourceTypes != null ? NotificationSourceTypes.Name : string.Empty

                    }).ToList();

            return data;
        }
        public IEnumerable<NotificationData> GetAllAppNotification(int MemberID, int subscriberID)
        {
            var data = new List<NotificationData>();
            data = (from Data in Context.Notifications
                    let NotificationType = Data.NotificationType
                    let NotificationSourceTypes = Data.NotificationSourceTypes
                    where Data.MemberID == MemberID && Data.SubscriberID == subscriberID && Data.StatusID == 1 && Data.Type == 19
                    orderby Data.NotificationID descending
                    select new NotificationData
                    {
                        NotificationID = Data.NotificationID,
                        Headline = Data.Headline,
                        Type = Data.Type,
                        Notificationtext = Data.Notificationtext,
                        Imagepath = Data.Imagepath,
                        IsRead = Data.IsRead,
                        NotificationSourceType = Data.NotificationSourceType,
                        MemberID = Data.MemberID,
                        Typename = NotificationType.Name != null ? NotificationType.Name : string.Empty,
                        NotificationSourceTypename = Data.NotificationSourceTypes != null ? NotificationSourceTypes.Name : string.Empty

                    }).ToList();

            return data;
        }
        public IEnumerable<NotificationData> GetSMSNotification(int MemberID, int subscriberID)
        {
            var data = new List<NotificationData>();
            data = (from Data in Context.Notifications
                    let NotificationType = Data.NotificationType
                    let NotificationSourceTypes = Data.NotificationSourceTypes
                    where Data.MemberID == MemberID && Data.SubscriberID == subscriberID && Data.StatusID == 1 && Data.IsRead == false && Data.Type == 20
                    orderby Data.NotificationID descending
                    select new NotificationData
                    {
                        NotificationID = Data.NotificationID,
                        Headline = Data.Headline,
                        Type = Data.Type,
                        Notificationtext = Data.Notificationtext,
                        Imagepath = Data.Imagepath,
                        IsRead = Data.IsRead,
                        NotificationSourceType = Data.NotificationSourceType,
                        MemberID = Data.MemberID,
                        Typename = NotificationType.Name != null ? NotificationType.Name : string.Empty,
                        NotificationSourceTypename = Data.NotificationSourceTypes != null ? NotificationSourceTypes.Name : string.Empty

                    }).ToList();

            return data;
        }
        public IEnumerable<NotificationData> GetAllSMSNotification(int MemberID, int subscriberID)
        {
            var data = new List<NotificationData>();
            data = (from Data in Context.Notifications
                    let NotificationType = Data.NotificationType
                    let NotificationSourceTypes = Data.NotificationSourceTypes
                    where Data.MemberID == MemberID && Data.SubscriberID == subscriberID && Data.StatusID == 1 && Data.Type == 20
                    orderby Data.NotificationID descending
                    select new NotificationData
                    {
                        NotificationID = Data.NotificationID,
                        Headline = Data.Headline,
                        Type = Data.Type,
                        Notificationtext = Data.Notificationtext,
                        Imagepath = Data.Imagepath,
                        IsRead = Data.IsRead,
                        NotificationSourceType = Data.NotificationSourceType,
                        MemberID = Data.MemberID,
                        Typename = NotificationType.Name != null ? NotificationType.Name : string.Empty,
                        NotificationSourceTypename = Data.NotificationSourceTypes != null ? NotificationSourceTypes.Name : string.Empty

                    }).ToList();

            return data;
        }
        public IEnumerable<NotificationData> GetWhatsAppNotification(int MemberID, int subscriberID)
        {
            var data = new List<NotificationData>();
            data = (from Data in Context.Notifications
                    let NotificationType = Data.NotificationType
                    let NotificationSourceTypes = Data.NotificationSourceTypes
                    where Data.MemberID == MemberID && Data.SubscriberID == subscriberID && Data.StatusID == 1 && Data.IsRead == false && Data.Type == 27
                    orderby Data.NotificationID descending
                    select new NotificationData
                    {
                        NotificationID = Data.NotificationID,
                        Headline = Data.Headline,
                        Type = Data.Type,
                        Notificationtext = Data.Notificationtext,
                        Imagepath = Data.Imagepath,
                        IsRead = Data.IsRead,
                        NotificationSourceType = Data.NotificationSourceType,
                        MemberID = Data.MemberID,
                        Typename = NotificationType.Name != null ? NotificationType.Name : string.Empty,
                        NotificationSourceTypename = Data.NotificationSourceTypes != null ? NotificationSourceTypes.Name : string.Empty

                    }).ToList();

            return data;
        }
        public IEnumerable<NotificationData> GetAllWhatsAppNotification(int MemberID, int subscriberID)
        {
            var data = new List<NotificationData>();
            data = (from Data in Context.Notifications
                    let NotificationType = Data.NotificationType
                    let NotificationSourceTypes = Data.NotificationSourceTypes
                    where Data.MemberID == MemberID && Data.SubscriberID == subscriberID && Data.StatusID == 1 && Data.Type == 27
                    orderby Data.NotificationID descending
                    select new NotificationData
                    {
                        NotificationID = Data.NotificationID,
                        Headline = Data.Headline,
                        Type = Data.Type,
                        Notificationtext = Data.Notificationtext,
                        Imagepath = Data.Imagepath,
                        IsRead = Data.IsRead,
                        NotificationSourceType = Data.NotificationSourceType,
                        MemberID = Data.MemberID,
                        Typename = NotificationType.Name != null ? NotificationType.Name : string.Empty,
                        NotificationSourceTypename = Data.NotificationSourceTypes != null ? NotificationSourceTypes.Name : string.Empty

                    }).ToList();

            return data;
        }
    }
}
