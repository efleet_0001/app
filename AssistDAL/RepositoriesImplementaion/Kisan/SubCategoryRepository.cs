﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB.Kisan.Master;
using AssistDB.Kisan.Master.Data;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.Kisan
{
    public class SubCategoryRepository : GenericRepository<SubCategory>, ISubCategoryRepository
    {
        public SubCategoryRepository(ApplicationContext context) : base(context)
        {
        }
        public SubCategoryData GetSubCatById(int SubCatID)
        {
            var subcatDetails = new SubCategoryData();
            subcatDetails = (from Data in Context.SubCategorys
                             let Cate = Data.Cate
                             where Data.Subcat_ID == SubCatID
                             select new SubCategoryData
                             {
                                 Subcat_ID = Data.Subcat_ID,
                                 CateID = Data.CateID,
                                 Name = Data.Name,
                                 Alias = Data.Alias,
                                 Description = Data.Description,
                                 CategoryName = Cate != null ? Cate.Alias : string.Empty

                             }).FirstOrDefault();

            return subcatDetails;
        }
        public SubCategory CheckDuplicateSubCat(string SubCatName, int? SubscriberID, int SubCatID, string type)
        {
            var subcatDetails = new SubCategory();
            if (type == "EDIT")
            {
                subcatDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Name == SubCatName && x.Subcat_ID != SubCatID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                subcatDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Name == SubCatName && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return subcatDetails;
        }
        public IEnumerable<SubCategoryData> GetAllSubCat(int SubscriberID)
        {
            var subcatDetails = new List<SubCategoryData>();
            subcatDetails = (from Data in Context.SubCategorys
                             let Cate = Data.Cate
                             where Data.SubscriberID == SubscriberID
                             select new SubCategoryData
                             {
                                 Subcat_ID = Data.Subcat_ID,
                                 CateID = Data.CateID,
                                 Name = Data.Name,
                                 Alias = Data.Alias,
                                 Description = Data.Description,
                                 CategoryName = Cate != null ? Cate.Alias : string.Empty

                             }).ToList();

            return subcatDetails;
        }


    }
}
