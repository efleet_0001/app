﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistDB.Kisan.Trans;
using AssistDB.Kisan.Trans.Data;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.Kisan
{
    public class NewsCategoryRepository : GenericRepository<NewsCategory>, INewsCategoryRepository
    {
        public NewsCategoryRepository(ApplicationContext context) : base(context)
        {
        }
        public NewsCategoryData GetNewsCatById(int NewsCatID)
        {
            var NewsCategoryDetails = new NewsCategoryData();
            NewsCategoryDetails = (from Data in Context.NewsCategorys
                                   let Category = Data.Category
                                   where Data.NewsCatID == NewsCatID

                                   select new NewsCategoryData
                                   {
                                       NewsCatID = Data.NewsCatID,
                                       NewsCatName = Data.NewsCatName,
                                       NewsDiscription = Data.NewsDiscription,
                                       CategoryID = Data.CategoryID,
                                       NewsTypeName = Data.Category != null ? Category.Name : string.Empty

                                   }).FirstOrDefault();

            return NewsCategoryDetails;
        }
        public NewsCategory CheckDuplicateNewsCategory(string NewsCatName, int? SubscriberID, int NewsCatID, string type)
        {
            var NewsCategoryDataDetails = new NewsCategory();
            if (type == "EDIT")
            {
                NewsCategoryDataDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.NewsCatName == NewsCatName && x.NewsCatID != NewsCatID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                NewsCategoryDataDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.NewsCatName == NewsCatName && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return NewsCategoryDataDetails;
        }
        public IEnumerable<NewsCategoryData> GetAllNewsCat(int SubscriberID)
        {
            var NewsCategoryDetails = new List<NewsCategoryData>();
            NewsCategoryDetails = (from Data in Context.NewsCategorys
                                   let Category = Data.Category
                                   where Data.SubscriberID == SubscriberID

                                   select new NewsCategoryData
                                   {
                                       NewsCatID = Data.NewsCatID,
                                       NewsCatName = Data.NewsCatName,
                                       NewsDiscription = Data.NewsDiscription,
                                       CategoryID = Data.CategoryID,
                                       NewsTypeName = Data.Category != null ? Category.Name : string.Empty

                                   }).ToList();

            return NewsCategoryDetails;
        }

    }
}
