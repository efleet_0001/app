﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistDB.Kisan.Trans;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.Kisan
{
    public class AnswerRepository : GenericRepository<Answer>, IAnswerRepository
    {
        public AnswerRepository(ApplicationContext context) : base(context)
        {
        }
        public Answer GetAnswerById(int AnswerID)
        {
            var AnswerDetails = new Answer();
            AnswerDetails = (from Data in Context.Answers
                             where Data.AnsID == AnswerID
                             select new Answer
                             {
                                 AnsID = Data.AnsID,
                                 AnswerText = Data.AnswerText,
                                 Anslike = Data.Anslike,
                                 Ansdislike = Data.Ansdislike,
                                 AnsComments = Data.AnsComments,
                                 Description = Data.Description,

                             }).FirstOrDefault();

            return AnswerDetails;
        }
        public Answer CheckDuplicateAnswer(string AnswerName, int? SubscriberID, int AnswerID, string type)
        {
            var AnswerDetails = new Answer();
            if (type == "EDIT")
            {
                AnswerDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.AnswerText == AnswerName && x.AnsID != AnswerID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                AnswerDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.AnswerText == AnswerName && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return AnswerDetails;
        }
        public IEnumerable<Answer> GetAllAnswer(int SubscriberID)
        {
            var AnswerDetails = new List<Answer>();
            AnswerDetails = (from Data in Context.Answers
                             where Data.SubscriberID == SubscriberID
                             select new Answer
                             {
                                 AnsID = Data.AnsID,
                                 AnswerText = Data.AnswerText,
                                 Anslike = Data.Anslike,
                                 Ansdislike = Data.Ansdislike,
                                 AnsComments = Data.AnsComments,
                                 Description = Data.Description,

                             }).ToList();

            return AnswerDetails;
        }

    }
}
