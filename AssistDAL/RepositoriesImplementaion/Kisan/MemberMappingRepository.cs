﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB.Kisan.Trans;
using AssistDB.UserControl;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.Kisan
{
    public class MemberMappingRepository : GenericRepository<MemberMapping>, IMemberMappingRepository
    {
        public MemberMappingRepository(ApplicationContext context) : base(context)
        {
        }
        public MemberMapping GetMemberMappingById(int MemberID)
        {
            var details = new MemberMapping();
            details = (from Data in Context.MemberMappings
                       where Data.MemberId  == MemberID && Data.StatusID == 1
                       select new MemberMapping
                       {
                           MappingID = Data.MappingID,
                           MemberTypeId = Data.MemberTypeId,
                           MemberId = Data.MemberId,
                           CategoryId= Data.CategoryId,
                           SubCategoryId= Data.SubCategoryId,
                           EntityId= Data.EntityId,
                           Description= Data.Description
                       }).FirstOrDefault();

            return details;
        }
        public MemberMapping CheckDuplicateMemberMapping(long MemberID, int? SubscriberID, long MappingID, string type)
        {
            var details = new MemberMapping();
            if (type == "EDIT")
            {
                details = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.MemberId == MemberID && x.MappingID != MappingID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                details = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.MemberId == MemberID && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return details;
        }
        public IEnumerable<MemberMapping> GetAllMemberMapping(int SubscriberID)
        {
            var details = new List<MemberMapping>();
            details = (from Data in Context.MemberMappings
                       where Data.SubscriberID == SubscriberID && Data.StatusID == 1
                       select new MemberMapping
                       {
                           MappingID = Data.MappingID,
                           MemberTypeId = Data.MemberTypeId,
                           MemberId = Data.MemberId,
                           CategoryId = Data.CategoryId,
                           SubCategoryId = Data.SubCategoryId,
                           EntityId = Data.EntityId,
                           Description = Data.Description
                       }).ToList();

            return details;
        }


    }
}
