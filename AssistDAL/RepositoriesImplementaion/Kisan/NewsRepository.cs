﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistDB.Kisan.Trans;
using AssistDB.Kisan.Trans.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.Kisan
{
    public class NewsRepository : GenericRepository<News>, INewsRepository
    {
        public NewsRepository(ApplicationContext context) : base(context)
        {
        }
        public NewsData GetNewsById(int NewsID)
        {
            var NewsDetails = new NewsData();
            NewsDetails = (from Data in Context.Newss
                           let Cat = Data.Cat
                           let SorceType = Data.SorceType
                           where Data.NewsID == NewsID && Data.Approved == true
                           select new NewsData
                           {
                               NewsID = Data.NewsID,
                               CatID = Data.CatID,
                               SorceTypeID = Data.SorceTypeID,
                               Date = Data.Date,
                               SourceInfoName = Data.SourceInfoName,
                               Headline = Data.Headline,
                               MediaSourceTypeID = Data.MediaSourceTypeID,
                               CategoryName = Data.Cat != null ? Cat.NewsCatName : string.Empty,
                               SourceTypeName = Data.SorceType != null ? SorceType.Name : string.Empty

                           }).FirstOrDefault();

            return NewsDetails;
        }
        public News CheckDuplicateAnswer(string NewsName, int? SubscriberID, int NewsID, string type)
        {
            var NewsDetails = new News();
            if (type == "EDIT")
            {
                NewsDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Headline == NewsName && x.NewsID != NewsID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                NewsDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Headline == NewsName && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return NewsDetails;
        }
        public IEnumerable<NewsData> GetAllNews(int SubscriberID)
        {
            var NewsDetails = new List<NewsData>();
            NewsDetails = (from Data in Context.Newss
                           let Cat = Data.Cat
                           let SorceType = Data.SorceType
                           where Data.SubscriberID == SubscriberID && Data.Approved == true
                           select new NewsData
                           {
                               NewsID = Data.NewsID,
                               CatID = Data.CatID,
                               SorceTypeID = Data.SorceTypeID,
                               Date = Data.Date,
                               SourceInfoName = Data.SourceInfoName,
                               Headline = Data.Headline,
                               MediaSourceTypeID = Data.MediaSourceTypeID,
                               CategoryName = Data.Cat != null ? Cat.NewsCatName : string.Empty,
                               SourceTypeName = Data.SorceType != null ? SorceType.Name : string.Empty

                           }).ToList();

            return NewsDetails;
        }
        public IEnumerable<NewCategoryData> GetCateGory(int SubscriberID, int LanguageID, long UserID, int EntityID)
        {
            var data = Context.NewCategoryDatas
             .FromSqlRaw("PROC_BUS_GBL_E_GetEntityNews {0},{1},{2}", SubscriberID, LanguageID, EntityID).ToList();
            return data;
        }
        public IEnumerable<EntityNewData> GetEntityNew(long CatID, int SubscriberID, int LanguageID, long UserID, int EntityID)
        {
            var data = Context.EntityNewDatas
            .FromSqlRaw("PROC_BUS_GBL_E_GetEntityNewSub {0},{1},{2},{3},{4}", CatID, SubscriberID, LanguageID, UserID, EntityID).ToList();
            return data;
        }

    }
}
