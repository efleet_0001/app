﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.General;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB.GBLMaster.Master;
using AssistDB.General;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.General
{
    public class HolidayMasterRepository : GenericRepository<Holiday>, IHolidayMasterRepository
    {
        public HolidayMasterRepository(ApplicationContext context) : base(context)
        {
        }
        public Holiday GetHolidayById(int HolidayID)
        {
            var HolidayDetails = new Holiday();
            HolidayDetails = (from Data in Context.Holidays
                              where Data.HoliDayID == HolidayID
                              select new Holiday
                              {
                                  HoliDayID = Data.HoliDayID,
                                  HolidayName = Data.HolidayName,
                                  HolidayDate = Data.HolidayDate


                              }).FirstOrDefault();

            return HolidayDetails;
        }
        public Holiday CheckDuplicateHoliday(string HolidayName, int? SubscriberID, int HolidayID, string type)
        {
            var HolidayDetails = new Holiday();
            if (type == "EDIT")
            {
                HolidayDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.HolidayName == HolidayName && x.HoliDayID != HolidayID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                HolidayDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.HolidayName == HolidayName && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return HolidayDetails;
        }
        public IEnumerable<Holiday> GetAllHoliday(int SubscriberID)
        {
            var HolidayDetails = new List<Holiday>();
            HolidayDetails = (from Data in Context.Holidays
                              where Data.SubscriberID == SubscriberID
                              select new Holiday
                              {
                                  HoliDayID = Data.HoliDayID,
                                  HolidayName = Data.HolidayName,
                                  HolidayDate = Data.HolidayDate


                              }).ToList();

            return HolidayDetails;
        }

    }
}
