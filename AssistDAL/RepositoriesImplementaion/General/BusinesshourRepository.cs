﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.General;
using AssistDB.General;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.General
{
    public class BusinesshourRepository : GenericRepository<BusinessHour>, IBusinesshourRepository
    {
        public BusinesshourRepository(ApplicationContext context) : base(context)
        {
        }
        public BusinessHour GetBusinesshourById(int ID)
        {
            var BusinessHourDetails = new BusinessHour();
            BusinessHourDetails = (from Data in Context.BusinessHours
                                 where Data.ID == ID
                                 select new BusinessHour
                                 {
                                     TypeID = Data.TypeID,
                                     DayID = Data.DayID,
                                     StartDayID = Data.StartDayID,
                                     StartTime=Data.StartTime,
                                     EndTime=Data.EndTime,
                                     Description=Data.Description


                                 }).FirstOrDefault();

            return BusinessHourDetails;
        }
        public BusinessHour CheckDuplicateBusinesshour(int TypeID, int? SubscriberID, int ID, string type)
        {
            var BusinessHour = new BusinessHour();
            if (type == "EDIT")
            {
                BusinessHour = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.ID != ID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                BusinessHour = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return BusinessHour;
        }
        public IEnumerable<BusinessHour> GetAllBusinesshour(int SubscriberID)
        {
            var BusinessHourDetails = new List<BusinessHour>();
            BusinessHourDetails = (from Data in Context.BusinessHours
                                 where Data.SubscriberID == SubscriberID
                                 select new BusinessHour
                                 {
                                     TypeID = Data.TypeID,
                                     DayID = Data.DayID,
                                     StartDayID = Data.StartDayID,
                                     StartTime = Data.StartTime,
                                     EndTime = Data.EndTime,
                                     Description = Data.Description


                                 }).ToList();

            return BusinessHourDetails;
        }

    }
}
