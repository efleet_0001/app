﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.General;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB.GBLMaster.Master;
using AssistDB.General;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.General
{
    public class FiscalMasterRepository : GenericRepository<FiscalYear>, IFiscalMasterRepository
    {
        public FiscalMasterRepository(ApplicationContext context) : base(context)
        {
        }
        public FiscalYear GetFiscalById(int FYID)
        {
            var FiscalYearDetails = new FiscalYear();
            FiscalYearDetails = (from Data in Context.FiscalYears
                                 where Data.FYID == FYID
                                 select new FiscalYear
                                 {
                                     FYID = Data.FYID,
                                     StartDate = Data.StartDate,
                                     EndDate = Data.EndDate


                                 }).FirstOrDefault();

            return FiscalYearDetails;
        }
        public FiscalYear CheckDuplicateFiscal(string FiscalName, int? SubscriberID, int FiscalID, string type)
        {
            var FiscalYear = new FiscalYear();
            if (type == "EDIT")
            {
                FiscalYear = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.FYID != FiscalID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                FiscalYear = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return FiscalYear;
        }
        public IEnumerable<FiscalYear> GetAllFiscal(int SubscriberID)
        {
            var FiscalYearDetails = new List<FiscalYear>();
            FiscalYearDetails = (from Data in Context.FiscalYears
                                 where Data.FYID == SubscriberID
                                 select new FiscalYear
                                 {
                                     FYID = Data.FYID,
                                     StartDate = Data.StartDate,
                                     EndDate = Data.EndDate


                                 }).ToList();

            return FiscalYearDetails;
        }

    }
}
