﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.General;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB.GBLMaster.Master;
using AssistDB.GBLMaster.Master.ViewData;
using AssistDB.General;
using AssistDB.General.ViewData;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.General
{
    public class CompanyMasterRepository : GenericRepository<CompanyDetails>, ICompanyMasterRepository
    {
        public CompanyMasterRepository(ApplicationContext context) : base(context)
        {
        }
        public CompanyDetailsData GetCompanyById(int CompanyID)
        {
            var CompanyDetails = new CompanyDetailsData();
            CompanyDetails = (from Data in Context.CompanyDetails
                              let Role = Data.Role
                              where Data.CompID == CompanyID
                              select new CompanyDetailsData
                              {
                                  CompID = Data.CompID,
                                  Name = Data.Name,
                                  Alias = Data.Alias,
                                  EmpCount = Data.EmpCount,
                                  Phone = Data.Phone,
                                  Mobile = Data.Mobile,
                                  Website = Data.Website,
                                  RoleID = Data.RoleID,
                                  Description = Data.Description,
                                  RoleName = Role != null ? Role.Name : string.Empty
                              }).FirstOrDefault();

            return CompanyDetails;
        }
        public CompanyDetails CheckDuplicateCompany(string CompanyName, int? SubscriberID, int CompanyID, string type)
        {
            var CompanyDetails = new CompanyDetails();
            if (type == "EDIT")
            {
                CompanyDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Name == CompanyName && x.CompID != CompanyID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                CompanyDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Name == CompanyName && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return CompanyDetails;
        }
        public IEnumerable<CompanyDetailsData> GetAllCompany(int SubscriberID)
        {
            var CompanyDetails = new List<CompanyDetailsData>();
            CompanyDetails = (from Data in Context.CompanyDetails
                              let Role = Data.Role
                              where Data.SubscriberID == SubscriberID
                              select new CompanyDetailsData
                              {
                                  CompID = Data.CompID,
                                  Name = Data.Name,
                                  Alias = Data.Alias,
                                  EmpCount = Data.EmpCount,
                                  Phone = Data.Phone,
                                  Mobile = Data.Mobile,
                                  Website = Data.Website,
                                  RoleID = Data.RoleID,
                                  Description = Data.Description,
                                  RoleName = Role != null ? Role.Name : string.Empty
                              }).ToList();

            return CompanyDetails;
        }

    }
}
