﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.General;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB.GBLMaster.Master;
using AssistDB.General;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.General
{
    public class CurrenciesRepository : GenericRepository<Currencies>, ICurrenciesRepository
    {
        public CurrenciesRepository(ApplicationContext context) : base(context)
        {
        }
        public Currencies GetCurrenciesById(int ID)
        {
            var CurrenciesDetails = new Currencies();
            CurrenciesDetails = (from Data in Context.Currenciess
                                 where Data.ID == ID
                                 select new Currencies
                                 {
                                     ID=Data.ID,
                                     CurrencyID = Data.CurrencyID,
                                     Format=Data.Format,
                                     ExchangeRate=Data.ExchangeRate,
                                     Symbol=Data.Symbol,
                                     ThousandSeprator=Data.ThousandSeprator,
                                     DecimalPlace=Data.DecimalPlace,
                                     DecimalSeprator=Data.DecimalSeprator,
                                     Description=Data.Description,

                                 }).FirstOrDefault();

            return CurrenciesDetails;
        }
        public IEnumerable<Currencies> GetAllCurrencies(int SubscriberID)
        {
            var CurrenciesDetails = new List<Currencies>();
            CurrenciesDetails = (from Data in Context.Currenciess
                                 where Data.SubscriberID == SubscriberID
                                 select new Currencies
                                 {
                                     ID = Data.ID,
                                     CurrencyID = Data.CurrencyID,
                                     Format = Data.Format,
                                     ExchangeRate = Data.ExchangeRate,
                                     Symbol = Data.Symbol,
                                     ThousandSeprator = Data.ThousandSeprator,
                                     DecimalPlace = Data.DecimalPlace,
                                     DecimalSeprator = Data.DecimalSeprator,
                                     Description = Data.Description,

                                 }).ToList();

            return CurrenciesDetails;
        }
        public Currencies CheckDuplicateCurrencies(int TypeID, int? SubscriberID, int ID, string type)
        {
            var Currencies = new Currencies();
            if (type == "EDIT")
            {
                Currencies = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.ID != ID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                Currencies = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return Currencies;
        }
    }
}
