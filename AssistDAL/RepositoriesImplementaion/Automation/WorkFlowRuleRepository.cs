﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Automation;
using AssistDB.Automation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace AssistDAL.RepositoriesImplementaion.Automation
{
    public class WorkFlowRuleRepository : GenericRepository<WorkFlowRule>, IWorkFlowRuleRepository
    {
        public WorkFlowRuleRepository(ApplicationContext context) : base(context)
        {
        }
        public WorkFlowRule GetWorkFlowRuleById(int RuleId)
        {
            var data = new WorkFlowRule();
            data = (from Data in Context.WorkFlowRules
                    where Data.RuleId == RuleId
                    select new WorkFlowRule
                    {
                        RuleId = Data.RuleId,
                        SubscrberFormId = Data.SubscrberFormId,
                        Name = Data.Name,
                        Description = Data.Description

                    }).FirstOrDefault();

            return data;
        }
        public IEnumerable<WorkFlowRule> GetAllWorkFlowRule(int SubscriberID)
        {
            var data = new List<WorkFlowRule>();
            data = (from Data in Context.WorkFlowRules

                    where Data.SubscriberID == SubscriberID
                    select new WorkFlowRule
                    {
                        RuleId = Data.RuleId,
                        SubscrberFormId = Data.SubscrberFormId,
                        Name = Data.Name,
                        Description = Data.Description


                    }).ToList();

            return data;
        }
        public WorkFlowRule CheckDuplicateWorkFlowRule(string RuleName, int? SubscriberID, int RuleId, string type)
        {
            var ProfileDetails = new WorkFlowRule();
            if (type == "EDIT")
            {
                ProfileDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Name == RuleName && x.RuleId != RuleId);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                ProfileDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Name == RuleName && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return ProfileDetails;
        }
    }
}
