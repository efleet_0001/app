﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Automation;
using AssistDB.Automation;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.Automation
{

    public class WorkFlowScheduleConditionRepository : GenericRepository<WorkFlowScheduleCondition>, IWorkFlowScheduleConditionRepository
    {
        public WorkFlowScheduleConditionRepository(ApplicationContext context) : base(context)
        {
        }
        public WorkFlowScheduleCondition GetWFScheduleConditionById(int RuleId)
        {
            var data = new WorkFlowScheduleCondition();
            data = (from Data in Context.WorkFlowScheduleConditions
                    where Data.RuleId == RuleId
                    select new WorkFlowScheduleCondition
                    {
                        ConditionId = Data.ConditionId,
                        ScheduleId = Data.ScheduleId,
                        RuleId = Data.RuleId,
                        FormId = Data.FormId,
                        FieldId=data.FieldId,
                        ConditionTypeId=data.ConditionTypeId,
                        Description= data.Description
                    }).FirstOrDefault();

            return data;
        }
        public IEnumerable<WorkFlowScheduleCondition> GetAllWFScheduleCondition(int SubscriberID)
        {
            var data = new List<WorkFlowScheduleCondition>();
            data = (from Data in Context.WorkFlowScheduleConditions

                    where Data.SubscriberID == SubscriberID
                    select new WorkFlowScheduleCondition
                    {
                        ConditionId = Data.ConditionId,
                        ScheduleId = Data.ScheduleId,
                        RuleId = Data.RuleId,
                        FormId = Data.FormId,
                        FieldId = Data.FieldId,
                        ConditionTypeId = Data.ConditionTypeId,
                        Description = Data.Description
                    }).ToList();

            return data;
        }
        public WorkFlowScheduleCondition CheckDuplicateWFScheduleCondition(int ScheduleId, int? SubscriberID, int RuleId, string type)
        {
            var ProfileDetails = new WorkFlowScheduleCondition();
            if (type == "EDIT")
            {
                ProfileDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.RuleId == RuleId && x.RuleId != RuleId);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                ProfileDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.RuleId == RuleId && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return ProfileDetails;
        }
    }
}
