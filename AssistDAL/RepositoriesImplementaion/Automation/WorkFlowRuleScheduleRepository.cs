﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Automation;
using AssistDB.Automation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace AssistDAL.RepositoriesImplementaion.Automation
{
   
    public class WorkFlowRuleScheduleRepository : GenericRepository<WorkFlowRuleSchedule>, IWorkFlowRuleScheduleRepository
    {
        public WorkFlowRuleScheduleRepository(ApplicationContext context) : base(context)
        {
        }
        public WorkFlowRuleSchedule GetWorkFlowRuleScheduleById(int ScheduleId)
        {
            var data = new WorkFlowRuleSchedule();
            data = (from Data in Context.WorkFlowRuleSchedules
                    where Data.RuleId == ScheduleId
                    select new WorkFlowRuleSchedule
                    {
                        ScheduleId = Data.ScheduleId,
                        ScheduleName = Data.ScheduleName,
                        RuleId = Data.RuleId,
                        ActiontypeId = Data.ActiontypeId,
                        SubActionTypeId= Data.SubActionTypeId,
                        Description= Data.Description

                    }).FirstOrDefault();

            return data;
        }
        public IEnumerable<WorkFlowRuleSchedule> GetAllWorkFlowRuleSchedule(int SubscriberID)
        {
            var data = new List<WorkFlowRuleSchedule>();
            data = (from Data in Context.WorkFlowRuleSchedules
                    where Data.SubscriberID == SubscriberID
                    select new WorkFlowRuleSchedule
                    {
                        ScheduleId = Data.ScheduleId,
                        ScheduleName = Data.ScheduleName,
                        RuleId = Data.RuleId,
                        ActiontypeId = Data.ActiontypeId,
                        SubActionTypeId = Data.SubActionTypeId,
                        Description = Data.Description

                    }).ToList();

            return data;
        }
        public WorkFlowRuleSchedule CheckDuplicateWorkFlowRuleSchedule(string ScheduleName, int? SubscriberID, int ScheduleId, string type)
        {
            var ProfileDetails = new WorkFlowRuleSchedule();
            if (type == "EDIT")
            {
                ProfileDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.ScheduleName == ScheduleName && x.ScheduleId != ScheduleId);
            }
            else
            {
                ProfileDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.ScheduleName == ScheduleName && x.StatusID == 1); 
            }
            return ProfileDetails;
        }
    }
}
