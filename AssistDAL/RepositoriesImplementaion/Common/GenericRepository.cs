﻿using AssistDAL.RepositoriesInterface.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AssistDAL.RepositoriesImplementaion.Common
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected ApplicationContext Context;
        protected DbSet<T> DbSet;
        public GenericRepository(ApplicationContext context)
        {
            Context = context;
            DbSet = Context.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return DbSet.AsEnumerable<T>();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await DbSet.ToListAsync();
        }

        public IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            var query = DbSet.Where(predicate).AsEnumerable();
            return query;
        }

        public int Count(Expression<Func<T, bool>> predicate = null)
        {
            if (predicate == null)
            {
                return DbSet.Count();
            }
            else
            {
                return DbSet.Count(predicate);
            }
        }

        public virtual IEnumerable<T> FindWithInclude(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "", int? skip = null, int? take = null)
        {
            IQueryable<T> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split(
                new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }
            if (skip.HasValue)
            {
                var skipValue = skip.Value;
                query = query.Skip(skipValue);
            }
            if (take.HasValue)
            {
                var takeValue = take.Value;
                query = query.Take(takeValue);
            }
            return query.AsEnumerable();
        }


        public virtual T FirstOrDefault(
           Expression<Func<T, bool>> filter = null)
        {
            IQueryable<T> query = DbSet;
            if (filter == null)
            {
                return query.FirstOrDefault();
            }
            else
            {
                return query.FirstOrDefault(filter);
            }
        }

        public virtual T FirstOrDefaultWithInclude(
           Expression<Func<T, bool>> filter = null, string includeProperties = "")
        {
            IQueryable<T> query = DbSet;

            foreach (var includeProperty in includeProperties.Split(
                new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (filter == null)
            {
                return query.FirstOrDefault();
            }
            else
            {
                return query.FirstOrDefault(filter);
            }
        }

        public T Add(T entity)
        {
            DbSet.Add(entity);
            return entity;
        }

        public T Delete(T entity)
        {
            DbSet.Remove(entity);
            return entity;
        }

        public void Edit(T entity)
        {
            Context.Entry(entity).State = EntityState.Deleted;
            Context.Entry(entity).State = EntityState.Modified;
        }

        public int Save()
        {
            return Context.SaveChanges();
        }

        public IEnumerable<T> ExecuteStoreQuery(string commandText, params object[] parameters)
        {
            return Context.Query<T>().FromSqlRaw(commandText, parameters).ToList();
        }
        //public IEnumerable<T> ExcuteSqlQuery(string sqlQuery, CommandType commandType, SqlParameter[] parameters = null)
        //{
        //    if (commandType == CommandType.Text)
        //    {
        //        return SqlQuery(sqlQuery, parameters);
        //    }
        //    else if (commandType == CommandType.StoredProcedure)
        //    {
        //        return StoredProcedure(sqlQuery, parameters);
        //    }

        //    return null;
        //}

        //public void ExecuteNonQuery(string commandText, CommandType commandType, SqlParameter[] parameters = null)
        //{
        //    Context.Database.OpenConnection();
        //    SqlCommand command = new SqlCommand();
        //    command.CommandText = commandText;
        //    command.CommandType = commandType;

        //    if (parameters != null)
        //    {
        //        foreach (var parameter in parameters)
        //        {
        //            command.Parameters.Add(parameter);
        //        }
        //    }

        //    command.ExecuteNonQuery();
        //}

        //public IEnumerable<T> ExecuteReader(string commandText, CommandType commandType, SqlParameter[] parameters = null)
        //{
        //    Context.Database.OpenConnection();
        //    SqlCommand command = new SqlCommand();
        //    command.CommandText = commandText;
        //    command.CommandType = commandType;

        //    if (parameters != null)
        //    {
        //        foreach (var parameter in parameters)
        //        {
        //            command.Parameters.Add(parameter);
        //        }
        //    }

        //    using (var reader = command.ExecuteReader())
        //    {
        //        var mapper = new DataReaderMapper();
        //        return mapper.MapToList(reader);
        //    }
        //}

        //private IEnumerable<T> SqlQuery(string sqlQuery, SqlParameter[] parameters = null)
        //{
        //    if (parameters != null & parameters.Any())
        //    {
        //        var parameterNames = new string[parameters.Length];
        //        for (int i = 0; i<parameters.Length; i++)
        //        {
        //            parameterNames[i] = parameters[i].ParameterName;
        //        }

        //        var result = Context.Database.SqlQuery<T>(string.Format("{0}", sqlQuery, string.Join(",", parameterNames), parameters));
        //        return result.ToList();
        //    }
        //    else
        //    {
        //        var result = Context.Database.SqlQuery<T>(sqlQuery);
        //        return result.ToList();
        //    }
        //}

        //private IEnumerable<T> StoredProcedure(string storedProcedureName, SqlParameter[] parameters = null)
        //{
        //    if (parameters != null & parameters.Any())
        //    {
        //        var parameterNames = new string[parameters.Length];
        //        for (int i = 0; i<parameters.Length; i++)
        //        {
        //            parameterNames[i] = parameters[i].ParameterName;
        //        }

        //        var result = Context.Database.SqlQuery(string.Format("EXEC {0} {1}", storedProcedureName, string.Join(",", parameterNames), parameters));
        //        return result.ToList(T);
        //    }
        //    else
        //    {
        //        var result = Context.Database.SqlQuery(string.Format("EXEC {0}", storedProcedureName));
        //        return result.ToList();
        //    }
        //}
    }
}
