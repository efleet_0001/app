﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.UserControl;
using AssistDB.GBLMaster.Master.ViewData;
using AssistDB.UserControl;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.UserControl
{
    public class RoleMasterRepository : GenericRepository<Role>, IRoleMasterRepository
    {
        public RoleMasterRepository(ApplicationContext context) : base(context)
        {
        }
        public RoleData GetRoleById(int RoleID)
        {
            var RoleDetails = new RoleData();
            RoleDetails = (from Data in Context.Roles
                           let Clone = Data.Clone
                           where Data.RoleID == RoleID
                           select new RoleData
                           {
                               RoleID = Data.RoleID,
                               Name = Data.Name,
                               CloneID = Data.CloneID,
                               Description = Data.Description,
                               CloneName = Data.Clone != null ? Clone.Name : string.Empty,
                           }).FirstOrDefault();

            return RoleDetails;
        }
        public Role CheckDuplicateRole(string RoleName, int? SubscriberID, int RoleID, string type)
        {
            var RoleDetails = new Role();
            if (type == "EDIT")
            {
                RoleDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Name == RoleName && x.RoleID != RoleID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                RoleDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Name == RoleName && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return RoleDetails;
        }
        public IEnumerable<RoleData> GetAllRole(int SubscriberID)
        {
            var RoleDetails = new List<RoleData>();
            RoleDetails = (from Data in Context.Roles
                           let Clone = Data.Clone
                           where Data.SubscriberID == SubscriberID
                           select new RoleData
                           {
                               RoleID = Data.RoleID,
                               Name = Data.Name,
                               CloneID = Data.CloneID,
                               Description = Data.Description,
                               CloneName = Data.Clone != null ? Clone.Name : string.Empty,
                           }).ToList();

            return RoleDetails;
        }


    }
}
