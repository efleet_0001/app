﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.UserControl;
using AssistDB.GBLMaster.Master.ViewData;
using AssistDB.UserControl;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.UserControl
{
    public class ProfileRepository : GenericRepository<Profile_D>, IProfileRepository
    {
        public ProfileRepository(ApplicationContext context) : base(context)
        {
        }
        public ProfileData GetProfileById(int ProfileID)
        {
            var ProfileDetails = new ProfileData();
            ProfileDetails = (from Data in Context.Profiles
                              let Type = Data.Type
                              let Clone = Data.Clone
                              where Data.ProfileID == ProfileID
                              select new ProfileData
                              {
                                  ProfileID = Data.ProfileID,
                                  Name = Data.Name,
                                  TypeID = Data.TypeID,
                                  CloneID = Data.CloneID,
                                  Discription = Data.Discription,
                                  TypeName = Data.Type != null ? Type.Name : string.Empty,
                                  CloneName = Data.Clone != null ? Clone.Name : string.Empty


                              }).FirstOrDefault();

            return ProfileDetails;
        }
        public Profile_D CheckDuplicateProfile(string ProfileName, int? SubscriberID, int ProfileID, string type)
        {
            var ProfileDetails = new Profile_D();
            if (type == "EDIT")
            {
                ProfileDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Name == ProfileName && x.ProfileID != ProfileID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                ProfileDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Name == ProfileName && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return ProfileDetails;
        }
        public IEnumerable<ProfileData> GetAllProfile(int SubscriberID)
        {
            var ProfileDetails = new List<ProfileData>();
            ProfileDetails = (from Data in Context.Profiles
                              let Type = Data.Type
                              let Clone = Data.Clone
                              where Data.SubscriberID == SubscriberID
                              select new ProfileData
                              {
                                  ProfileID = Data.ProfileID,
                                  Name = Data.Name,
                                  TypeID = Data.TypeID,
                                  CloneID = Data.CloneID,
                                  Discription = Data.Discription,
                                  TypeName = Data.Type != null ? Type.Name : string.Empty,
                                  CloneName = Data.Clone != null ? Clone.Name : string.Empty


                              }).ToList();

            return ProfileDetails;
        }

    }
}
