﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Master;
using AssistDAL.RepositoriesInterface.UserControl;
using AssistDB.UserControl;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.UserControl
{
    public class UserGroupRepository : GenericRepository<UserGroup>, IUserGroupRepository
    {
        public UserGroupRepository(ApplicationContext context) : base(context)
        {
        }
        public UserGroup GetUserGroupById(int GroupId)
        {
            var details = new UserGroup();
            details = (from Data in Context.UserGroups
                       where Data.GroupId == GroupId && Data.StatusID == 1
                       select new UserGroup
                       {
                           GroupId = Data.GroupId,
                           GroupName = Data.GroupName,
                           Description = Data.Description
                       }).FirstOrDefault();

            return details;
        }
        public UserGroup CheckDuplicateUserGroup(string GroupName, int? SubscriberID, int GroupID, string type)
        {
            var details = new UserGroup();
            if (type == "EDIT")
            {
                details = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.GroupName == GroupName && x.GroupId != GroupID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                details = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.GroupName == GroupName && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return details;
        }
        public IEnumerable<UserGroup> GetAllUserGroup(int SubscriberID)
        {
            var details = new List<UserGroup>();
            details = (from Data in Context.UserGroups
                       where Data.GroupId == SubscriberID && Data.StatusID == 1
                       select new UserGroup
                       {
                           GroupId = Data.GroupId,
                           GroupName = Data.GroupName,
                           Description = Data.Description
                       }).ToList();

            return details;
        }


    }
}
