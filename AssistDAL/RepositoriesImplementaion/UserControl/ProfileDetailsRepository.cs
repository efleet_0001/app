﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.UserControl;
using AssistDB.GBLMaster.Master.ViewData;
using AssistDB.UserControl;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.UserControl
{
    public class ProfileDetailsRepository : GenericRepository<ProfileDetail>, IProfileDetailsRepository
    {
        public ProfileDetailsRepository(ApplicationContext context) : base(context)
        {
        }
        public ProfileDetailsData GetProfileDetailById(int ProfileID)
        {
            var ProfileDetails = new ProfileDetailsData();
            ProfileDetails = (from Data in Context.ProfileDetails
                              let Profile = Data.Profile
                              let Cate = Data.Cate
                              let SubCat = Data.SubCat
                              let Entity = Data.Entity
                              where Data.ProfileID == ProfileID
                              select new ProfileDetailsData
                              {
                                  ProfileD_ID = Data.ProfileD_ID,
                                  ProfileID = Data.ProfileID,
                                  BG_Image = Data.BG_Image,
                                  DP_Image = Data.DP_Image,
                                  CateID = Data.CateID,
                                  SubCatID = Data.SubCatID,
                                  EntityID = Data.EntityID,
                                  Discription = Data.Discription,
                                  ProfileName = Data.Profile != null ? Profile.Name : string.Empty,
                                  CategoryName = Data.Cate != null ? Cate.Name : string.Empty,
                                  SubCategoryName = Data.SubCat != null ? SubCat.Name : string.Empty,
                                  EntityName = Data.Entity != null ? Entity.Name : string.Empty


                              }).FirstOrDefault();

            return ProfileDetails;
        }
        public ProfileDetail CheckDuplicateProfileDetail(string ProfileName, int? SubscriberID, int ProfileID, string type)
        {
            var ProfileDetails = new ProfileDetail();
            if (type == "EDIT")
            {
                ProfileDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.ProfileID != ProfileID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                ProfileDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return ProfileDetails;
        }
        public IEnumerable<ProfileDetailsData> GetAllProfileDetail(int SubscriberID)
        {
            var ProfileDetails = new List<ProfileDetailsData>();
            ProfileDetails = (from Data in Context.ProfileDetails
                              let Profile = Data.Profile
                              let Cate = Data.Cate
                              let SubCat = Data.SubCat
                              let Entity = Data.Entity
                              where Data.SubscriberID == SubscriberID
                              select new ProfileDetailsData
                              {
                                  ProfileD_ID = Data.ProfileD_ID,
                                  ProfileID = Data.ProfileID,
                                  BG_Image = Data.BG_Image,
                                  DP_Image = Data.DP_Image,
                                  CateID = Data.CateID,
                                  SubCatID = Data.SubCatID,
                                  EntityID = Data.EntityID,
                                  Discription = Data.Discription,
                                  ProfileName = Data.Profile != null ? Profile.Name : string.Empty,
                                  CategoryName = Data.Cate != null ? Cate.Name : string.Empty,
                                  SubCategoryName = Data.SubCat != null ? SubCat.Name : string.Empty,
                                  EntityName = Data.Entity != null ? Entity.Name : string.Empty


                              }).ToList();

            return ProfileDetails;
        }

    }
}
