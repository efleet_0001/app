﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Master;
using AssistDAL.RepositoriesInterface.UserControl;
using AssistDB;
using AssistDB.UserControl;
using AssistDB.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.UserControl
{
    public class UserMasterRepository : GenericRepository<Usermaster>, IUserMasterRepository
    {
        public UserMasterRepository(ApplicationContext context) : base(context)
        {
        }
        public UsermasterData GetUserById(int UserID)
        {
            var userMasterDetails = new UsermasterData();
            userMasterDetails = (from data in Context.Usermasters
                                 let Role = data.Role
                                 let Profile = data.Profile
                                 let Locale = data.Locale
                                 where data.StatusID != 3 && data.UserID == UserID
                                 orderby data.CreatedDate descending
                                 select new UsermasterData
                                 {
                                     UserID = data.UserID,
                                     First_Name = data.First_Name,
                                     Last_Name = data.Last_Name,
                                     Email = data.Email,
                                     RoleID = data.RoleID,
                                     ProfileID = data.ProfileID,
                                     LocaleID = data.LocaleID,
                                     Alias = data.Alias,
                                     Phone = data.Phone,
                                     Mobile = data.Mobile,
                                     Website = data.Website,
                                     DOB = data.DOB,
                                     EmailConfrmed = data.EmailConfrmed,
                                     PasswordHash = data.PasswordHash,
                                     SecurityStamp = data.SecurityStamp,
                                     MobileNoConfirmed = data.MobileNoConfirmed,
                                     TwoFactorEnabled = data.TwoFactorEnabled,
                                     blocked = data.blocked,
                                     RoleName = Role != null ? Role.Name : string.Empty,
                                     ProfileName = Profile != null ? Profile.Name : string.Empty,
                                     LocaleInfo = Locale != null ? Locale.Signature : string.Empty,
                                     StatusID = data.StatusID,
                                     CSID = data.CSID,
                                     MSID = data.MSID,
                                     SubscriberID = data.SubscriberID,
                                     CreatedBy = data.CreatedBy,
                                     ModifiedBy = data.ModifiedBy,
                                     Rowguid = data.Rowguid,
                                     CreatedDate = data.CreatedDate,
                                     ModifiedDate = data.ModifiedDate

                                 }).FirstOrDefault();

            return userMasterDetails;
        }
        public Usermaster CheckDuplicateUser(string UserName, int? SubscriberID, int UserID, string type)
        {
            var usermaster = new Usermaster();
            if (type == "EDIT")
            {
                usermaster = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.First_Name == UserName && x.UserID != UserID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                usermaster = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Email == UserName); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return usermaster;
        }
        public UsermasterData GetUserDetails(int UserID)
        {
            var userMasterDetails = new UsermasterData();
            userMasterDetails = (from data in Context.Usermasters
                                 let Role = data.Role
                                 let Profile = data.Profile
                                 let Locale = data.Locale
                                 where data.StatusID != 3 && data.UserID == UserID
                                 orderby data.CreatedDate descending
                                 select new UsermasterData
                                 {
                                     UserID = data.UserID,
                                     First_Name = data.First_Name,
                                     Last_Name = data.Last_Name,
                                     Email = data.Email,
                                     RoleID = data.RoleID,
                                     ProfileID = data.ProfileID,
                                     LocaleID = data.LocaleID,
                                     Alias = data.Alias,
                                     Phone = data.Phone,
                                     Mobile = data.Mobile,
                                     Website = data.Website,
                                     DOB = data.DOB,
                                     EmailConfrmed = data.EmailConfrmed,
                                     PasswordHash = data.PasswordHash,
                                     SecurityStamp = data.SecurityStamp,
                                     MobileNoConfirmed = data.MobileNoConfirmed,
                                     TwoFactorEnabled = data.TwoFactorEnabled,
                                     blocked = data.blocked,
                                     RoleName = Role != null ? Role.Name : string.Empty,
                                     ProfileName = Profile != null ? Profile.Name : string.Empty,
                                     LocaleInfo = Locale != null ? Locale.Signature : string.Empty,
                                     StatusID = data.StatusID,
                                     CSID = data.CSID,
                                     MSID = data.MSID,
                                     SubscriberID = data.SubscriberID,
                                     CreatedBy = data.CreatedBy,
                                     ModifiedBy = data.ModifiedBy,
                                     Rowguid = data.Rowguid,
                                     CreatedDate = data.CreatedDate,
                                     ModifiedDate = data.ModifiedDate

                                 }).FirstOrDefault();

            return userMasterDetails;
        }
        public IEnumerable<UsermasterData> GetAll(int SubscriberID)
        {
            var userMasterDetails = new List<UsermasterData>();
            userMasterDetails = (from data in Context.Usermasters
                                 let Role = data.Role
                                 let Profile = data.Profile
                                 let Locale = data.Locale
                                 where data.StatusID != 3 && data.SubscriberID == SubscriberID
                                 orderby data.CreatedDate descending
                                 select new UsermasterData
                                 {
                                     UserID = data.UserID,
                                     First_Name = data.First_Name,
                                     Last_Name = data.Last_Name,
                                     Email = data.Email,
                                     RoleID = data.RoleID,
                                     ProfileID = data.ProfileID,
                                     LocaleID = data.LocaleID,
                                     Alias = data.Alias,
                                     Phone = data.Phone,
                                     Mobile = data.Mobile,
                                     Website = data.Website,
                                     DOB = data.DOB,
                                     EmailConfrmed = data.EmailConfrmed,
                                     PasswordHash = data.PasswordHash,
                                     SecurityStamp = data.SecurityStamp,
                                     MobileNoConfirmed = data.MobileNoConfirmed,
                                     TwoFactorEnabled = data.TwoFactorEnabled,
                                     blocked = data.blocked,
                                     RoleName = Role != null ? Role.Name : string.Empty,
                                     ProfileName = Profile != null ? Profile.Name : string.Empty,
                                     LocaleInfo = Locale != null ? Locale.Signature : string.Empty,
                                     StatusID = data.StatusID,
                                     CSID = data.CSID,
                                     MSID = data.MSID,
                                     SubscriberID = data.SubscriberID,
                                     CreatedBy = data.CreatedBy,
                                     ModifiedBy = data.ModifiedBy,
                                     Rowguid = data.Rowguid,
                                     CreatedDate = data.CreatedDate,
                                     ModifiedDate = data.ModifiedDate
                                 }).ToList();

            return userMasterDetails;
        }

    }
}
