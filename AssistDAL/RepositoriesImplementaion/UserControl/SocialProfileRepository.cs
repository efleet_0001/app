﻿using AssistDAL.RepositoriesImplementaion.Common;
using AssistDAL.RepositoriesInterface.Master;
using AssistDAL.RepositoriesInterface.UserControl;
using AssistDB.GBLMaster.Master;
using AssistDB.UserControl;
using System.Collections.Generic;
using System.Linq;

namespace AssistDAL.RepositoriesImplementaion.UserControl
{
    public class SocialProfileRepository : GenericRepository<SocialProfile>, ISocialProfileRepository
    {
        public SocialProfileRepository(ApplicationContext context) : base(context)
        {
        }
        public SocialProfile GetSProfileById(int S_ProfileID)
        {
            var SProfileDetails = new SocialProfile();
            SProfileDetails = (from Data in Context.SocialProfiles
                               where Data.SocID == S_ProfileID
                               select new SocialProfile
                               {
                                   SocID = Data.SocID,
                                   EntityTypeID = Data.EntityTypeID,
                                   EntityID = Data.EntityID,
                                   SocialType = Data.SocialType,
                                   SocialID = Data.SocialID,
                                   Authtoken = Data.Authtoken,
                                   Authurl = Data.Authurl,
                                   AuthPassword = Data.AuthPassword,

                               }).FirstOrDefault();

            return SProfileDetails;
        }
        public SocialProfile CheckDuplicateSProfile(string S_ProfileName, int? SubscriberID, int S_ProfileID, string type)
        {
            var SProfileDetails = new SocialProfile();
            if (type == "EDIT")
            {
                SProfileDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Authurl == S_ProfileName && x.SocID != S_ProfileID);// && x.IsDeleted == false && x.IsStatus == true
            }
            else
            {
                SProfileDetails = FirstOrDefault(x => x.SubscriberID == SubscriberID && x.Authurl == S_ProfileName && x.StatusID == 1); //&& x.IsDeleted == false && x.IsStatus == true
            }
            return SProfileDetails;
        }
        public IEnumerable<SocialProfile> GetAllS_Profile(int SubscriberID)
        {
            var SProfileDetails = new List<SocialProfile>();
            SProfileDetails = (from Data in Context.SocialProfiles
                               where Data.SubscriberID == SubscriberID
                               select new SocialProfile
                               {
                                   SocID = Data.SocID,
                                   EntityTypeID = Data.EntityTypeID,
                                   EntityID = Data.EntityID,
                                   SocialType = Data.SocialType,
                                   SocialID = Data.SocialID,
                                   Authtoken = Data.Authtoken,
                                   Authurl = Data.Authurl,
                                   AuthPassword = Data.AuthPassword,

                               }).ToList();

            return SProfileDetails;
        }


    }
}
