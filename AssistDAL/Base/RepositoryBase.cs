﻿using AssistDB;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace AssistDAL.Base
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : BaseEntity
    {
        protected ApplicationContext _context { get; set; }

        public RepositoryBase(ApplicationContext context)
        {
            _context = context;
        }

        public IEnumerable<T> GetAll()
        {
            return _context.Set<T>();
        }

        public IEnumerable<T> FindWithCondition(Expression<Func<T, bool>> expression)
        {
            yield return _context.Set<T>().Find(expression);
        }

        public IEnumerable<T> ExecuteStoreQuery(string commandText, params object[] parameters)
        {
            return _context.Set<T>().FromSqlRaw(commandText, parameters).ToList();

        }
        public T GetById(int id)
        {
            return _context.Set<T>().Where(t => t.SubscriberID == id).First();
        }

        public int Add(T entity)
        {
            try
            {
                _context.Add(entity);
                this._context.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int Update(T enity)
        {
            throw new NotImplementedException();
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int AddMany(List<T> ls)
        {
            throw new NotImplementedException();
        }

        public int UpdateMany(List<T> ls)
        {
            throw new NotImplementedException();
        }

        public int DeleleMany(List<int> ls)
        {
            throw new NotImplementedException();
        }
    }
}
