﻿using AssistDAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistDAL
{
    public interface IRepositoryWrapper
    {
        IUserRepository UserRepository { get; }
        IAgriDasboardRepository AgriDasboardRepository { get; }
        // new repository interface here 
    }
}
