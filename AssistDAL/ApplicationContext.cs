﻿using AssistDB.Automation;
using AssistDB.BaseMaster;
using AssistDB.GBLMaster.Master;
using AssistDB.GBLMaster.Trans;
using AssistDB.General;
using AssistDB.Kisan.Master;
using AssistDB.Kisan.Trans;
using AssistDB.Kisan.Trans.Data;
using AssistDB.login.DB.login;
using AssistDB.UserControl;
using AssistDB.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace AssistDAL
{
    //  Install-Package Microsoft.EntityFrameworkCore.Relational -Version 2.1.0
    //  Microsoft.EntityFrameworkCore.SqlServer
    //  Microsoft.EntityFrameworkCore.Design

    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {

        }
        #region GBLModel
        public DbSet<Usermaster> Usermasters { get; set; }
        public DbSet<FiscalYear> FiscalYears { get; set; }
        public DbSet<CompanyDetails> CompanyDetails { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Holiday> Holidays { get; set; }
        public DbSet<LocaleInformation> LocaleInformations { get; set; }
        public DbSet<Profile_D> Profiles { get; set; }
        public DbSet<ProfileDetail> ProfileDetails { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<SocialProfile> SocialProfiles { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Constant> Constants { get; set; }
        public DbSet<Country> Countrys { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<BusinessHour> BusinessHours { get; set; }
        public DbSet<Currencies> Currenciess { get; set; }
        #endregion
        #region KisanModel
        public DbSet<Category> Categorys { get; set; }
        public DbSet<SubCategory> SubCategorys { get; set; }
        public DbSet<Entity> Entitys { get; set; }
        public DbQuery<AgriDashboardModel> AgriDashboardModels { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<News> Newss { get; set; }
        public DbSet<NewsCategory> NewsCategorys { get; set; }
        public DbSet<ProblemMaster> ProblemMasters { get; set; }
        public DbSet<Solution> Solutions { get; set; }
        public DbQuery<LProfileDetail> LProfileDetails { get; set; }
        public DbQuery<FYModel> FYModels { get; set; }
        public DbQuery<SessionModel> SessionModels { get; set; }
        public DbSet<loginModel> loginModels { get; set; }
        public DbQuery<Subscriber> Subscribers { get; set; }
        public DbQuery<MProfile> MProfiles { get; set; }
        public DbQuery<LocaleInfo> LocaleInfos { get; set; }
        public DbSet<NotificationData> NotificationDatas { get; set; }
        public DbSet<MessageData> MessageDatas { get; set; }
        public DbSet<NewCategoryData> NewCategoryDatas { get; set; }
        public DbSet<EntityNewData> EntityNewDatas { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<MemberMapping> MemberMappings { get; set; }
        #endregion KisanModel
        #region Trans
        public DbSet<ImageUpload> imageUploads { get; set; }
        #endregion Trans
        #region Automation
        public DbSet<WorkFlowRule> WorkFlowRules { get; set; }
        public DbSet<WorkFlowRuleSchedule> WorkFlowRuleSchedules { get; set; }
        public DbSet<WorkFlowScheduleCondition> WorkFlowScheduleConditions { get; set; }
        #endregion
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.ForSqlServerUseSequenceHiLo("DBSequence", "dbo");
             modelBuilder.HasSequence("DBSequence","dbo");
            #region UserMaster
            modelBuilder.Entity<Role>()
            .HasOne<Usermaster>(r => r.usermaster)
            .WithOne(r => r.Role)
            .HasForeignKey<Usermaster>(con => con.RoleID);

            modelBuilder.Entity<Profile_D>()
            .HasOne<Usermaster>(r => r.usermaster)
            .WithOne(r => r.Profile)
            .HasForeignKey<Usermaster>(con => con.ProfileID);

            modelBuilder.Entity<LocaleInformation>()
            .HasOne<Usermaster>(r => r.usermaster)
            .WithOne(r => r.Locale)
            .HasForeignKey<Usermaster>(con => con.LocaleID);
            #endregion
            #region Solution
            modelBuilder.Entity<ProblemMaster>()
            .HasOne<Solution>(r => r.solution)
            .WithOne(r => r.problemMaster)
            .HasForeignKey<Solution>(con => con.ProblemID);
            #endregion
            #region ProblemMaster
            modelBuilder.Entity<Usermaster>()
            .HasOne<ProblemMaster>(r => r.problemMaster)
            .WithOne(r => r.User)
            .HasForeignKey<ProblemMaster>(con => con.UserID);

            modelBuilder.Entity<SubCategory>()
            .HasOne<ProblemMaster>(r => r.problemMaster)
            .WithOne(r => r.SubCate)
            .HasForeignKey<ProblemMaster>(con => con.SubCateID);


            modelBuilder.Entity<Entity>()
            .HasOne<ProblemMaster>(r => r.problemMaster)
            .WithOne(r => r.Entity)
            .HasForeignKey<ProblemMaster>(con => con.EntityID);
            #endregion
            #region NewsCategory
            modelBuilder.Entity<Constant>()
            .HasOne<NewsCategory>(r => r.newsCategory)
            .WithOne(r => r.Category)
            .HasForeignKey<NewsCategory>(l => l.CategoryID);
            #endregion
            #region News
            modelBuilder.Entity<NewsCategory>()
            .HasOne<News>(r => r.news)
            .WithOne(r => r.Cat)
            .HasForeignKey<News>(l => l.CatID);
            modelBuilder.Entity<Constant>()
            .HasOne<News>(r => r.news)
            .WithOne(r => r.SorceType)
            .HasForeignKey<News>(l => l.SorceTypeID);
            modelBuilder.Entity<Constant>()
            .HasOne<News>(r => r.news)
            .WithOne(r => r.SorceType)
            .HasForeignKey<News>(l => l.SorceTypeID);
            #endregion
            #region SubCategory
            //  modelBuilder.Entity<Category>()
            //.HasMany<SubCategory>(r => r.SubCategories)
            //.WithOne(r => r.Cate)
            //.HasForeignKey<SubCategory>(l => l.CateID);
            #endregion
            #region Contact
            modelBuilder.Entity<District>()
            .HasOne<Contact>(r => r.contact)
            .WithOne(r => r.District)
            .HasForeignKey<Contact>(con => con.DistrictID);


            modelBuilder.Entity<Country>()
            .HasOne<Contact>(r => r.contact)
            .WithOne(r => r.Country)
            .HasForeignKey<Contact>(con => con.CountryID);

            modelBuilder.Entity<State>()
            .HasOne<Contact>(r => r.contact)
            .WithOne(r => r.State)
            .HasForeignKey<Contact>(con => con.StateID);
            #endregion
            #region  LocaleInformation
            modelBuilder.Entity<Language>()
            .HasOne<LocaleInformation>(r => r.localeInformation)
            .WithOne(r => r.Language)
            .HasForeignKey<LocaleInformation>(l => l.LanguageID);

            modelBuilder.Entity<State>()
            .HasOne<LocaleInformation>(r => r.localeInformation)
            .WithOne(r => r.State)
            .HasForeignKey<LocaleInformation>(l => l.StateID);

            modelBuilder.Entity<Country>()
            .HasOne<LocaleInformation>(r => r.localeInformation)
            .WithOne(r => r.Country)
            .HasForeignKey<LocaleInformation>(l => l.CountryID);

            modelBuilder.Entity<District>()
            .HasOne<LocaleInformation>(r => r.localeInformation)
            .WithOne(r => r.District)
            .HasForeignKey<LocaleInformation>(l => l.DistrictID);
            #endregion
            #region Profile
            modelBuilder.Entity<Constant>()
            .HasOne<Profile_D>(p => p.profile_D)
            .WithOne(con => con.Type)
            .HasForeignKey<Profile_D>(l => l.TypeID);
            #endregion
            #region socialProfile
            modelBuilder.Entity<Constant>()
            .HasOne<SocialProfile>(p => p.socialProfile)
            .WithOne(con => con.EntityType)
            .HasForeignKey<SocialProfile>(l => l.EntityTypeID);
            #endregion
            #region CompanyDetails
            modelBuilder.Entity<Role>()
            .HasOne<CompanyDetails>(p => p.companyDetails)
            .WithOne(con => con.Role)
            .HasForeignKey<CompanyDetails>(l => l.RoleID);
            #endregion
            #region ProfileDetails
            modelBuilder.Entity<Profile_D>()
            .HasOne<ProfileDetail>(p => p.profileDetails)
            .WithOne(con => con.Profile)
            .HasForeignKey<ProfileDetail>(l => l.ProfileID);
            modelBuilder.Entity<Category>()
            .HasOne<ProfileDetail>(p => p.profileDetails)
            .WithOne(con => con.Cate)
            .HasForeignKey<ProfileDetail>(l => l.CateID);
            modelBuilder.Entity<SubCategory>()
            .HasOne<ProfileDetail>(p => p.profileDetails)
            .WithOne(con => con.SubCat)
            .HasForeignKey<ProfileDetail>(l => l.SubCatID);
            modelBuilder.Entity<Entity>()
            .HasOne<ProfileDetail>(p => p.profileDetails)
            .WithOne(con => con.Entity)
            .HasForeignKey<ProfileDetail>(l => l.EntityID);
            #endregion
            #region Notification
            modelBuilder.Entity<Constant>()
           .HasOne<Notification>(r => r.notification)
           .WithOne(r => r.NotificationType)
           .HasForeignKey<Notification>(l => l.Type);
            modelBuilder.Entity<Constant>()
         .HasOne<Notification>(r => r.notification1)
         .WithOne(r => r.NotificationSourceTypes)
         .HasForeignKey<Notification>(l => l.NotificationSourceType);
            #endregion
            // Logger.Log('aaaa');

        }
    }


}
