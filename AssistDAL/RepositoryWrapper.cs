﻿using AssistDAL.Repository;

namespace AssistDAL
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private ApplicationContext _context;
        private IUserRepository _userRepository;
        private IAgriDasboardRepository _agriDasboardRepository;

        public RepositoryWrapper(ApplicationContext context)
        {
            _context = context;
        }

        public IUserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    return new UserRepository(_context);
                }
                return this._userRepository;
            }
        }
        // new get method here

        public IAgriDasboardRepository AgriDasboardRepository
        {
            get
            {
                if (_agriDasboardRepository == null)
                {
                    return new AgriDashboardRepository(_context);
                }
                return this._agriDasboardRepository;
            }
        }
    }
}
