﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.GBLMaster.Master;
using AssistDB.General;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.General
{
    public interface IFiscalMasterRepository : IGenericRepository<FiscalYear>
    {
        FiscalYear GetFiscalById(int FYID);
        IEnumerable<FiscalYear> GetAllFiscal(int SubscriberID);
        FiscalYear CheckDuplicateFiscal(string FiscalName, int? SubscriberID, int FiscalID, string type);
    }
}
