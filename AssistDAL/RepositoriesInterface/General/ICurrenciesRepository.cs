﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistDAL.RepositoriesInterface.General
{
    public interface ICurrenciesRepository : IGenericRepository<Currencies>
    {
        Currencies GetCurrenciesById(int ID);
        IEnumerable<Currencies> GetAllCurrencies(int SubscriberID);
        Currencies CheckDuplicateCurrencies(int TypeID, int? SubscriberID, int ID, string type);
    }
}
