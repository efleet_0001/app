﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.GBLMaster.Master;
using AssistDB.General;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.General
{
    public interface IHolidayMasterRepository : IGenericRepository<Holiday>
    {
        Holiday GetHolidayById(int HolidayID);
        IEnumerable<Holiday> GetAllHoliday(int SubscriberID);
        Holiday CheckDuplicateHoliday(string HolidayName, int? SubscriberID, int HolidayID, string type);
    }
}
