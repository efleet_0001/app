﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.GBLMaster.Master;
using AssistDB.GBLMaster.Master.ViewData;
using AssistDB.General;
using AssistDB.General.ViewData;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.General
{
    public interface ICompanyMasterRepository : IGenericRepository<CompanyDetails>
    {
        CompanyDetailsData GetCompanyById(int CompanyID);
        IEnumerable<CompanyDetailsData> GetAllCompany(int SubscriberID);
        CompanyDetails CheckDuplicateCompany(string CompanyName, int? SubscriberID, int CompanyID, string type);
    }
}
