﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.General;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.General
{
   
    public interface IBusinesshourRepository : IGenericRepository<BusinessHour>
    {
        BusinessHour GetBusinesshourById(int ID);
        IEnumerable<BusinessHour> GetAllBusinesshour(int SubscriberID);
        BusinessHour CheckDuplicateBusinesshour(int TypeID, int? SubscriberID, int ID, string type);
    }
}
