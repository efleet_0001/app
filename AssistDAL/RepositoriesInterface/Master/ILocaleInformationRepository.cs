﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.GBLMaster.Master;
using AssistDB.GBLMaster.Master.ViewData;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Master
{
    public interface ILocaleInformationRepository : IGenericRepository<LocaleInformation>
    {
        LocaleInformationData GetLocaleInfoById(int LocaleInfoID);
        IEnumerable<LocaleInformationData> GetAll(int SubscriberID);
        LocaleInformation CheckDuplicateLocaleInfo(string LocaleInfoName, int? SubscriberID, int LocaleInfoID, string type);
    }
}
