﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.GBLMaster.Master;
using AssistDB.GBLMaster.Master.ViewData;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Master
{
    public interface IContactMasterRepository : IGenericRepository<Contact>
    {
        ContactData GetContactById(int ContactID);
        IEnumerable<ContactData> GetAllContact(int SubscriberID);
        Contact CheckDuplicateContact(string ContactName, int? SubscriberID, int ContactID, string type);
    }
}
