﻿using AssistDB.BaseMaster;
using AssistDB.GBLMaster.Master;
using AssistDB.General;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Master
{
    public interface IStaticRepository
    {
        IEnumerable<Application> GetAllApplication(int SubscriberID);
        IEnumerable<Client> GetAllClient(int SubscriberID);
        IEnumerable<Constant> GetAllConstant(int SubscriberID, int ConstantGroupID);
        IEnumerable<Country> GetAllCountry(int SubscriberID);
        IEnumerable<State> GetAllState(int SubscriberID, int CountryID);
        IEnumerable<District> GetAllDistrict(int SubscriberID, int StateID);
        IEnumerable<Language> GetAllLanguage(int SubscriberID);
        IEnumerable<Holiday> GetAllHolidays(int DistrictID);


    }
}
