﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Kisan.Trans;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Kisan
{
    public interface IAnswerRepository : IGenericRepository<Answer>
    {
        Answer GetAnswerById(int AnswerID);
        IEnumerable<Answer> GetAllAnswer(int SubscriberID);
        Answer CheckDuplicateAnswer(string AnswerName, int? SubscriberID, int AnswerID, string type);
    }
}
