﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Kisan.Trans;
using AssistDB.Kisan.Trans.Data;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Kisan
{
    public interface INewsRepository : IGenericRepository<News>
    {
        NewsData GetNewsById(int NewsID);
        IEnumerable<NewsData> GetAllNews(int SubscriberID);
        News CheckDuplicateAnswer(string NewsName, int? SubscriberID, int NewsID, string type);
        IEnumerable<NewCategoryData> GetCateGory(int SubscriberID, int LanguageID, long UserID, int EntityID);
        IEnumerable<EntityNewData> GetEntityNew(long CateID, int SubscriberID, int LanguageID, long UserID, int EntityID);
    }
}
