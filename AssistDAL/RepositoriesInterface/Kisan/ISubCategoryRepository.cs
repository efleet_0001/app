﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Kisan.Master;
using AssistDB.Kisan.Master.Data;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Kisan
{
    public interface ISubCategoryRepository : IGenericRepository<SubCategory>
    {
        SubCategoryData GetSubCatById(int SubCatID);
        IEnumerable<SubCategoryData> GetAllSubCat(int SubscriberID);
        SubCategory CheckDuplicateSubCat(string SubCatName, int? SubscriberID, int SubCatID, string type);
    }
}
