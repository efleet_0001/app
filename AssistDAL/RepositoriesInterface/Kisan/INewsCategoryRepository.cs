﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Kisan.Trans;
using AssistDB.Kisan.Trans.Data;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Kisan
{
    public interface INewsCategoryRepository : IGenericRepository<NewsCategory>
    {
        NewsCategoryData GetNewsCatById(int NewsCatID);
        IEnumerable<NewsCategoryData> GetAllNewsCat(int SubscriberID);
        NewsCategory CheckDuplicateNewsCategory(string NewsCatName, int? SubscriberID, int NewsCatID, string type);

    }
}
