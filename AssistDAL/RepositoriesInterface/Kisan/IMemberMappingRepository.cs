﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Kisan.Trans;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Kisan
{
    public interface IMemberMappingRepository : IGenericRepository<MemberMapping>
    {
        MemberMapping GetMemberMappingById(int MemberID);
        IEnumerable<MemberMapping> GetAllMemberMapping(int SubscriberID);
        MemberMapping CheckDuplicateMemberMapping(long MemberID, int? SubscriberID, long MappingID, string type);

    }
}
