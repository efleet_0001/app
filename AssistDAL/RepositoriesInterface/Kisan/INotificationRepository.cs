﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Kisan.Trans;
using AssistDB.Kisan.Trans.Data;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Kisan
{
    public interface INotificationRepository : IGenericRepository<Notification>
    {
        IEnumerable<NotificationData> GetAppNotification(int MemberID, int subscriberID);
        IEnumerable<NotificationData> GetAllAppNotification(int MemberID, int subscriberID);
        IEnumerable<NotificationData> GetSMSNotification(int MemberID, int subscriberID);
        IEnumerable<NotificationData> GetAllSMSNotification(int MemberID, int subscriberID);
        IEnumerable<NotificationData> GetWhatsAppNotification(int MemberID, int subscriberID);
        IEnumerable<NotificationData> GetAllWhatsAppNotification(int MemberID, int subscriberID);
    }
}
