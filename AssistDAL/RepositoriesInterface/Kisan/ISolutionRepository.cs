﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Kisan.Trans;
using AssistDB.Kisan.Trans.Data;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Kisan
{
    public interface ISolutionRepository : IGenericRepository<Solution>
    {
        SolutionData GetSolutionById(int SolnID);
        IEnumerable<SolutionData> GetAllSolution(int SubscriberID);
        Solution CheckDuplicateSolution(string SolutionTag, int? SubscriberID, int SolnID, string type);
    }
}
