﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Kisan.Master;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Kisan
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
        Category GetCategoryById(int CategoryID);
        IEnumerable<Category> GetAllCategory(int SubscriberID);
        Category CheckDuplicateCategory(string CategoryName, int? SubscriberID, int CategoryID, string type);
    }
}
