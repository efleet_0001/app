﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.GBLMaster.Trans;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Trans
{
    public interface IImageuploadRepository : IGenericRepository<ImageUpload>
    {
        ImageUpload GetImageById(long TransactionID, int TransactionTypeID);
        IEnumerable<ImageUpload> GetAllImage(int SubscriberID);
        ImageUpload CheckDuplicateImage(string Name, int? SubscriberID, long TransactionID, int TransactypeID, string type);
    }
}
