﻿using AssistDB.login.DB.login;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Trans
{
    public interface IloginRepository
    {

        IEnumerable<FYModel> GetFYDetails(int SubscriberID, int UserID);
        SessionModel CreateSession(SessionModel sessionModel);
        string GetPasswordHash(int SubscriberID, int UserID);
        string ChangePassword(long SubscriberID, long UserID, string username, string passwordhash);
        loginModel Getlogin(string username, string password);
        Subscriber GetSubscriber(long UserID);
        MProfile GetMProfile(long ProfileID);
        LProfileDetail ProfileDetail(long ProfileID);
        LocaleInfo GetLocaleInfo(long UserID);

    }
}
