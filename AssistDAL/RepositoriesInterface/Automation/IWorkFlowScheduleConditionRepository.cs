﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Automation;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Automation
{
    public interface IWorkFlowScheduleConditionRepository : IGenericRepository<WorkFlowScheduleCondition>
    {
        WorkFlowScheduleCondition GetWFScheduleConditionById(int RuleId);
        IEnumerable<WorkFlowScheduleCondition> GetAllWFScheduleCondition(int SubscriberID);
        WorkFlowScheduleCondition CheckDuplicateWFScheduleCondition(int ScheduleId, int? SubscriberID, int RuleId, string type);
    }
}
