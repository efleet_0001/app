﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Automation;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Automation
{
    public interface IWorkFlowRuleScheduleRepository : IGenericRepository<WorkFlowRuleSchedule>
    {
        WorkFlowRuleSchedule GetWorkFlowRuleScheduleById(int ScheduleId);
        IEnumerable<WorkFlowRuleSchedule> GetAllWorkFlowRuleSchedule(int SubscriberID);
        WorkFlowRuleSchedule CheckDuplicateWorkFlowRuleSchedule(string ScheduleName, int? SubscriberID, int ScheduleId, string type);
    }
}
