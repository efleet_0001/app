﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Automation;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.Automation
{
    public interface IWorkFlowRuleRepository : IGenericRepository<WorkFlowRule>
    {
        WorkFlowRule GetWorkFlowRuleById(int RuleId);
        IEnumerable<WorkFlowRule> GetAllWorkFlowRule(int SubscriberID);
        WorkFlowRule CheckDuplicateWorkFlowRule(string RuleName, int? SubscriberID, int RuleId, string type);
    }
}
