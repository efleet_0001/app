﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.UserControl;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.UserControl
{
    public interface IUserGroupRepository : IGenericRepository<UserGroup>
    {
        UserGroup GetUserGroupById(int GroupId);
        IEnumerable<UserGroup> GetAllUserGroup(int SubscriberID);
        UserGroup CheckDuplicateUserGroup(string GroupName, int? SubscriberID, int GroupID, string type);
    }
}
