﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB;
using AssistDB.UserControl;
using AssistDB.ViewModels;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.UserControl
{
    public interface IUserMasterRepository : IGenericRepository<Usermaster>
    {
        UsermasterData GetUserById(int UserID);
        UsermasterData GetUserDetails(int UserID);
        IEnumerable<UsermasterData> GetAll(int SubscriberID);
        Usermaster CheckDuplicateUser(string UserName, int? SubscriberID, int UserID, string type);
    }
}
