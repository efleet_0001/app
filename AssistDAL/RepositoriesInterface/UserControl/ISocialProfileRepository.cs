﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.GBLMaster.Master;
using AssistDB.UserControl;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.UserControl
{
    public interface ISocialProfileRepository : IGenericRepository<SocialProfile>
    {
        SocialProfile GetSProfileById(int S_ProfileID);
        IEnumerable<SocialProfile> GetAllS_Profile(int SubscriberID);
        SocialProfile CheckDuplicateSProfile(string S_ProfileName, int? SubscriberID, int S_ProfileID, string type);
    }
}
