﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.GBLMaster.Master;
using AssistDB.GBLMaster.Master.ViewData;
using AssistDB.UserControl;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.UserControl
{
    public interface IRoleMasterRepository : IGenericRepository<Role>
    {
        RoleData GetRoleById(int RoleID);
        IEnumerable<RoleData> GetAllRole(int SubscriberID);
        Role CheckDuplicateRole(string RoleName, int? SubscriberID, int RoleID, string type);
    }
}
