﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.GBLMaster.Master.ViewData;
using AssistDB.UserControl;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.UserControl
{
    public interface IProfileDetailsRepository : IGenericRepository<ProfileDetail>
    {
        ProfileDetailsData GetProfileDetailById(int ProfileID);
        IEnumerable<ProfileDetailsData> GetAllProfileDetail(int SubscriberID);
        ProfileDetail CheckDuplicateProfileDetail(string ProfileName, int? SubscriberID, int ProfileID, string type);
    }
}
