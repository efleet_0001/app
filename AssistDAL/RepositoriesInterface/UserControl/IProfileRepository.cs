﻿using AssistDAL.RepositoriesInterface.Common;
using AssistDB.GBLMaster.Master.ViewData;
using AssistDB.UserControl;
using System.Collections.Generic;

namespace AssistDAL.RepositoriesInterface.UserControl
{
    public interface IProfileRepository : IGenericRepository<Profile_D>
    {
        ProfileData GetProfileById(int ProfileID);
        IEnumerable<ProfileData> GetAllProfile(int SubscriberID);
        Profile_D CheckDuplicateProfile(string ProfileName, int? SubscriberID, int ProfileID, string type);
    }
}
