﻿using AssistDAL.Base;
using AssistDB.ViewModels;
using System.Collections.Generic;

namespace AssistDAL.Repository
{
    public interface IAgriDasboardRepository : IRepositoryBase<AgriDashboardModel>
    {
        IEnumerable<T> ExecuteStoreQuery<T>(string query, object[] paramters);
    }
}
