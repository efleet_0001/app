﻿using AssistDAL.Base;
using AssistDB;
using AssistDB.UserControl;
using AssistDB.ViewModels;

namespace AssistDAL.Repository
{
    public interface IUserRepository : IRepositoryBase<Usermaster>
    {
        // new specific methods need to implement
       // UsermasterData GetUserById(int UserID);
    }


}
