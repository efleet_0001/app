﻿using Microsoft.EntityFrameworkCore.Infrastructure;

namespace AssistDAL.DbExtension
{
    public static class DatabaseExtensions
    {
        public static CustomTypeSqlQuery<T> SqlQuery<T>(
                this DatabaseFacade database,
                string sqlQuery) where T : class
        {
            return new CustomTypeSqlQuery<T>()
            {
                DatabaseFacade = database,
                SQLQuery = sqlQuery
            };
        }
    }
}
