﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace AssistDAL.DbExtension
{
    public class DataReaderMapper where T : class
    {
        public List MapToList(DbDataReader dr)
        {
            if (dr != null & amp; &amp; dr.HasRows)
            {
                var entity = typeof(T);
                var entities = new List();
                var propDict = new Dictionary& lt; string, PropertyInfo&gt; ();
                var props = entity.GetProperties(BindingFlags.Instance | BindingFlags.Public);
                propDict = props.ToDictionary(p = &gt; p.Name.ToUpper(), p = &gt; p);

                T newObject = default(T);
                while (dr.Read())
                {
                    newObject = Activator.CreateInstance();

                    for (int index = 0; index & lt; dr.FieldCount; index++)
                    {
                        if (propDict.ContainsKey(dr.GetName(index).ToUpper()))
                        {
                            var info = propDict[dr.GetName(index).ToUpper()];
                            if ((info != null) & amp; &amp; info.CanWrite)
                            {
                                var val = dr.GetValue(index);
                                info.SetValue(newObject, (val == DBNull.Value) ? null : val, null);
                            }
                        }
                    }

                    entities.Add(newObject);
                }

                return entities;
            }

            return null;
        }
    }
}
