﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AssistDB.login
{
    namespace DB.login
    {
        public class FYModel
        {
            public FYModel()
            { }
            [Key]
            public int FYID { get; set; }
            public string FYName { get; set; }
            public string Abbreviation { get; set; }
            public DateTime? FromDate { get; set; }
            public DateTime? ToDate { get; set; }
            public string DefaultYear { get; set; }
            public string IsLock { get; set; }
        }
        public class SessionModel
        {
            public SessionModel() { }
            [Key]
            public int SessionID { get; set; }
            public int SubscriberID { get; set; }
            public int UserID { get; set; }
            public DateTime? StartTimeDate { get; set; }
            public DateTime? EndTimeDate { get; set; }
            public string IPAddress { get; set; }
        }
        public class loginModel
        {
            public loginModel() { }
            [Key]
            public long UserID { get; set; }
            public string First_Name { get; set; }
            public string Last_Name { get; set; }
            public string Email { get; set; }
            public long RoleID { get; set; }
            public long ProfileID { get; set; }
            public long LocaleID { get; set; }
            public string Alias { get; set; }
            public string Mobile { get; set; }
            public DateTime? DOB { get; set; }
        }
        public class Subscriber
        {
            public long SubscriberID { get; set; }
            public string Name { get; set; }
            public string Alias { get; set; }
            public string ImagePath { get; set; }
            public long AppID { get; set; }
            public long ClientID { get; set; }
        }
        public class MProfile
        {
            [Key]
            public long ProfileID { get; set; }
            public string Name { get; set; }
            public int TypeID { get; set; }
            public string TypeName { get; set; }
        }
        public class LProfileDetail
        {
            [Key]
            public long ProfileID { get; set; }
            public string BG_Image { get; set; }
            public string DP_Image { get; set; }
            public long CateID { get; set; }
            public string CateName { get; set; }
            public long SubCatID { get; set; }
            public string SubCatName { get; set; }
        }
        public class LocaleInfo
        {
            [Key]
            public long LocaleID { get; set; }
            public int LanguageID { get; set; }
            public string LanguageName { get; set; }
            public int CountryID { get; set; }
            public string CountryName { get; set; }
            public int StateID { get; set; }
            public string StateName { get; set; }
            public int DistrictID { get; set; }
            public string DistrictName { get; set; }
            public int Gender { get; set; }
            public string PinCode { get; set; }
            public string VillMName { get; set; }
            public string Latlong { get; set; }
            public string Currency { get; set; }
        }
    }
}
    

