﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AssistDB.Kisan.Trans.Data
{
   
    public class NewCategoryData
    {
        [Key]
        public long CatID { get; set; }
        public string CateName { get; set; }
    }
    public class EntityNewData
    {
        [Key]
        public long NewsID { get; set; }
        public int SorceTypeID { get; set; }
        public string SorceTypeName { get; set; }
        public int MediaSourceTypeID { get; set; }
        public string MediaSourceTypeName { get; set; }
        public DateTime Date { get; set; }
        public string SourceInfoName { get; set; }
        public string Headline { get; set; }
        public string NewsContent { get; set; }
        public string NewsByName { get; set; }
        public string Time { get; set; }
    }
}
