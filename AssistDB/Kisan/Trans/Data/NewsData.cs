﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistDB.Kisan.Trans.Data
{
    public class NewsData:News
    {
        public string CategoryName { get; set; }
        public string SourceTypeName { get; set; }
    }
}
