﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AssistDB.Kisan.Trans.Data
{
   public class MessageData
    {
        [Key]
        public long ID { get; set; }
        public string Name { get; set; }
        public string text { get; set; }
        public string time { get; set; }
    }
}
