﻿namespace AssistDB.Kisan.Trans
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Solution")]
    public class Solution : BaseEntity
    {
        public Solution() { }
        [Key]
        public long SolnID { get; set; }
        public long ProblemID { get; set; }
        public string SolutionTag { get; set; }
        public string Description { get; set; }
        public ProblemMaster problemMaster { get; set; }
    }
}
