﻿
namespace AssistDB.Kisan.Trans
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using AssistDB.BaseMaster;

    [Table("News")]
    public class News : BaseEntity
    {
        public News() { }
        [Key]
        public long NewsID { get; set; }
        public long CatID { get; set; }
        public int? SorceTypeID { get; set; }
        public DateTime? Date { get; set; }
        public string SourceInfoName { get; set; }
        public string Headline { get; set; }
        public int MediaSourceTypeID { get; set; }
        public string NewsContent { get; set; }
        public bool Approved { get; set; }
        public bool Read { get; set; }
        public int LanguageID { get; set; }
        public NewsCategory Cat { get; set; }
        public Constant SorceType { get; set; }
        public Constant MediaSourceType { get; set; }
    }
}
