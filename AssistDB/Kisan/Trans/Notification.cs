﻿

namespace AssistDB.Kisan.Trans
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using AssistDB.BaseMaster;

    [Table("Notification")]
    public class Notification : BaseEntity
    {
        public Notification() { }
        [Key]
        public long NotificationID { get; set; }
        public string Headline { get; set; }
        public int Type { get; set; }
        public string Notificationtext { get; set; }
        public string Imagepath { get; set; }
        public bool IsRead { get; set; }
        public int NotificationSourceType { get; set; }
        public long MemberID { get; set; }
        public Constant NotificationType { get; set; }
        public Constant NotificationSourceTypes { get; set; }
    }
}
