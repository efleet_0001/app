﻿namespace AssistDB.Kisan.Trans
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("Answer")]
    public  class Answer : BaseEntity
    {
        public Answer() { }
        [Key]
        public long AnsID { get; set; }
        public string AnswerText { get; set; }
        public string Anslike { get; set; }
        public string Ansdislike { get; set; }
        public string AnsComments { get; set; }
        public string Description { get; set; }
    }
}
