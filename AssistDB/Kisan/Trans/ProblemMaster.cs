﻿namespace AssistDB.Kisan.Trans
{
    using AssistDB.GBLMaster.Master;
    using AssistDB.Kisan.Master;
    using AssistDB.UserControl;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("ProblemMaster")]
    public class ProblemMaster : BaseEntity
    {
        public ProblemMaster() { }
        [Key]
        public long ProblemID { get; set; }
        public string ProblemName { get; set; }
        public long UserID { get; set; }
        public long? SubCateID { get; set; }
        public long? EntityID { get; set; }
        public string Description { get; set; }
        public Usermaster User { get; set; }
        public SubCategory SubCate { get; set; }
        public Entity Entity { get; set; }
        public Solution solution { get; set; }
    }
}
