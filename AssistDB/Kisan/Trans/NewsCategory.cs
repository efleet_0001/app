﻿
namespace AssistDB.Kisan.Trans
{
    using AssistDB.GBLMaster.Master;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using AssistDB.BaseMaster;

    [Table("NewsCategory")]
   public class NewsCategory:BaseEntity
    {
        public NewsCategory() { }
        [Key]
        public long NewsCatID { get; set; }
        public string NewsCatName { get; set; }
        public string NewsDiscription { get; set; }
        public int? CategoryID { get; set; }
        public Constant Category { get; set; }
        public News news { get; set; }
    }
}
