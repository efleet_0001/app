﻿namespace AssistDB.Kisan.Trans
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("MemberMapping")]
    public class MemberMapping : BaseEntity
    {
        public MemberMapping() { }
        [Key]
        public long MappingID { get; set; }
        public int MemberTypeId { get; set; }
        public long MemberId { get; set; }
        public long? CategoryId { get; set; }
        public long? SubCategoryId { get; set; }
        public long? EntityId { get; set; }
        public string Description { get; set; }
    }
}
