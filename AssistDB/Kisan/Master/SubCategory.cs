﻿namespace AssistDB.Kisan.Master
{
    using AssistDB.GBLMaster.Master;
    using AssistDB.Kisan.Trans;
    using AssistDB.UserControl;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("SubCategory")]
    public class SubCategory : BaseEntity
    {
        public SubCategory() { }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Subcat_ID { get; set; }
        public long? CateID { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(50)]
        public string Alias { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public Category Cate { get; set; }
        public ProfileDetail profileDetails { get; set; }
        public ProblemMaster problemMaster { get; set; }
    }
}
