﻿
namespace AssistDB.Kisan.Master
{
    using AssistDB.GBLMaster.Master;
    using AssistDB.Kisan.Trans;
    using AssistDB.UserControl;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Entity")]
    public  class Entity:BaseEntity
    {
        public Entity() { }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Ent_ID { get; set; }
        public long? CatID { get; set; }
        public long? SubCatID { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(50)]
        public string Alias { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public Category Cat { get; set; }
        public SubCategory SubCat { get; set; }
        public ProfileDetail profileDetails { get; set; }
        public ProblemMaster problemMaster { get; set; }
    }

  
}
