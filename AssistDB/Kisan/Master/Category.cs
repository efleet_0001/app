﻿namespace AssistDB.Kisan.Master
{
    using AssistDB.GBLMaster.Master;
    using AssistDB.UserControl;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("Category")]
    public  class Category : BaseEntity
    {
        public Category()
        {
            SubCategories = new HashSet<SubCategory>();
        }

        [Key]
        public long CatID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Alias { get; set; }

        [StringLength(500)]
        public string Description { get; set; }
                      
        public virtual ICollection<SubCategory> SubCategories { get; set; }
        public ProfileDetail profileDetails { get; set; }

    }
}
