﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistDB.Kisan.Master.Data
{
   public class EntityData :Entity
    {
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
    }
}
