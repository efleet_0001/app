﻿
namespace AssistDB.UserControl
{
    using AssistDB.Kisan.Master;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ProfileDetails")]
    public class ProfileDetail : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ProfileD_ID { get; set; }
        public long ProfileID { get; set; }
        public byte[] BG_Image { get; set; }
        public byte[] DP_Image { get; set; }
        public long? CateID { get; set; }
        public long? SubCatID { get; set; }
        public long? EntityID { get; set; }
        [StringLength(500)]
        public string Discription { get; set; }
        public Profile_D Profile { get; set; }
        public Category Cate { get; set; }
        public SubCategory SubCat { get; set; }
        public Entity Entity { get; set; }
    }
}
