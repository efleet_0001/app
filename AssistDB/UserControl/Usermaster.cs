﻿using AssistDB.UserControl;
using AssistDB.Kisan.Trans;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AssistDB.GBLMaster.Master;

namespace AssistDB.UserControl
{
    [Table("Usermaster")]
    public class Usermaster : BaseEntity
    {
        public Usermaster()
        {
            blocked = false;
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long UserID { get; set; }
        [StringLength(50)]
        public string First_Name { get; set; }
        [StringLength(50)]
        public string Last_Name { get; set; }
        [StringLength(50)]
        public string Email { get; set; }
        public long? RoleID { get; set; }
        public long? ProfileID { get; set; }
        public long? LocaleID { get; set; }
        [Required]
        [StringLength(50)]
        public string Alias { get; set; }
        [StringLength(50)]
        public string Phone { get; set; }
        [StringLength(50)]
        public string Mobile { get; set; }
        [StringLength(50)]
        public string Website { get; set; }
        [Column(TypeName = "date")]
        public DateTime? DOB { get; set; }
        public bool? EmailConfrmed { get; set; }
        [StringLength(50)]
        public string PasswordHash { get; set; }
        [StringLength(50)]
        public string SecurityStamp { get; set; }
        public bool? MobileNoConfirmed { get; set; }
        public bool? TwoFactorEnabled { get; set; }
        public bool? blocked { get; set; }
        public Role Role { get; set; }
        public Profile_D Profile { get; set; }
        public LocaleInformation Locale { get; set; }
        public ProblemMaster problemMaster { get; set; }

    }


}
