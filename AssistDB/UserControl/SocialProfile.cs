﻿namespace AssistDB.UserControl
{
    using AssistDB.BaseMaster;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("SocialProfile")]
    public  class SocialProfile:BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SocID { get; set; }
        public int? EntityTypeID { get; set; }
        public long? EntityID { get; set; }
        [StringLength(50)]
        public string SocialType { get; set; }
        [StringLength(50)]
        public string SocialID { get; set; }
        [StringLength(500)]
        public string Authtoken { get; set; }
        [StringLength(500)]
        public string Authurl { get; set; }
        [StringLength(500)]
        public string AuthPassword { get; set; }
        public Constant EntityType { get; set; }


    }
}
