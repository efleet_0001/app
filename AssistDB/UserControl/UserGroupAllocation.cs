﻿namespace AssistDB.UserControl
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("UserGroupAllocation")]
    public class UserGroupAllocation : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AllocationId { get; set; }
        public int GroupSourceTypeId { get; set; }
        public int GroupId { get; set; }
        public long RoleId { get; set; }
        public long UserId { get; set; }
        public bool Mapped { get; set; }
        public string Description { get; set; }
    }
}
