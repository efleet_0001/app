﻿namespace AssistDB.UserControl
{
    using AssistDB.General;
    using AssistDB.UserControl;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    [Table("Role")]
    public  class Role:BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long RoleID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public long? CloneID { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public Role Clone { get; set; }
        public CompanyDetails companyDetails { get; set; }
        public Usermaster usermaster { get; set; }

    }
}
