﻿namespace AssistDB.UserControl
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using AssistDB.BaseMaster;
    using AssistDB.UserControl;

    [Table("Profile")]
    public  class Profile_D:BaseEntity
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ProfileID { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        public int TypeID { get; set; }
        public long? CloneID { get; set; }
        [StringLength(500)]
        public string Discription { get; set; }
        public Constant Type { get; set; }
        public Profile_D Clone { get; set; }
        public ProfileDetail profileDetails { get; set; }
        public Usermaster usermaster { get; set; }

    }
}
