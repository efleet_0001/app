﻿namespace AssistDB.General
{
    using AssistDB.UserControl;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("CompanyDetails")]
    public class CompanyDetails : BaseEntity
    {
        public CompanyDetails() { }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long CompID { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public int EmpCount { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Website { get; set; }
        public long? RoleID { get; set; }
        public string RegNo { get; set; }
        public string PANNo { get; set; }
        public string TANNo { get; set; }
        public string Description { get; set; }
        public Role Role { get; set; }
    }
}
