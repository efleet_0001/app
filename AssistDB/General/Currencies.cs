﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistDB.General
{

    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("Currencies")]
    public class Currencies : BaseEntity
    {
        public Currencies() { }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int? CurrencyID { get; set; }
        public string Format { get; set; }
        public float ? ExchangeRate { get; set; }
        public int? Symbol { get; set; }
        public int? ThousandSeprator { get; set; }
        public int? DecimalPlace { get; set; }
        public string DecimalSeprator { get; set; }
        public string Description { get; set; }
    }
}
