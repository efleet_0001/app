﻿namespace AssistDB.General
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Holiday")]
    public  class Holiday:BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long HoliDayID { get; set; }

        [StringLength(50)]
        public string HolidayName { get; set; }
        [Column(TypeName = "date")]
        public DateTime? HolidayDate { get; set; }
        public int DistrictID { get; set; }
        public string Description { get; set; }

    }
}
