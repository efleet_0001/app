﻿namespace AssistDB.General
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("BusinessHour")]
    public class BusinessHour : BaseEntity
    {
        public BusinessHour() { }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int? TypeID { get; set; }
        public int? DayID { get; set; }
        public int? StartDayID { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Description { get; set; }
    }
   
}
