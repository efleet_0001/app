﻿

namespace AssistDB.BaseMaster
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("ConstantGroup")]
    public  class ConstantGroup
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ConsGrpID { get; set; }

        public int? ConsGrpName { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public bool? StatusID { get; set; }
    }
}
