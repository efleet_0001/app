﻿namespace AssistDB.BaseMaster
{
    using AssistDB.GBLMaster.Master;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("District")]
   public class District
    {
        public District() { }
        [Key]
        public int DistrictID { get; set; }
        public string DistrictName { get; set; }
        public string Districthood { get; set; }
        public string Code { get; set; }
        public string Capital { get; set; }
        public string LargestCity { get; set; }
        public string Population { get; set; }
        public string Area { get; set; }
        public string Languge { get; set; }
        public int StateID { get; set; }
        public string Discription { get; set; }
        public int Status { get; set; }
        public Contact contact { get; set; }
        public LocaleInformation localeInformation { get; set; }
    }
}
