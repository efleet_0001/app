﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistDB.BaseMaster
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("GBLApplication")]
    public class Application : BaseEntity
    {
        public Application() { }
        [Key]
        public int ApplicationId { get; set; }
        public int AppTypeId { get; set; }
        public int AppNatureId { get; set; }
        public string AppName { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }

    }
}
