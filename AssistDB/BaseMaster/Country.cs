﻿namespace AssistDB.BaseMaster
{
    using AssistDB.GBLMaster.Master;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("Country")]
    public  class Country
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContryID { get; set; }

        [StringLength(50)]
        public string CountryName { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public bool? StatusID { get; set; }
        public Contact contact { get; set; }
        public LocaleInformation localeInformation { get; set; }
    }
}
