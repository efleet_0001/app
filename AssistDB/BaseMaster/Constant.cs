﻿

namespace AssistDB.BaseMaster
{
    using AssistDB.GBLMaster.Master;
    using AssistDB.Kisan.Trans;
    using AssistDB.UserControl;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("Constant")]
    public  class Constant
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ConstantID { get; set; }

        public int? ConsGroupID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Alias { get; set; }
        public Profile_D profile_D { get; set; }
        public SocialProfile socialProfile { get; set; }
        public NewsCategory newsCategory { get; set; }
        public News news { get; set; }
        public Notification notification { get; set; }
        public Notification notification1 { get; set; }
    }
}
