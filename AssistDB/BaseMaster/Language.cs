﻿namespace AssistDB.BaseMaster
{
    using System;
    using AssistDB.GBLMaster.Master;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("Language")]
    public  class Language
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LanID { get; set; }

        [StringLength(50)]
        public string Culture { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public bool? StatusID { get; set; }
        public LocaleInformation localeInformation { get; set; }
    }
}
