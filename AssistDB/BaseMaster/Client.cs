﻿namespace AssistDB.BaseMaster
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("GBLClient")]
    public class Client : BaseEntity
    {
        public Client() { }
        [Key]
        public int ClientID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

    }
}
