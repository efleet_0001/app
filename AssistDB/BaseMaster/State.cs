﻿using AssistDB.GBLMaster.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AssistDB.BaseMaster
{
    [Table("State")]
    public class State
    {
        public State() { }
        public int StateID { get; set; }
        public string StateName { get; set; }
        public DateTime? Statehood { get; set; }
        public string Code { get; set; }
        public string Capital { get; set; }
        public string LargestCity { get; set; }
        public string Population { get; set; }
        public string Area { get; set; }
        public string Languge { get; set; }
        public string Discription { get; set; }
        public bool Status { get; set; }
        public int CoutryID { get; set; }
        public Contact contact { get; set; }
        public LocaleInformation localeInformation { get; set; }
    }
}
