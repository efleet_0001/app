﻿
namespace AssistDB.Customization
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using AssistDB.BaseMaster;

    [Table("Action")]
    public class Action:BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ActionId { get; set; }
        public string ActionName { get; set; }
        public int? ActionTypeId { get; set; }
        public int? TemplateId { get; set; }
        public string Description { get; set; }
    }
}
