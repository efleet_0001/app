﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssistDB
{
    public class BaseEntity
    {
        public BaseEntity() {
            StatusID = 0;
            CreatedDate = DateTime.Now;
            Rowguid = Guid.NewGuid().ToString();
            
        }
        public int? StatusID { get; set; }
        public long? CSID { get; set; }
        public long? MSID { get; set; }
        public long? SubscriberID { get; set; }
        public long? CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }
        [StringLength(500)]
        public string Rowguid { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }


    }
}
