﻿namespace AssistDB.GBLMaster.Trans
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
   
    [Table("ImageData")]
    public  class ImageUpload:BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ImageID { get; set; }
        public long TransctionID { get; set; }
        public string Name { get; set; }
        public int ImageTypeID { get; set; }
        public string Path { get; set; }
        public string Discription { get; set; }
    }
}
