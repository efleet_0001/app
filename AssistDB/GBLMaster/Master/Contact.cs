﻿namespace AssistDB.GBLMaster.Master
{
    using AssistDB.BaseMaster;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Contact")]
    public class Contact : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ConID { get; set; }
        [StringLength(500)]
        public string Street { get; set; }
        [StringLength(50)]
        public string City { get; set; }
        public int? DistrictID { get; set; }
        public int? StateID { get; set; }
        public int? CountryID { get; set; }
        [StringLength(50)]
        public string Zip_Code { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public District District { get; set; }
        public State State { get; set; }
        public Country Country { get; set; }
    }
}
