﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistDB.GBLMaster.Master.ViewData
{
   public class ContactData:Contact
    {
        public string DistrictName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
    }
}
