﻿using AssistDB.UserControl;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistDB.GBLMaster.Master.ViewData
{
   public class ProfileDetailsData:ProfileDetail
    {
        public string ProfileName { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string EntityName { get; set; }
    }
}
