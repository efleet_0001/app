﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistDB.GBLMaster.Master.ViewData
{
   public class LocaleInformationData:LocaleInformation
    {
        public string LanguageName { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string DistrictName { get; set; }
    }
}
