﻿using AssistDB.UserControl;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistDB.GBLMaster.Master.ViewData
{
   public class ProfileData:Profile_D
    {
        public string TypeName { get; set; }
        public string CloneName { get; set; }
    }
}
