﻿namespace AssistDB.GBLMaster.Master
{
    using AssistDB.BaseMaster;
    using AssistDB.UserControl;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("LocaleInformation")]
    public class LocaleInformation : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long LocID { get; set; }
        public int? LanguageID { get; set; }
        public int? CountryID { get; set; }
        public int? StateID { get; set; }
        public int? DistrictID { get; set; }
        public int? Gender { get; set; }
        public string PinCode { get; set; }
        public string VillMName { get; set; }
        public string Latlong { get; set; }
        public int? TimeFormat { get; set; }
        public string Time_Zone { get; set; }
        [StringLength(50)]
        public string Currency { get; set; }
        [StringLength(500)]
        public string Signature { get; set; }
        public string AdharNo { get; set; }
        public string VoterNo { get; set; }
        public string PANNo { get; set; }
        public Language Language { get; set; }
        public Country Country { get; set; }
        public State State { get; set; }
        public District District { get; set; }
        public Usermaster usermaster { get; set; }
    }
}
    