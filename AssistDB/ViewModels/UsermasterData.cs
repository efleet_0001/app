﻿using AssistDB.UserControl;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistDB.ViewModels
{
   public class UsermasterData :Usermaster
    {
        public string RoleName { get; set; }
        public string ProfileName { get; set; }
        public string LocaleInfo { get; set; }
    }
}
