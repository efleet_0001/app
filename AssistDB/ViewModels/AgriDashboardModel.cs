﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AssistDB.ViewModels
{
    public class AgriDashboardModel:BaseEntity
    {
        public AgriDashboardModel()
        { }
        [Key]
        public int EntityID { get; set; }
        public string EntiyName { get; set; }
        public int SubCategoryID { get; set; }
        public string SubCategoryName { get; set; }
             
    }
}
