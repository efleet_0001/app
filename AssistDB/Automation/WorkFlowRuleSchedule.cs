﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AssistDB.BaseMaster;

namespace AssistDB.Automation
{
    [Table("WorkFlowRuleSchedule")]
    public class WorkFlowRuleSchedule:BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ScheduleId { get; set; }
        public string ScheduleName { get; set; }
        public long RuleId { get; set; }
        public int ActiontypeId { get; set; }
        public int SubActionTypeId { get; set; }
        public string Description { get; set; }
    }
}
