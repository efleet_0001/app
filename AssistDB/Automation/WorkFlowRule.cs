﻿
namespace AssistDB.Automation
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
   
    [Table("WorkFlowRule")]
    public class WorkFlowRule : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long RuleId { get; set; }
        public int SubscrberFormId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
