﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssistDB.Automation
{
    [Table("WorkFlowScheduleCondition")]
    public class WorkFlowScheduleCondition : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ConditionId { get; set; }
        public long ScheduleId { get; set; }
        public long RuleId { get; set; }
        public int FormId { get; set; }
        public int FieldId { get; set; }
        public int ConditionTypeId { get; set; }
        public int Description { get; set; }
    }
}
