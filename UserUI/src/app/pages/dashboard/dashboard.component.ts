import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { AppConfig } from '../../app.config';
import { DashboardService } from './dashboard.service';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'az-dashboard',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [ DashboardService ] 
})
export class DashboardComponent implements OnInit { 
    public config:any;
    public configFn:any; 
    public bgColor:any;
    public date = new Date(); 
    public weatherData:any;
    private res: any;
    public form: FormGroup;
    public chatForm: FormGroup;
    image: string;
    name: string;
    totalRes: any = [];

    //
    showHide: boolean = false;
    //form:any=[];
    viewMode = 'tab1';
  

    constructor(private _appConfig:AppConfig, private _dashboardService:DashboardService, private fb:FormBuilder){
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;
        this.weatherData = _dashboardService.getWeatherData();
    } 

    ngOnInit(){
      this.getslider();

      this.form = this.fb.group({
        index: [{value: null, disabled:true}],
        fullname: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]]
      });

    }

    getslider(){
      this.totalRes = [
        {
          image: "./assets/img/organic-banner1.jpg"
        },
        {
          image: "./assets/img/organic-slide-2.jpg"
        },
        {
          image: "./assets/img/organic-banner1.jpg"
        },
        {
          image: "./assets/img/organic-banner1.jpg"
        },
        {
          image: "./assets/img/organic-slide-2.jpg"
        },
        {
          image: "./assets/img/organic-banner1.jpg"
        },
        {
          image: "./assets/img/organic-banner1.jpg"
        },
        {
          image: "./assets/img/organic-slide-2.jpg"
        },
        {
          image: "./assets/img/organic-banner1.jpg"
        }

      ]
    }

     // chat 
  showLiveChat() {
    this.showHide = !this.showHide;
  }
 

}
