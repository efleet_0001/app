import { Injectable } from '@angular/core';
import { AppConfig } from "../../../app.config";

@Injectable()
export class DataMapService {
    constructor(public _appConfig:AppConfig){
        this._appConfig = _appConfig;   
    }

    data = {
        "201910": {
          "Madhubani": {
            "users": 100,
            "fillKey": "Madhubani"
          },
          "Darbhanga": {
            "users": 10,
            "fillKey": "Darbhanga"
          }
        },
        "201911": {
          "Madhubani": {
            "users": 200,
            "fillKey": "Madhubani"
          },
          "Darbhanga": {
            "users": 100,
            "fillKey": "Darbhanga"
          }
        },
        "201912": {
          "Madhubani": {
            "users": 500,
            "fillKey": "Madhubani"
          },
          "Darbhanga": {
            "users": 200,
            "fillKey": "Darbhanga"
          }
        },
       "202001": {
          "Madhubani": {
            "users": 5000,
            "fillKey": "Madhubani"
          },
          "Darbhanga": {
            "users": 1100,
            "fillKey": "Darbhanga"
          }
        },
        "202002": {
          "Madhubani": {
            "users": 5000,
            "fillKey": "Madhubani"
          },
          "Darbhanga": {
            "users": 1100,
            "fillKey": "Darbhanga"
          }
        },
        "202003": {
          "Madhubani": {
            "users": 5000,
            "fillKey": "Madhubani"
          },
          "Darbhanga": {
            "users": 1100,
            "fillKey": "Darbhanga"
          }
        },
       "202004": {
          "Madhubani": {
            "users": 5000,
            "fillKey": "Madhubani"
          },
          "Darbhanga": {
            "users": 1100,
            "fillKey": "Darbhanga"
          }
        }
      };


    bubbles = {
        "2010": [
          {
            "name": "Madubani",
            "users": 1200,
            "latitude": 26.347883,
            "longitude": 86.071861,
            "radius": 11,
            "fillKey": "success"
          },
          {
            "name": "Darbhanga",
            "users": 402,
            "latitude": 26.152973,
            "longitude": 85.901413,
            "radius": 8,
            "fillKey": "info"
          },
          {
            "name": "Sitamarhi",
            "users": 882,
            "latitude": 26.6,
            "longitude": 85.4833,
            "radius": 7,
            "fillKey": "danger"
          },
          {
            "name": "Muzaffarpur",
            "users": 980,
            "latitude": 26.121473,
            "longitude": 85.368752,
            "radius": 9,
            "fillKey": "primary"
          },
          {
            "name": "Supaul",
            "users": 120,
            "latitude": 27.282644,
            "longitude": 84.599503,
            "radius": 5,
            "fillKey": "warning"
          }
        ],
        "2011": [
          {
            "name": "Madubani",
            "users": 1200,
            "latitude": 26.347883,
            "longitude": 86.071861,
            "radius": 11,
            "fillKey": "success"
          },
          {
            "name": "Darbhanga",
            "users": 402,
            "latitude": 26.152973,
            "longitude": 85.901413,
            "radius": 8,
            "fillKey": "info"
          },
          {
            "name": "Sitamarhi",
            "users": 882,
            "latitude": 26.6,
            "longitude": 85.4833,
            "radius": 7,
            "fillKey": "danger"
          },
          {
            "name": "Muzaffarpur",
            "users": 980,
            "latitude": 26.121473,
            "longitude": 85.368752,
            "radius": 9,
            "fillKey": "primary"
          },
          {
            "name": "Supaul",
            "users": 120,
            "latitude": 27.282644,
            "longitude": 84.599503,
            "radius": 5,
            "fillKey": "warning"
          }
        ],
        "2012": [
          {
            "name": "Madubani",
            "users": 1200,
            "latitude": 26.347883,
            "longitude": 86.071861,
            "radius": 11,
            "fillKey": "success"
          },
          {
            "name": "Darbhanga",
            "users": 402,
            "latitude": 26.152973,
            "longitude": 85.901413,
            "radius": 8,
            "fillKey": "info"
          },
          {
            "name": "Sitamarhi",
            "users": 882,
            "latitude": 26.6,
            "longitude": 85.4833,
            "radius": 7,
            "fillKey": "danger"
          },
          {
            "name": "Muzaffarpur",
            "users": 980,
            "latitude": 26.121473,
            "longitude": 85.368752,
            "radius": 9,
            "fillKey": "primary"
          },
          {
            "name": "Supaul",
            "users": 120,
            "latitude": 27.282644,
            "longitude": 84.599503,
            "radius": 5,
            "fillKey": "warning"
          }
        ],
        "2013": [
          {
            "name": "Madubani",
            "users": 1200,
            "latitude": 26.347883,
            "longitude": 86.071861,
            "radius": 11,
            "fillKey": "success"
          },
          {
            "name": "Darbhanga",
            "users": 402,
            "latitude": 26.152973,
            "longitude": 85.901413,
            "radius": 8,
            "fillKey": "info"
          },
          {
            "name": "Sitamarhi",
            "users": 882,
            "latitude": 26.6,
            "longitude": 85.4833,
            "radius": 7,
            "fillKey": "danger"
          },
          {
            "name": "Muzaffarpur",
            "users": 980,
            "latitude": 26.121473,
            "longitude": 85.368752,
            "radius": 9,
            "fillKey": "primary"
          },
          {
            "name": "Supaul",
            "users": 120,
            "latitude": 27.282644,
            "longitude": 84.599503,
            "radius": 5,
            "fillKey": "warning"
          }
        ],
        "2014": [
          {
            "name": "Madubani",
            "users": 1200,
            "latitude": 26.347883,
            "longitude": 86.071861,
            "radius": 11,
            "fillKey": "success"
          },
          {
            "name": "Darbhanga",
            "users": 402,
            "latitude": 26.152973,
            "longitude": 85.901413,
            "radius": 8,
            "fillKey": "info"
          },
          {
            "name": "Sitamarhi",
            "users": 882,
            "latitude": 26.6,
            "longitude": 85.4833,
            "radius": 7,
            "fillKey": "danger"
          },
          {
            "name": "Muzaffarpur",
            "users": 980,
            "latitude": 26.121473,
            "longitude": 85.368752,
            "radius": 9,
            "fillKey": "primary"
          },
          {
            "name": "Supaul",
            "users": 120,
            "latitude": 27.282644,
            "longitude": 84.599503,
            "radius": 5,
            "fillKey": "warning"
          }
        ],
        "2015": [
          {
            "name": "Madubani",
            "users": 1200,
            "latitude": 26.347883,
            "longitude": 86.071861,
            "radius": 11,
            "fillKey": "success"
          },
          {
            "name": "Darbhanga",
            "users": 402,
            "latitude": 26.152973,
            "longitude": 85.901413,
            "radius": 8,
            "fillKey": "info"
          },
          {
            "name": "Sitamarhi",
            "users": 882,
            "latitude": 26.6,
            "longitude": 85.4833,
            "radius": 7,
            "fillKey": "danger"
          },
          {
            "name": "Muzaffarpur",
            "users": 980,
            "latitude": 26.121473,
            "longitude": 85.368752,
            "radius": 9,
            "fillKey": "primary"
          },
          {
            "name": "Supaul",
            "users": 120,
            "latitude": 27.282644,
            "longitude": 84.599503,
            "radius": 5,
            "fillKey": "warning"
          }
        ],
        "2016": [
          {
            "name": "Madubani",
            "users": 1200,
            "latitude": 26.347883,
            "longitude": 86.071861,
            "radius": 11,
            "fillKey": "success"
          },
          {
            "name": "Darbhanga",
            "users": 402,
            "latitude": 26.152973,
            "longitude": 85.901413,
            "radius": 8,
            "fillKey": "info"
          },
          {
            "name": "Sitamarhi",
            "users": 882,
            "latitude": 26.6,
            "longitude": 85.4833,
            "radius": 7,
            "fillKey": "danger"
          },
          {
            "name": "Muzaffarpur",
            "users": 980,
            "latitude": 26.121473,
            "longitude": 85.368752,
            "radius": 9,
            "fillKey": "primary"
          },
          {
            "name": "Supaul",
            "users": 120,
            "latitude": 27.282644,
            "longitude": 84.599503,
            "radius": 5,
            "fillKey": "warning"
          }
        ]
      };


    public getData():Object {
        return this.data;
    }

    public getBubbles():Object {
        return this.bubbles;
    }



}