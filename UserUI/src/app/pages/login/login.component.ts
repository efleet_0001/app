import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { environment } from 'src/environments/environment';
import { HttpService  } from 'src/app/common/http.service';
import { AuthGuard  } from 'src/app/auth-guard';

@Component({
  selector: 'az-login',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent  {
    viewModel: any = {};
    public router: Router;
    public form: FormGroup;
    public email: AbstractControl;
    public password: AbstractControl;
    public loginkeep: AbstractControl;

    constructor(router: Router, fb: FormBuilder, private _httpservice: HttpService,private _AuthGuard: AuthGuard) {
        this.router = router;
        this.form = fb.group({
            'email': ['', Validators.compose([Validators.required, Validators.minLength(10)])],
            'password': ['', Validators.compose([Validators.required, Validators.minLength(6)])],
            'loginkeep':['','']
        });

        this.email = this.form.controls['email'];
        this.password = this.form.controls['password'];
        this.loginkeep = this.form.controls['loginkeep'];
    }
    Reset() {
      if(this.form.value.loginkeep==true)
      {
        sessionStorage.clear();
      }
      else {
        sessionStorage.clear();
        localStorage.clear();
        localStorage.removeItem('userID');
        localStorage.removeItem('alias');
        localStorage.removeItem('profileID');
        localStorage.removeItem('localeID');
        localStorage.removeItem('subscriberID');
        localStorage.removeItem('login');
        localStorage.removeItem('subscriber');
        localStorage.removeItem('profile');
        localStorage.removeItem('profiledetails');
        localStorage.removeItem('locale');
      }
     
    }
    public onSubmit(values: Object): void {
        if (this.form.valid) {
           this.viewModel = {
             username: this.form.value.email,
             password: this.form.value.password
           }
           this.Reset();
             console.log(this.viewModel);
             this._httpservice.create(environment.apiUrl + 'login/Getlogin/', this.viewModel ).subscribe(data => {
               if (data) {
                var getlogin: any = {}
                console.log(data);
                getlogin = data;
                localStorage.setItem('userID', getlogin.userID);
                localStorage.setItem('alias', getlogin.alias);
                localStorage.setItem('profileID', getlogin.profileID);
                localStorage.setItem('localeID', getlogin.localeID);
                localStorage.setItem('login', JSON.stringify(data));
                this.Subscriber();
                this.Profile();
                this.ProfileDetail();
                this.LocaleInfo();
                this._AuthGuard.canActivate();            
                this.router.navigate(['pages/dashboard']);
              }

            });
            console.log(this.form.value);
            
            
        }
    }
    Subscriber() {
    var  userid =localStorage.getItem("userID");
    this._httpservice.get(environment.apiUrl + 'login/GetSubscriber/' + userid ).subscribe(data => {
      if (data) {
        var getSubscriber: any = {}
        console.log(data);
        getSubscriber = data;
       localStorage.setItem('subscriberID', getSubscriber.subscriberID);
       localStorage.setItem('subscriber', JSON.stringify(getSubscriber));
     }

   });
   }
   Profile() {
    var profileID = localStorage.getItem("profileID");
    this._httpservice.get(environment.apiUrl + 'login/GetMProfile/' + profileID ).subscribe(data => {
      if (data) {
        var getMProfile: any = {}
        console.log(data);
        getMProfile = data;
       localStorage.setItem('profile', JSON.stringify(getMProfile));
     }
     else {
      this.router.navigate(['pages/settings/timeline']);
     }


   });
   }
   ProfileDetail() {
    var profileID = localStorage.getItem("profileID");
    this._httpservice.get(environment.apiUrl + 'login/ProfileDetail/' + profileID ).subscribe(data => {
      if (data) {
        var profileDetail: any = {}
        console.log(data);
        profileDetail = data;
       localStorage.setItem('profiledetails', JSON.stringify(profileDetail));
     }
     else {
      this.router.navigate(['pages/settings/timeline']);
     }

   });
   }
   LocaleInfo() {
    var localeID =localStorage.getItem("localeID"); 
    this._httpservice.get(environment.apiUrl + 'login/GetLocaleInfo/' + localeID ).subscribe(data => {
      if (data) {
        var getLocaleInfo: any = {}
        console.log(data);
        getLocaleInfo = data;
       localStorage.setItem('locale', JSON.stringify(getLocaleInfo));
     }

   });
   }
   public OnInit() {
   this.Reset();
   this._AuthGuard.canActivate();
  }

  signOut(): void {
   this.Reset();
   this.router.navigate(['login']);
   this._AuthGuard.canActivate();
  }
}

export function emailValidator(control: FormControl): {[key: string]: any} {
    var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    if (control.value && !emailRegexp.test(control.value)) {
        return {invalidEmail: true};
    }
}
