import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { ProductDescritionComponent } from './inputs/product-descrition.component';
import { WizardComponent } from './wizard/wizard.component';
import { WizardValidationService } from './wizard/wizard-validation.service';
import { NgxGalleryModule } from 'ngx-gallery';
import { TabsModule } from 'ngx-bootstrap';

export const routes = [
  { path: '', redirectTo: 'product-description', pathMatch: 'full'},
  { path: 'product-description', component: ProductDescritionComponent, data: { breadcrumb: 'product-description' } },
  {
    path: 'multiwidget',
    component: WizardComponent,
    data: {
      breadcrumb: 'multiwidget'
    }
  }

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MultiselectDropdownModule,
    DirectivesModule,    
    RouterModule.forChild(routes),
    NgxGalleryModule,
    TabsModule.forRoot()
  ],
  declarations: [
    ProductDescritionComponent, WizardComponent
  ],
  providers: [
    WizardValidationService
  ]
})
export class ProductItemsModule { }
