import { Component, ViewEncapsulation } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
@Component({
  selector: 'az-oats',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './oats.component.html',
  styleUrls: ['./oats.component.scss'],
})
export class OatsComponent{
  public counter : number = 1;
  galleryOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];
    productnote: any;
 constructor(){
  this.productnote = [
    {
      "title": "Product Description",
      "description": "This Samsung double-door refrigerator, with its plethora of innovative features, can keep your vegetables and fruits garden-fresh, and your beverages ice-cold for a long duration. With its large storage space, this refrigerator provides more than enough room to accommodate almost everything you love to eat - desserts, fruits, vegetables, dairy products and meat"
    },
    {
      "title": "Recessed Handle",
      "description": "The recessed handle, along with the hidden hinges, not only offers a touch of elegance to this Samsung double-door fridge, but it also adds to your convenience."
    },
    {
      "title": "Twist Ice Maker",
      "description": "This Samsung double-door refrigerator, with its plethora of innovative features, can keep your vegetables and fruits garden-fresh, and your beverages ice-cold for a long duration. With its large storage space, this refrigerator provides more than enough room to accommodate almost everything you love to eat - desserts, fruits, vegetables, dairy products and meat"
    }
  ]

 }

 ngOnInit(){
  this.galleryOptions = [
    {
        width: '100%',
        height: '400px',
        thumbnailsColumns: 3,
        imageAnimation: NgxGalleryAnimation.Slide
    },
    // max-width 800
    {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 100,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
    },
    // max-width 400
    {
        breakpoint: 400,
        preview: false
    }
];
this.galleryImages = [
  {
    small: '../../../assets/img/cereals/oats/oats_s_1.jpg',
    medium: '../../../assets/img/cereals/oats/oats_m_1.jpg',
    big: '../../../assets/img/cereals/oats/oats_l_1.jpg'
  },
  {
    small: '../../../assets/img/cereals/oats/oats_s_2.jpg',
    medium: '../../../assets/img/cereals/oats/oats_m_2.jpg',
    big: '../../../assets/img/cereals/oats/oats_l_2.jpg'
  },
  {
    small: '../../../assets/img/cereals/oats/oats_s_3.jpg',
    medium: '../../../assets/img/cereals/oats/oats_m_3.jpg',
    big: '../../../assets/img/cereals/oats/oats_l_3.jpg'
  }
];

}
 
increment(){
  this.counter += 1;
}

decrement(){
  this.counter -= 1;

  if(this.counter < 1){
    window.location.reload();
  }
}
}