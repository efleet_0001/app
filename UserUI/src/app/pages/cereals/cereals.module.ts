import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { PipesModule } from '../../theme/pipes/pipes.module';
import { PaddyComponent } from './paddy/paddy.component';
import { BarelyComponent } from './barely/barely.component';
import { OatsComponent } from './oats/oats.component';
import { TriticaleComponent } from './triticale/triticale.component';
import { WheatComponent } from './wheat/wheat.component';
import { NgxGalleryModule } from 'ngx-gallery';
import { TabsModule } from 'ngx-bootstrap';


export const routes = [
  { path: '', redirectTo: 'paddy', pathMatch: 'full' },
  { path: 'paddy', component: PaddyComponent, data: { breadcrumb: 'Paddy' } },
  { path: 'barely', component: BarelyComponent, data: { breadcrumb: 'barely' } },
  { path: 'oats', component: OatsComponent, data: { breadcrumb: 'oats' } },
  { path: 'triticale', component: TriticaleComponent, data: { breadcrumb: 'triticale' } },
  { path: 'wheat', component: WheatComponent, data: { breadcrumb: 'wheat' } },
  
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DirectivesModule,
    PipesModule,
    RouterModule.forChild(routes),
    NgxGalleryModule,
    TabsModule.forRoot()
  ],
  declarations: [
    PaddyComponent,
    BarelyComponent,
    OatsComponent,
    TriticaleComponent,
    WheatComponent
  ]
})

export class CerealsModule { }
