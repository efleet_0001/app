import { Component, ViewEncapsulation } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { environment } from 'src/environments/environment';
import { HttpService } from 'src/app/common/http.service';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { AlertComponent } from 'ngx-bootstrap';
@Component({
  selector: 'az-paddy',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './paddy.component.html',
  styleUrls: ['./paddy.component.scss'],
})
export class PaddyComponent {
  public counter: number = 1;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  newcategory: any;
  newsdetails: any;
  userid = localStorage.getItem("userID");
  subscriberid = localStorage.getItem("subscriberID");
  languageid = 2;
  entityid=20;
  constructor(private _httpservice: HttpService) {
    // this.productnote = [
    //   {
    //     "title": "Product Description",
    //     "description": "This Samsung double-door refrigerator, with its plethora of innovative features, can keep your vegetables and fruits garden-fresh, and your beverages ice-cold for a long duration. With its large storage space, this refrigerator provides more than enough room to accommodate almost everything you love to eat - desserts, fruits, vegetables, dairy products and meat"
    //   },
    //   {
    //     "title": "Corp News",
    //     "description": "The recessed handle, along with the hidden hinges, not only offers a touch of elegance to this Samsung double-door fridge, but it also adds to your convenience."
    //   },
    //   {
    //     "title": "Machinery News",
    //     "description": "This Samsung double-door refrigerator, with its plethora of innovative features, can keep your vegetables and fruits garden-fresh, and your beverages ice-cold for a long duration. With its large storage space, this refrigerator provides more than enough room to accommodate almost everything you love to eat - desserts, fruits, vegetables, dairy products and meat"
    //   },
    //   {
    //     "title": "Business News",
    //     "description": "This Samsung double-door refrigerator, with its plethora of innovative features, can keep your vegetables and fruits garden-fresh, and your beverages ice-cold for a long duration. With its large storage space, this refrigerator provides more than enough room to accommodate almost everything you love to eat - desserts, fruits, vegetables, dairy products and meat"
    //   }
    // ]

  }


  ngOnInit() {
  this.imageload();
  this.getnewcategorydetail();
 
  }
 imageload() {
  this.galleryOptions = [
    {
      width: '100%',
      height: '400px',
      thumbnailsColumns: 3,
      imageAnimation: NgxGalleryAnimation.Slide
    },
    // max-width 800
    {
      breakpoint: 800,
      width: '100%',
      height: '600px',
      imagePercent: 100,
      thumbnailsPercent: 20,
      thumbnailsMargin: 20,
      thumbnailMargin: 20
    },
    // max-width 400
    {
      breakpoint: 400,
      preview: false
    }
  ];
  this.galleryImages = [
    {
      small: '../../../assets/img/cereals/paddy/paddy_s_1.jpg',
      medium: '../../../assets/img/cereals/paddy/paddy_m_1.jpg',
      big: '../../../assets/img/cereals/paddy/paddy_l_1.jpg'
    },
    {
      small: '../../../assets/img/cereals/paddy/paddy_s_2.jpg',
      medium: '../../../assets/img/cereals/paddy/paddy_m_2.jpg',
      big: '../../../assets/img/cereals/paddy/paddy_l_2.jpg'
    },
    {
      small: '../../../assets/img/cereals/paddy/paddy_s_3.jpg',
      medium: '../../../assets/img/cereals/paddy/paddy_m_3.jpg',
      big: '../../../assets/img/cereals/paddy/paddy_l_3.jpg'
    }
  ];
 }
  increment() {
    this.counter += 1;
  }

  decrement() {
    this.counter -= 1;

    if (this.counter < 1) {
      window.location.reload();
    }
  }
  getnewcategorydetail() {
    this._httpservice.get(environment.apiUrl + 'News/GetEntityNewsCat/' + this.subscriberid + '/' + this.languageid + '/' + this.userid + '/'+this.entityid).subscribe(data => {
      if (data) {
        this.newcategory = data;
        console.log("newcategory",data);
      }
    });
  }
  getnewsdetails() {
    this._httpservice.get(environment.apiUrl + 'News/GetEntityNewsby/'+ this.newcategory.catID ? this.newcategory.catID : 0+'/'+this.subscriberid ? this.subscriberid : 0+'/'+this.languageid ? this.languageid : 0+'/'+ this.userid ? this.userid : 0+'/'+this.entityid ? this.entityid : 0).subscribe(data => {
      if (data) {
        this.newsdetails = data;
        console.log("newcategory",data);
      }
    });
  }
  getdetails(catID) {
    alert("hi");
  }
}