import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoogleLoginProvider, FacebookLoginProvider, AuthService  } from 'angularx-social-login';
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { PipesModule } from '../../theme/pipes/pipes.module';
import { SignupComponent } from './signup.component';
import { WizardComponent } from './Details/wizard.component';

export const routes = [
  { path: '', component: SignupComponent, pathMatch: 'full' },
  {    path: 'wizard', component: WizardComponent,pathMatch: 'full'  }
];
export function socialConfigs() {  
  const config = new AuthServiceConfig(  
    [  
      {  
        id: FacebookLoginProvider.PROVIDER_ID,  
        provider: new FacebookLoginProvider('496492134346373')  
      },  
      {  
        id: GoogleLoginProvider.PROVIDER_ID,  
        provider: new GoogleLoginProvider('97419153982-fmogtd5gh490u8q9bt2r1qheumhhob44.apps.googleusercontent.com')  
      }  
    ]  
  );  
  return config;  
}  

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DirectivesModule,
    PipesModule,
    SocialLoginModule,
    RouterModule.forChild(routes)
  ],
  providers: [  
    AuthService,  
    {  
      provide: AuthServiceConfig,  
      useFactory: socialConfigs  
    }  
  ],
  declarations: [ SignupComponent, WizardComponent ]
})
export class SignupModule { }
