import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { GoogleLoginProvider, FacebookLoginProvider, AuthService  } from 'angularx-social-login';
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { environment } from 'src/environments/environment';
import { HttpService } from 'src/app/common/http.service';

@Component({
  selector: 'az-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  viewModel: any = {};
  public router: Router;
  public form: FormGroup;
  public email: AbstractControl;
  public password: AbstractControl;
  profileID: number =0;
  response;
  viewdata: any;
  constructor(router: Router, fb: FormBuilder, private _httpservice: HttpService,public OAuth: AuthService) {
    this.router = router;
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(10)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });

    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];
  }
  /**
   * CheckUserRegister
   */
  public CheckUserRegister() {
    this._httpservice.get(environment.apiUrl + 'UserMaster/CheckDuplicateUser/' + this.email.value+'/2/0/Add').subscribe(data => {
      if (data) {
       if(data==true)
       this.AddProfile();
      }
      else {
        this.router.navigate(['/login']);
      }
  
    });
}
/**
 * CreateNew
 */
AddProfile() {
  let ApiParam = {
    "profileID": 0,
    "name": this.email.value.split("@")[0],
    "typeID": 0,
    "cloneID": 0,
    "discription": "",
    "statusID":1
   
  }
  this._httpservice.create(environment.apiUrl + 'Profile/Add',ApiParam ).subscribe(data => {
      if(data!=null)
      {
        this.profileID=parseInt(data["profileID"])
        this.CreateNew();
      }
    });
}

public CreateNew() {
  let ApiParam = {
    "userID": 0,
    "first_Name": this.email.value.split("@")[0],
    "last_Name": "",
    "email":this.email.value,
    "roleID": 3,
    "profileID": this.profileID,
    "localeID": 0,
    "alias": this.email.value.split("@")[0],
    "phone": "",
    "mobile": "",
    "website": "",
    "dob": "2020-01-04T10:03:21.960Z",
    "emailConfrmed": true,
    "passwordHash": this.password.value,
    "securityStamp": "",
    "mobileNoConfirmed": true,
    "twoFactorEnabled": false,
    "statusID": 1,
    "subscriberID": 2,
    "blocked": false
  }
  this._httpservice.create(environment.apiUrl + 'UserMaster/Add',ApiParam ).subscribe(data => {
      if(parseInt(data["userID"])>0)
      {
        this.viewModel = {
          username: this.form.value.email,
          password: this.form.value.password
        }
        this._httpservice.create(environment.apiUrl + 'login/Getlogin/', this.viewModel).subscribe(data => {
          if (data) {
            var getlogin: any = {}
            console.log(data);
            getlogin = data;
            localStorage.setItem('userID', getlogin.userID);
            localStorage.setItem('alias', getlogin.alias);
            localStorage.setItem('profileID', getlogin.profileID);
            localStorage.setItem('localeID', getlogin.localeID);
            localStorage.setItem('login', JSON.stringify(data));
            this.Subscriber();
            this.Profile();
            this.ProfileDetail();
            this.LocaleInfo();
            
          }
      
        });
      }
      else
      {
        this.router.navigate(['pages/signup']);
      }
  });
}

ngOnInit(){
this.Reset();
}
Reset() {
  localStorage.clear();
  localStorage.removeItem('userID');
  localStorage.removeItem('alias');
  localStorage.removeItem('profileID');
  localStorage.removeItem('localeID');
  localStorage.removeItem('subscriberID');
  localStorage.removeItem('login');
  localStorage.removeItem('subscriber');
  localStorage.removeItem('profile');
  localStorage.removeItem('profiledetails');
  localStorage.removeItem('locale');
}
public onSubmit(values: Object): void {
  if(this.form.valid) {
  this.viewModel = {
    username: this.form.value.email,
    password: this.form.value.password
  }
  this.Reset();
  this.CheckUserRegister();
}
}
Subscriber() {
  var userid = localStorage.getItem("userID");
  this._httpservice.get(environment.apiUrl + 'login/GetSubscriber/' + userid).subscribe(data => {
    if (data) {
      var getSubscriber: any = {}
      console.log(data);
      getSubscriber = data;
      localStorage.setItem('subscriberID', getSubscriber.subscriberID);
      localStorage.setItem('subscriber', JSON.stringify(getSubscriber));
    }

  });
}
Profile() {
  var profileID = localStorage.getItem("profileID");
  this._httpservice.get(environment.apiUrl + 'login/GetMProfile/' + profileID).subscribe(data => {
    if (data) {
      var getMProfile: any = {}
      console.log(data);
      getMProfile = data;
      localStorage.setItem('profile', JSON.stringify(getMProfile));
      this.router.navigate(['pages/dashboard']);
    }

  });
}
ProfileDetail() {
  var profileID = localStorage.getItem("profileID");
  this._httpservice.get(environment.apiUrl + 'login/ProfileDetail/' + profileID).subscribe(data => {
    if (data) {
      var profileDetail: any = {}
      console.log(data);
      profileDetail = data;
      localStorage.setItem('profiledetails', JSON.stringify(profileDetail));
    }

  });
}
LocaleInfo() {
  var localeID = localStorage.getItem("localeID");
  this._httpservice.get(environment.apiUrl + 'login/GetLocaleInfo/' + localeID).subscribe(data => {
    if (data) {
      var getLocaleInfo: any = {}
      console.log(data);
      getLocaleInfo = data;
      localStorage.setItem('locale', JSON.stringify(getLocaleInfo));
    }

  });
}
   public OnInit() {
  this.Reset();
}

signOut(): void {
  this.Reset();
  this.router.navigate(['login']);
}

public GotoProcess() {
   this.router.navigate(['./signup/multistepwizard'])
}

public socialSignIn(socialProvider: string) {  
  let socialPlatformProvider;  
  if (socialProvider === 'facebook') {  
    socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;  
  } else if (socialProvider === 'google') {  
    socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;  
  }  
  this.OAuth.signIn(socialPlatformProvider).then(socialusers => {  
    this.viewdata = socialusers;
    console.log(socialProvider, socialusers);  
    console.log(socialusers);  
   // this.Savesresponse(socialusers);  
   this.CheckUserSocialRegister();
  });  
  
}  
public CheckUserSocialRegister() {
  this._httpservice.get(environment.apiUrl + 'UserMaster/CheckDuplicateUser/' + this.viewdata.email+'/2/0/Add').subscribe(data => {
    if (data) {
     if(data==true)
     this.AddSocialProfile();
    }
    else {
      this.router.navigate(['/login']);
    }

  });
}
/**
* CreateNew
*/
AddSocialProfile() {
let ApiParam = {
  "profileID": 0,
  "name": this.viewdata.name,
  "typeID": 0,
  "cloneID": 0,
  "discription": "",
  "statusID":1
 
}
this._httpservice.create(environment.apiUrl + 'Profile/Add',ApiParam ).subscribe(data => {
    if(data!=null)
    {
      this.profileID=parseInt(data["profileID"])
      this.SocialCreateNew();
      this.SocialProfileNew();
    }
  });
}

public SocialCreateNew() {
let ApiParam = {
  "userID": localStorage.getItem("userID"),
  "first_Name": this.viewdata.firstName,
  "last_Name":this.viewdata.lastName,
  "email":this.viewdata.email,
  "roleID": 3,
  "profileID": this.profileID,
  "localeID": 0,
  "alias": this.viewdata.name,
  "phone": "",
  "mobile": "",
  "website": "",
  "dob": "2020-01-04T10:03:21.960Z",
  "emailConfrmed": true,
  "passwordHash": this.viewdata.idToken,
  "securityStamp": "",
  "mobileNoConfirmed": true,
  "twoFactorEnabled": false,
  "statusID": 1,
  "subscriberID": 2,
  "blocked": false
}
this._httpservice.create(environment.apiUrl + 'UserMaster/Add',ApiParam ).subscribe(data => {
    if(parseInt(data["userID"])>0)
    {
      this.viewModel = {
        username: this.viewdata.email,
        password: this.viewdata.idToken
      }
      this._httpservice.create(environment.apiUrl + 'login/Getlogin/', this.viewModel).subscribe(data => {
        if (data) {
          var getlogin: any = {}
          console.log(data);
          getlogin = data;
          localStorage.setItem('userID', getlogin.userID);
          localStorage.setItem('alias', getlogin.alias);
          localStorage.setItem('profileID', getlogin.profileID);
          localStorage.setItem('localeID', getlogin.localeID);
          localStorage.setItem('login', JSON.stringify(data));
          this.Subscriber();
          this.Profile();
          this.ProfileDetail();
          this.LocaleInfo();
          
        }
    
      });
    }
    else
    {
      this.router.navigate(['pages/signup']);
    }
});
}
public SocialProfileNew() {
let ApiParam = 
  {
    "socID": 0,
    "entityTypeID": this.viewdata.provider=="google"? 24:25 ,
    "entityID": this.viewdata.id,
    "socialType": this.viewdata.provider,
    "socialID": this.viewdata.id,
    "authtoken": this.viewdata.authToken,
    "authurl": this.viewdata.photoUrl,
    "authPassword": this.viewdata.id,
    "statusID": 1,
   
  }

this._httpservice.create(environment.apiUrl + 'UserMaster/Add',ApiParam ).subscribe(data => {
    if(parseInt(data["userID"])>0)
    {
      this.viewModel = {
        username: this.form.value.email,
        password: this.form.value.password
      }
      this._httpservice.create(environment.apiUrl + 'login/Getlogin/', this.viewModel).subscribe(data => {
        if (data) {
          var getlogin: any = {}
          console.log(data);
          getlogin = data;
          localStorage.setItem('userID', getlogin.userID);
          localStorage.setItem('alias', getlogin.alias);
          localStorage.setItem('profileID', getlogin.profileID);
          localStorage.setItem('localeID', getlogin.localeID);
          localStorage.setItem('login', JSON.stringify(data));
          this.Subscriber();
          this.Profile();
          this.ProfileDetail();
          this.LocaleInfo();
          
        }
    
      });
    }
    else
    {
      this.router.navigate(['pages/signup']);
    }
});
}

}

export function emailValidator(control: FormControl): { [key: string]: any } {
  var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
  if (control.value && !emailRegexp.test(control.value)) {
    return { invalidEmail: true };
  }
}
