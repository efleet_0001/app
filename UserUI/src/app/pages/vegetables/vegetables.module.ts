import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { PipesModule } from '../../theme/pipes/pipes.module';
import { AmaranthusComponent } from './amaranthus/amaranthus.component';
import { AmarphophallusComponent } from './amarphophallus/amarphophallus.component';
import { AurmComponent } from './aurm/aurm.component';
import { BabycornComponent } from './babycorn/babycorn.component';
import { BeansComponent } from './beans/beans.component';
import { BeatrootComponent } from './beatroot/beatroot.component';
import { BellpeperComponent } from './bellpeper/bellpeper.component';
import { BittergourdComponent } from './bittergourd/bittergourd.component';
import { BoardbeanComponent } from './boardbean/boardbean.component';
import { BottlegourdComponent } from './bottlegourd/bottlegourd.component';
import { BrinjalComponent } from './brinjal/brinjal.component';
import { BroccoliComponent } from './broccoli/broccoli.component';
import { BrussilComponent } from './brussil/brussil.component';
import { BushsquashComponent } from './bushsquash/bushsquash.component';
import { ButterpeaComponent } from './butterpea/butterpea.component';
import { CabbageComponent } from './cabbage/cabbage.component';
import { CapsicumComponent } from './capsicum/capsicum.component';
import { CarrotComponent } from './carrot/carrot.component';
import { CauliflowerComponent } from './cauliflower/cauliflower.component';
import { ChapankadduComponent } from './chapankaddu/chapankaddu.component';
import { ChilliesComponent } from './chillies/chillies.component';
import { ChinesecabbageComponent } from './chinesecabbage/chinesecabbage.component';
import { ClusterbeanComponent } from './clusterbean/clusterbean.component';
import { ColocasiaComponent } from './colocasia/colocasia.component';
import { CowpeaComponent } from './cowpea/cowpea.component';
import { CucumberComponent } from './cucumber/cucumber.component';
import { CurryleafComponent } from './curryleaf/curryleaf.component';
import { DolichosbeanComponent } from './dolichosbean/dolichosbean.component';
import { DrumstickComponent } from './drumstick/drumstick.component';
import { FrenchbeanComponent } from './frenchbean/frenchbean.component';
import { GardenpeaComponent } from './gardenpea/gardenpea.component';
import { GobhisarsonComponent } from './gobhisarson/gobhisarson.component';
import { GreateryamComponent } from './greateryam/greateryam.component';
import { IndianbeanComponent } from './indianbean/indianbean.component';
import { KnolkholComponent } from './knolkhol/knolkhol.component';
import { KorondaComponent } from './koronda/koronda.component';
import { KundruComponent } from './kundru/kundru.component';
import { LablabComponent } from './lablab/lablab.component';
import { LadysfingerComponent } from './ladysfinger/ladysfinger.component';
import { LeafvegitableComponent } from './leafvegitable/leafvegitable.component';
import { LesseryamComponent } from './lesseryam/lesseryam.component';
import { LettuceComponent } from './lettuce/lettuce.component';
import { LobiaComponent } from './lobia/lobia.component';
import { LongmelonComponent } from './longmelon/longmelon.component';
import { LvygourdComponent } from './lvygourd/lvygourd.component';
import { MushroomComponent } from './mushroom/mushroom.component';
import { OnionComponent } from './onion/onion.component';
import { PeavegitableComponent } from './peavegitable/peavegitable.component';
import { PethaComponent } from './petha/petha.component';
import { PointedgourdComponent } from './pointedgourd/pointedgourd.component';
import { PotatoComponent } from './potato/potato.component';
import { PumpkinComponent } from './pumpkin/pumpkin.component';
import { RadishComponent } from './radish/radish.component';
import { RibbedgourdComponent } from './ribbedgourd/ribbedgourd.component';
import { RidgegourdComponent } from './ridgegourd/ridgegourd.component';
import { RunnerbeanComponent } from './runnerbean/runnerbean.component';
import { SmoothgauardComponent } from './smoothgauard/smoothgauard.component';
import { SnakesgourdComponent } from './snakesgourd/snakesgourd.component';
import { SpinchpalakComponent } from './spinchpalak/spinchpalak.component';
import { SpinegourdComponent } from './spinegourd/spinegourd.component';
import { SpongegourdComponent } from './spongegourd/spongegourd.component';
import { SweetpatatoComponent } from './sweetpatato/sweetpatato.component';
import { TindaComponent } from './tinda/tinda.component';
import { TomatoComponent } from './tomato/tomato.component';
import { TumbaComponent } from './tumba/tumba.component';
import { WatermelonComponent } from './watermelon/watermelon.component';
import { WhiteyamComponent } from './whiteyam/whiteyam.component';
import { NgxGalleryModule } from 'ngx-gallery';
import { TabsModule } from 'ngx-bootstrap';


export const routes = [
  { path: '', redirectTo: 'amaranthus', pathMatch: 'full' },
  { path: 'amaranthus', component: AmaranthusComponent, data: { breadcrumb: 'amaranthus' } },
  { path: 'amarphophallus', component: AmarphophallusComponent, data: { breadcrumb: 'amarphophallus' } },
  { path: 'aurm', component: AurmComponent, data: { breadcrumb: 'aurm' } },
  { path: 'babycorn', component: BabycornComponent, data: { breadcrumb: 'babycorn' } },
  { path: 'beatroot', component: BeatrootComponent, data: { breadcrumb: 'beatroot' } },
  { path: 'bellpeper', component: BellpeperComponent, data: { breadcrumb: 'bellpeper' } },
  { path: 'ladysfinger', component: LadysfingerComponent, data: { breadcrumb: 'beans' } },
  { path: 'bittergourd', component: BittergourdComponent, data: { breadcrumb: 'bittergourd' } },
  { path: 'bottlegourd', component: BottlegourdComponent, data: { breadcrumb: 'bottlegourd' } },
  { path: 'brinjal', component: BrinjalComponent, data: { breadcrumb: 'brinjal' } },
  { path: 'boardbean', component: BoardbeanComponent, data: { breadcrumb: 'boardbean' } },
  { path: 'broccoli', component: BroccoliComponent, data: { breadcrumb: 'broccoli' } },
  { path: 'brussil', component: BrussilComponent, data: { breadcrumb: 'brussil' } },
  { path: 'bushsquash', component: BushsquashComponent, data: { breadcrumb: 'bushsquash' } },
  { path: 'butterpea', component: ButterpeaComponent, data: { breadcrumb: 'butterpea' } },
  { path: 'cabbage', component: CabbageComponent, data: { breadcrumb: 'cabbage' } },
  { path: 'capsicum', component: CapsicumComponent, data: { breadcrumb: 'capsicum' } },
  { path: 'carrot', component: CarrotComponent, data: { breadcrumb: 'carrot' } },
  { path: 'cauliflower', component: CauliflowerComponent, data: { breadcrumb: 'cauliflower' } },
  { path: 'chapankaddu', component: ChapankadduComponent, data: { breadcrumb: 'chapankaddu' } },
  { path: 'chillies', component: ChilliesComponent, data: { breadcrumb: 'chillies' } },
  { path: 'chinesecabbage', component: ChinesecabbageComponent, data: { breadcrumb: 'chinesecabbage' } },
  { path: 'clusterbean', component: ClusterbeanComponent, data: { breadcrumb: 'clusterbean' } },
  { path: 'colocasia', component: ColocasiaComponent, data: { breadcrumb: 'colocasia' } },
  { path: 'cowpea', component: CowpeaComponent, data: { breadcrumb: 'cowpea' } },
  { path: 'cucumber', component: CucumberComponent, data: { breadcrumb: 'cucumber' } },
  { path: 'curryleaf', component: CurryleafComponent, data: { breadcrumb: 'curryleaf' } },
  { path: 'dolichosbean', component: DolichosbeanComponent, data: { breadcrumb: 'dolichosbean' } },
  { path: 'drumstick', component: DrumstickComponent, data: { breadcrumb: 'drumstick' } },
  { path: 'frenchbean', component: FrenchbeanComponent, data: { breadcrumb: 'frenchbean' } },
  { path: 'gardenpea', component: GardenpeaComponent, data: { breadcrumb: 'gardenpea' } },
  { path: 'gobhisarson', component: GobhisarsonComponent, data: { breadcrumb: 'gobhisarson' } },
  { path: 'greateryam', component: GreateryamComponent, data: { breadcrumb: 'greateryam' } },
  { path: 'indianbean', component: IndianbeanComponent, data: { breadcrumb: 'indianbean' } },
  { path: 'tinda', component: TindaComponent, data: { breadcrumb: 'tinda' } },
  { path: 'lvygourd', component: LvygourdComponent, data: { breadcrumb: 'lvygourd' } },
  { path: 'knolkhol', component: KnolkholComponent, data: { breadcrumb: 'knolkhol' } },
  { path: 'koronda', component: KorondaComponent, data: { breadcrumb: 'koronda' } },
  { path: 'kundru', component: KundruComponent, data: { breadcrumb: 'kundru' } },
  { path: 'lablab', component: LablabComponent, data: { breadcrumb: 'lablab' } },
  { path: 'leafvegitable', component: LeafvegitableComponent, data: { breadcrumb: 'leafvegitable' } },
  { path: 'lesseryam', component: LesseryamComponent, data: { breadcrumb: 'lesseryam' } },
  { path: 'lettuce', component: LettuceComponent, data: { breadcrumb: 'lettuce' } },
  { path: 'lobia', component: LobiaComponent, data: { breadcrumb: 'lobia' } },
  { path: 'longmelon', component: LongmelonComponent, data: { breadcrumb: 'longmelon' } },
  { path: 'mushroom', component: MushroomComponent, data: { breadcrumb: 'mushroom' } },
  { path: 'onion', component: OnionComponent, data: { breadcrumb: 'onion' } },
  { path: 'peavegitable', component: PeavegitableComponent, data: { breadcrumb: 'peavegitable' } },
  { path: 'petha', component: PethaComponent, data: { breadcrumb: 'petha' } },
  { path: 'pointedgourd', component: PointedgourdComponent, data: { breadcrumb: 'pointedgourd' } },
  { path: 'potato', component: PotatoComponent, data: { breadcrumb: 'potato' } },
  { path: 'pumpkin', component: PumpkinComponent, data: { breadcrumb: 'pumpkin' } },
  { path: 'radish', component: RadishComponent, data: { breadcrumb: 'radish' } },
  { path: 'ribbedgourd', component: RibbedgourdComponent, data: { breadcrumb: 'ribbedgourd' } },
  { path: 'ridgegourd', component: RidgegourdComponent, data: { breadcrumb: 'ridgegourd' } },
  { path: 'runnerbean', component: RunnerbeanComponent, data: { breadcrumb: 'runnerbean' } },
  { path: 'snakesgourd', component: SnakesgourdComponent, data: { breadcrumb: 'snakesgourd' } },
 // { path: 'snapmelon', component: snapmelon, data: { breadcrumb: 'smoothgauard' } },
  { path: 'spinchpalak', component: SpinchpalakComponent, data: { breadcrumb: 'spinchpalak' } },
  { path: 'spinegourd', component: SpinegourdComponent, data: { breadcrumb: 'spinegourd' } },
  { path: 'spongegourd', component: SpongegourdComponent, data: { breadcrumb: 'spongegourd' } },
  { path: 'sweetpatato', component: SweetpatatoComponent, data: { breadcrumb: 'sweetpatato' } },
  { path: 'tomato', component: TomatoComponent, data: { breadcrumb: 'tomato' } },
 // { path: 'spinegourd', component: SpinegourdComponent, data: { breadcrumb: 'spinegourd' } },
  { path: 'Tumba', component: TumbaComponent, data: { breadcrumb: 'Tumba' } },
  { path: 'watermelon', component: WatermelonComponent, data: { breadcrumb: 'watermelon' } },
  { path: 'whiteyam', component: WhiteyamComponent, data: { breadcrumb: 'whiteyam' } },

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DirectivesModule,
    PipesModule,
    RouterModule.forChild(routes),
    NgxGalleryModule,
    TabsModule.forRoot()
  ],
  declarations: [
    AmaranthusComponent,
    AmarphophallusComponent,
    AurmComponent,
    BabycornComponent,
    BeansComponent,
    BeatrootComponent,
    BellpeperComponent,
    LadysfingerComponent,
    BittergourdComponent,
    BottlegourdComponent,
    BrinjalComponent,
    BoardbeanComponent,
    BroccoliComponent,
    BrussilComponent,
    BushsquashComponent,
    ButterpeaComponent,
    CabbageComponent,
    CapsicumComponent,
    CarrotComponent,
    CauliflowerComponent,
    ChapankadduComponent,
    ChilliesComponent,
    ChinesecabbageComponent,
    ClusterbeanComponent,
    ColocasiaComponent,
    CowpeaComponent,
    CucumberComponent,
    CurryleafComponent,
    DolichosbeanComponent,
    DrumstickComponent,
    FrenchbeanComponent,
    GardenpeaComponent,
    GobhisarsonComponent,
    GreateryamComponent,
    IndianbeanComponent,
    TindaComponent,
    LvygourdComponent,
    KnolkholComponent,
    KorondaComponent,
    KundruComponent,
    LablabComponent,
    LeafvegitableComponent,
    LesseryamComponent,
    LettuceComponent,
    LobiaComponent,
    LongmelonComponent,
    MushroomComponent,
    OnionComponent,
    PeavegitableComponent,
    PointedgourdComponent,
    PotatoComponent,
    PumpkinComponent,
    RadishComponent,
    RibbedgourdComponent,
    RidgegourdComponent,
    RunnerbeanComponent,
    SmoothgauardComponent,
	SpinegourdComponent,
	SnakesgourdComponent,
	SpinchpalakComponent,
    PethaComponent,
	SpongegourdComponent,
	SweetpatatoComponent,
	TomatoComponent,
	TumbaComponent,
	WatermelonComponent,
	WhiteyamComponent,
  ]
})

export class VegetablesModule { }
