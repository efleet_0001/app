import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';

@Component({
  selector: 'az-companyDetails',
  templateUrl: './companyDetails.component.html',
  styleUrls: ['./companyDetails.component.scss'],
})
export class companyDetailsComponent implements OnInit{
  toggleForm: boolean = false;
  holidayform: boolean = false;
  companyEdit: boolean = true;
  OpenEditForm: boolean = false;
  localeInfo: boolean = true;
  setLocaleInfo: boolean = false;
  demoForm: FormGroup;
  phaseForm: FormGroup;
  selectedValue: string;
  fileData: File = null;
  previewUrl:any = null;
  fileUploadProgress: string = null;

  arrayItems: {
    id: number;
    title: string;
  }[];

  constructor(private _location: Location, private _fb: FormBuilder){
    this.previewUrl = "../../../../assets/img/company.jpg";
 }

 ngOnInit(){
  this.phaseForm = this._fb.group({
    phaseExecutions: this._fb.group({
      PRE: this._fb.array([this.addPhase()])
    })
  });
  this.selectedValue = "";
  this.companyEdit = true;
}
CreateGroups(){
  this.toggleForm = !this.toggleForm;
}

CreateHolidays(){
  this.holidayform = !this.holidayform;
}
gotoBack(){
  this._location.back();
}

// add and remove row 
addPhase() {
  return this._fb.group({
    phaseType: [''],
    phaseValue: ['']
  });
}

addMorePhase() {
  this.phaseArray.push(this.addPhase());
  
}

removePhase(){
  if(this.phaseArray.length == 1){
    return false
  }
  else{
    this.phaseArray.removeAt(this.phaseArray.length - 1);
  }
}
onSubmit() {
  console.log(this.phaseForm.value);
}

onChange(val, index: number) {
  if (val === 'EMS') {
    (<FormGroup>this.phaseArray.at(index))
      .addControl('phaseValue1', this._fb.control([]));
  } else {
    (<FormGroup>this.phaseArray.at(index))
      .removeControl('phaseValue1');
  }
}

hasPhaseValue1At(index) {
  return (<FormGroup>this.phaseArray.at(index)).get('phaseValue1') ? true : false;
}

get phaseArray() {
  const control = <FormArray>(<FormGroup>this.phaseForm.get('phaseExecutions')).get('PRE');
  return control;
}

// company form edit
editCompanyForm(){
  this.companyEdit = false;
  this.OpenEditForm = true;
}
refresh(){
  this.companyEdit = true;
  this.OpenEditForm = false;
}
// upload form 


fileProgress(fileInput: any) {
  this.fileData = <File>fileInput.target.files[0];
  this.preview();
}
preview() {
  // Show preview 
  var mimeType = this.fileData.type;
  if (mimeType.match(/image\/*/) == null) {
    return;
  }

  var reader = new FileReader();      
  reader.readAsDataURL(this.fileData); 
  reader.onload = (_event) => { 
    this.previewUrl = reader.result; 
  }
}

// locale infp
createLocaleInfo(){
  this.localeInfo = !this.localeInfo;
  this.setLocaleInfo = !this.setLocaleInfo;
}

}
