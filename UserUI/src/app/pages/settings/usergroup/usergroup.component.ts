import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'az-usergroup',
  templateUrl: './usergroup.component.html',
  styleUrls: ['./usergroup.component.scss'],
})
export class UsergroupComponent implements OnInit{
  toggleForm: boolean = false;
  constructor(private _location: Location){
 

 }

 ngOnInit(){
 
}
CreateGroups(){
  this.toggleForm = !this.toggleForm;
}
gotoBack(){
  this._location.back();
}

}
