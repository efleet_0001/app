import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { ProfileHeaderComponent } from './profileHeader/profileHeader.component';
import { TabsModule } from 'ngx-bootstrap';
import { TimelineComponent } from './timeline/timeline.component';
import { UsergroupComponent } from './usergroup/usergroup.component';
import { companyDetailsComponent } from './companyDetails/companyDetails.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

export const routes = [
  { path: '', redirectTo: 'timeline', pathMatch: 'full'},
  { path: 'timeline', component: TimelineComponent, data: { breadcrumb: 'timeline' } },
  {
    path: 'groups', component: UsergroupComponent, data: { breadcrumb: 'groups'}
  },
  {
    path: 'companydetails', component: companyDetailsComponent, data: { breadcrumb: 'companydetails'}
  }
 

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MultiselectDropdownModule,
    DirectivesModule,    
    RouterModule.forChild(routes),
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  declarations: [
    TimelineComponent, ProfileHeaderComponent, UsergroupComponent, companyDetailsComponent
  ],
  providers: [
  ]
})
export class SettingsModule { }
