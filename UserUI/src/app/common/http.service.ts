import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppError } from './app-error';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private http: HttpClient) { }

  getAll(url) {
    return this.http.get(url);
  }
  get(url) {
    return this.http.get(url);
  }

  create(url, resource) {
    return this.http.post(url, resource);
  }

  fetch(url, resource) {
    return this.http.post(url, resource);
  }

  update(url, resource) {
    return this.http.put(url, resource);
  }

  delete(url) {
    return this.http.delete(url);
  }

  private handleError(error: Response) {
    return Observable.throw(new AppError(error));
  }

}
