export const menuItems = [
  {
    title: 'Dashboard',
    routerLink: 'dashboard',
    icon: 'fa-home',
    selected: false,
    expanded: false,
    order: 0
  },
   {
        title: 'Vegetables',
        routerLink: 'vegetables',
        icon: 'fa-home',
        selected: false,
        expanded: false,
        order: 200,
        subMenu: [
          {
             'title': 'Grain Amaranthus',
             'routerLink': 'vegetables/amaranthus'
          },
          {
             'title': 'Amarphophallus',
             'routerLink': 'vegetables/amarphophallus'
          },
          {
             'title': 'Aurm',
             'routerLink': 'vegetables/aurm'
          },
          {
             'title': 'Ash Gourd (Petha)',
             'routerLink': 'vegetables/petha'
          },
          {
             'title': 'Baby Corn',
             'routerLink': 'vegetables/babycorn'
          },
          {
             'title': 'Beans',
             'routerLink': 'vegetables/beans'
          },
          {
             'title': 'Beet Root',
             'routerLink': 'vegetables/beatroot'
          },
          {
             'title': 'Bell Pepper',
             'routerLink': 'vegetables/bellpeper'
          },
          {
             'title': 'Ladysfinger',
             'routerLink': 'vegetables/ladysfinger'
          },
          {
             'title': 'Bitter Gourd',
             'routerLink': 'vegetables/bittergourd'
          },
          {
             'title': 'Bottle Gourd',
             'routerLink': 'vegetables/bottlegourd'
          },
          {
             'title': 'Brinjal',
             'routerLink': 'vegetables/brinjal'
          },
          {
             'title': 'Broad Bean',
             'routerLink': 'vegetables/boardbean'
          },
          {
             'title': 'Broccoli',
             'routerLink': 'vegetables/broccoli'
          },
          {
             'title': 'Brussil',
             'routerLink': 'vegetables/brussil'
          },
          {
             'title': 'Bush Squash',
             'routerLink': 'vegetables/bushsquash'
          },
          {
             'title': 'Butter Pea',
             'routerLink': 'vegetables/butterpea'
          },
          {
             'title': 'Cabbage',
             'routerLink': 'vegetables/cabbage'
          },
          {
             'title': 'Capsicum',
             'routerLink': 'vegetables/capsicum'
          },
          {
             'title': 'Carrot',
             'routerLink': 'vegetables/carrot'
          },
          {
             'title': 'Cauliflower',
             'routerLink': 'vegetables/cauliflower'
          },
          {
             'title': 'Chapan Kaddu',
             'routerLink': 'vegetables/chapankaddu'
          },
          {
             'title': 'Chillies',
             'routerLink': 'vegetables/chillies'
          },
          {
             'title': 'Chinese Cabbage',
             'routerLink': 'vegetables/chinesecabbage'
          },
          {
             'title': 'Cluster Bean',
             'routerLink': 'vegetables/clusterbean'
          },
          {
             'title': 'Colocasia (Arvi, Arbi)',
             'routerLink': 'vegetables/colocasia'
          },
          {
             'title': 'Cowpea (Vegetable)',
             'routerLink': 'vegetables/cowpea'
          },
          {
             'title': 'Cucumber',
             'routerLink': 'vegetables/cucumber'
          },
          {
             'title': 'Curry Leaf',
             'routerLink': 'vegetables/curryleaf'
          },
          {
             'title': 'Dolichos Bean',
             'routerLink': 'vegetables/dolichosbean'
          },
          {
             'title': 'Drum Stick',
             'routerLink': 'vegetables/drumstick'
          },
          {
             'title': 'French Bean',
             'routerLink': 'vegetables/frenchbean'
          },
          {
             'title': 'Garden Pea',
             'routerLink': 'vegetables/gardenpea'
          },
          {
             'title': 'Gobhi Sarson',
             'routerLink': 'vegetables/gobhisarson'
          },
          {
             'title': 'Greater Yam',
             'routerLink': 'vegetables/greateryam'
          },
          {
             'title': 'Indian Bean',
             'routerLink': 'vegetables/indianbean'
          },
          {
             'title': 'Tinda',
             'routerLink': 'vegetables/tinda'
          },
          {
             'title': 'Ivy Gourd',
             'routerLink': 'vegetables/lvygourd'
          },
          {
             'title': 'Knol-Khol',
             'routerLink': 'vegetables/knolkhol'
          },
          {
             'title': 'Koronda',
             'routerLink': 'vegetables/koronda'
          },
          {
             'title': 'Kundru',
             'routerLink': 'vegetables/kundru'
          },
          {
             'title': 'Lab Lab',
             'routerLink': 'vegetables/lablab'
          },
          {
             'title': 'Leafy Vegetable',
             'routerLink': 'vegetables/wleafvegitable'
          },
          {
             'title': 'Lesser Yam (Rafula)',
             'routerLink': 'vegetables/lesseryam'
          },
          {
             'title': 'Lettuce',
             'routerLink': 'vegetables/lettuce'
          },
          {
             'title': 'lobia',
             'routerLink': 'vegetables/lobia'
          },
          {
             'title': 'Long Melon',
             'routerLink': 'vegetables/longmelon'
          }
          ,
          {
             'title': 'Mushroom',
             'routerLink': 'vegetables/mushroom'
          }
          ,
          {
             'title': 'Onion',
             'routerLink': 'vegetables/onion'
          }
          ,
          {
             'title': 'Pea (Vegetable)',
             'routerLink': 'vegetables/peavegitable'
          }
          ,
          {
             'title': 'Pointed Gourd',
             'routerLink': 'vegetables/pointedgourd'
          }
          ,
          {
             'title': 'Potato',
             'routerLink': 'vegetables/potato'
          }
          ,
          {
             'title': 'Pumpkin',
             'routerLink': 'vegetables/pumpkin'
          }
          ,
          {
             'title': 'Radish',
             'routerLink': 'vegetables/radish'
          }
          ,
          {
             'title': 'Ribbed Gourd',
             'routerLink': 'vegetables/ribbedgourd'
          }
          ,
          {
             'title': 'Ridge Gourd',
             'routerLink': 'vegetables/ridgegourd'
          }
          ,
          {
             'title': 'Runner Bean',
             'routerLink': 'vegetables/runnerbean'
          }
          ,
          {
             'title': 'Smooth Guard',
             'routerLink': 'vegetables/smoothgauard'
          }
          ,
          {
             'title': 'Snake Gourd',
             'routerLink': 'vegetables/snakesgourd'
          }
          ,
          {
             'title': 'Snap Melon',
             'routerLink': 'vegetables/snapmelon'
          }
          ,
          {
             'title': 'Spinach (Palak)',
             'routerLink': 'vegetables/spinchpalak'
          }
          ,
          {
             'title': 'Spine Gourd',
             'routerLink': 'vegetables/spinegourd'
          }
          ,
          {
             'title': 'Sponge Gourd',
             'routerLink': 'vegetables/spongegourd'
          }
          ,
          {
             'title': 'Sweet Potato',
             'routerLink': 'vegetables/sweetpatato'
          }
          ,
          {
             'title': 'Tomato',
             'routerLink': 'vegetables/tomato'
          }
          ,
          {
             'title': 'Spine Gourd',
             'routerLink': 'vegetables/spinegourd'
          }
          ,
          {
             'title': 'Tumba',
             'routerLink': 'vegetables/tumba'
          }
          ,
          {
             'title': 'Watermelon',
             'routerLink': 'vegetables/watermelon'
          }
          ,
          {
             'title': 'White Yam',
             'routerLink': 'vegetables/whiteyam'
          }
          ,
          {
             'title': 'Yard Long Bean',
             'routerLink': 'vegetables/wheat'
          }

       ]
      },
     {
      title: 'Cereals',
        routerLink: 'cereals',
        icon: 'fa-home',
        selected: false,
        expanded: false,
        order: 200,
        subMenu: [
          {
             'title': 'Barley',
             'routerLink': 'cereals/barely'
          },
          {
             'title': 'Oats',
             'routerLink': 'cereals/oats'
          },
          {
             'title': 'Paddy',
             'routerLink': 'cereals/paddy'
          },
          {
             'title': 'Triticale',
             'routerLink': 'cereals/triticale'
          },
          {
             'title': 'Wheat',
             'routerLink': 'cereals/wheat'
          }
       ]
       },
       {
        title: 'Fiber Crops',
        routerLink: 'Fiber Crops',
        icon: 'fa-home',
        selected: false,
        expanded: false,
        order: 200,
        subMenu: [
          {
             'title': 'Cotton (Kapas)',
             'routerLink': 'Cotton (Kapas)'
          },
          {
             'title': 'Jute',
             'routerLink': 'Jute'
          },
          {
             'title': 'Roselle (Mesta)',
             'routerLink': 'Roselle (Mesta)'
          }
       ]
       },
       {
        title: 'Fodder Crops',
        routerLink: 'Fodder Crops',
        icon: 'fa-home',
        selected: false,
        expanded: false,
        order: 200,
        subMenu: [
          {
             'title': 'African Sarson',
             'routerLink': 'African Sarson'
          },
          {
             'title': 'Anjan Grass',
             'routerLink': 'Anjan Grass'
          },
          {
             'title': 'Bajra Napier ',
             'routerLink': 'Bajra Napier '
          },
          {
             'title': 'Barseem',
             'routerLink': 'Barseem'
          },
          {
             'title': 'Birdwood Grass',
             'routerLink': 'Birdwood Grass'
          },
          {
             'title': 'Buffel Grass (Anjan Grass)',
             'routerLink': 'Buffel Grass (Anjan Grass)'
          },
          {
             'title': 'Dharaf Grass',
             'routerLink': 'Dharaf Grass'
          },
          {
             'title': 'Dinanath Grass',
             'routerLink': 'Dinanath Grass'
          },
          {
             'title': 'Fodder Maize ',
             'routerLink': 'fodder maize '
          },
          {
             'title': 'Fodder Sorghum',
             'routerLink': 'fodder sorghum'
          },
          {
             'title': 'Golden Thimothy',
             'routerLink': 'Golden Thimothy'
          },
          {
             'title': 'Guar',
             'routerLink': 'Guar'
          },
          {
             'title': 'Guinea Grass',
             'routerLink': 'Guinea Grass'
          },
          {
             'title': 'Lucerne (Alfalfa)',
             'routerLink': 'Lucerne (Alfalfa)'
          },
          {
             'title': 'Marvel Grass',
             'routerLink': 'Marvel Grass'
          },
          {
             'title': 'Napier Grass',
             'routerLink': 'Napier Grass'
          },
          {
             'title': 'Other',
             'routerLink': 'Other'
          },
          {
             'title': 'Persian Clover',
             'routerLink': 'Persian Clover'
          },
          {
             'title': 'Rajka Bajri.',
             'routerLink': 'rajka bajri.'
          },
          {
             'title': 'Red Clover',
             'routerLink': 'Red Clover'
          },
          {
             'title': 'Ricebean',
             'routerLink': 'Ricebean'
          },
          {
             'title': 'Ryegrass',
             'routerLink': 'Ryegrass'
          },
          {
             'title': 'Sen Grass',
             'routerLink': 'Sen Grass'
          },
          {
             'title': 'Senji',
             'routerLink': 'Senji'
          },
          {
             'title': 'Setaria Grass',
             'routerLink': 'Setaria Grass'
          },
          {
             'title': 'Stylosanthes',
             'routerLink': 'Stylosanthes'
          },
          {
             'title': 'Sudan Grass',
             'routerLink': 'Sudan Grass'
          },
          {
             'title': 'Tall Fescue Grass',
             'routerLink': 'Tall Fescue Grass'
          },
          {
             'title': 'Teosinte',
             'routerLink': 'Teosinte'
          },
          {
             'title': 'Velimasal',
             'routerLink': 'velimasal'
          },
          {
             'title': 'White Clover (Shaftal)',
             'routerLink': 'White Clover (Shaftal)'
          }
       ]
       },
       {
        title: 'Green Manure',
        routerLink: 'Green Manure',
        icon: 'fa-home',
        selected: false,
        expanded: false,
        order: 200,
        subMenu: [
          {
             'title': 'Dhaincha',
             'routerLink': 'Dhaincha'
          },
          {
             'title': 'Kolanchi(Tephrosia Purpurea)',
             'routerLink': 'Kolanchi(Tephrosia Purpurea)'
          },
          {
             'title': 'Mesta',
             'routerLink': 'Mesta'
          },
          {
             'title': 'Pillipesara',
             'routerLink': 'Pillipesara'
          },
          {
             'title': 'Sunnhemp (Patua)',
             'routerLink': 'Sunnhemp (Patua)'
          }
       ]
       },
       {
       title: 'Millets',
        routerLink: 'Millets',
        icon: 'fa-home',
        selected: false,
        expanded: false,
        order: 200,
        subMenu: [
           {
             'title': 'Barnyard Millet (Kundiraivlli/Sawan)',
             'routerLink': 'Barnyard Millet (Kundiraivlli/Sawan'
          },
          {
             'title': 'Buck Wheat (Kaspat)',
             'routerLink': 'Buck Wheat (Kaspat)'
          },
          {
             'title': 'Common Millet (Panivaragu/Chena/Proso Millet/Hogm)',
             'routerLink': 'Common Millet (Panivaragu/Chena/Proso Millet/Hogm'
          },
          {
             'title': 'FingerMillet  (Ragi/Mandika)',
             'routerLink': 'FingerMillet  (Ragi/Mandika)'
          },
          {
             'title': 'Italian Millet (Thenai/Navane/Foxtail Millet/Kang)',
             'routerLink': 'Italian Millet (Thenai/Navane/Foxtail Millet/Kang)'
          },
          {
             'title': 'Kodo Millet (Kodara/Varagu)',
             'routerLink': 'Kodo Millet (Kodara/Varagu)'
          },
          {
             'title': 'Little Millet (Samai/Kutki/kodo-kutki)',
             'routerLink': 'Little Millet (Samai/Kutki/kodo-kutki)'
          },
          {
             'title': 'Maize (Makka)',
             'routerLink': 'Maize (Makka)'
          },
          {
             'title': 'Pearl Millet (Bajra/Bulrush Millet/Spiked Millet)',
             'routerLink': 'Pearl Millet (Bajra/Bulrush Millet/Spiked Millet)'
          },
          {
             'title': 'Sorghum (Jowar/Great Millet)',
             'routerLink': 'Sorghum (Jowar/Great Millet)'
          }
       ]
       },
       {
        title: 'Oil Crops',
        routerLink: 'Oil Crops',
        icon: 'fa-home',
        selected: false,
        expanded: false,
        order: 200,
        subMenu: [
          {
             'title': 'Oil palm',
             'routerLink': 'Oil palm'
          }
       ]
       },
       {
       title: 'Oil Seeds',
        routerLink: 'Oil Seeds',
        icon: 'fa-home',
        selected: false,
        expanded: false,
        order: 200,
        subMenu: [
          {
             'title': 'Brown Sarson',
             'routerLink': 'Brown Sarson'
          },
          {
             'title': 'Castor (Rehri, Rendi, Arandi) ',
             'routerLink': 'Castor (Rehri, Rendi, Arandi) '
          },
          {
             'title': 'Groundnut (pea nut/mung phalli)',
             'routerLink': 'Groundnut (pea nut/mung phalli)'
          },
          {
             'title': 'Indian rapeseed and mustard (yellow sarson)',
             'routerLink': 'Indian rapeseed and mustard (yellow sarson)'
          },
          {
             'title': 'Jojoba',
             'routerLink': 'Jojoba'
          },
          {
             'title': 'Karan Rai',
             'routerLink': 'Karan Rai'
          },
          {
             'title': 'Linseed (alsi)',
             'routerLink': 'Linseed (alsi)'
          },
          {
             'title': 'Mustard',
             'routerLink': 'Mustard'
          },
          {
             'title': 'Niger (Ramtil)',
             'routerLink': 'Niger (Ramtil)'
          },
          {
             'title': 'Olive',
             'routerLink': 'Olive'
          },
          {
             'title': 'Raya (Indian Mustard)',
             'routerLink': 'Raya (Indian Mustard)'
          },
          {
             'title': 'Rocket salad (taramira)',
             'routerLink': 'Rocket salad (taramira)'
          },
          {
             'title': 'Safflower (kusum/kardi)',
             'routerLink': 'Safflower (kusum/kardi)'
          },
          {
             'title': 'Sesame (Gingelly/Til)/Sesamum',
             'routerLink': 'Sesame (Gingelly/Til)/Sesamum'
          },
          {
             'title': 'Soybean (bhat)',
             'routerLink': 'Soybean (bhat)'
          },
          {
             'title': 'Sunflower (suryamukhi)',
             'routerLink': 'Sunflower (suryamukhi)'
          },
          {
             'title': 'Toria',
             'routerLink': 'Toria'
          }
       ]
       },
       {
       title: 'Others',
        routerLink: 'Others',
        icon: 'fa-home',
        selected: false,
        expanded: false,
        order: 200,
        subMenu: [
          {
             'title': 'Others',
             'routerLink': 'Others'
          }
       ]
       },
       {
         title: 'Pulses',
        routerLink: 'Pulses',
        icon: 'fa-home',
        selected: false,
        expanded: false,
        order: 200,
        subMenu: [
          {
             'title': 'Bengal Gram (Gram/Chick Pea/Kabuli/Chana)',
             'routerLink': 'Bengal Gram (Gram/Chick Pea/Kabuli/Chana)'
          },
          {
             'title': 'Black Gram (urd bean)',
             'routerLink': 'Black Gram (urd bean)'
          },
          {
             'title': 'Faba Bean (Horse Bean/windsor Bean)',
             'routerLink': 'Faba Bean (Horse Bean/windsor Bean)'
          },
          {
             'title': 'Green Gram (Moong Bean/ Moong)',
             'routerLink': 'Green Gram (Moong Bean/ Moong)'
          },
          {
             'title': 'Horse Gram (kulthi/kultha)',
             'routerLink': 'Horse Gram (kulthi/kultha)'
          },
          {
             'title': 'Khesari (chickling vetch/ grass pea)',
             'routerLink': 'Khesari (chickling vetch/ grass pea)'
          },
          {
             'title': 'Lentil (Masur)',
             'routerLink': 'Lentil (Masur)'
          },
          {
             'title': 'Lethyrus',
             'routerLink': 'Lethyrus'
          },
          {
             'title': 'Mochai (lab-lab)',
             'routerLink': 'Mochai (lab-lab)'
          },
          {
             'title': 'Moth Bean (kidney bean/ deww gram)',
             'routerLink': 'Moth Bean (kidney bean/ deww gram)'
          },
          {
             'title': 'Pigeon pea (red gram/arhar/tur)',
             'routerLink': 'Pigeon pea (red gram/arhar/tur)'
          },
          {
             'title': 'Rajma (french bean)',
             'routerLink': 'Rajma (french bean)'
          },
          {
             'title': 'Rajmash Bean',
             'routerLink': 'Rajmash Bean'
          },
          {
             'title': 'Winged Bean',
             'routerLink': 'Winged Bean'
          }
       ]
       },
       {
        title: 'Sugar and Starch Crops',
        routerLink: 'Sugar and Starch Crops',
        icon: 'fa-home',
        selected: false,
        expanded: false,
        order: 200,
        subMenu: [
          {
             'title': 'Sugar Beet',
             'routerLink': 'Sugar Beet'
          },
          {
             'title': 'Sugarcane (Noble Cane)',
             'routerLink': 'Sugarcane (Noble Cane)'
          },
          {
             'title': 'Tapioca (Cassava)',
             'routerLink': 'Tapioca (Cassava)'
          }
       ]
    }
  // {
  //   title: 'UI Features',
  //   routerLink: 'ui',
  //   icon: 'fa-laptop',
  //   selected: false,
  //   expanded: false,
  //   order: 300,
  //   subMenu: [
  //     {
  //       title: 'Buttons',
  //       routerLink: 'ui/buttons'
  //     },
  //     {
  //       title: 'Cards',
  //       routerLink: 'ui/cards'
  //     },
  //     {
  //       title: 'Components',
  //       routerLink: 'ui/components'
  //     },
  //     {
  //       title: 'Icons',
  //       routerLink: 'ui/icons'
  //     },
  //     {
  //       title: 'Grid',
  //       routerLink: 'ui/grid'
  //     },
  //     {
  //       title: 'List Group',
  //       routerLink: 'ui/list-group'
  //     },
  //     {
  //       title: 'Media Objects',
  //       routerLink: 'ui/media-objects'
  //     },
  //     {
  //       title: 'Tabs & Accordions',
  //       routerLink: 'ui/tabs-accordions'
  //     },
  //     {
  //       title: 'Typography',
  //       routerLink: 'ui/typography'
  //     }
  //   ]
  // },
  // {
  //   title: 'Tools',
  //   routerLink: 'tools',
  //   icon: 'fa-wrench',
  //   selected: false,
  //   expanded: false,
  //   order: 550,
  //   subMenu: [
  //     {
  //       title: 'Drag & Drop',
  //       routerLink: 'tools/drag-drop'
  //     },
  //     {
  //       title: 'Resizable',
  //       routerLink: 'tools/resizable'
  //     },
  //     {
  //       title: 'Toastr',
  //       routerLink: 'tools/toaster'
  //     }
  //   ]
  // },
  // {
  //   title: 'Mail',
  //   routerLink: 'mail/mail-list/inbox',
  //   icon: 'fa-envelope-o',
  //   selected: false,
  //   expanded: false,
  //   order: 330
  // },
  // {
  //   title: 'Calendar',
  //   routerLink: 'calendar',
  //   icon: 'fa-calendar',
  //   selected: false,
  //   expanded: false,
  //   order: 350
  // },
  // {
  //   title: 'Form Elements',
  //   routerLink: 'form-elements',
  //   icon: 'fa-pencil-square-o',
  //   selected: false,
  //   expanded: false,
  //   order: 400,
  //   subMenu: [
  //     {
  //       title: 'Form Inputs',
  //       routerLink: 'form-elements/inputs'
  //     },
  //     {
  //       title: 'Form Layouts',
  //       routerLink: 'form-elements/layouts'
  //     },
  //     {
  //       title: 'Form Validations',
  //       routerLink: 'form-elements/validations'
  //     },
  //     {
  //       title: 'Form Wizard',
  //       routerLink: 'form-elements/wizard'
  //     }
  //   ]
  // },
  // {
  //   title: 'Tables',
  //   routerLink: 'tables',
  //   icon: 'fa-table',
  //   selected: false,
  //   expanded: false,
  //   order: 500,
  //   subMenu: [
  //     {
  //       title: 'Basic Tables',
  //       routerLink: 'tables/basic-tables'
  //     },
  //     {
  //       title: 'Dynamic Tables',
  //       routerLink: 'tables/dynamic-tables'
  //     }
  //   ]
  // },
  // {
  //   title: 'Editors',
  //   routerLink: 'editors',
  //   icon: 'fa-pencil',
  //   selected: false,
  //   expanded: false,
  //   order: 550,
  //   subMenu: [
  //     {
  //       title: 'Froala Editor',
  //       routerLink: 'editors/froala-editor'
  //     },
  //     {
  //       title: 'Ckeditor',
  //       routerLink: 'editors/ckeditor'
  //     }
  //   ]
  // },
  // {
  //   title: 'Maps',
  //   routerLink: 'maps',
  //   icon: 'fa-globe',
  //   selected: false,
  //   expanded: false,
  //   order: 600,
  //   subMenu: [
  //     {
  //       title: 'Vector Maps',
  //       routerLink: 'maps/vectormaps'
  //     },
  //     {
  //       title: 'Google Maps',
  //       routerLink: 'maps/googlemaps'
  //     },
  //     {
  //       title: 'Leaflet Maps',
  //       routerLink: 'maps/leafletmaps'
  //     }
  //   ]
  // },
  // {
  //   title: 'Pages',
  //   routerLink: ' ',
  //   icon: 'fa-file-o',
  //   selected: false,
  //   expanded: false,
  //   order: 650,
  //   subMenu: [
  //     {
  //       title: 'Login',
  //       routerLink: '/login'
  //     },
  //     {
  //       title: 'Register',
  //       routerLink: '/register'
  //     },
  //     {
  //       title: 'Blank Page',
  //       routerLink: 'blank'
  //     },
  //     {
  //       title: 'Error Page',
  //       routerLink: '/pagenotfound'
  //     }
  //   ]
  // },
  // {
  //   title: 'Menu Level 1',
  //   icon: 'fa-ellipsis-h',
  //   selected: false,
  //   expanded: false,
  //   order: 700,
  //   subMenu: [
  //     {
  //       title: 'Menu Level 1.1',
  //       url: '#',
  //       disabled: true,
  //       selected: false,
  //       expanded: false
  //     },
  //     {
  //       title: 'Menu Level 1.2',
  //       url: '#',
  //       subMenu: [{
  //         title: 'Menu Level 1.2.1',
  //         url: '#',
  //         disabled: true,
  //         selected: false,
  //         expanded: false
  //       }]
  //     }
  //   ]
  // },
  // {
  //   title: 'External Link',
  //   url: 'http://themeseason.com',
  //   icon: 'fa-external-link',
  //   selected: false,
  //   expanded: false,
  //   order: 800,
  //   target: '_blank'
  // }
];
