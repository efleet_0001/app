import { Component, ViewEncapsulation } from '@angular/core';
import { AppState } from '../../../app.state';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { HttpService  } from 'src/app/common/http.service';
import { ThrowStmt } from '@angular/compiler';
declare const googleTranslateElementInit:any;
declare const getcall:any;

@Component({
  selector: 'az-navbar',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent {
  
    public isMenuCollapsed: boolean = false;
        ddlanguage: any = [];
        logindata:[];
        profiledata:[];
        valid: boolean = false;
        doj:string;
       constructor(private translate: TranslateService, private _state: AppState, private  _httpservice: HttpService ) {
        this.translate.addLangs(['en', 'hi']);
        this.translate.setDefaultLang('hi');
        this.loadScripts();
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|hi/) ? browserLang : 'hi');
        this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
            this.isMenuCollapsed = isCollapsed;
           
        });
       
    }



    loadScripts() {
      const externalScriptArray = [
        '../assets/js/languageconvert.js'
      ];
      for (let i = 0; i < externalScriptArray.length; i++) {
        const scriptTag = document.createElement('script');
        scriptTag.src = externalScriptArray[i];
        scriptTag.type = 'text/javascript';
        scriptTag.async = false;
        scriptTag.charset = 'utf-8';
        document.getElementsByTagName('head')[0].appendChild(scriptTag);
      }
    }

    
  

    ngOnInit() {
     
     googleTranslateElementInit();
    // getcall();
        this.getalllanguge();
        this.getuserdetail();
        this.logindata=JSON.parse(localStorage.getItem('login'));
        this.profiledata=JSON.parse(localStorage.getItem('profile'));
        console.log("Profile Data :-", this.profiledata)
        if(this.logindata!= null)
        {
          this.valid=true;
        }
        else {
          this.valid=false;
        }
    }
    getuserdetail() {
     console.log(localStorage.getItem('login'));
    }
    getalllanguge() {
      var  subscriberID =localStorage.getItem("subscriberID");
      this._httpservice.get(environment.apiUrl + 'Static/GetAllLanguage/' + 1 ).subscribe(data => {
        if (data) {
          var response: any = {}
          response=JSON.stringify(data);
          this.ddlanguage = data;
        //   if (data != null ) {
        //     for (let i = 0; i < response.length; i++) {
        //    let x = {
        //        name: response[i].name,
        //        culture: response[i].culture,
        //        lanID: response[i].lanID
        //     };
        //    this.ddlanguage.push(x);
        //   }
        // }
        }
      });
      console.log(this.ddlanguage);

    }
   public changeLang(selectedValue: string) {
      this.translate.use(selectedValue);
      getcall();
    }
    public closeSubMenus() {
       /* when using <az-sidebar> instead of <az-menu> uncomment this line */
      // this._sidebarService.closeAllSubMenus();
    }

    public toggleMenu() {
        this.isMenuCollapsed = !this.isMenuCollapsed; 
        this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
    }
    

}
