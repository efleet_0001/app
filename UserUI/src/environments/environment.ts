// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
     production: false,
     baseUrl: 'https://jsonplaceholder.typicode.com',
     apiUrl: 'https://localhost:44355/api/',
     uploadPath : 'https://localhost:44355/Uploads/',
     webaddress: 'http://localhost:44355'
  
    // production: false,
    // baseUrl: 'https://jsonplaceholder.typicode.com',
    // apiUrl: 'http://103.10.234.204/api/',
    // uploadPath : 'http://103.10.234.204/Uploads/',
    // webaddress: 'http://assistkisan.com',
    // reportUrl:'http://localhost:8080/ReportsLive'
    // production: false,
    // apiUrl: 'https://api.assistkisan.com/api/',
    // uploadPath : 'https://api.assistkisan.com/uploads/images/',
    // webaddress: 'https://assistkisan.com',
    // reportUrl:'https://api.assistkisan.com/ReportsLive'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
