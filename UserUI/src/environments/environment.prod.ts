export const environment = {
  production: true,
  apiUrl: 'https://api.assistkisan.com/api/',
  uploadPath : 'https://api.assistkisan.com/uploads/images/',
  webaddress: 'https://assistkisan.com',
  reportUrl:'https://api.assistkisan.com/ReportsLive'
};
