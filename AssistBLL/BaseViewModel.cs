﻿using System;

namespace AssistBLL
{
    public class BaseViewModel
    {
        public BaseViewModel()
        {
            StatusID = 0;
            CreatedDate = DateTime.Now;
            Rowguid = Guid.NewGuid().ToString();
        }
        public int? StatusID { get; set; }
        public long? CSID { get; set; }
        public long? MSID { get; set; }
        public int? SubscriberID { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public string Rowguid { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}

