﻿using System;
using System.Text;
using System.Security.Cryptography;

/// <summary>
/// Summary description for Protection
/// </summary>
public class Protection
{
    public Protection()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static string Encrypt(string toEncrypt, string key)
    {
        byte[] keyArray;
        byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
        bool useHashing = true;
        if (useHashing)
        {
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            //Always release the resources and flush data
            // of the Cryptographic service provide. Best Practice

            hashmd5.Clear();
        }
        else
            keyArray = UTF8Encoding.UTF8.GetBytes(key);

        TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
        //set the secret key for the tripleDES algorithm
        tdes.Key = keyArray;
        //mode of operation. there are other 4 modes.
        //We choose ECB(Electronic code Book)
        tdes.Mode = CipherMode.ECB;
        //padding mode(if any extra byte added)

        tdes.Padding = PaddingMode.PKCS7;

        ICryptoTransform cTransform = tdes.CreateEncryptor();
        //transform the specified region of bytes array to resultArray
        byte[] resultArray =
          cTransform.TransformFinalBlock(toEncryptArray, 0,
          toEncryptArray.Length);
        //Release resources held by TripleDes Encryptor
        tdes.Clear();
        //Return the encrypted data into unreadable string format
        return ToHex(Convert.ToBase64String(resultArray, 0, resultArray.Length));
    }
    public static string Decrypt(string cipherString, string key)
    {
        byte[] keyArray;
        //get the byte code of the string

        byte[] toEncryptArray = Convert.FromBase64String(ToAsc(cipherString));
        bool useHashing = true;

        if (useHashing)
        {
            //if hashing was used get the hash code with regards to your key
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            //release any resource held by the MD5CryptoServiceProvider

            hashmd5.Clear();
        }
        else
        {
            //if hashing was not implemented get the byte code of the key
            keyArray = UTF8Encoding.UTF8.GetBytes(key);
        }

        TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
        //set the secret key for the tripleDES algorithm
        tdes.Key = keyArray;
        //mode of operation. there are other 4 modes. 
        //We choose ECB(Electronic code Book)

        tdes.Mode = CipherMode.ECB;
        //padding mode(if any extra byte added)
        tdes.Padding = PaddingMode.PKCS7;

        ICryptoTransform cTransform = tdes.CreateDecryptor();
        byte[] resultArray = cTransform.TransformFinalBlock(
                             toEncryptArray, 0, toEncryptArray.Length);
        //Release resources held by TripleDes Encryptor                
        tdes.Clear();
        //return the Clear decrypted TEXT
        return UTF8Encoding.UTF8.GetString(resultArray);
    }
    public static string ToHex(string plainText)
    {
        char[] charArray = plainText.ToCharArray();

#pragma warning disable CS0219 // The variable 'output' is assigned but its value is never used
        string output = "";
#pragma warning restore CS0219 // The variable 'output' is assigned but its value is never used

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < charArray.Length; i++)
        {
            int num = Convert.ToInt32(charArray[i]);
            string hex = num.ToString("x");
            builder.Append(hex);
        }

        return builder.ToString();
    }
    public static string ToAsc(string Data)
    {

        string Data1 = "";

        string sData = "";

        while (Data.Length > 0)

        //first take two hex value using substring.

        //then convert Hex value into ascii.

        //then convert ascii value into character.
        {
            Data1 = System.Convert.ToChar(System.Convert.ToUInt32(Data.Substring(0, 2), 16)).ToString();

            sData = sData + Data1;

            Data = Data.Substring(2, Data.Length - 2);

        }
        return sData;
    }

}
