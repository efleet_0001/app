﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace AssistBLL.Security
{
    public static class Utill
    {
        public static string GetEncryptPassword(string strPlainPassword, string Key)
        {
            if (strPlainPassword == "") { strPlainPassword = "BLANK"; }
            string FromEncryptions = Protection.Encrypt(strPlainPassword, Key);
            string strEncPassord = GetEncryptPassword(FromEncryptions);
            return strEncPassord;
        }
        public static string GetEncryptPassword(string strPlainPassword)
        {
            SHA1 algorithm = SHA1.Create();
            byte[] data = algorithm.ComputeHash(Encoding.UTF8.GetBytes(strPlainPassword));
            string strEncPassord = "";
            for (int i = 0; i < data.Length; i++)
            {
                strEncPassord += data[i].ToString("X2").ToUpperInvariant();
            }
            //string strEncPassord =HashPasswordForStoringInConfigFile(strPlainPassword, "MD5");
            return strEncPassord;
        }

    }
}
