﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistDAL.RepositoriesInterface.Common;
using System;
using System.Collections.Generic;
using AssistDB.General;
using AssistDB.General.ViewData;
using AssistDAL.RepositoriesInterface.General;
using AssistBLL.Services.General;
using AssistBLL.ViewModel.General;
using AssistBLL.ViewModel.General.Data;

namespace AssistBLL.Implementation.General
{
    public class CompanyMasterService : EntityService<CompanyDetails>, ICompanyMasterService
    {
        protected readonly ICompanyMasterRepository _iCompanyMasterRepository;
        private IMapper _mapper;
        public CompanyMasterService(IUnitOfWork unitOfWork, IMapper mapper, ICompanyMasterRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iCompanyMasterRepository = repository;
            _mapper = mapper;
        }
        public Result<CompanyDetailsModel> Add(CompanyDetailsModel um)
        {
            var res = new Result<CompanyDetailsModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<CompanyDetailsModel, CompanyDetails>(um);
            model.CompID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<CompanyDetails, CompanyDetailsModel>(model);
            }
            return res;
        }

        public Result<CompanyDetailsModel> Delete(int UserID)
        {
            var res = new Result<CompanyDetailsModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var CompanyDetailsModel = _iCompanyMasterRepository.FirstOrDefault(x => x.CompID == UserID);
            if (CompanyDetailsModel == null)
            {
                res.Errors.Add($"We could not find the UserID Master with id = {UserID.ToString()}");
                return res;
            }
            CompanyDetailsModel.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(CompanyDetailsModel);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<CompanyDetails, CompanyDetailsModel>(CompanyDetailsModel);
            }
            return res;
        }

        public Result<CompanyDetailsModel> Edit(int UserID, CompanyDetailsModel um)
        {
            var res = new Result<CompanyDetailsModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<CompanyDetailsModel, CompanyDetails>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<CompanyDetails, CompanyDetailsModel>(model);
            }
            return res;
        }

        public IEnumerable<CompanyDetailsModelData> GetAllCompany(int SubscriberID)
        {
            var response = _iCompanyMasterRepository.GetAllCompany(SubscriberID);
            return _mapper.Map<IEnumerable<CompanyDetailsData>, List<CompanyDetailsModelData>>(response);
        }

        public CompanyDetailsModelData GetCompanyById(int CompanyID)
        {
            var response = _iCompanyMasterRepository.GetCompanyById(CompanyID);
            return _mapper.Map<CompanyDetailsData, CompanyDetailsModelData>(response);
        }
       
        public CompanyDetailsModel CheckDuplicateCompany(string CompanyName, int? SubscriberID, int CompanyID, string type)
        {
            var response = _iCompanyMasterRepository.CheckDuplicateCompany(CompanyName, SubscriberID, CompanyID, type);
            return _mapper.Map<CompanyDetails, CompanyDetailsModel>(response);
        }
    }
}
