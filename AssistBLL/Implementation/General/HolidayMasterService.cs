﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistBLL.Services.Master;
using AssistBLL.ViewModel.Master;
using AssistDAL.RepositoriesInterface.Common;
using System;
using System.Collections.Generic;
using AssistDB.General;
using AssistDAL.RepositoriesInterface.General;
using AssistBLL.Services.General;
using AssistBLL.ViewModel.General;

namespace AssistBLL.Implementation.General
{
    public class HolidayMasterService : EntityService<Holiday>, IHolidayMasterService
    {
        protected readonly IHolidayMasterRepository _iHolidayMasterRepository;
        private IMapper _mapper;
        public HolidayMasterService(IUnitOfWork unitOfWork, IMapper mapper, IHolidayMasterRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iHolidayMasterRepository = repository;
            _mapper = mapper;
        }
        public Result<HolidayModel> Add(HolidayModel um)
        {
            var res = new Result<HolidayModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<HolidayModel, Holiday>(um);
            model.HoliDayID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Holiday, HolidayModel>(model);
            }
            return res;
        }

        public Result<HolidayModel> Delete(int HoliDayID)
        {
            var res = new Result<HolidayModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var Holiday = _iHolidayMasterRepository.FirstOrDefault(x => x.HoliDayID == HoliDayID);
            if (Holiday == null)
            {
                res.Errors.Add($"We could not find the Holiday Master with id = {HoliDayID.ToString()}");
                return res;
            }
            Holiday.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(Holiday);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Holiday, HolidayModel>(Holiday);
            }
            return res;
        }

        public Result<HolidayModel> Edit(int HoliDayID, HolidayModel um)
        {
            var res = new Result<HolidayModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<HolidayModel, Holiday>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Holiday, HolidayModel>(model);
            }
            return res;
        }

        public IEnumerable<HolidayModel> GetAllHoliday(int SubscriberID)
        {
            var response = _iHolidayMasterRepository.GetAllHoliday(SubscriberID);
            return _mapper.Map<IEnumerable<Holiday>, List<HolidayModel>>(response);
        }

        public HolidayModel GetHolidayById(int HolidayID)
        {
            var response = _iHolidayMasterRepository.GetHolidayById(HolidayID);
            return _mapper.Map<Holiday, HolidayModel>(response);
        }
        
        public HolidayModel CheckDuplicateHoliday(string HolidayName, int? SubscriberID, int HolidayID, string type)
        {
            var response = _iHolidayMasterRepository.CheckDuplicateHoliday(HolidayName, SubscriberID, HolidayID, type);
            return _mapper.Map<Holiday, HolidayModel>(response);
        }
    }
}
