﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistBLL.Services.Master;
using AssistBLL.ViewModel.Master;
using AssistDAL.RepositoriesInterface.Common;
using System;
using System.Collections.Generic;
using AssistDB.General;
using AssistDAL.RepositoriesInterface.General;
using AssistBLL.Services.General;
using AssistBLL.ViewModel.General;

namespace AssistBLL.Implementation.General
{
    public class BusinessHourService : EntityService<BusinessHour>, IBusinessHourService
    {
        protected readonly IBusinesshourRepository _iBusinesshourRepository;
        private IMapper _mapper;
        public BusinessHourService(IUnitOfWork unitOfWork, IMapper mapper, IBusinesshourRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iBusinesshourRepository = repository;
            _mapper = mapper;
        }
        public Result<BusinessHourModel> Add(BusinessHourModel um)
        {
            var res = new Result<BusinessHourModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<BusinessHourModel, BusinessHour>(um);
            model.ID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<BusinessHour, BusinessHourModel>(model);
            }
            return res;
        }

        public Result<BusinessHourModel> Delete(int ID)
        {
            var res = new Result<BusinessHourModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var FiscalYear = _iBusinesshourRepository.FirstOrDefault(x => x.ID == ID);
            if (FiscalYear == null)
            {
                res.Errors.Add($"We could not find the FYID Master with id = {ID.ToString()}");
                return res;
            }
            FiscalYear.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(FiscalYear);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<BusinessHour, BusinessHourModel>(FiscalYear);
            }
            return res;
        }

        public Result<BusinessHourModel> Edit(int FYID, BusinessHourModel um)
        {
            var res = new Result<BusinessHourModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<BusinessHourModel, BusinessHour>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<BusinessHour, BusinessHourModel>(model);
            }
            return res;
        }

        public IEnumerable<BusinessHourModel> GetAllBusinesshour(int SubscriberID)
        {
            var response = _iBusinesshourRepository.GetAllBusinesshour(SubscriberID);
            return _mapper.Map<IEnumerable<BusinessHour>, List<BusinessHourModel>>(response);
        }

        public BusinessHourModel GetBusinesshourById(int ID)
        {
            var response = _iBusinesshourRepository.GetBusinesshourById(ID);
            return _mapper.Map<BusinessHour, BusinessHourModel>(response);
        }
       
        public BusinessHourModel CheckDuplicateBusinesshour(int TypeID, int? SubscriberID, int ID, string type)
        {
            var response = _iBusinesshourRepository.CheckDuplicateBusinesshour(TypeID, SubscriberID, ID, type);
            return _mapper.Map<BusinessHour, BusinessHourModel>(response);
        }
    }
}
