﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistDAL.RepositoriesInterface.Common;
using System;
using System.Collections.Generic;
using AssistDB.General;
using AssistDAL.RepositoriesInterface.General;
using AssistBLL.Services.General;
using AssistBLL.ViewModel.General;

namespace AssistBLL.Implementation.General
{
    public class CurrenciesService : EntityService<Currencies>, ICurrenciesService
    {
        protected readonly ICurrenciesRepository _iCurrenciesRepository;
        private IMapper _mapper;
        public CurrenciesService(IUnitOfWork unitOfWork, IMapper mapper, ICurrenciesRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iCurrenciesRepository = repository;
            _mapper = mapper;
        }
        public Result<CurrenciesModel> Add(CurrenciesModel um)
        {
            var res = new Result<CurrenciesModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<CurrenciesModel, Currencies>(um);
            model.ID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Currencies, CurrenciesModel>(model);
            }
            return res;
        }

        public Result<CurrenciesModel> Delete(int ID)
        {
            var res = new Result<CurrenciesModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var Currencies = _iCurrenciesRepository.FirstOrDefault(x => x.ID == ID);
            if (Currencies == null)
            {
                res.Errors.Add($"We could not find the FYID Master with id = {ID.ToString()}");
                return res;
            }
            Currencies.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(Currencies);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Currencies, CurrenciesModel>(Currencies);
            }
            return res;
        }

        public Result<CurrenciesModel> Edit(int FYID, CurrenciesModel um)
        {
            var res = new Result<CurrenciesModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<CurrenciesModel, Currencies>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Currencies, CurrenciesModel>(model);
            }
            return res;
        }

        public IEnumerable<CurrenciesModel> GetAllCurrencies(int SubscriberID)
        {
            var response = _iCurrenciesRepository.GetAllCurrencies(SubscriberID);
            return _mapper.Map<IEnumerable<Currencies>, List<CurrenciesModel>>(response);
        }

        public CurrenciesModel GetCurrenciesById(int ID)
        {
            var response = _iCurrenciesRepository.GetCurrenciesById(ID);
            return _mapper.Map<Currencies, CurrenciesModel>(response);
        }
       
        public CurrenciesModel  CheckDuplicateCurrencies(int TypeID, int? SubscriberID, int ID, string type)
        {
            var response = _iCurrenciesRepository.CheckDuplicateCurrencies(TypeID, SubscriberID, ID, type);
            return _mapper.Map<Currencies, CurrenciesModel>(response);
        }
    }
}
