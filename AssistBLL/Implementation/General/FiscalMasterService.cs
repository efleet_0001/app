﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistBLL.Services.Master;
using AssistBLL.ViewModel.Master;
using AssistDAL.RepositoriesInterface.Common;
using System;
using System.Collections.Generic;
using AssistDB.General;
using AssistDAL.RepositoriesInterface.General;
using AssistBLL.Services.General;
using AssistBLL.ViewModel.General;

namespace AssistBLL.Implementation.General
{
    public class FiscalMasterService : EntityService<FiscalYear>, IFiscalMasterService
    {
        protected readonly IFiscalMasterRepository _iFiscalMasterRepository;
        private IMapper _mapper;
        public FiscalMasterService(IUnitOfWork unitOfWork, IMapper mapper, IFiscalMasterRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iFiscalMasterRepository = repository;
            _mapper = mapper;
        }
        public Result<FiscalYearModel> Add(FiscalYearModel um)
        {
            var res = new Result<FiscalYearModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<FiscalYearModel, FiscalYear>(um);
            model.FYID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<FiscalYear, FiscalYearModel>(model);
            }
            return res;
        }

        public Result<FiscalYearModel> Delete(int FYID)
        {
            var res = new Result<FiscalYearModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var FiscalYear = _iFiscalMasterRepository.FirstOrDefault(x => x.FYID == FYID);
            if (FiscalYear == null)
            {
                res.Errors.Add($"We could not find the FYID Master with id = {FYID.ToString()}");
                return res;
            }
            FiscalYear.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(FiscalYear);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<FiscalYear, FiscalYearModel>(FiscalYear);
            }
            return res;
        }

        public Result<FiscalYearModel> Edit(int FYID, FiscalYearModel um)
        {
            var res = new Result<FiscalYearModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<FiscalYearModel, FiscalYear>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<FiscalYear, FiscalYearModel>(model);
            }
            return res;
        }

        public IEnumerable<FiscalYearModel> GetAllFiscal(int SubscriberID)
        {
            var response = _iFiscalMasterRepository.GetAllFiscal(SubscriberID);
            return _mapper.Map<IEnumerable<FiscalYear>, List<FiscalYearModel>>(response);
        }

        public FiscalYearModel GetFiscalById(int FYID)
        {
            var response = _iFiscalMasterRepository.GetFiscalById(FYID);
            return _mapper.Map<FiscalYear, FiscalYearModel>(response);
        }
       
        public FiscalYearModel CheckDuplicateFiscal(string FiscalName, int? SubscriberID, int FiscalID, string type)
        {
            var response = _iFiscalMasterRepository.CheckDuplicateFiscal(FiscalName, SubscriberID, FiscalID, type);
            return _mapper.Map<FiscalYear, FiscalYearModel>(response);
        }
    }
}
