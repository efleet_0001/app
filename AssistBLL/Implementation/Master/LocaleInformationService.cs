﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistBLL.Services.Master;
using AssistBLL.ViewModel.Master;
using AssistDAL.RepositoriesInterface.Common;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB;
using AssistDB.GBLMaster.Master;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.Implementation.Master
{
    public class LocaleInformationService : EntityService<LocaleInformation>, ILocaleInformationService
    {
        protected readonly ILocaleInformationRepository _iLocaleInformationRepository;
        private IMapper _mapper;
        public LocaleInformationService(IUnitOfWork unitOfWork, IMapper mapper, ILocaleInformationRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iLocaleInformationRepository = repository;
            _mapper = mapper;
        }
        public Result<LocaleInformationModel> Add(LocaleInformationModel um)
        {
            var res = new Result<LocaleInformationModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<LocaleInformationModel, LocaleInformation>(um);
            model.LocID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<LocaleInformation, LocaleInformationModel>(model);
            }
            return res;
        }

        public Result<LocaleInformationModel> Delete(int UserID)
        {
            var res = new Result<LocaleInformationModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var LocaleInformation = _iLocaleInformationRepository.FirstOrDefault(x => x.LocID == UserID);
            if (LocaleInformation == null)
            {
                res.Errors.Add($"We could not find the UserID Master with id = {UserID.ToString()}");
                return res;
            }
            LocaleInformation.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(LocaleInformation);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<LocaleInformation, LocaleInformationModel>(LocaleInformation);
            }
            return res;
        }

        public Result<LocaleInformationModel> Edit(int UserID, LocaleInformationModel um)
        {
            var res = new Result<LocaleInformationModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<LocaleInformationModel, LocaleInformation>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<LocaleInformation, LocaleInformationModel>(model);
            }
            return res;
        }

        public IEnumerable<LocaleInformationModel> GetAll(int SubscriberID)
        {
            var response = _iLocaleInformationRepository.GetAll(SubscriberID);
            return _mapper.Map<IEnumerable<LocaleInformation>, List<LocaleInformationModel>>(response);
        }

        public LocaleInformationModel GetLocaleInfoById(int LocaleInfoID)
        {
            var response = _iLocaleInformationRepository.GetLocaleInfoById(LocaleInfoID);
            return _mapper.Map<LocaleInformation, LocaleInformationModel>(response);
        }
        
        public LocaleInformationModel CheckDuplicateLocaleInfo(string LocaleInfoName, int? SubscriberID, int LocaleInfoID, string type)
        {
            var response = _iLocaleInformationRepository.CheckDuplicateLocaleInfo(LocaleInfoName, SubscriberID, LocaleInfoID, type);
            return _mapper.Map<LocaleInformation, LocaleInformationModel>(response);
        }
    }
}
