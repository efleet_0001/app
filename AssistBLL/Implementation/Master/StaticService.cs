﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.KisanModel.Master;
using AssistBLL.Models;
using AssistBLL.Services.Master;
using AssistBLL.ViewModel.Master;
using AssistDAL.RepositoriesInterface.Common;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB;
using AssistDB.BaseMaster;
using AssistDB.GBLMaster.Master;
using AssistDB.Kisan.Master;
using System;
using System.Collections.Generic;
using System.Text;
using AssistDB.General;
using AssistBLL.ViewModel.General;

namespace AssistBLL.Implementation.Master
{
    public class StaticService : IStaticService
    {
        protected readonly IStaticRepository _iStaticRepository;
        private IMapper _mapper;
        public StaticService(IMapper mapper, IStaticRepository repository) 
        {
            _iStaticRepository = repository;
            _mapper = mapper;
        }
        public IEnumerable<ApplicationModel> GetAllApplication(int SubscriberID)
        {
            var response = _iStaticRepository.GetAllApplication(SubscriberID);
            return _mapper.Map<IEnumerable<Application>, List<ApplicationModel>>(response);
        }
        public IEnumerable<ClientModel> GetAllClient(int SubscriberID)
        {
            var response = _iStaticRepository.GetAllClient(SubscriberID);
            return _mapper.Map<IEnumerable<Client>, List<ClientModel>>(response);
        }
        public IEnumerable<ConstantModel> GetAllConstant(int SubscriberID, int ConstantGroupID)
        {
            var response = _iStaticRepository.GetAllConstant(SubscriberID,ConstantGroupID);
            return _mapper.Map<IEnumerable<Constant>, List<ConstantModel>>(response);
        }
        public IEnumerable<CountryModel> GetAllCountry(int SubscriberID)
        {
            var response = _iStaticRepository.GetAllCountry(SubscriberID);
            return _mapper.Map<IEnumerable<Country>, List<CountryModel>>(response);
        }
        public IEnumerable<StateModel> GetAllState(int SubscriberID, int CountryID)
        {
            var response = _iStaticRepository.GetAllState(SubscriberID, CountryID);
            return _mapper.Map<IEnumerable<State>, List<StateModel>>(response);
        }
        public IEnumerable<DistrictModel> GetAllDistrict(int SubscriberID, int StateID)
        {
            var response = _iStaticRepository.GetAllDistrict(SubscriberID, StateID);
            return _mapper.Map<IEnumerable<District>, List<DistrictModel>>(response);
        }
        public IEnumerable<LanguageModel> GetAllLanguage(int SubscriberID)
        {
            var response = _iStaticRepository.GetAllLanguage(SubscriberID);
            return _mapper.Map<IEnumerable<Language>, List<LanguageModel>>(response);
        }
        public IEnumerable<HolidayModel> GetAllHolidays(int DistrictID)
        {
            var response = _iStaticRepository.GetAllHolidays(DistrictID);
            return _mapper.Map<IEnumerable<Holiday>, List<HolidayModel>>(response);
        }
    }
}
