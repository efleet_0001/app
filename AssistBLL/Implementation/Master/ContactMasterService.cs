﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistBLL.Services.Master;
using AssistBLL.ViewModel.Master;
using AssistDAL.RepositoriesInterface.Common;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB.GBLMaster.Master;
using AssistDB.GBLMaster.Master.ViewData;
using System;
using System.Collections.Generic;
using AssistBLL.ViewModel.General.Data;

namespace AssistBLL.Implementation.Master
{
    public class ContactMasterService : EntityService<Contact>, IContactMasterService
    {
        protected readonly IContactMasterRepository _iContactMasterRepository;
        private IMapper _mapper;
        public ContactMasterService(IUnitOfWork unitOfWork, IMapper mapper, IContactMasterRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iContactMasterRepository = repository;
            _mapper = mapper;
        }
        public Result<ContactModel> Add(ContactModel um)
        {
            var res = new Result<ContactModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<ContactModel, Contact>(um);
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Contact, ContactModel>(model);
            }
            return res;
        }

        public Result<ContactModel> Delete(int ContactID)
        {
            var res = new Result<ContactModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var contactMaster = _iContactMasterRepository.FirstOrDefault(x => x.ConID == ContactID);
            if (contactMaster == null)
            {
                res.Errors.Add($"We could not find the Contact Master with id = {ContactID.ToString()}");
                return res;
            }
          
            Update(contactMaster);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Contact, ContactModel>(contactMaster);
            }
            return res;
        }

        public Result<ContactModel> Edit(int ContactID, ContactModel um)
        {
            var res = new Result<ContactModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<ContactModel, Contact>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Contact, ContactModel>(model);
            }
            return res;
        }

        public IEnumerable<ContactModelData> GetAllContact(int SubscriberID)
        {
            var response = _iContactMasterRepository.GetAllContact(SubscriberID);
            return _mapper.Map<IEnumerable<ContactData>, List<ContactModelData>>(response);
        }

        public ContactModelData GetContactById(int ContactID)
        {
            var response = _iContactMasterRepository.GetContactById(ContactID);
            return _mapper.Map<ContactData, ContactModelData>(response);
        }
        public ContactModelData GetContactDetails(int ContactID)
        {
            var response = _iContactMasterRepository.GetContactById(ContactID);
            return _mapper.Map<ContactData, ContactModelData>(response);
        }
        public ContactModel CheckDuplicateContact(string ContactName, int? SubscriberID, int ContactID, string type)
        {
            var response = _iContactMasterRepository.CheckDuplicateContact(ContactName, SubscriberID, ContactID, type);
            return _mapper.Map<Contact, ContactModel>(response);
        }
    }
}
