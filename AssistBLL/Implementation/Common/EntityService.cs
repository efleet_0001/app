﻿using AssistBLL.Services.Common;
using AssistDAL.RepositoriesInterface.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.Implementation.Common
{
    public class EntityService<T> : IEntityService<T> where T : class
    {
        protected IUnitOfWork UnitOfWork;
        protected IGenericRepository<T> Repository;
        public EntityService(IUnitOfWork unitOfWork, IGenericRepository<T> repository)
        {
            UnitOfWork = unitOfWork;
            Repository = repository;
        }
        public IEnumerable<T> GetAll()
        {
            return Repository.GetAll();
        }
        public void Create(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            Repository.Add(entity);
        }

        public void Update(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            Repository.Edit(entity);
        }
        public void Delete(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            Repository.Delete(entity);
        }
    }
}
