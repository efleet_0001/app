﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistBLL.Services.Master;
using AssistBLL.ViewModel.Master;
using AssistDAL.RepositoriesInterface.Common;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB;
using AssistDB.GBLMaster.Master;
using System;
using System.Collections.Generic;
using System.Text;
using AssistDAL.RepositoriesInterface.Automation;
using AssistDB.Automation;
using AssistBLL.ViewModel.Automation;

namespace AssistBLL.Implementation.Master
{
    public class WorkFlowScheduleConditionService : EntityService<WorkFlowScheduleCondition>, IWorkFlowScheduleConditionService
    {
        protected readonly IWorkFlowScheduleConditionRepository _iWorkFlowScheduleConditionRepository;
        private IMapper _mapper;
        public WorkFlowScheduleConditionService(IUnitOfWork unitOfWork, IMapper mapper, IWorkFlowScheduleConditionRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iWorkFlowScheduleConditionRepository = repository;
            _mapper = mapper;
        }
        public Result<WorkFlowScheduleConditionModel> Add(WorkFlowScheduleConditionModel um)
        {
            var res = new Result<WorkFlowScheduleConditionModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<WorkFlowScheduleConditionModel, WorkFlowScheduleCondition>(um);
            model.ConditionId = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<WorkFlowScheduleCondition, WorkFlowScheduleConditionModel>(model);
            }
            return res;
        }

        public Result<WorkFlowScheduleConditionModel> Delete(int RuleId)
        {
            var res = new Result<WorkFlowScheduleConditionModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var WorkFlowScheduleCondition = _iWorkFlowScheduleConditionRepository.FirstOrDefault(x => x.ConditionId == RuleId);
            if (WorkFlowScheduleCondition == null)
            {
                res.Errors.Add($"We could not find the RuleId Master with id = {RuleId.ToString()}");
                return res;
            }
            WorkFlowScheduleCondition.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(WorkFlowScheduleCondition);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<WorkFlowScheduleCondition, WorkFlowScheduleConditionModel>(WorkFlowScheduleCondition);
            }
            return res;
        }

        public Result<WorkFlowScheduleConditionModel> Edit(int RuleId, WorkFlowScheduleConditionModel um)
        {
            var res = new Result<WorkFlowScheduleConditionModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<WorkFlowScheduleConditionModel, WorkFlowScheduleCondition>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<WorkFlowScheduleCondition, WorkFlowScheduleConditionModel>(model);
            }
            return res;
        }

        public IEnumerable<WorkFlowScheduleConditionModel> GetAllWFScheduleCondition(int SubscriberID)
        {
            var response = _iWorkFlowScheduleConditionRepository.GetAllWFScheduleCondition(SubscriberID);
            return _mapper.Map<IEnumerable<WorkFlowScheduleCondition>, List<WorkFlowScheduleConditionModel>>(response);
        }

        public WorkFlowScheduleConditionModel GetWFScheduleConditionById(int RuleId)
        {
            var response = _iWorkFlowScheduleConditionRepository.GetWFScheduleConditionById(RuleId);
            return _mapper.Map<WorkFlowScheduleCondition, WorkFlowScheduleConditionModel>(response);
        }
        
        public WorkFlowScheduleConditionModel CheckDuplicateWFScheduleCondition(int ScheduleId, int? SubscriberID, int RuleId, string type)
        {
            var response = _iWorkFlowScheduleConditionRepository.CheckDuplicateWFScheduleCondition(ScheduleId, SubscriberID, RuleId, type);
            return _mapper.Map<WorkFlowScheduleCondition, WorkFlowScheduleConditionModel>(response);
        }
    }
}
