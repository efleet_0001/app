﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistBLL.Services.Master;
using AssistBLL.ViewModel.Master;
using AssistDAL.RepositoriesInterface.Common;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB;
using AssistDB.GBLMaster.Master;
using System;
using System.Collections.Generic;
using System.Text;
using AssistDB.Automation;
using AssistDAL.RepositoriesInterface.Automation;
using AssistBLL.ViewModel.Automation;

namespace AssistBLL.Implementation.Master
{
    public class WorkFlowRuleService : EntityService<WorkFlowRule>, IWorkFlowRuleService
    {
        protected readonly IWorkFlowRuleRepository _iWorkFlowRuleRepository;
        private IMapper _mapper;
        public WorkFlowRuleService(IUnitOfWork unitOfWork, IMapper mapper, IWorkFlowRuleRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iWorkFlowRuleRepository = repository;
            _mapper = mapper;
        }
        public Result<WorkFlowRuleModel> Add(WorkFlowRuleModel um)
        {
            var res = new Result<WorkFlowRuleModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<WorkFlowRuleModel, WorkFlowRule>(um);
            model.RuleId = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<WorkFlowRule, WorkFlowRuleModel>(model);
            }
            return res;
        }

        public Result<WorkFlowRuleModel> Delete(int RuleId)
        {
            var res = new Result<WorkFlowRuleModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var WorkFlowRule = _iWorkFlowRuleRepository.FirstOrDefault(x => x.RuleId == RuleId);
            if (WorkFlowRule == null)
            {
                res.Errors.Add($"We could not find the RuleId Master with id = {RuleId.ToString()}");
                return res;
            }
            WorkFlowRule.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(WorkFlowRule);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<WorkFlowRule, WorkFlowRuleModel>(WorkFlowRule);
            }
            return res;
        }

        public Result<WorkFlowRuleModel> Edit(int RuleId, WorkFlowRuleModel um)
        {
            var res = new Result<WorkFlowRuleModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<WorkFlowRuleModel, WorkFlowRule>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<WorkFlowRule, WorkFlowRuleModel>(model);
            }
            return res;
        }

        public IEnumerable<WorkFlowRuleModel> GetAllWorkFlowRule(int SubscriberID)
        {
            var response = _iWorkFlowRuleRepository.GetAllWorkFlowRule(SubscriberID);
            return _mapper.Map<IEnumerable<WorkFlowRule>, List<WorkFlowRuleModel>>(response);
        }

        public WorkFlowRuleModel GetWorkFlowRuleById(int RuleId)
        {
            var response = _iWorkFlowRuleRepository.GetWorkFlowRuleById(RuleId);
            return _mapper.Map<WorkFlowRule, WorkFlowRuleModel>(response);
        }
        
        public WorkFlowRuleModel CheckDuplicateWorkFlowRule(string RuleName, int? SubscriberID, int RuleId, string type)
        {
            var response = _iWorkFlowRuleRepository.CheckDuplicateWorkFlowRule(RuleName, SubscriberID, RuleId, type);
            return _mapper.Map<WorkFlowRule, WorkFlowRuleModel>(response);
        }
    }
}
