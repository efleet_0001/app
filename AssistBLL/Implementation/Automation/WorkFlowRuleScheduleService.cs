﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistBLL.Services.Master;
using AssistBLL.ViewModel.Master;
using AssistDAL.RepositoriesInterface.Common;
using AssistDAL.RepositoriesInterface.Master;
using AssistDB;
using AssistDB.GBLMaster.Master;
using System;
using System.Collections.Generic;
using System.Text;
using AssistDAL.RepositoriesInterface.Automation;
using AssistDB.Automation;
using AssistBLL.ViewModel.Automation;

namespace AssistBLL.Implementation.Master
{
    public class WorkFlowRuleScheduleService : EntityService<WorkFlowRuleSchedule>, IWorkFlowRuleScheduleService
    {
        protected readonly IWorkFlowRuleScheduleRepository _iWorkFlowRuleScheduleRepository;
        private IMapper _mapper;
        public WorkFlowRuleScheduleService(IUnitOfWork unitOfWork, IMapper mapper, IWorkFlowRuleScheduleRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iWorkFlowRuleScheduleRepository = repository;
            _mapper = mapper;
        }
        public Result<WorkFlowRuleScheduleModel> Add(WorkFlowRuleScheduleModel um)
        {
            var res = new Result<WorkFlowRuleScheduleModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<WorkFlowRuleScheduleModel, WorkFlowRuleSchedule>(um);
            model.ScheduleId = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<WorkFlowRuleSchedule, WorkFlowRuleScheduleModel>(model);
            }
            return res;
        }

        public Result<WorkFlowRuleScheduleModel> Delete(int ScheduleId)
        {
            var res = new Result<WorkFlowRuleScheduleModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var WorkFlowRuleSchedule = _iWorkFlowRuleScheduleRepository.FirstOrDefault(x => x.ScheduleId == ScheduleId);
            if (WorkFlowRuleSchedule == null)
            {
                res.Errors.Add($"We could not find the ScheduleId Master with id = {ScheduleId.ToString()}");
                return res;
            }
            WorkFlowRuleSchedule.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(WorkFlowRuleSchedule);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<WorkFlowRuleSchedule, WorkFlowRuleScheduleModel>(WorkFlowRuleSchedule);
            }
            return res;
        }

        public Result<WorkFlowRuleScheduleModel> Edit(int ScheduleId, WorkFlowRuleScheduleModel um)
        {
            var res = new Result<WorkFlowRuleScheduleModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<WorkFlowRuleScheduleModel, WorkFlowRuleSchedule>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<WorkFlowRuleSchedule, WorkFlowRuleScheduleModel>(model);
            }
            return res;
        }

        public IEnumerable<WorkFlowRuleScheduleModel> GetAllWorkFlowRuleSchedule(int SubscriberID)
        {
            var response = _iWorkFlowRuleScheduleRepository.GetAllWorkFlowRuleSchedule(SubscriberID);
            return _mapper.Map<IEnumerable<WorkFlowRuleSchedule>, List<WorkFlowRuleScheduleModel>>(response);
        }

        public WorkFlowRuleScheduleModel GetWorkFlowRuleScheduleById(int ScheduleId)
        {
            var response = _iWorkFlowRuleScheduleRepository.GetWorkFlowRuleScheduleById(ScheduleId);
            return _mapper.Map<WorkFlowRuleSchedule, WorkFlowRuleScheduleModel>(response);
        }
        
        public WorkFlowRuleScheduleModel CheckDuplicateWorkFlowRuleSchedule(string ScheduleName, int? SubscriberID, int ScheduleId, string type)
        {
            var response = _iWorkFlowRuleScheduleRepository.CheckDuplicateWorkFlowRuleSchedule(ScheduleName, SubscriberID, ScheduleId, type);
            return _mapper.Map<WorkFlowRuleSchedule, WorkFlowRuleScheduleModel>(response);
        }
    }
}
