﻿using AutoMapper;
using AssistBLL.login;
using AssistBLL.Security;
using AssistBLL.Services.Trans;
using AssistDAL.RepositoriesInterface.Trans;
using AssistDB.login.DB.login;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.Implementation.Trans
{
    public class loginServices : IloginServices
    {
        private readonly IloginRepository _IloginRepository;
        private IMapper _mapper;
        public loginServices(IMapper mapper, IloginRepository repository)
        {
            _IloginRepository = repository;
            _mapper = mapper;
        }
      
        public IEnumerable<FYModelView> GetFYDetails(int SubscriberID, int UserID)
        {
            var response = _IloginRepository.GetFYDetails(SubscriberID, UserID);
            return _mapper.Map<IEnumerable<FYModel>, IEnumerable<FYModelView>>(response);
        }
        
        public SessionModelVeiw CreateSession(SessionModel viewSessionModel)
        {
            var response = _IloginRepository.CreateSession(viewSessionModel);
            return _mapper.Map<SessionModel, SessionModelVeiw>(response);
        }
      
        public string GetPasswordHash(int SubscriberID, int UserID)
        {
            var userPasswordHash = _IloginRepository.GetPasswordHash(SubscriberID, UserID);
            return userPasswordHash.ToString();
        }
        public string ChangePassword(long SubscriberID, long UserID, string username, string passwordhash)
        {
            string enpassword = Utill.GetEncryptPassword(passwordhash, username);
            var userPasswordHash = _IloginRepository.ChangePassword(SubscriberID, UserID, username, enpassword);
            return userPasswordHash.ToString();
        }   
        public loginModelView Getlogin(string username, string password)
        {
            string enpassword = Utill.GetEncryptPassword(password, username);
            var response = _IloginRepository.Getlogin(username, enpassword);
            return _mapper.Map<loginModel, loginModelView>(response);
        }
        public SubscriberView GetSubscriber(long UserID)
        {
            var response = _IloginRepository.GetSubscriber(UserID);
            return _mapper.Map<Subscriber, SubscriberView>(response);
        }
        public MProfileView GetMProfile(long ProfileID)
        {
            var response = _IloginRepository.GetMProfile(ProfileID);
            return _mapper.Map<MProfile, MProfileView>(response);
        }
        public ProfileDetailView ProfileDetail(long ProfileID)
        {
            var response = _IloginRepository.ProfileDetail(ProfileID);
            return _mapper.Map<LProfileDetail, ProfileDetailView>(response);
        }
        public LocaleInfoView GetLocaleInfo(long LocaleID)
        {
            var response = _IloginRepository.GetLocaleInfo(LocaleID);
            return _mapper.Map<LocaleInfo, LocaleInfoView>(response);
        }
    }
}
