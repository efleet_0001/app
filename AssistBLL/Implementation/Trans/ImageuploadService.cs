﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.KisanModel.Trans;
using AssistBLL.KisanModel.Trans.Data;
using AssistBLL.Models;
using AssistBLL.Services.Trans;
using AssistDAL.RepositoriesInterface.Common;
using AssistDAL.RepositoriesInterface.Trans;
using AssistDB.Kisan.Trans;
using AssistDB.Kisan.Trans.Data;
using System;
using System.Collections.Generic;
using AssistDB.GBLMaster.Trans;
using AssistBLL.ViewModel.Trans;

namespace AssistBLL.Implementation.Trans
{
    public class ImageuploadService : EntityService<ImageUpload>, IImageuploadService
    {
        protected readonly IImageuploadRepository _iImageuploadRepository;
        private IMapper _mapper;
        public ImageuploadService(IUnitOfWork unitOfWork, IMapper mapper, IImageuploadRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iImageuploadRepository = repository;
            _mapper = mapper;
        }
        public Result<ImageUploadModel> Add(ImageUploadModel um)
        {
            var res = new Result<ImageUploadModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<ImageUploadModel, ImageUpload>(um);
            Create(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<ImageUpload, ImageUploadModel>(model);
            }
            return res;
        }

        public Result<ImageUploadModel> Delete(long TransactionID, int TransactionTypeID)
        {
            var res = new Result<ImageUploadModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var detail = _iImageuploadRepository.FirstOrDefault(x => x.TransctionID == TransactionID && x.ImageTypeID==TransactionTypeID);
            if (detail == null)
            {
                res.Errors.Add($"We could not find the imgage with id = {TransactionID.ToString()}");
                return res;
            }
            detail.StatusID = 3; // 1 status marked as Deleted

            Delete(detail);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<ImageUpload, ImageUploadModel>(detail);
            }
            return res;
        }

        public Result<ImageUploadModel> Edit(long TransactionID, int TransactionTypeID, ImageUploadModel um)
        {
            var res = new Result<ImageUploadModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<ImageUploadModel, ImageUpload>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<ImageUpload, ImageUploadModel>(model);
            }
            return res;
        }

        public IEnumerable<ImageUploadModel> GetAllImage(int SubscriberID)
        {
            var response = _iImageuploadRepository.GetAllImage(SubscriberID);
            return _mapper.Map<IEnumerable<ImageUpload>, List<ImageUploadModel>>(response);
        }

        public ImageUploadModel GetImageById(long TransactionID, int TransactionTypeID)
        {
            var response = _iImageuploadRepository.GetImageById(TransactionID,TransactionTypeID);
            return _mapper.Map<ImageUpload, ImageUploadModel>(response);
        }

        public ImageUploadModel CheckDuplicateImage(string Name, int? SubscriberID, long TransactionID, int TransactypeID, string type)
        {
            var response = _iImageuploadRepository.CheckDuplicateImage(Name, SubscriberID, TransactionID, TransactypeID, type);
            return _mapper.Map<ImageUpload, ImageUploadModel>(response);
        }
    }
}
