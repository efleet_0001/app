﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.KisanModel.Trans;
using AssistBLL.KisanModel.Trans.Data;
using AssistBLL.Models;
using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Kisan.Trans;
using AssistDB.Kisan.Trans.Data;
using System;
using System.Collections.Generic;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistBLL.Services.Kisan;

namespace AssistBLL.Implementation.Kisan
{
    public class NewsCategoryService : EntityService<NewsCategory>, INewsCategoryService
    {
        protected readonly INewsCategoryRepository _iNewsCategoryRepository;
        private IMapper _mapper;
        public NewsCategoryService(IUnitOfWork unitOfWork, IMapper mapper, INewsCategoryRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iNewsCategoryRepository = repository;
            _mapper = mapper;
        }
        public Result<NewsCategoryModel> Add(NewsCategoryModel um)
        {
            var res = new Result<NewsCategoryModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<NewsCategoryModel, NewsCategory>(um);
            model.CategoryID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<NewsCategory, NewsCategoryModel>(model);
            }
            return res;
        }
        public Result<NewsCategoryModel> Delete(int CategoryID)
        {
            var res = new Result<NewsCategoryModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var Answer = _iNewsCategoryRepository.FirstOrDefault(x => x.CategoryID == CategoryID);
            if (Answer == null)
            {
                res.Errors.Add($"We could not find the News Cat Master with id = {CategoryID.ToString()}");
                return res;
            }
            Answer.StatusID = 3; // 1 status marked as Deleted

            Update(Answer);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<NewsCategory, NewsCategoryModel>(Answer);
            }
            return res;
        }
        public Result<NewsCategoryModel> Edit(int CategoryID, NewsCategoryModel um)
        {
            var res = new Result<NewsCategoryModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<NewsCategoryModel, NewsCategory>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<NewsCategory, NewsCategoryModel>(model);
            }
            return res;
        }
        public IEnumerable<NewsCategoryModelData> GetAllNewsCat(int SubscriberID)
        {
            var response = _iNewsCategoryRepository.GetAllNewsCat(SubscriberID);
            return _mapper.Map<IEnumerable<NewsCategoryData>, List<NewsCategoryModelData>>(response);
        }
        public NewsCategoryModelData GetNewsCatById(int CategoryID)
        {
            var response = _iNewsCategoryRepository.GetNewsCatById(CategoryID);
            return _mapper.Map<NewsCategoryData, NewsCategoryModelData>(response);
        }
        public NewsCategoryModel CheckDuplicateNewsCat(string NewsCatName, int? SubscriberID, int NewsCategoryID, string type)
        {
            var response = _iNewsCategoryRepository.CheckDuplicateNewsCategory(NewsCatName, SubscriberID, NewsCategoryID, type);
            return _mapper.Map<NewsCategory, NewsCategoryModel>(response);
        }
    }
}
