﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.KisanModel.Trans;
using AssistBLL.Models;
using AssistBLL.Services.Trans;
using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Kisan.Trans;
using System;
using System.Collections.Generic;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistBLL.Services.Kisan;

namespace AssistBLL.Implementation.Kisan
{
    public class AnswerService : EntityService<Answer>, IAnswerService
    {
        protected readonly IAnswerRepository _iAnswerRepository;
        private IMapper _mapper;
        public AnswerService(IUnitOfWork unitOfWork, IMapper mapper, IAnswerRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iAnswerRepository = repository;
            _mapper = mapper;
        }
        public Result<AnswerModel> Add(AnswerModel um)
        {
            var res = new Result<AnswerModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<AnswerModel, Answer>(um);
            model.AnsID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Answer, AnswerModel>(model);
            }
            return res;
        }

        public Result<AnswerModel> Delete(int AnswerID)
        {
            var res = new Result<AnswerModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var Answer = _iAnswerRepository.FirstOrDefault(x => x.AnsID == AnswerID);
            if (Answer == null)
            {
                res.Errors.Add($"We could not find the Cat Master with id = {AnswerID.ToString()}");
                return res;
            }
            Answer.StatusID = 3; // 1 status marked as Deleted

            Update(Answer);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Answer, AnswerModel>(Answer);
            }
            return res;
        }

        public Result<AnswerModel> Edit(int AnswerID, AnswerModel um)
        {
            var res = new Result<AnswerModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<AnswerModel, Answer>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Answer, AnswerModel>(model);
            }
            return res;
        }

        public IEnumerable<AnswerModel> GetAllAnswer(int SubscriberID)
        {
            var response = _iAnswerRepository.GetAllAnswer(SubscriberID);
            return _mapper.Map<IEnumerable<Answer>, List<AnswerModel>>(response);
        }

        public AnswerModel GetAnswerById(int AnswerID)
        {
            var response = _iAnswerRepository.FirstOrDefault(x => x.AnsID == AnswerID);
            return _mapper.Map<Answer, AnswerModel>(response);
        }

        public AnswerModel CheckDuplicateAnswer(string AnswerName, int? SubscriberID, int AnswerID, string type)
        {
            var response = _iAnswerRepository.CheckDuplicateAnswer(AnswerName, SubscriberID, AnswerID, type);
            return _mapper.Map<Answer, AnswerModel>(response);
        }
    }
}
