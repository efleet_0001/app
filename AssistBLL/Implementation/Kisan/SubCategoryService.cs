﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.KisanModel.Master;
using AssistBLL.KisanModel.Master.Data;
using AssistBLL.Models;
using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Kisan.Master;
using AssistDB.Kisan.Master.Data;
using System;
using System.Collections.Generic;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistBLL.Services.Kisan;

namespace AssistBLL.Implementation.Kisan
{
    public class SubCategoryService : EntityService<SubCategory>, ISubCategoryService
    {
        protected readonly ISubCategoryRepository _iSubCategoryRepository;
        private IMapper _mapper;
        public SubCategoryService(IUnitOfWork unitOfWork, IMapper mapper, ISubCategoryRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iSubCategoryRepository = repository;
            _mapper = mapper;
        }
        public Result<SubCategoryModel> Add(SubCategoryModel um)
        {
            var res = new Result<SubCategoryModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<SubCategoryModel, SubCategory>(um);
            model.Subcat_ID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<SubCategory, SubCategoryModel>(model);
            }
            return res;
        }

        public Result<SubCategoryModel> Delete(int SubCatID)
        {
            var res = new Result<SubCategoryModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var subCategory = _iSubCategoryRepository.FirstOrDefault(x => x.Subcat_ID == SubCatID);
            if (subCategory == null)
            {
                res.Errors.Add($"We could not find the SubCatID Master with id = {SubCatID.ToString()}");
                return res;
            }
            subCategory.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(subCategory);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<SubCategory, SubCategoryModel>(subCategory);
            }
            return res;
        }

        public Result<SubCategoryModel> Edit(int SubCatID, SubCategoryModel um)
        {
            var res = new Result<SubCategoryModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<SubCategoryModel, SubCategory>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<SubCategory, SubCategoryModel>(model);
            }
            return res;
        }

        public IEnumerable<SubCategoryModelData> GetAllSubCat(int SubscriberID)
        {
            var response = _iSubCategoryRepository.GetAllSubCat(SubscriberID);
            return _mapper.Map<IEnumerable<SubCategoryData>, List<SubCategoryModelData>>(response);
        }

        public SubCategoryModelData GetSubCatById(int SubCatID)
        {
            var response = _iSubCategoryRepository.GetSubCatById(SubCatID);
            return _mapper.Map<SubCategoryData, SubCategoryModelData>(response);
        }
       
        public SubCategoryModel CheckDuplicateSubCat(string SubCatName, int? SubscriberID, int SubCatID, string type)
        {
            var response = _iSubCategoryRepository.CheckDuplicateSubCat(SubCatName, SubscriberID, SubCatID, type);
            return _mapper.Map<SubCategory, SubCategoryModel>(response);
        }
    }
}
