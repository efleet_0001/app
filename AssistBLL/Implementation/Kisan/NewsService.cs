﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.KisanModel.Trans;
using AssistBLL.KisanModel.Trans.Data;
using AssistBLL.Models;
using AssistBLL.Services.Trans;
using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Kisan.Trans;
using AssistDB.Kisan.Trans.Data;
using System;
using System.Collections.Generic;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistBLL.Services.Kisan;

namespace AssistBLL.Implementation.Kisan
{
    public class NewsService : EntityService<News>, INewsService
    {
        protected readonly INewsRepository _iNewsRepository;
        private IMapper _mapper;
        public NewsService(IUnitOfWork unitOfWork, IMapper mapper, INewsRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iNewsRepository = repository;
            _mapper = mapper;
        }
        public Result<NewsModel> Add(NewsModel um)
        {
            var res = new Result<NewsModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<NewsModel, News>(um);
            model.NewsID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<News, NewsModel>(model);
            }
            return res;
        }

        public Result<NewsModel> Delete(int NewsID)
        {
            var res = new Result<NewsModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var Answer = _iNewsRepository.FirstOrDefault(x => x.NewsID == NewsID);
            if (Answer == null)
            {
                res.Errors.Add($"We could not find the News with id = {NewsID.ToString()}");
                return res;
            }
            Answer.StatusID = 3; // 1 status marked as Deleted

            Update(Answer);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<News, NewsModel>(Answer);
            }
            return res;
        }

        public Result<NewsModel> Edit(int NewsID, NewsModel um)
        {
            var res = new Result<NewsModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<NewsModel, News>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<News, NewsModel>(model);
            }
            return res;
        }

        public IEnumerable<NewsModelData> GetAllNews(int SubscriberID)
        {
            var response = _iNewsRepository.GetAllNews(SubscriberID);
            return _mapper.Map<IEnumerable<NewsData>, List<NewsModelData>>(response);
        }

        public NewsModelData GetNewsById(int NewsID)
        {
            var response = _iNewsRepository.GetNewsById(NewsID);
            return _mapper.Map<NewsData, NewsModelData>(response);
        }

        public NewsModel CheckDuplicateNews(string NewsName, int? SubscriberID, int NewsID, string type)
        {
            var response = _iNewsRepository.CheckDuplicateAnswer(NewsName, SubscriberID, NewsID, type);
            return _mapper.Map<News, NewsModel>(response);
        }
        public IEnumerable<NewCategoryDataModel> GetCateGory(int SubscriberID, int LanguageID, long UserID, int EntityID)
        {
            var response = _iNewsRepository.GetCateGory(SubscriberID, LanguageID, UserID, EntityID);
            return _mapper.Map<IEnumerable<NewCategoryData>, IEnumerable<NewCategoryDataModel>>(response);
        }
        public IEnumerable<EntityNewDataModel> GetEntityNew(long CateID, int SubscriberID, int LanguageID, long UserID, int EntityID)
        {
            var response = _iNewsRepository.GetEntityNew(CateID, SubscriberID, LanguageID, UserID, EntityID);
            return _mapper.Map<IEnumerable<EntityNewData>, IEnumerable<EntityNewDataModel>>(response);
        }
    }
}
