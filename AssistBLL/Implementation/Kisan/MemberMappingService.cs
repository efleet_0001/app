﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistBLL.Services.Master;
using AssistDAL.RepositoriesInterface.Common;
using System;
using System.Collections.Generic;
using AssistDB.Kisan.Trans;
using AssistBLL.KisanModel.Trans;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistBLL.Services.Kisan;

namespace AssistBLL.Implementation.Kisan
{
    public class MemberMappingService : EntityService<MemberMapping>, IMemberMappingService
    {
        protected readonly IMemberMappingRepository _MemberMappingRepository;
        private IMapper _mapper;
        public MemberMappingService(IUnitOfWork unitOfWork, IMapper mapper, IMemberMappingRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _MemberMappingRepository = repository;
            _mapper = mapper;
        }
        public Result<MemberMappingModel> Add(MemberMappingModel um)
        {
            var res = new Result<MemberMappingModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<MemberMappingModel, MemberMapping>(um);
            model.MappingID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<MemberMapping, MemberMappingModel>(model);
            }
            return res;
        }

        public Result<MemberMappingModel> Delete(int MappingID)
        {
            var res = new Result<MemberMappingModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var Profile = _MemberMappingRepository.FirstOrDefault(x => x.MappingID == MappingID);
            if (Profile == null)
            {
                res.Errors.Add($"We could not find the  Mapping Master with id = {MappingID.ToString()}");
                return res;
            }
            Profile.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(Profile);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<MemberMapping, MemberMappingModel>(Profile);
            }
            return res;
        }

        public Result<MemberMappingModel> Edit(int MappingID, MemberMappingModel um)
        {
            var res = new Result<MemberMappingModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<MemberMappingModel, MemberMapping>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<MemberMapping, MemberMappingModel>(model);
            }
            return res;
        }

        public IEnumerable<MemberMappingModel> GetAllMemberMapping(int SubscriberID)
        {
            var response = _MemberMappingRepository.GetAllMemberMapping(SubscriberID);
            return _mapper.Map<IEnumerable<MemberMapping>, List<MemberMappingModel>>(response);
        }

        public MemberMappingModel GetMemberMappingById(int MemberID)
        {
            var response = _MemberMappingRepository.GetMemberMappingById(MemberID);
            return _mapper.Map<MemberMapping, MemberMappingModel>(response);
        }
        
        public MemberMappingModel CheckDuplicateMemberMapping(long MemberID, int? SubscriberID, long MappingID, string type)
        {
            var response = _MemberMappingRepository.CheckDuplicateMemberMapping(MemberID, SubscriberID, MappingID, type);
            return _mapper.Map<MemberMapping, MemberMappingModel>(response);
        }
    }
}
