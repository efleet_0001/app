﻿using AssistBLL.Implementation.Common;
using AssistBLL.KisanModel.Trans;
using AssistBLL.KisanModel.Trans.Data;
using AssistBLL.Models;
using AssistBLL.Services.Kisan;
using AssistBLL.Services.Trans;
using AssistDAL.RepositoriesInterface.Common;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistDB.Kisan.Trans;
using AssistDB.Kisan.Trans.Data;
using AutoMapper;
using System;
using System.Collections.Generic;

namespace AssistBLL.Implementation.Kisan
{
    public class NotificationService: EntityService<Notification>,  INotificationService
    {
        protected readonly INotificationRepository _iNotificationRepository;
        private IMapper _mapper;
        public NotificationService(IUnitOfWork unitOfWork, IMapper mapper, INotificationRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iNotificationRepository = repository;
            _mapper = mapper;
        }
        public Result<NotificationModel> Add(NotificationModel um)
        {
            var res = new Result<NotificationModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<NotificationModel, Notification>(um);
            model.NotificationID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Notification, NotificationModel>(model);
            }
            return res;
        }

        public Result<NotificationModel> Delete(int NotificationID)
        {
            var res = new Result<NotificationModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var Answer = _iNotificationRepository.FirstOrDefault(x => x.NotificationID == NotificationID);
            if (Answer == null)
            {
                res.Errors.Add($"We could not find the Notification with id = {NotificationID.ToString()}");
                return res;
            }
            Answer.StatusID = 3; // 1 status marked as Deleted

            Update(Answer);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Notification, NotificationModel>(Answer);
            }
            return res;
        }

        public Result<NotificationModel> Edit(int NotificationID, NotificationModel um)
        {
            var res = new Result<NotificationModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<NotificationModel, Notification>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Notification, NotificationModel>(model);
            }
            return res;
        }

        public IEnumerable<NotificationModelData> GetAppNotification(int MemberID, int subscriberID)
        {
            var response = _iNotificationRepository.GetAppNotification(MemberID,subscriberID);
            return _mapper.Map<IEnumerable<NotificationData>, List<NotificationModelData>>(response);
        }
        public IEnumerable<NotificationModelData> GetAllAppNotification(int MemberID, int subscriberID)
        {
            var response = _iNotificationRepository.GetAllAppNotification(MemberID, subscriberID);
            return _mapper.Map<IEnumerable<NotificationData>, List<NotificationModelData>>(response);
        }
        public IEnumerable<NotificationModelData> GetSMSNotification(int MemberID, int subscriberID)
        {
            var response = _iNotificationRepository.GetSMSNotification(MemberID, subscriberID);
            return _mapper.Map<IEnumerable<NotificationData>, List<NotificationModelData>>(response);
        }
        public IEnumerable<NotificationModelData> GetAllSMSNotification(int MemberID, int subscriberID)
        {
            var response = _iNotificationRepository.GetAllSMSNotification(MemberID, subscriberID);
            return _mapper.Map<IEnumerable<NotificationData>, List<NotificationModelData>>(response);
        }
        public IEnumerable<NotificationModelData> GetWhatsAppNotification(int MemberID, int subscriberID)
        {
            var response = _iNotificationRepository.GetWhatsAppNotification(MemberID, subscriberID);
            return _mapper.Map<IEnumerable<NotificationData>, List<NotificationModelData>>(response);
        }
        public IEnumerable<NotificationModelData> GetAllWhatsAppNotification(int MemberID, int subscriberID)
        {
            var response = _iNotificationRepository.GetAllWhatsAppNotification(MemberID, subscriberID);
            return _mapper.Map<IEnumerable<NotificationData>, List<NotificationModelData>>(response);
        }

    }
}
