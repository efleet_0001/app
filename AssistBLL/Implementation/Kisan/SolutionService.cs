﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.KisanModel.Trans;
using AssistBLL.KisanModel.Trans.Data;
using AssistBLL.Models;
using AssistBLL.Services.Trans;
using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Kisan.Trans;
using AssistDB.Kisan.Trans.Data;
using System;
using System.Collections.Generic;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistBLL.Services.Kisan;

namespace AssistBLL.Implementation.Kisan
{
    public class SolutionService : EntityService<Solution>, ISolutionService
    {
        protected readonly ISolutionRepository _iSolutionRepository;
        private IMapper _mapper;
        public SolutionService(IUnitOfWork unitOfWork, IMapper mapper, ISolutionRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iSolutionRepository = repository;
            _mapper = mapper;
        }
        public Result<SolutionModel> Add(SolutionModel um)
        {
            var res = new Result<SolutionModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<SolutionModel, Solution>(um);
            Create(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Solution, SolutionModel>(model);
            }
            return res;
        }

        public Result<SolutionModel> Delete(int SolnID)
        {
            var res = new Result<SolutionModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var Answer = _iSolutionRepository.FirstOrDefault(x => x.SolnID == SolnID);
            if (Answer == null)
            {
                res.Errors.Add($"We could not find the News with id = {SolnID.ToString()}");
                return res;
            }
            Answer.StatusID = 3; // 1 status marked as Deleted

            Update(Answer);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Solution, SolutionModel>(Answer);
            }
            return res;
        }

        public Result<SolutionModel> Edit(int SolnID, SolutionModel um)
        {
            var res = new Result<SolutionModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<SolutionModel, Solution>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Solution, SolutionModel>(model);
            }
            return res;
        }

        public IEnumerable<SolutionModelData> GetAllSolution(int SubscriberID)
        {
            var response = _iSolutionRepository.GetAllSolution(SubscriberID);
            return _mapper.Map<IEnumerable<SolutionData>, List<SolutionModelData>>(response);
        }

        public SolutionModelData GetSolutionById(int SolnID)
        {
            var response = _iSolutionRepository.GetSolutionById(SolnID);
            return _mapper.Map<SolutionData, SolutionModelData>(response);
        }

        public SolutionModel CheckDuplicateSolution(string SolutionTag, int? SubscriberID, int SolnID, string type)
        {
            var response = _iSolutionRepository.CheckDuplicateSolution(SolutionTag, SubscriberID, SolnID, type);
            return _mapper.Map<Solution, SolutionModel>(response);
        }
    }
}
