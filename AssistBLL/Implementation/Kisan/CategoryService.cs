﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.KisanModel.Master;
using AssistBLL.Models;
using AssistBLL.Services.Master;
using AssistDAL.RepositoriesInterface.Common;
using AssistDB.Kisan.Master;
using System;
using System.Collections.Generic;
using AssistDAL.RepositoriesInterface.Kisan;
using AssistBLL.Services.Kisan;

namespace AssistBLL.Implementation.Kisan
{
    public class CategoryService : EntityService<Category>, ICategoryService
    {
        protected readonly ICategoryRepository _iCategoryRepository;
        private IMapper _mapper;
        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper, ICategoryRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iCategoryRepository = repository;
            _mapper = mapper;
        }
        public Result<CategoryModel> Add(CategoryModel um)
        {
            var res = new Result<CategoryModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<CategoryModel, Category>(um);
            model.CatID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Category, CategoryModel>(model);
            }
            return res;
        }

        public Result<CategoryModel> Delete(int CategoryID)
        {
            var res = new Result<CategoryModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var Category = _iCategoryRepository.FirstOrDefault(x => x.CatID == CategoryID);
            if (Category == null)
            {
                res.Errors.Add($"We could not find the Cat Master with id = {CategoryID.ToString()}");
                return res;
            }
            Category.StatusID = 3; // 1 status marked as Deleted

            Update(Category);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Category, CategoryModel>(Category);
            }
            return res;
        }

        public Result<CategoryModel> Edit(int CategoryID, CategoryModel um)
        {
            var res = new Result<CategoryModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<CategoryModel, Category>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Category, CategoryModel>(model);
            }
            return res;
        }

        public IEnumerable<CategoryModel> GetAllCategory(int SubscriberID)
        {
            var response = _iCategoryRepository.GetAllCategory(SubscriberID);
            return _mapper.Map<IEnumerable<Category>, List<CategoryModel>>(response);
        }

        public CategoryModel GetCategoryById(int CategoryID)
        {
            var response = _iCategoryRepository.FirstOrDefault(x => x.CatID == CategoryID);
            return _mapper.Map<Category, CategoryModel>(response);
        }
      
        public CategoryModel CheckDuplicateCategory(string CategoryName, int? SubscriberID, int CategoryID, string type)
        {
            var response = _iCategoryRepository.CheckDuplicateCategory(CategoryName, SubscriberID, CategoryID, type);
            return _mapper.Map<Category, CategoryModel>(response);
        }
    }
}
