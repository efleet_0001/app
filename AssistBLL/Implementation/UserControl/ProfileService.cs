﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistDAL.RepositoriesInterface.Common;
using System;
using System.Collections.Generic;
using AssistDAL.RepositoriesInterface.UserControl;
using AssistBLL.Services.UserControl;
using AssistBLL.ViewModel.UserControl;
using AssistDB.UserControl;

namespace AssistBLL.Implementation.UserControl
{
    public class ProfileService : EntityService<Profile_D>, IProfileService
    {
        protected readonly IProfileRepository _ProfileRepository;
        private IMapper _mapper;
        public ProfileService(IUnitOfWork unitOfWork, IMapper mapper, IProfileRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _ProfileRepository = repository;
            _mapper = mapper;
        }
        public Result<ProfileModel> Add(ProfileModel um)
        {
            var res = new Result<ProfileModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<ProfileModel, Profile_D>(um);
            model.ProfileID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Profile_D, ProfileModel>(model);
            }
            return res;
        }

        public Result<ProfileModel> Delete(int ProfileID)
        {
            var res = new Result<ProfileModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var Profile = _ProfileRepository.FirstOrDefault(x => x.ProfileID == ProfileID);
            if (Profile == null)
            {
                res.Errors.Add($"We could not find the ProfileID Master with id = {ProfileID.ToString()}");
                return res;
            }
            Profile.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(Profile);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Profile_D, ProfileModel>(Profile);
            }
            return res;
        }

        public Result<ProfileModel> Edit(int ProfileID, ProfileModel um)
        {
            var res = new Result<ProfileModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<ProfileModel, Profile_D>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Profile_D, ProfileModel>(model);
            }
            return res;
        }

        public IEnumerable<ProfileModel> GetAllProfile(int SubscriberID)
        {
            var response = _ProfileRepository.GetAllProfile(SubscriberID);
            return _mapper.Map<IEnumerable<Profile_D>, List<ProfileModel>>(response);
        }

        public ProfileModel GetProfileById(int ProfileID)
        {
            var response = _ProfileRepository.GetProfileById(ProfileID);
            return _mapper.Map<Profile_D, ProfileModel>(response);
        }
        
        public ProfileModel CheckDuplicateProfile(string ProfileName, int? SubscriberID, int ProfileID, string type)
        {
            var response = _ProfileRepository.CheckDuplicateProfile(ProfileName, SubscriberID, ProfileID, type);
            return _mapper.Map<Profile_D, ProfileModel>(response);
        }
    }
}
