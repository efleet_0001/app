﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistBLL.Services.Master;
using AssistBLL.ViewModel.Master;
using AssistDAL.RepositoriesInterface.Common;
using System;
using System.Collections.Generic;
using AssistDB.UserControl;
using AssistDAL.RepositoriesInterface.UserControl;
using AssistBLL.Services.UserControl;
using AssistBLL.ViewModel.UserControl;

namespace AssistBLL.Implementation.UserControl
{
    public class SocialProfileService : EntityService<SocialProfile>, ISocialProfileService
    {
        protected readonly ISocialProfileRepository _iSocialProfileRepository;
        private IMapper _mapper;
        public SocialProfileService(IUnitOfWork unitOfWork, IMapper mapper, ISocialProfileRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iSocialProfileRepository = repository;
            _mapper = mapper;
        }
        public Result<SocialProfileModel> Add(SocialProfileModel um)
        {
            var res = new Result<SocialProfileModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<SocialProfileModel, SocialProfile>(um);
            model.SocID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<SocialProfile, SocialProfileModel>(model);
            }
            return res;
        }

        public Result<SocialProfileModel> Delete(int S_ProfileID)
        {
            var res = new Result<SocialProfileModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var SocialProfile = _iSocialProfileRepository.FirstOrDefault(x => x.SocID == S_ProfileID);
            if (SocialProfile == null)
            {
                res.Errors.Add($"We could not find the S_ProfileID Master with id = {S_ProfileID.ToString()}");
                return res;
            }
            SocialProfile.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(SocialProfile);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<SocialProfile, SocialProfileModel>(SocialProfile);
            }
            return res;
        }

        public Result<SocialProfileModel> Edit(int S_ProfileID, SocialProfileModel um)
        {
            var res = new Result<SocialProfileModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<SocialProfileModel, SocialProfile>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<SocialProfile, SocialProfileModel>(model);
            }
            return res;
        }

        public IEnumerable<SocialProfileModel> GetAllS_Profile(int SubscriberID)
        {
            var response = _iSocialProfileRepository.GetAllS_Profile(SubscriberID);
            return _mapper.Map<IEnumerable<SocialProfile>, List<SocialProfileModel>>(response);
        }

        public SocialProfileModel GetSProfileById(int S_ProfileID)
        {
            var response = _iSocialProfileRepository.GetSProfileById(S_ProfileID);
            return _mapper.Map<SocialProfile, SocialProfileModel>(response);
        }
       
        public SocialProfileModel CheckDuplicateSProfile(string S_ProfileName, int? SubscriberID, int S_ProfileID, string type)
        {
            var response = _iSocialProfileRepository.CheckDuplicateSProfile(S_ProfileName, SubscriberID, S_ProfileID, type);
            return _mapper.Map<SocialProfile, SocialProfileModel>(response);
        }
    }
}
