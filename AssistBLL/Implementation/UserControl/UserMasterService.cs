﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistDAL.RepositoriesInterface.Common;
using AssistDB.ViewModels;
using System;
using System.Collections.Generic;
using AssistBLL.Security;
using AssistDB.UserControl;
using AssistDAL.RepositoriesInterface.UserControl;
using AssistBLL.Services.UserControl;
using AssistBLL.ViewModel.UserControl;
using AssistBLL.ViewModel.UserControl.Data;

namespace AssistBLL.Implementation.UserControl
{
    public class UserMasterService : EntityService<Usermaster>, IUserMasterService
    {
        protected readonly IUserMasterRepository _UserMasterRepository;
        private IMapper _mapper;
        public UserMasterService(IUnitOfWork unitOfWork, IMapper mapper, IUserMasterRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _UserMasterRepository = repository;
            _mapper = mapper;
        }
        public Result<UserMasteModel> Add(UserMasteModel um)
        {
            var res = new Result<UserMasteModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            um.PasswordHash = Utill.GetEncryptPassword(um.PasswordHash, um.Email);
            var model = _mapper.Map<UserMasteModel, Usermaster>(um);
            model.UserID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Usermaster, UserMasteModel>(model);
            }
            return res;
        }

        public Result<UserMasteModel> Delete(int UserID)
        {
            var res = new Result<UserMasteModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var UserMaster = _UserMasterRepository.FirstOrDefault(x => x.UserID == UserID);
            if (UserMaster == null)
            {
                res.Errors.Add($"We could not find the UserID Master with id = {UserID.ToString()}");
                return res;
            }
            UserMaster.blocked = false; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(UserMaster);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Usermaster, UserMasteModel>(UserMaster);
            }
            return res;
        }

        public Result<UserMasteModel> Edit(int UserID, UserMasteModel um)
        {
            var res = new Result<UserMasteModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            um.PasswordHash = Utill.GetEncryptPassword(um.PasswordHash, um.Email);
            var model = _mapper.Map<UserMasteModel, Usermaster>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Usermaster, UserMasteModel>(model);
            }
            return res;
        }

        public IEnumerable<UserMasteModelData> GetListOfUser(int SubscriberID)
        {
            var response = _UserMasterRepository.GetAll(SubscriberID);
            return _mapper.Map<IEnumerable<UsermasterData>, IEnumerable<UserMasteModelData>>(response);
        }

        public UserMasteModelData GetUserById(int UserID)
        {
            var response = _UserMasterRepository.GetUserById(UserID);
            return _mapper.Map<UsermasterData, UserMasteModelData>(response);
        }
        public UserMasteModelData GetUserDetails(int UserID)
        {
            var response = _UserMasterRepository.GetUserDetails(UserID);
            return _mapper.Map<UsermasterData, UserMasteModelData>(response);
        }
        public UserMasteModel CheckDuplicateUser(string UserName, int? SubscriberID, int UserID, string type)
        {
            var response = _UserMasterRepository.CheckDuplicateUser(UserName, SubscriberID, UserID, type);
            return _mapper.Map<Usermaster, UserMasteModel>(response);
        }
    }
}
