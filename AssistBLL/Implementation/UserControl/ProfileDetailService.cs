﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistDAL.RepositoriesInterface.Common;
using AssistDB.GBLMaster.Master.ViewData;
using System;
using System.Collections.Generic;
using AssistDAL.RepositoriesInterface.UserControl;
using AssistBLL.Services.UserControl;
using AssistDB.UserControl;
using AssistBLL.ViewModel.UserControl;
using AssistBLL.ViewModel.UserControl.Data;

namespace AssistBLL.Implementation.UserControl
{
    public class ProfileDetailService : EntityService<ProfileDetail>, IProfileDetailService
    {
        protected readonly IProfileDetailsRepository _iProfileDetailsRepository;
        private IMapper _mapper;
        public ProfileDetailService(IUnitOfWork unitOfWork, IMapper mapper, IProfileDetailsRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iProfileDetailsRepository = repository;
            _mapper = mapper;
        }
        public Result<ProfileDetailsModel> Add(ProfileDetailsModel um)
        {
            var res = new Result<ProfileDetailsModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<ProfileDetailsModel, ProfileDetail>(um);
            model.ProfileID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<ProfileDetail, ProfileDetailsModel>(model);
            }
            return res;
        }

        public Result<ProfileDetailsModel> Delete(int ProfileID)
        {
            var res = new Result<ProfileDetailsModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var Profile = _iProfileDetailsRepository.FirstOrDefault(x => x.ProfileID == ProfileID);
            if (Profile == null)
            {
                res.Errors.Add($"We could not find the ProfileID Master with id = {ProfileID.ToString()}");
                return res;
            }
            Profile.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(Profile);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<ProfileDetail, ProfileDetailsModel>(Profile);
            }
            return res;
        }

        public Result<ProfileDetailsModel> Edit(int ProfileID, ProfileDetailsModel um)
        {
            var res = new Result<ProfileDetailsModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<ProfileDetailsModel, ProfileDetail>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<ProfileDetail, ProfileDetailsModel>(model);
            }
            return res;
        }

        public IEnumerable<ProfileDetailsModelData> GetAllProfile(int SubscriberID)
        {
            var response = _iProfileDetailsRepository.GetAllProfileDetail(SubscriberID);
            return _mapper.Map<IEnumerable<ProfileDetailsData>, List<ProfileDetailsModelData>>(response);
        }
        public ProfileDetailsModelData GetProfileById(int ProfileID)
        {
            var response = _iProfileDetailsRepository.GetProfileDetailById(ProfileID);
            return _mapper.Map<ProfileDetailsData, ProfileDetailsModelData>(response);
        }
        public ProfileDetailsModel CheckDuplicateProfile(string ProfileName, int? SubscriberID, int ProfileID, string type)
        {
            var response = _iProfileDetailsRepository.CheckDuplicateProfileDetail(ProfileName, SubscriberID, ProfileID, type);
            return _mapper.Map<ProfileDetail, ProfileDetailsModel>(response);
        }
    }
}
