﻿using AutoMapper;
using AssistBLL.Implementation.Common;
using AssistBLL.Models;
using AssistDAL.RepositoriesInterface.Common;
using AssistDB.GBLMaster.Master.ViewData;
using System;
using System.Collections.Generic;
using AssistDAL.RepositoriesInterface.UserControl;
using AssistBLL.Services.UserControl;
using AssistDB.UserControl;
using AssistBLL.ViewModel.UserControl;
using AssistBLL.ViewModel.UserControl.Data;

namespace AssistBLL.Implementation.UserControl
{
    public class RoleMasterService : EntityService<Role>, IRoleMasterService
    {
        protected readonly IRoleMasterRepository _iRoleMasterRepository;
        private IMapper _mapper;
        public RoleMasterService(IUnitOfWork unitOfWork, IMapper mapper, IRoleMasterRepository repository) : base(unitOfWork, repository)
        {
            UnitOfWork = unitOfWork;
            _iRoleMasterRepository = repository;
            _mapper = mapper;
        }
        public Result<RoleModel> Add(RoleModel um)
        {
            var res = new Result<RoleModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<RoleModel, Role>(um);
           // model.RoleID = 0;
            Create(model);

            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Role, RoleModel>(model);
            }
            return res;
        }

        public Result<RoleModel> Delete(int RoleID)
        {
            var res = new Result<RoleModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var RoleMaster = _iRoleMasterRepository.FirstOrDefault(x => x.RoleID == RoleID);
            if (RoleMaster == null)
            {
                res.Errors.Add($"We could not find the RoleID Master with id = {RoleID.ToString()}");
                return res;
            }
            RoleMaster.StatusID = 3; // 1 status marked as Deleted
            //   DueMaster.IsDeleted = true;

            Update(RoleMaster);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Role, RoleModel>(RoleMaster);
            }
            return res;
        }

        public Result<RoleModel> Edit(int RoleID, RoleModel um)
        {
            var res = new Result<RoleModel>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = _mapper.Map<RoleModel, Role>(um);
            Update(model);
            if (UnitOfWork.Commit() > 0)
            {
                res.IsSuccess = true;
                res.Data = _mapper.Map<Role, RoleModel>(model);
            }
            return res;
        }

        public IEnumerable<RoleModelData> GetAllRole(int SubscriberID)
        {
            var response = _iRoleMasterRepository.GetAllRole(SubscriberID);
            return _mapper.Map<IEnumerable<RoleData>, List<RoleModelData>>(response);
        }

        public RoleModelData GetRoleById(int RoleID)
        {
            var response = _iRoleMasterRepository.GetRoleById(RoleID);
            return _mapper.Map<RoleData, RoleModelData>(response);
        }
       
        public RoleModel CheckDuplicateRole(string RoleName, int? SubscriberID, int RoleID, string type)
        {
            var response = _iRoleMasterRepository.CheckDuplicateRole(RoleName, SubscriberID, RoleID, type);
            return _mapper.Map<Role, RoleModel>(response);
        }
    }
}
