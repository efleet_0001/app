﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.ViewModel.Automation
{
    public class WorkFlowRuleScheduleModel:BaseViewModel
    {
        public long ScheduleId { get; set; }
        public string ScheduleName { get; set; }
        public long RuleId { get; set; }
        public int ActiontypeId { get; set; }
        public int SubActionTypeId { get; set; }
        public string Description { get; set; }
    }
}
