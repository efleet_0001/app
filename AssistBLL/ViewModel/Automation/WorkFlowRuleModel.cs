﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.ViewModel.Automation
{
    public class WorkFlowRuleModel:BaseViewModel
    {
        public long RuleId { get; set; }
        public int SubscrberFormId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
