﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.ViewModel.Automation
{
    public class WorkFlowScheduleConditionModel : BaseViewModel
    {
        public long ConditionId { get; set; }
        public long ScheduleId { get; set; }
        public long RuleId { get; set; }
        public int FormId { get; set; }
        public int FieldId { get; set; }
        public int ConditionTypeId { get; set; }
        public int Description { get; set; }
    }
}
