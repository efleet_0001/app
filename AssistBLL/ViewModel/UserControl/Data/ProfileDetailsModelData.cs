﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.ViewModel.UserControl.Data
{
    public class ProfileDetailsModelData
    {
        public long ProfileD_ID { get; set; }
        public long ProfileID { get; set; }
        public byte[] BG_Image { get; set; }
        public byte[] DP_Image { get; set; }
        public int? CateID { get; set; }
        public int? SubCatID { get; set; }
        public int? EntityID { get; set; }
        public string Discription { get; set; }
        public string ProfileName { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string EntityName { get; set; }

    }
}
