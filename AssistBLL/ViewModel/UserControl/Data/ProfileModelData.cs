﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.ViewModel.UserControl.Data
{
   public class ProfileModelData
    {
        public long ProfileID { get; set; }
        public string Name { get; set; }
        public int TypeID { get; set; }
        public long? CloneID { get; set; }
        public string Discription { get; set; }
        public string TypeName { get; set; }
        public string CloneName { get; set; }
    }
}
