﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.ViewModel.UserControl.Data
{
   public class RoleModelData
    {
        public int RoleID { get; set; }
        public string Name { get; set; }
        public int? CloneID { get; set; }
        public string Description { get; set; }
        public string CloneName { get; set; }
    }
}
