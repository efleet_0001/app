﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.ViewModel.UserControl
{
    public class UserGroupModel : BaseViewModel
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
    }
}
