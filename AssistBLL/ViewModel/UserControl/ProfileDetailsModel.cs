﻿namespace AssistBLL.ViewModel.UserControl
{
    public class ProfileDetailsModel
    {
        public long ProfileD_ID { get; set; }
        public long ProfileID { get; set; }
        public byte[] BG_Image { get; set; }
        public byte[] DP_Image { get; set; }
        public int? CateID { get; set; }
        public int? SubCatID { get; set; }
        public int? EntityID { get; set; }
        public string Discription { get; set; }
    }
}
