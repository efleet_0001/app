﻿namespace AssistBLL.ViewModel.UserControl
{
    using System.ComponentModel.DataAnnotations;


    public class SocialProfileModel : BaseViewModel
    {
        public long SocID { get; set; }
        public int? EntityTypeID { get; set; }
        public int? EntityID { get; set; }
        [StringLength(50)]
        public string SocialType { get; set; }
        [StringLength(50)]
        public string SocialID { get; set; }
        [StringLength(500)]
        public string Authtoken { get; set; }
        [StringLength(500)]
        public string Authurl { get; set; }
        [StringLength(500)]
        public string AuthPassword { get; set; }


    }
}
