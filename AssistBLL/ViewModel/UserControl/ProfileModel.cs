﻿namespace AssistBLL.ViewModel.UserControl
{
    using System.ComponentModel.DataAnnotations;

    public class ProfileModel : BaseViewModel
    {

        public long ProfileID { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        public int TypeID { get; set; }
        public long? CloneID { get; set; }
        [StringLength(500)]
        public string Discription { get; set; }


    }
}
