﻿namespace AssistBLL.ViewModel.UserControl
{
    using System.ComponentModel.DataAnnotations;

    public class RoleModel : BaseViewModel
    {
        public int RoleID { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        public int? CloneID { get; set; }
        [StringLength(500)]
        public string Description { get; set; }


    }
}
