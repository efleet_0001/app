﻿

namespace AssistBLL.ViewModel.Master
{
    using System.ComponentModel.DataAnnotations;


    public partial class ConstantGroupModel
    {
        public int ConsGrpID { get; set; }
        public int? ConsGrpName { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public bool? StatusID { get; set; }
    }
}
