﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.ViewModel.Master
{
    public class ClientModel:BaseViewModel
    {
        public int ClientID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
