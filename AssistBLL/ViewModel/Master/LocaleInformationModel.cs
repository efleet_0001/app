﻿namespace AssistBLL.ViewModel.Master
{
    using System.ComponentModel.DataAnnotations;


    public class LocaleInformationModel : BaseViewModel
    {
        public long LocID { get; set; }
        public int? LanguageID { get; set; }
        public int? CountryID { get; set; }
        public int? StateID { get; set; }
        public int? DistrictID { get; set; }
        public string PinCode { get; set; }
        public string VillMName { get; set; }
        public string Latlong { get; set; }
        public int? TimeFormat { get; set; }
        public string Time_Zone { get; set; }
        [StringLength(50)]
        public string Currency { get; set; }
        [StringLength(500)]
        public string Signature { get; set; }

    }
}
