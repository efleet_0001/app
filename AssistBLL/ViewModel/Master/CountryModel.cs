﻿namespace AssistBLL.ViewModel.Master
{
    using System.ComponentModel.DataAnnotations;


    public partial class CountryModel
    {
        public int ContryID { get; set; }
        [StringLength(50)]
        public string CountryName { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public bool? StatusID { get; set; }
    }
}
