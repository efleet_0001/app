﻿namespace AssistBLL.ViewModel.Master
{
    public class DistrictModel
    {
        public int DistrictID { get; set; }
        public string DistrictName { get; set; }
        public string Districthood { get; set; }
        public string Code { get; set; }
        public string Capital { get; set; }
        public string LargestCity { get; set; }
        public string Population { get; set; }
        public string Area { get; set; }
        public string Languge { get; set; }
        public int StateID { get; set; }
        public string Discription { get; set; }
        public int Status { get; set; }
    }
}
