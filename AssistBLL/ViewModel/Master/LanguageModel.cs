﻿namespace AssistBLL.ViewModel.Master
{
    using System.ComponentModel.DataAnnotations;


    public partial class LanguageModel
    {
        public int LanID { get; set; }
        [StringLength(50)]
        public string Culture { get; set; }
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public bool? StatusID { get; set; }
    }
}
