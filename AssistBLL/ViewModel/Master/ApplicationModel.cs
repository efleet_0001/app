﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.ViewModel.Master
{
    public class ApplicationModel:BaseViewModel
    {
        public int ApplicationId { get; set; }
        public int AppTypeId { get; set; }
        public int AppNatureId { get; set; }
        public string AppName { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }

    }
}
