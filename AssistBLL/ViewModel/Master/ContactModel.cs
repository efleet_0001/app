﻿namespace AssistBLL.ViewModel.Master
{
    using System.ComponentModel.DataAnnotations;

    public class ContactModel : BaseViewModel
    {
        public long ConID { get; set; }
        [StringLength(500)]
        public string Street { get; set; }
        [StringLength(50)]
        public string City { get; set; }
        public int? DistrictID { get; set; }
        public int? StateID { get; set; }
        public int? CountryID { get; set; }
        [StringLength(50)]
        public string Zip_Code { get; set; }
        [StringLength(500)]
        public string Description { get; set; }

    }
}
