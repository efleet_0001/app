﻿namespace AssistBLL.ViewModel.Master
{
    public class StateModel
    {
        public StateModel() { }
        public int StateID { get; set; }
        public string StateName { get; set; }
    }
}
