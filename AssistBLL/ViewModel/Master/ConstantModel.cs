﻿namespace AssistBLL.ViewModel.Master
{
    using System.ComponentModel.DataAnnotations;


    public partial class ConstantModel
    {
        public int ConstantID { get; set; }
        public int? ConsGroupID { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(50)]
        public string Alias { get; set; }
    }
}
