﻿namespace AssistBLL.ViewModel.General
{
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("CompanyDetails")]
    public class CompanyDetailsModel : BaseViewModel
    {
        public CompanyDetailsModel() { }
        public long CompID { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public int EmpCount { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Website { get; set; }
        public int RoleID { get; set; }
        public string Description { get; set; }

    }
}
