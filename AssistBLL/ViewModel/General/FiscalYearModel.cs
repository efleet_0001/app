﻿namespace AssistBLL.ViewModel.General
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;


    public class FiscalYearModel : BaseViewModel
    {
        public int FYID { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }


    }
}
