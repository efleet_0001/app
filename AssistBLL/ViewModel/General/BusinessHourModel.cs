﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.ViewModel.General
{
    public class BusinessHourModel : BaseViewModel
    {
        public int ID { get; set; }
        public int? TypeID { get; set; }
        public int? DayID { get; set; }
        public int? StartDayID { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Description { get; set; }
    }
}
