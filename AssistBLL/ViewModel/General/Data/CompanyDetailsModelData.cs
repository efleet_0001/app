﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.ViewModel.General.Data
{
  public  class CompanyDetailsModelData:BaseViewModel
    {
        public long CompID { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public int EmpCount { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Website { get; set; }
        public int RoleID { get; set; }
        public string Description { get; set; }
        public string RoleName { get; set; }
    }
}
