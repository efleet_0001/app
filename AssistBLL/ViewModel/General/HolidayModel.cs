﻿namespace AssistBLL.ViewModel.General
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class HolidayModel : BaseViewModel
    {
        public int HoliDayID { get; set; }
        [StringLength(50)]
        public string HolidayName { get; set; }
        public DateTime? HolidayDate { get; set; }


    }
}
