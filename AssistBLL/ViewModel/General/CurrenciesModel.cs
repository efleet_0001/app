﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.ViewModel.General
{
   public class CurrenciesModel :BaseViewModel
    {
        public int ID { get; set; }
        public int? CurrencyID { get; set; }
        public string Format { get; set; }
        public float? ExchangeRate { get; set; }
        public int? Symbol { get; set; }
        public int? ThousandSeprator { get; set; }
        public int? DecimalPlace { get; set; }
        public string DecimalSeprator { get; set; }
        public string Description { get; set; }
    }
}
