﻿namespace AssistBLL.ViewModel.Trans
{
    public class ImageUploadModel : BaseViewModel
    {
        public long ImageID { get; set; }
        public long TransctionID { get; set; }
        public string Name { get; set; }
        public int ImageTypeID { get; set; }
        public string Path { get; set; }
        public string Discription { get; set; }
    }
}
