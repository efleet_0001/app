﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.General;
using AssistDB.General;
using System.Collections.Generic;

namespace AssistBLL.Services.General
{
    public interface IBusinessHourService : IEntityService<BusinessHour>
    {
        Result<BusinessHourModel> Add(BusinessHourModel um);
        Result<BusinessHourModel> Edit(int ID, BusinessHourModel um);
        Result<BusinessHourModel> Delete(int ID);
        BusinessHourModel GetBusinesshourById(int ID);
        IEnumerable<BusinessHourModel> GetAllBusinesshour(int SubscriberID);
        BusinessHourModel CheckDuplicateBusinesshour(int TypeID, int? SubscriberID, int ID, string type);
    }
}
