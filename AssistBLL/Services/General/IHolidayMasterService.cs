﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.General;
using AssistDB.General;
using System.Collections.Generic;

namespace AssistBLL.Services.General
{
    public interface IHolidayMasterService : IEntityService<Holiday>
    {
        Result<HolidayModel> Add(HolidayModel um);
        Result<HolidayModel> Edit(int HolidayID, HolidayModel um);
        Result<HolidayModel> Delete(int HolidayID);
        HolidayModel GetHolidayById(int HolidayID);
        IEnumerable<HolidayModel> GetAllHoliday(int SubscriberID);
        HolidayModel CheckDuplicateHoliday(string HolidayName, int? SubscriberID, int HolidayID, string type);
    }
}
