﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.General;
using AssistDB.General;
using System.Collections.Generic;

namespace AssistBLL.Services.General
{
    public interface ICurrenciesService : IEntityService<Currencies>
    {
        Result<CurrenciesModel> Add(CurrenciesModel um);
        Result<CurrenciesModel> Edit(int ID, CurrenciesModel um);
        Result<CurrenciesModel> Delete(int ID);
        CurrenciesModel GetCurrenciesById(int ID);
        IEnumerable<CurrenciesModel> GetAllCurrencies(int SubscriberID);
        CurrenciesModel CheckDuplicateCurrencies(int TypeID, int? SubscriberID, int ID, string type);
    }
}
