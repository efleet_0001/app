﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.General;
using AssistBLL.ViewModel.General.Data;
using AssistDB.General;
using System.Collections.Generic;

namespace AssistBLL.Services.General
{
    public interface ICompanyMasterService : IEntityService<CompanyDetails>
    {
        Result<CompanyDetailsModel> Add(CompanyDetailsModel um);
        Result<CompanyDetailsModel> Edit(int CompanyID, CompanyDetailsModel um);
        Result<CompanyDetailsModel> Delete(int CompanyID);
        CompanyDetailsModelData GetCompanyById(int CompanyID);
        IEnumerable<CompanyDetailsModelData> GetAllCompany(int SubscriberID);
        CompanyDetailsModel CheckDuplicateCompany(string CompanyName, int? SubscriberID, int CompanyID, string type);
    }
}
