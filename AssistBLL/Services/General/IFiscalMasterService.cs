﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.General;
using AssistDB.General;
using System.Collections.Generic;

namespace AssistBLL.Services.General
{
    public interface IFiscalMasterService : IEntityService<FiscalYear>
    {
        Result<FiscalYearModel> Add(FiscalYearModel um);
        Result<FiscalYearModel> Edit(int FYID, FiscalYearModel um);
        Result<FiscalYearModel> Delete(int FYID);
        FiscalYearModel GetFiscalById(int FYID);
        IEnumerable<FiscalYearModel> GetAllFiscal(int SubscriberID);
        FiscalYearModel CheckDuplicateFiscal(string FiscalName, int? SubscriberID, int FiscalID, string type);
    }
}
