﻿using System;
using System.Collections.Generic;
using System.Text;
using AssistBLL.login;
using AssistDB.login.DB.login;

namespace AssistBLL.Services.Trans
{
    public interface IloginServices
    {

        IEnumerable<FYModelView> GetFYDetails(int SubscriberID, int UserID);
        SessionModelVeiw CreateSession(SessionModel viewSessionModel);
        string GetPasswordHash(int SubscriberID, int UserID);
        string ChangePassword(long SubscriberID, long UserID, string username, string passwordhash);
        loginModelView Getlogin(string username, string password);
        SubscriberView GetSubscriber(long UserID);
        MProfileView GetMProfile(long ProfileID);
        ProfileDetailView ProfileDetail(long ProfileID);
        LocaleInfoView GetLocaleInfo(long LocaleID);
    }

}
