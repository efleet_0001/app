﻿using AssistBLL.KisanModel.Trans;
using AssistBLL.KisanModel.Trans.Data;
using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.Trans;
using AssistDB.GBLMaster.Trans;
using AssistDB.Kisan.Trans;
using System.Collections.Generic;

namespace AssistBLL.Services.Trans
{
    public interface IImageuploadService : IEntityService<ImageUpload>
    {
        Result<ImageUploadModel> Add(ImageUploadModel um);
        Result<ImageUploadModel> Edit(long TransactionID, int TransactionTypeID, ImageUploadModel um);
        Result<ImageUploadModel> Delete(long TransactionID, int TransactionTypeID);
        ImageUploadModel GetImageById(long TransactionID, int TransactionTypeID);
        IEnumerable<ImageUploadModel> GetAllImage(int SubscriberID);
        ImageUploadModel CheckDuplicateImage(string Name, int? SubscriberID, long TransactionID, int TransactypeID, string type);
    }
}
