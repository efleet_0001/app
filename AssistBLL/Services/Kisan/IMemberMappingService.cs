﻿using AutoMapper;
using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.Master;
using AssistDB;
using AssistDB.GBLMaster.Master;
using System;
using System.Collections.Generic;
using System.Text;
using AssistDB.Kisan.Trans;
using AssistBLL.KisanModel.Trans;

namespace AssistBLL.Services.Kisan
{
    public interface IMemberMappingService : IEntityService<MemberMapping>
    {
        Result<MemberMappingModel> Add(MemberMappingModel um);
        Result<MemberMappingModel> Edit(int MappingID, MemberMappingModel um);
        Result<MemberMappingModel> Delete(int MappingID);
        MemberMappingModel GetMemberMappingById(int MemberID);
        IEnumerable<MemberMappingModel> GetAllMemberMapping(int SubscriberID);
        MemberMappingModel CheckDuplicateMemberMapping(long MemberID, int? SubscriberID, long MappingID, string type);
    }
}
