﻿using AssistBLL.KisanModel.Trans;
using AssistBLL.KisanModel.Trans.Data;
using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistDB.Kisan.Trans;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.Services.Kisan
{
    public interface INotificationService : IEntityService<Notification>
    {
        Result<NotificationModel> Add(NotificationModel um);
        Result<NotificationModel> Edit(int NotificationID, NotificationModel um);
        Result<NotificationModel> Delete(int NotificationID);
        IEnumerable<NotificationModelData> GetAppNotification(int MemberID, int subscriberID);
        IEnumerable<NotificationModelData> GetAllAppNotification(int MemberID, int subscriberID);
        IEnumerable<NotificationModelData> GetSMSNotification(int MemberID, int subscriberID);
        IEnumerable<NotificationModelData> GetAllSMSNotification(int MemberID, int subscriberID);
        IEnumerable<NotificationModelData> GetWhatsAppNotification(int MemberID, int subscriberID);
        IEnumerable<NotificationModelData> GetAllWhatsAppNotification(int MemberID, int subscriberID);
    }
}
