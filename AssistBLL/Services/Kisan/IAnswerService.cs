﻿using AssistBLL.KisanModel.Trans;
using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistDB.Kisan.Trans;
using System.Collections.Generic;

namespace AssistBLL.Services.Kisan
{
    public interface IAnswerService : IEntityService<Answer>
    {
        Result<AnswerModel> Add(AnswerModel um);
        Result<AnswerModel> Edit(int AnswerID, AnswerModel um);
        Result<AnswerModel> Delete(int AnswerID);
        AnswerModel GetAnswerById(int AnswerID);
        IEnumerable<AnswerModel> GetAllAnswer(int SubscriberID);
        AnswerModel CheckDuplicateAnswer(string AnswerName, int? SubscriberID, int AnswerID, string type);
    }
}
