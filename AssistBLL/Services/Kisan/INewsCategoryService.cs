﻿using AssistBLL.KisanModel.Master.Data;
using AssistBLL.KisanModel.Trans;
using AssistBLL.KisanModel.Trans.Data;
using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.Master;
using AssistDB;
using AssistDB.GBLMaster.Master;
using AssistDB.Kisan.Master;
using AssistDB.Kisan.Trans;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.Services.Kisan
{
    public interface INewsCategoryService : IEntityService<NewsCategory>
    {
        Result<NewsCategoryModel> Add(NewsCategoryModel um);
        Result<NewsCategoryModel> Edit(int NewsCategoryID, NewsCategoryModel um);
        Result<NewsCategoryModel> Delete(int NewsCategoryID);
        NewsCategoryModelData GetNewsCatById(int NewsCategoryID);
        IEnumerable<NewsCategoryModelData> GetAllNewsCat(int SubscriberID);
        NewsCategoryModel CheckDuplicateNewsCat(string NewsCatName, int? SubscriberID, int NewsCategoryID, string type);
    }
}
