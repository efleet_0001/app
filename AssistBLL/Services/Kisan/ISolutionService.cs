﻿using AssistBLL.KisanModel.Trans;
using AssistBLL.KisanModel.Trans.Data;
using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistDB.Kisan.Trans;
using System.Collections.Generic;

namespace AssistBLL.Services.Kisan
{
    public interface ISolutionService : IEntityService<Solution>
    {
        Result<SolutionModel> Add(SolutionModel um);
        Result<SolutionModel> Edit(int SolnID, SolutionModel um);
        Result<SolutionModel> Delete(int SolnID);
        SolutionModelData GetSolutionById(int SolnID);
        IEnumerable<SolutionModelData> GetAllSolution(int SubscriberID);
        SolutionModel CheckDuplicateSolution(string SolutionTag, int? SubscriberID, int SolnID, string type);
    }
}
