﻿using AssistBLL.KisanModel.Master;
using AssistBLL.KisanModel.Master.Data;
using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistDB.Kisan.Master;
using System.Collections.Generic;

namespace AssistBLL.Services.Kisan
{
    public interface ISubCategoryService : IEntityService<SubCategory>
    {
        Result<SubCategoryModel> Add(SubCategoryModel um);
        Result<SubCategoryModel> Edit(int SubCatID, SubCategoryModel um);
        Result<SubCategoryModel> Delete(int SubCatID);
        SubCategoryModelData GetSubCatById(int SubCatID);
        IEnumerable<SubCategoryModelData> GetAllSubCat(int SubscriberID);
        SubCategoryModel CheckDuplicateSubCat(string SubCatName, int? SubscriberID, int SubCatID, string type);
    }
}
