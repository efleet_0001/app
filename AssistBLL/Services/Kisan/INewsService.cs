﻿using AssistBLL.KisanModel.Master.Data;
using AssistBLL.KisanModel.Trans;
using AssistBLL.KisanModel.Trans.Data;
using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.Master;
using AssistDB;
using AssistDB.GBLMaster.Master;
using AssistDB.Kisan.Master;
using AssistDB.Kisan.Trans;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.Services.Kisan
{
    public interface INewsService : IEntityService<News>
    {
        Result<NewsModel> Add(NewsModel um);
        Result<NewsModel> Edit(int NewsID, NewsModel um);
        Result<NewsModel> Delete(int NewsID);
        NewsModelData GetNewsById(int NewsID);
        IEnumerable<NewsModelData> GetAllNews(int SubscriberID);
        NewsModel CheckDuplicateNews(string NewsName, int? SubscriberID, int NewsID, string type);
        IEnumerable<NewCategoryDataModel> GetCateGory(int SubscriberID, int LanguageID, long UserID, int EntityID);
        IEnumerable<EntityNewDataModel> GetEntityNew(long CateID, int SubscriberID, int LanguageID, long UserID, int EntityID);
    }
}
