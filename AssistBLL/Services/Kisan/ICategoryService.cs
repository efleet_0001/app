﻿using AssistBLL.KisanModel.Master;
using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.Master;
using AssistDB;
using AssistDB.Kisan.Master;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.Services.Kisan
{
    public interface ICategoryService : IEntityService<Category>
    {
        Result<CategoryModel> Add(CategoryModel um);
        Result<CategoryModel> Edit(int CategoryID, CategoryModel um);
        Result<CategoryModel> Delete(int CategoryID);
        CategoryModel GetCategoryById(int CategoryID);
        IEnumerable<CategoryModel> GetAllCategory(int SubscriberID);
        CategoryModel CheckDuplicateCategory(string CategoryName, int? SubscriberID, int CategoryID, string type);
    }
}
