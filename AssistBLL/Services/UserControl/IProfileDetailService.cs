﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using System.Collections.Generic;
using AssistBLL.ViewModel.UserControl;
using AssistDB.UserControl;
using AssistBLL.ViewModel.UserControl.Data;

namespace AssistBLL.Services.UserControl
{
    public interface IProfileDetailService : IEntityService<ProfileDetail>
    {
        Result<ProfileDetailsModel> Add(ProfileDetailsModel um);
        Result<ProfileDetailsModel> Edit(int ProfileID, ProfileDetailsModel um);
        Result<ProfileDetailsModel> Delete(int ProfileID);
        ProfileDetailsModelData GetProfileById(int ProfileID);
        IEnumerable<ProfileDetailsModelData> GetAllProfile(int SubscriberID);
        ProfileDetailsModel CheckDuplicateProfile(string ProfileName, int? SubscriberID, int ProfileID, string type);
    }
}
