﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using System.Collections.Generic;
using AssistBLL.ViewModel.UserControl;
using AssistDB.UserControl;

namespace AssistBLL.Services.UserControl
{
    public interface IProfileService : IEntityService<Profile_D>
    {
        Result<ProfileModel> Add(ProfileModel um);
        Result<ProfileModel> Edit(int ProfileID, ProfileModel um);
        Result<ProfileModel> Delete(int ProfileID);
        ProfileModel GetProfileById(int ProfileID);
        IEnumerable<ProfileModel> GetAllProfile(int SubscriberID);
        ProfileModel CheckDuplicateProfile(string ProfileName, int? SubscriberID, int ProfileID, string type);
    }
}
