﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.UserControl;
using AssistBLL.ViewModel.UserControl.Data;
using AssistDB.UserControl;
using System.Collections.Generic;

namespace AssistBLL.Services.UserControl
{
    public interface IRoleMasterService : IEntityService<Role>
    {
        Result<RoleModel> Add(RoleModel um);
        Result<RoleModel> Edit(int RoleID, RoleModel um);
        Result<RoleModel> Delete(int RoleID);
        RoleModelData GetRoleById(int RoleID);
        IEnumerable<RoleModelData> GetAllRole(int SubscriberID);
        RoleModel CheckDuplicateRole(string RoleName, int? SubscriberID, int RoleID, string type);
    }
}
