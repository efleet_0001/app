﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.UserControl;
using AssistDB.UserControl;
using System.Collections.Generic;

namespace AssistBLL.Services.UserControl
{
    public interface ISocialProfileService : IEntityService<SocialProfile>
    {
        Result<SocialProfileModel> Add(SocialProfileModel um);
        Result<SocialProfileModel> Edit(int S_ProfileID, SocialProfileModel um);
        Result<SocialProfileModel> Delete(int S_ProfileID);
        SocialProfileModel GetSProfileById(int S_ProfileID);
        IEnumerable<SocialProfileModel> GetAllS_Profile(int SubscriberID);
        SocialProfileModel CheckDuplicateSProfile(string S_ProfileName, int? SubscriberID, int S_ProfileID, string type);
    }
}
