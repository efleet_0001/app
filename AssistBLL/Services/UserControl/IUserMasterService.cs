﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.UserControl;
using AssistBLL.ViewModel.UserControl.Data;
using AssistDB.UserControl;
using System.Collections.Generic;

namespace AssistBLL.Services.UserControl
{
    public interface IUserMasterService : IEntityService<Usermaster>
    {
        Result<UserMasteModel> Add(UserMasteModel um);
        Result<UserMasteModel> Edit(int UserID, UserMasteModel um);
        Result<UserMasteModel> Delete(int UserID);
        UserMasteModelData GetUserById(int UserID);
        UserMasteModelData GetUserDetails(int UserID);
        IEnumerable<UserMasteModelData> GetListOfUser(int SubscriberID);
        UserMasteModel CheckDuplicateUser(string UserName, int? SubscriberID, int UserID, string type);
    }
}
