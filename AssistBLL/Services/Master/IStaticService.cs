﻿using AssistBLL.KisanModel.Master;
using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.General;
using AssistBLL.ViewModel.Master;
using AssistDB;
using AssistDB.Kisan.Master;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.Services.Master
{
    public interface IStaticService 
    {
        IEnumerable<ApplicationModel> GetAllApplication(int SubscriberID);
        IEnumerable<ClientModel> GetAllClient(int SubscriberID);
        IEnumerable<ConstantModel> GetAllConstant(int SubscriberID, int ConstantGroupID);
        IEnumerable<CountryModel> GetAllCountry(int SubscriberID);
        IEnumerable<StateModel> GetAllState(int SubscriberID, int CountryID);
        IEnumerable<DistrictModel> GetAllDistrict(int SubscriberID, int StateID);
        IEnumerable<LanguageModel> GetAllLanguage(int SubscriberID);
        IEnumerable<HolidayModel> GetAllHolidays(int SubscriberID);
    }
}
