﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.General.Data;
using AssistBLL.ViewModel.Master;
using AssistDB.GBLMaster.Master;
using System.Collections.Generic;

namespace AssistBLL.Services.Master
{
    public interface IContactMasterService : IEntityService<Contact>
    {
        Result<ContactModel> Add(ContactModel um);
        Result<ContactModel> Edit(int ContactID, ContactModel um);
        Result<ContactModel> Delete(int ContactID);
        ContactModelData GetContactById(int ContactID);
        ContactModelData GetContactDetails(int ContactID);
        IEnumerable<ContactModelData> GetAllContact(int SubscriberID);
        ContactModel CheckDuplicateContact(string ContactName, int? SubscriberID, int ContactID, string type);
    }
}
