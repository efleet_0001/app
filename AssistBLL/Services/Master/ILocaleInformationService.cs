﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.Master;
using AssistDB;
using AssistDB.GBLMaster.Master;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.Services.Master
{
    public interface ILocaleInformationService : IEntityService<LocaleInformation>
    {
        Result<LocaleInformationModel> Add(LocaleInformationModel um);
        Result<LocaleInformationModel> Edit(int LocaleInfoID, LocaleInformationModel um);
        Result<LocaleInformationModel> Delete(int LocaleInfoID);
        LocaleInformationModel GetLocaleInfoById(int LocaleInfoID);
        IEnumerable<LocaleInformationModel> GetAll(int SubscriberID);
        LocaleInformationModel CheckDuplicateLocaleInfo(string LocaleInfoName, int? SubscriberID, int LocaleInfoID, string type);
    }
}
