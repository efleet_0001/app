﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.Services.Common
{
    public interface IEntityService<T>
    {
        IEnumerable<T> GetAll();
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
