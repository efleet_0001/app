﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.Automation;
using AssistDB.Automation;
using System.Collections.Generic;

namespace AssistBLL.Services.Master
{
    public interface IWorkFlowRuleService : IEntityService<WorkFlowRule>
    {
        Result<WorkFlowRuleModel> Add(WorkFlowRuleModel um);
        Result<WorkFlowRuleModel> Edit(int RuleId, WorkFlowRuleModel um);
        Result<WorkFlowRuleModel> Delete(int RuleId);
        WorkFlowRuleModel GetWorkFlowRuleById(int RuleId);
        IEnumerable<WorkFlowRuleModel> GetAllWorkFlowRule(int SubscriberID);
        WorkFlowRuleModel CheckDuplicateWorkFlowRule(string RuleName, int? SubscriberID, int RuleId, string type);
    }
}
