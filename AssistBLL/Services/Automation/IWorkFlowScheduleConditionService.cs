﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.Automation;
using AssistBLL.ViewModel.Master;
using AssistDB;
using AssistDB.Automation;
using AssistDB.GBLMaster.Master;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.Services.Master
{
    public interface IWorkFlowScheduleConditionService : IEntityService<WorkFlowScheduleCondition>
    {
        Result<WorkFlowScheduleConditionModel> Add(WorkFlowScheduleConditionModel um);
        Result<WorkFlowScheduleConditionModel> Edit(int RuleId, WorkFlowScheduleConditionModel um);
        Result<WorkFlowScheduleConditionModel> Delete(int RuleId);
        WorkFlowScheduleConditionModel GetWFScheduleConditionById(int RuleId);
        IEnumerable<WorkFlowScheduleConditionModel> GetAllWFScheduleCondition(int SubscriberID);
        WorkFlowScheduleConditionModel CheckDuplicateWFScheduleCondition(int ScheduleId, int? SubscriberID, int RuleId, string type);
    }
}
