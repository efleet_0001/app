﻿using AssistBLL.Models;
using AssistBLL.Services.Common;
using AssistBLL.ViewModel.Automation;
using AssistDB.Automation;
using System.Collections.Generic;

namespace AssistBLL.Services.Master
{
    public interface IWorkFlowRuleScheduleService : IEntityService<WorkFlowRuleSchedule>
    {
        Result<WorkFlowRuleScheduleModel> Add(WorkFlowRuleScheduleModel um);
        Result<WorkFlowRuleScheduleModel> Edit(int ScheduleId, WorkFlowRuleScheduleModel um);
        Result<WorkFlowRuleScheduleModel> Delete(int ScheduleId);
        WorkFlowRuleScheduleModel GetWorkFlowRuleScheduleById(int ScheduleId);
        IEnumerable<WorkFlowRuleScheduleModel> GetAllWorkFlowRuleSchedule(int SubscriberID);
        WorkFlowRuleScheduleModel CheckDuplicateWorkFlowRuleSchedule(string ScheduleName, int? SubscriberID, int ScheduleId, string type);
    }
}
