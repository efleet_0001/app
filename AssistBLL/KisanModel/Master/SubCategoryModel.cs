﻿namespace AssistBLL.KisanModel.Master
{
    public class SubCategoryModel : BaseViewModel
    {
        public int Subcat_ID { get; set; }
        public int? CateID { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
    }
}
