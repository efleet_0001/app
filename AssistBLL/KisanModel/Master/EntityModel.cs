﻿
namespace AssistBLL.KisanModel.Master
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Entity")]
    public  class EntityModel: BaseViewModel
    {
        [Key]
        public int Ent_ID { get; set; }
        public int? CatID { get; set; }
        public int SubCatID { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(50)]
        public string Alias { get; set; }
        [StringLength(500)]
        public string Description { get; set; }

        
    }
}
