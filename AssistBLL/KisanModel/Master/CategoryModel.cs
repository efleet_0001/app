﻿namespace AssistBLL.KisanModel.Master
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("Category")]
    public partial class CategoryModel : BaseViewModel
    {
        public CategoryModel()
        {
            SubCategories = new HashSet<SubCategoryModel>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long CatID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Alias { get; set; }

        [StringLength(500)]
        public string Description { get; set; }
                      
        public virtual ICollection<SubCategoryModel> SubCategories { get; set; }
                


    }
}
