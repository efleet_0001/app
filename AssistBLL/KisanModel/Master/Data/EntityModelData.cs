﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.KisanModel.Master.Data
{
   public class EntityModelData
    {
        public int Ent_ID { get; set; }
        public int? CatID { get; set; }
        public int SubCatID { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }

    }
}
