﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.KisanModel.Master.Data
{
    public class SubCategoryModelData
    {
        public int Subcat_ID { get; set; }
        public int? CateID { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
    }
}
