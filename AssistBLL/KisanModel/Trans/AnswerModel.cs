﻿namespace AssistBLL.KisanModel.Trans
{
   public class AnswerModel : BaseViewModel
    {
        public AnswerModel() { }
        public int AnsID { get; set; }
        public string AnswerText { get; set; }
        public string Anslike { get; set; }
        public string Ansdislike { get; set; }
        public string AnsComments { get; set; }
        public string Description { get; set; }
    }
}
