﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.KisanModel.Trans
{
    public class MemberMappingModel :BaseViewModel
    {
        public long MappingID { get; set; }
        public int MemberTypeId { get; set; }
        public long MemberId { get; set; }
        public long? CategoryId { get; set; }
        public long? SubCategoryId { get; set; }
        public long? EntityId { get; set; }
        public string Description { get; set; }
    }
}
