﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.KisanModel.Trans
{
   public class ProblemMasterModel:BaseViewModel
    {
        public long ProblemID { get; set; }
        public string ProblemName { get; set; }
        public long UserID { get; set; }
        public int SubCateID { get; set; }
        public int EntityID { get; set; }
        public string Description { get; set; }
    }
}
