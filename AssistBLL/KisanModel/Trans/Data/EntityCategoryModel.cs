﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AssistBLL.KisanModel.Trans.Data
{
  public  class EntityCateModel
    {
        [Key]
        public long CatID { get; set; }
        public string CateName { get; set; }
        public  List<EntityNewDataModel> EntityNewDataModels { get; set; }
    }
    public class NewCategoryDataModel
    {
        public long CatID { get; set; }
        public string CateName { get; set; }
    }
    public class EntityNewDataModel
    {
        public long NewsID { get; set; }
        public int SorceTypeID { get; set; }
        public string SorceTypeName { get; set; }
        public int MediaSourceTypeID { get; set; }
        public string MediaSourceTypeName { get; set; }
        public DateTime Date { get; set; }
        public string SourceInfoName { get; set; }
        public string Headline { get; set; }
        public string NewsContent { get; set; }
        public string NewsByName { get; set; }
        public string Time { get; set; }
        
    }
    public class DatamapDashboard
    { 
        public int YearMonth { get; set; }
        public ICollection<DistrictDashboard> DistrictDashboard { get; set; }
    }
    public class DistrictDashboard
    { 
        public int DistrictID { get; set; }
        public string DistrictName { get; set; }
        public ICollection<UserData> UserData { get; set; }
    }
    public class UserData
    { 
        public int Users { get; set; }
        public string fillkey { get; set; }
    }
}
