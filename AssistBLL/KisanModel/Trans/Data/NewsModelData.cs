﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.KisanModel.Trans.Data
{
    public class NewsModelData
    {
        public int NewsID { get; set; }
        public int CatID { get; set; }
        public int SorceTypeID { get; set; }
        public DateTime Date { get; set; }
        public string SourceInfoName { get; set; }
        public string Headline { get; set; }
        public int MediaSourceTypeID { get; set; }
        public bool Approved { get; set; }
        public string CategoryName { get; set; }
        public string SourceTypeName { get; set; }
    }
}
