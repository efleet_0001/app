﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.KisanModel.Trans.Data
{
    public class NotificationModelData
    {
        public long NotificationID { get; set; }
        public string Headline { get; set; }
        public int Type { get; set; }
        public string Notificationtext { get; set; }
        public string Imagepath { get; set; }
        public bool IsRead { get; set; }
        public int NotificationSourceType { get; set; }
        public long MemberID { get; set; }
        public string Typename { get; set; }
        public string NotificationSourceTypename { get; set; }
    }
}
