﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.KisanModel.Trans.Data
{
    public class NewsCategoryModelData
    {
        public int NewsCatID { get; set; }
        public string NewsCatName { get; set; }
        public string NewsDiscription { get; set; }
        public int CategoryID { get; set; }
        public string NewsTypeName { get; set; }
    }
}
