﻿namespace AssistBLL.KisanModel.Trans
{
   public class NewsCategoryModel : BaseViewModel
    {
        public int NewsCatID { get; set; }
        public string NewsCatName { get; set; }
        public string NewsDiscription { get; set; }
        public int CategoryID { get; set; }
    }
}
