﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.KisanModel.Trans
{
    public class NewsModel:BaseViewModel
    {
        public int NewsID { get; set; }
        public int CatID { get; set; }
        public int SorceTypeID { get; set; }
        public DateTime Date { get; set; }
        public string SourceInfoName { get; set; }
        public string Headline { get; set; }
        public int MediaSourceTypeID { get; set; }
        public string NewsContent { get; set; }
        public bool Approved { get; set; }
        public bool Read { get; set; }
        public int LanguageID { get; set; }
    }
}
