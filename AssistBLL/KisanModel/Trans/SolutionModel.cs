﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.KisanModel.Trans
{
    public class SolutionModel : BaseViewModel
    {
        public long SolnID { get; set; }
        public long ProblemID { get; set; }
        public string SolutionTag { get; set; }
        public string Description { get; set; }
    }
}
