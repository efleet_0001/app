﻿using AssistBLL.Models;
using AssistDAL;
using AssistDB.ViewModels;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace AssistBLL.Business
{
    public class AgriDasboardBusiness : IAgriDashboardBusinesss
    {
        private IRepositoryWrapper _repositoryWrapper;

        public AgriDasboardBusiness(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;

        }
        public IEnumerable<AgriDashboardModel> ExecuteStoreQuery()
        {
            int CategoryID = 1;
            string Query = "EXECUTE [dbo].[Proc_BUS_Menuload_Dashboard] @CategoryID";
            var Paramters = new object[] { new SqlParameter("@CategoryID", CategoryID) };
               
            if (CategoryID != 0)
            {
                 var result= _repositoryWrapper.AgriDasboardRepository.ExecuteStoreQuery(Query, Paramters);
                return result;
            }

            return null;
        }


    }
}
