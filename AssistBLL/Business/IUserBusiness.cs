﻿using AssistBLL.Models;
using AssistDB;
using AssistDB.UserControl;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssistBLL.Business
{
    public interface IUserBusiness
    {
        bool Authenticate(string name, string password);
        Usermaster GetUser(int id = 0, string username = null);
        IEnumerable<Usermaster> GetUsers();
        Result<Usermaster> Add(Usermaster um);
        Result<Usermaster> Edit(int UserID, Usermaster um);
        Result<Usermaster> Delete(int UserID);
        
    }
}
