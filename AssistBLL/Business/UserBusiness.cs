﻿using AssistBLL.Models;
using AssistDAL;
using AssistDB;
using AssistDB.UserControl;
using System;
using System.Collections.Generic;
using System.Linq;


namespace AssistBLL.Business
{
    public class UserBusiness : IUserBusiness
    {
        private IRepositoryWrapper _repositoryWrapper;

        public UserBusiness(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;

        }
        public bool Authenticate(string username, string password)
        {
            //TODO: handle sercurity algorithrm to encrypt the password here
            return _repositoryWrapper.UserRepository.GetAll()
                .Where(t => t.Email.Equals(username)|| t.Mobile.Equals(username) && t.PasswordHash.Equals(password))
                .Any();
        }

        public IEnumerable<Usermaster> GetUsers()
        {
            var data = _repositoryWrapper.UserRepository.GetAll();
            return data;
        }

        public Usermaster GetUser(int id = 0, string username = null)
        {
            if (id != 0)
            {
                return _repositoryWrapper.UserRepository.GetById(id);
            }
            if (!string.IsNullOrWhiteSpace(username))
            {
                return _repositoryWrapper.UserRepository.GetAll().Where(t => t.Email.Equals(username) || t.Mobile.Equals(username)).FirstOrDefault();
            }
            return null;
        }

        public Result<Usermaster> Add(Usermaster um)
        {
            var res = new Result<Usermaster>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var model = (um);
            model.UserID = 0;
            if (res.Errors.FirstOrDefault() == null)
            {
                res.IsSuccess = true;
                res.Data = (model);
            }
            return res;
        }

        public Result<Usermaster> Edit(int UserID, Usermaster um)
        {
            var res = new Result<Usermaster>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var response = _repositoryWrapper.UserRepository.GetById(UserID);
            var usermaster = (response);

            if (usermaster == null)
            {
                res.Errors.Add($"We could not find the User Master with id = {UserID.ToString()}");
                return res;
            }
            var model = (usermaster);
            if (model.Rowguid != um.Rowguid)
            {
                res.IsSuccess = true;
                res.Data = (model);
            }
            return res;
        }

        public Result<Usermaster> Delete(int UserID)
        {
            var res = new Result<Usermaster>()
            {
                IsSuccess = false,
                Errors = new List<String>(),
                Data = null
            };
            var UserMaster = _repositoryWrapper.UserRepository.GetById(UserID);
            if (UserMaster == null)
            {
                res.Errors.Add($"We could not find the UserID Master with id = {UserID.ToString()}");
                return res;
            }
            UserMaster.blocked = false; // 1 status marked as Deleted
            if (res.Errors.FirstOrDefault() == null)
            {
                res.IsSuccess = true;
                res.Data = (UserMaster);
            }
            return res;
        }
    }
}
