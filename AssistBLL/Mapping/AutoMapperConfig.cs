﻿using AutoMapper;
using AssistBLL.KisanModel.Master;
using AssistBLL.KisanModel.Trans;
using AssistBLL.ViewModel.Master;
using AssistDB;
using AssistDB.GBLMaster.Master;
using AssistDB.Kisan.Master;
using AssistDB.Kisan.Trans;
using AssistDB.BaseMaster;
using AssistBLL.login;
using AssistDB.login.DB.login;
using AssistDB.Kisan.Trans.Data;
using AssistBLL.KisanModel.Trans.Data;
using AssistBLL.ViewModel.Trans;
using AssistDB.GBLMaster.Trans;
using AssistDB.General;
using AssistDB.UserControl;
using AssistBLL.ViewModel.General;
using AssistBLL.ViewModel.UserControl;

namespace AssistBLL.Mapping
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            #region Login
            CreateMap<FYModelView, FYModel>()
            .ReverseMap();
            CreateMap<SessionModelVeiw, SessionModel>()
           .ReverseMap();
            CreateMap<loginModelView, loginModel>()
             .ReverseMap();
            CreateMap<SubscriberView, Subscriber>()
             .ReverseMap();
            CreateMap<MProfileView, MProfile>()
            .ReverseMap();
            CreateMap<ProfileDetailView, LProfileDetail>()
            .ReverseMap();
            CreateMap<LocaleInfoView, LocaleInfo>()
           .ReverseMap();
            #endregion
            #region Static
            CreateMap<ApplicationModel, Application>()
           .ReverseMap();
            CreateMap<ClientModel, Client>()
            .ReverseMap();
           
            CreateMap<ConstantModel, Constant>()
               .ForMember(dest => dest.profile_D, config => config.Ignore())
               .ForMember(dest => dest.socialProfile, config => config.Ignore())
               .ForMember(dest => dest.newsCategory, config => config.Ignore())
               .ForMember(dest => dest.news, config => config.Ignore())
               .ForMember(dest => dest.notification, config => config.Ignore())
               .ForMember(dest => dest.notification1, config => config.Ignore())
         .ReverseMap();
            CreateMap<CountryModel, Country>()
                 .ForMember(dest => dest.contact, config => config.Ignore())
                 .ForMember(dest => dest.localeInformation, config => config.Ignore())
            .ReverseMap();
            CreateMap<StateModel, State>()
                  .ForMember(dest => dest.contact, config => config.Ignore())
                  .ForMember(dest => dest.localeInformation, config => config.Ignore())
             .ReverseMap();
            CreateMap<DistrictModel, District>()
                 .ForMember(dest => dest.contact, config => config.Ignore())
                 .ForMember(dest => dest.localeInformation, config => config.Ignore())
            .ReverseMap();
            CreateMap<LanguageModel, Language>()
                .ForMember(dest => dest.localeInformation, config => config.Ignore())
           .ReverseMap();
            CreateMap<HolidayModel, Holiday>()
          .ReverseMap();
            #endregion

            CreateMap<CompanyDetailsModel, CompanyDetails>()
                .ForMember(dest => dest.Role, config => config.Ignore())
                .ReverseMap();
            CreateMap<RoleModel, Role>()
                .ForMember(dest => dest.Clone, config => config.Ignore())
                .ForMember(dest => dest.companyDetails, config => config.Ignore())
                .ForMember(dest => dest.usermaster, config => config.Ignore())
                .ReverseMap();
            CreateMap<ContactModel, Contact>()
               .ForMember(dest => dest.District, config => config.Ignore())
               .ForMember(dest => dest.State, config => config.Ignore())
               .ForMember(dest => dest.Country, config => config.Ignore())
               .ReverseMap();
            CreateMap<LocaleInformationModel, LocaleInformation>()
               .ForMember(dest => dest.Language, config => config.Ignore())
               .ForMember(dest => dest.Country, config => config.Ignore())
               .ForMember(dest => dest.State, config => config.Ignore())
               .ForMember(dest => dest.District, config => config.Ignore())
               .ForMember(dest => dest.usermaster, config => config.Ignore())
               .ReverseMap();
            CreateMap<ProfileModel, Profile_D>()
                .ForMember(dest => dest.Type, config => config.Ignore())
                .ForMember(dest => dest.Clone, config => config.Ignore())
                .ForMember(dest => dest.profileDetails, config => config.Ignore())
                .ForMember(dest => dest.usermaster, config => config.Ignore())
                .ReverseMap();
            CreateMap<ProfileDetailsModel, ProfileDetail>()
                    .ForMember(dest => dest.Profile, config => config.Ignore())
                   .ForMember(dest => dest.Cate, config => config.Ignore())
                   .ForMember(dest => dest.SubCat, config => config.Ignore()) 
                   .ForMember(dest => dest.Entity, config => config.Ignore())
               .ReverseMap();
            CreateMap<SocialProfileModel, SocialProfile>()
               .ForMember(dest => dest.EntityType, config => config.Ignore())
               .ReverseMap();
            CreateMap<UserMasteModel, Usermaster>()
              .ForMember(dest => dest.Role, config => config.Ignore())
              .ForMember(dest => dest.Profile, config => config.Ignore())
              .ForMember(dest => dest.Locale, config => config.Ignore())
              .ForMember(dest => dest.problemMaster, config => config.Ignore())
              .ReverseMap();
            #region  Kisan
            ////////////////////// Kisan Master///////////////
            ///

            CreateMap<CategoryModel, Category>()
            .ForMember(dest => dest.profileDetails, config => config.Ignore())
            .ForMember(dest => dest.profileDetails, config => config.Ignore())
        .ReverseMap();
            CreateMap<SubCategoryModel, SubCategory>()
              .ForMember(dest => dest.Cate, config => config.Ignore())
              .ForMember(dest => dest.profileDetails, config => config.Ignore())
              .ForMember(dest => dest.problemMaster, config => config.Ignore())
          .ReverseMap();
            CreateMap<EntityModel, Entity>()
             
             .ForMember(dest => dest.Cat, config => config.Ignore())
             .ForMember(dest => dest.SubCat, config => config.Ignore())
             .ForMember(dest => dest.profileDetails, config => config.Ignore())
             .ForMember(dest => dest.problemMaster, config => config.Ignore())
         .ReverseMap();


            ////////////////////////Kisan Trans//////////////
            CreateMap<NewsModel, News>()
                .ForMember(dest => dest.Cat, config => config.Ignore())
                .ForMember(dest => dest.SorceType, config => config.Ignore())
                .ForMember(dest => dest.MediaSourceType, config => config.Ignore())
            .ReverseMap();
            CreateMap<NewsCategoryModel, NewsCategory>()
               .ForMember(dest => dest.Category, config => config.Ignore())
                .ForMember(dest => dest.news, config => config.Ignore())
           .ReverseMap();

            CreateMap<SolutionModel, Solution>()
           .ForMember(dest => dest.problemMaster, config => config.Ignore())
           .ReverseMap();

            CreateMap<ProblemMasterModel, ProblemMaster>()
             .ForMember(dest => dest.User, config => config.Ignore())
             .ForMember(dest => dest.SubCate, config => config.Ignore())
             .ForMember(dest => dest.Entity, config => config.Ignore())
             .ForMember(dest => dest.solution, config => config.Ignore())
             .ReverseMap();
            CreateMap<NewCategoryDataModel, NewCategoryData>()
          .ReverseMap();
            CreateMap<EntityNewDataModel, EntityNewData>()
         .ReverseMap();
            CreateMap<AnswerModel, Answer>()
           .ReverseMap();
            CreateMap<ContactModel, Contact>()
          .ReverseMap();
            CreateMap<NewsModelData, NewsData>()
        .ReverseMap();
            CreateMap<ImageUploadModel, ImageUpload>()
       .ReverseMap();
            CreateMap<NotificationModel, Notification>()
             .ForMember(dest => dest.NotificationType, config => config.Ignore())
             .ForMember(dest => dest.NotificationSourceTypes, config => config.Ignore())
             .ReverseMap();
            CreateMap<NotificationModelData, NotificationData>()
      .ReverseMap();
            CreateMap<FiscalYearModel, FiscalYear>()
      .ReverseMap();
             
            CreateMap<UserGroupModel, UserGroup>()
      .ReverseMap();
            ////////////////////////////////////////////////////////////
            #endregion
        }
    }
}
